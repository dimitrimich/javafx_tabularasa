/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB;

import DB.objects.Aclass;
import DB.objects.Attendance;
import DB.objects.Billing;
import DB.objects.BillingMonth;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfStudentsPayments;
import DB.objects.ListOfWLessons;
import DB.objects.PaymentDetails;
import DB.objects.Student;
import DB.objects.Teacher;
import DB.objects.WeeklyLesson;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javafx.concurrent.Task;
import javax.persistence.EntityManager;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class DBUpdateUtils {
	
	public static boolean updateStudentInfo(Student toBeUpdated){
		EntityManager em2;
		try{
			em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			if(toBeUpdated.getContactInfo() != null){
				if(toBeUpdated.getContactInfo().getAddress1() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress1().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress1());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress1());
					}
				}
				
				if(toBeUpdated.getContactInfo().getAddress2() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress2().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress2());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress2());
					}
				}
				
				if(Utils.isValidID(toBeUpdated.getContactInfo().getId())){
					em2.merge(toBeUpdated.getContactInfo());
				}else{
					em2.persist(toBeUpdated.getContactInfo());
				}
			}
			em2.merge(toBeUpdated.getBasicInfo());
		}catch(Exception e){
			System.err.println("Error while updating object");
			e.printStackTrace(System.err);
			return false;
		}
		
		return finalMerge(em2, toBeUpdated);
	}
	
	public static boolean updateTeacherInfo(Teacher toBeUpdated){
		EntityManager em2;
		try{
			em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			if(toBeUpdated.getContactInfo() != null){
				if(toBeUpdated.getContactInfo().getAddress1() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress1().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress1());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress1());
					}
				}
				
				if(toBeUpdated.getContactInfo().getAddress2() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress2().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress2());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress2());
					}
				}
				
				if(Utils.isValidID(toBeUpdated.getContactInfo().getId())){
					em2.merge(toBeUpdated.getContactInfo());
				}else{
					em2.persist(toBeUpdated.getContactInfo());
				}
			}
			em2.merge(toBeUpdated.getBasicInfo());
		}catch(Exception e){
			System.err.println("Error while updating object");
			e.printStackTrace(System.err);
			return false;
		}
		
		return finalMerge(em2, toBeUpdated);
	}
	
	public static boolean updateTeachersClasses(Teacher toBeUpdated){
		boolean somethingWasChanged = false;
		EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
		dbManager.getTransaction().begin();
		
		// remove reference of teacher from classes that were removed
		try{
			Teacher teacherFromDB = dbManager.find(Teacher.class, toBeUpdated.getId());
			if(teacherFromDB.getClasses() != null && toBeUpdated.getClasses() != null){
				for(Aclass classFromDB : teacherFromDB.getClasses()){
					if(!toBeUpdated.getClasses().contains(classFromDB)){
						classFromDB.setTeacher(null);
						dbManager.merge(classFromDB);
						System.out.println("Removing class "+classFromDB.getName());
						somethingWasChanged = true;
					}
				}
			}
		}catch(Exception e){
			System.err.println("Exception while removing classes");
			System.err.println(e);
			return false;
		}
		
		// add new classes
		try{
			if(toBeUpdated.getClasses() != null){
				for(Aclass teachersClass : toBeUpdated.getClasses()){
					if(teachersClass.getTeacher() != toBeUpdated){
						System.out.println("Adding class "+ teachersClass.getName());
						teachersClass.setTeacher(toBeUpdated);
						dbManager.merge(teachersClass);
						somethingWasChanged = true;
					}
				}
			}
		}catch(Exception e){
			System.err.println("Exception while adding classes");
			System.err.println(e);
			return false;
		}
		if(somethingWasChanged){
			return finalMerge(dbManager, toBeUpdated);
		}
		return true;
	}
	
	public boolean removeStudentFromClass(Aclass toBeUpdated, Student toRemove){
		try {
			RemoveStudentTask task = new RemoveStudentTask(toBeUpdated, toRemove);
			return task.call();
		} catch (Exception exception) {
			System.err.println("error while updating connection -123432-");
			exception.printStackTrace(System.err);
			return false;
		}
	}
	public boolean addStudentToClass(Aclass toBeUpdated, Student toRemove){
		try {
			AddStudentTask task = new AddStudentTask(toBeUpdated, toRemove);
			return task.call();
		} catch (Exception exception) {
			System.err.println("error while updating connection -12343-2-");
			exception.printStackTrace(System.err);
			return false;
		}
	}
	
	private class RemoveStudentTask extends Task {
		public RemoveStudentTask(Aclass theClass, Student toRemove){
			this.classToBeUpdated = theClass;
			this.studentToBeRemoved = toRemove;
		}
		private final Aclass classToBeUpdated;
		private final Student studentToBeRemoved;
		@Override
		protected Boolean call() throws Exception {
			System.out.println("call()");
			EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
			dbManager.getTransaction().begin();
			for(LocalDate key : classToBeUpdated.getSchedule().getAllELessons().keySet()){
				ListOfELessons listOfLessons = classToBeUpdated.getSchedule().getAllELessons().get(key);
				for(Lesson lesson : listOfLessons.getAllLessons()){
					Map <Long, Attendance> theMap = lesson.getAttendance();
					dbManager.remove(dbManager.getReference(Attendance.class, theMap.get(studentToBeRemoved.getId()).getId()));
					theMap.remove(studentToBeRemoved.getId());
					
					Map <Long, Billing> theBillingMap = lesson.getBilling();
					theBillingMap.remove(studentToBeRemoved.getId());
					dbManager.remove(dbManager.getReference(Billing.class, theBillingMap.get(studentToBeRemoved.getId()).getId()));
					dbManager.merge(lesson);
				}
			}
			for(LocalDate key : classToBeUpdated.getSchedule().getAllWLessons().keySet()){
				ListOfWLessons listOfLessons = classToBeUpdated.getSchedule().getAllWLessons().get(key);
				for(Lesson lesson : listOfLessons.getAllLessons()){
					Map <Long, Attendance> theMap = lesson.getAttendance();
					dbManager.remove(dbManager.getReference(Attendance.class, theMap.get(studentToBeRemoved.getId()).getId()));
					theMap.remove(studentToBeRemoved.getId());
					
					Map <Long, Billing> theBillingMap = lesson.getBilling();
					dbManager.remove(dbManager.getReference(Billing.class, theBillingMap.get(studentToBeRemoved.getId()).getId()));
					theBillingMap.remove(studentToBeRemoved.getId());
					dbManager.merge(lesson);
				}
			}
			
			// Remove billing Month of this Student
			// shouldn't remove payments to keep record ?!? Maybe
//			for(BillingMonth bMonth : classToBeUpdated.getBillingMonths()){
//				ListOfStudentsPayments list = bMonth.getAllPayments().get(studentToBeRemoved.getId());
//				for(PaymentDetails payment : list.getPayments()){
//				}
//			}
			studentToBeRemoved.getClasses().remove(classToBeUpdated);
			dbManager.merge(studentToBeRemoved);
			classToBeUpdated.getStudents().remove(studentToBeRemoved);
			dbManager.merge(classToBeUpdated);
			return closeConnection(dbManager);
		}
	}
	private class AddStudentTask extends Task {
		public AddStudentTask(Aclass theClass, Student studentToAdd){
			this.classToBeUpdated = theClass;
			this.studentToBeAdded = studentToAdd;
		}
		private final Aclass classToBeUpdated;
		private final Student studentToBeAdded;
		@Override
		protected Boolean call() throws Exception {
			System.out.println("call()");
			EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
			dbManager.getTransaction().begin();
			for(LocalDate key : classToBeUpdated.getSchedule().getAllELessons().keySet()){
				ListOfELessons listOfLessons = classToBeUpdated.getSchedule().getAllELessons().get(key);
				for(Lesson lesson : listOfLessons.getAllLessons()){
					Attendance newAttendance = new Attendance(studentToBeAdded);
					dbManager.persist(newAttendance);
					Map <Long, Attendance> theMap = lesson.getAttendance();
					theMap.put(studentToBeAdded.getId(), newAttendance);
					
					Billing newBilling = new Billing(studentToBeAdded, lesson);
					dbManager.persist(newBilling);
					Map <Long, Billing> theBillingMap = lesson.getBilling();
					theBillingMap.put(studentToBeAdded.getId(), newBilling);
					dbManager.merge(lesson);
				}
			}
			System.out.println("Save all Attendance also (Weekly Lesson)");
			for(LocalDate key : classToBeUpdated.getSchedule().getAllWLessons().keySet()){
				ListOfWLessons listOfWLessons = classToBeUpdated.getSchedule().getAllWLessons().get(key);
				for(Lesson lesson : listOfWLessons.getAllLessons()){
					System.out.println("Create attendance for each lesson of the student");
					Attendance newAttendance = new Attendance(studentToBeAdded);
					dbManager.persist(newAttendance);
					Map <Long, Attendance> theMap = lesson.getAttendance();
					theMap.put(studentToBeAdded.getId(), newAttendance);
					
					Billing newBilling = new Billing(studentToBeAdded, lesson);
					dbManager.persist(newBilling);
					Map <Long, Billing> theBillingMap = lesson.getBilling();
					theBillingMap.put(studentToBeAdded.getId(), newBilling);
					dbManager.merge(lesson);
				}
			}
			// update billing Month of this new Student
			for(BillingMonth bMonth : classToBeUpdated.getBillingMonths()){
				ListOfStudentsPayments list = new ListOfStudentsPayments();
				list.setStudentID(studentToBeAdded.getId());
				dbManager.persist(list);
				bMonth.getAllPayments().put(studentToBeAdded.getId(), list);
				dbManager.merge(bMonth);
			}
			
			studentToBeAdded.getClasses().add(classToBeUpdated);
			dbManager.merge(studentToBeAdded);
			classToBeUpdated.getStudents().add(studentToBeAdded);
			dbManager.merge(classToBeUpdated);
			return closeConnection(dbManager);
		}
	}
	
	public boolean removeClassFromTeacher(Aclass toBeUpdated, Teacher toRemove){
		try {
			RemoveClassTask task = new RemoveClassTask(toBeUpdated, toRemove);
			return task.call();
		} catch (Exception exception) {
			System.err.println("error while updating connection -123432-");
			exception.printStackTrace(System.err);
			return false;
		}
	}
	public boolean addClassToTeacher(Aclass toBeUpdated, Teacher toRemove){
		try {
			AddClassTask task = new AddClassTask(toBeUpdated, toRemove);
			return task.call();
		} catch (Exception exception) {
			System.err.println("error while updating connection -12343-2-");
			exception.printStackTrace(System.err);
			return false;
		}
	}
	
	private class RemoveClassTask extends Task {
		public RemoveClassTask(Aclass theClass, Teacher toRemove){
			this.toBeUpdated = theClass;
			this.toBeRemoved = toRemove;
		}
		private final Aclass toBeUpdated;
		private final Teacher toBeRemoved;
		@Override
		protected Boolean call() throws Exception {
			System.out.println("call()");
			EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
			dbManager.getTransaction().begin();
			toBeRemoved.getClasses().remove(toBeUpdated);
			dbManager.merge(toBeRemoved);
			toBeUpdated.setTeacher(null);
			dbManager.merge(toBeUpdated);
			return closeConnection(dbManager);
		}
	}
	private class AddClassTask extends Task {
		public AddClassTask(Aclass theClass, Teacher toRemove){
			this.toBeUpdated = theClass;
			this.toBeAdded = toRemove;
		}
		private final Aclass toBeUpdated;
		private final Teacher toBeAdded;
		@Override
		protected Boolean call() throws Exception {
			System.out.println("call()");
			EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
			dbManager.getTransaction().begin();
			toBeAdded.getClasses().add(toBeUpdated);
			dbManager.merge(toBeAdded);
			toBeUpdated.setTeacher(toBeAdded);
			dbManager.merge(toBeUpdated);
			return closeConnection(dbManager);
		}
	}
//    private class UpdateClassesStudentsTask extends Task {
//        public UpdateClassesStudentsTask(Aclass theClass ){
//            this.toBeUpdated = theClass;
//        }
//        private final Aclass toBeUpdated;
//        @Override
//        protected Boolean call() throws Exception {
//        System.out.println("call()");
//            boolean somethingWasChanged = false;
//            EntityManager dbManager = factory.createEntityManager();
//            dbManager.getTransaction().begin();
//
//            // remove reference of Class from students that were removed
//            try{
//                Aclass classFromDB = dbManager.find(Aclass.class, toBeUpdated.getId());
//                if(classFromDB.getStudents()!= null && toBeUpdated.getStudents() != null){
//                    Iterator<Student> it = classFromDB.getStudents().iterator();
//                    while(it.hasNext()){
//                        Student studentFromDB = it.next();
//                        if(!toBeUpdated.getStudents().contains(studentFromDB)){
//                            if(studentFromDB.getClasses() != null){
//                                studentFromDB.getClasses().remove(classFromDB);
//                                dbManager.merge(studentFromDB);
//                                it.remove();
//                                dbManager.merge(classFromDB);
//                                System.out.println("Removing Student "+studentFromDB.getFullName());
//                                somethingWasChanged = true;
//
//                                for(LocalDate key : toBeUpdated.getSchedule().getAllELessons().keySet()){
//                                    ListOfELessons listOfLessons = toBeUpdated.getSchedule().getAllELessons().get(key);
//                                    for(Lesson lesson : listOfLessons.getAllLessons()){
//                                        Map <Long, Attendance> theMap = lesson.getAttendance();
//                                        theMap.remove(studentFromDB.getId());
//                                        dbManager.merge(lesson);
//                                    }
//                                }
//                                for(LocalDate key : toBeUpdated.getSchedule().getAllWLessons().keySet()){
//                                    ListOfWLessons listOfLessons = toBeUpdated.getSchedule().getAllWLessons().get(key);
//                                    for(Lesson lesson : listOfLessons.getAllLessons()){
//                                        Map <Long, Attendance> theMap = lesson.getAttendance();
//                                        theMap.remove(studentFromDB.getId());
//                                        dbManager.merge(lesson);
//                                    }
//                                }
//                            }
//                        }else{
//                            System.out.println("Student already in the class");
//                        }
//                    }
//                }
//            }catch(Exception e){
//                System.err.println("Exception while removing students");
//                System.err.println(e);
//                return false;
//            }
//
//            // add class to students that were added
//            try{
//                if(toBeUpdated.getStudents() != null){
//                    for(Student classesStudent : toBeUpdated.getStudents()){
//                        if(classesStudent.getClasses()== null){
//                            classesStudent.setClasses(new ArrayList<>());
//                        }
//                        if(!classesStudent.getClasses().contains(toBeUpdated)){
//                            System.out.println("Adding student "+classesStudent.getFullName()+"to class ");
//                            System.out.println("Save all Attendance also (Extra Lesson)");
//                            for(LocalDate key : toBeUpdated.getSchedule().getAllELessons().keySet()){
//                                ListOfELessons listOfLessons = toBeUpdated.getSchedule().getAllELessons().get(key);
//                                for(Lesson lesson : listOfLessons.getAllLessons()){
//                                    Attendance newAttendance = new Attendance(classesStudent);
//                                    dbManager.persist(newAttendance);
//                                    Map <Long, Attendance> theMap = lesson.getAttendance();
//                                    theMap.put(classesStudent.getId(), newAttendance);
//                                    dbManager.merge(lesson);
//                                }
//                            }
//                            System.out.println("Save all Attendance also (Weekly Lesson)");
//                            for(LocalDate key : toBeUpdated.getSchedule().getAllWLessons().keySet()){
//                                ListOfWLessons listOfWLessons = toBeUpdated.getSchedule().getAllWLessons().get(key);
//                                for(Lesson lesson : listOfWLessons.getAllLessons()){
//                                    System.out.println("Create attendance for each lesson of the student");
//                                    Attendance newAttendance = new Attendance(classesStudent);
//                                    dbManager.persist(newAttendance);
//                                    Map <Long, Attendance> theMap = lesson.getAttendance();
//                                    theMap.put(classesStudent.getId(), newAttendance);
//                                    dbManager.merge(lesson);
//                                }
//                            }
//
//                            classesStudent.getClasses().add(toBeUpdated);
//                            dbManager.merge(classesStudent);
//                            somethingWasChanged = true;
//                        }else{
//                            System.out.println("Class already contains Student 1?!?");
//                        }
//                    }
//                }
//            }catch(Exception e){
//                System.err.println("Exception while adding classes");
//                System.err.println(e);
//                return false;
//            }
//            return finalMerge(dbManager, toBeUpdated);
//        }
//    }
	@Deprecated
	public static boolean updateStudentsClasses(Student toBeUpdated){
		boolean somethingWasChanged = false;
		EntityManager dbManager = FactoryMaker.getInstance().createEntityManager();
		dbManager.getTransaction().begin();
		
		// remove reference of Student from classes that were removed
		try{
			Student studentFromDB = dbManager.find(Student.class, toBeUpdated.getId());
			if(studentFromDB.getClasses() != null && toBeUpdated.getClasses() != null){
				for(Aclass classFromDB : studentFromDB.getClasses()){
					if(!toBeUpdated.getClasses().contains(classFromDB)){
						if(classFromDB.getStudents() != null){
							classFromDB.getStudents().remove(studentFromDB);
							dbManager.merge(classFromDB);
							System.out.println("Removing class "+classFromDB.getName());
							somethingWasChanged = true;
						}
					}
				}
			}
		}catch(Exception e){
			System.err.println("Exception while removing classes");
			System.err.println(e);
			return false;
		}
		
		// add student to new classes
		try{
			if(toBeUpdated.getClasses() != null){
				for(Aclass studentsClass : toBeUpdated.getClasses()){
					if(studentsClass.getStudents() == null){
						studentsClass.setStudents(new ArrayList<>());
					}
					if(!studentsClass.getStudents().contains(toBeUpdated)){
						System.out.println("Adding student to class "+ studentsClass.getName());
						studentsClass.getStudents().add(toBeUpdated);
						dbManager.merge(studentsClass);
						somethingWasChanged = true;
					}
				}
			}
		}catch(Exception e){
			System.err.println("Exception while adding classes");
			System.err.println(e);
			return false;
		}
		if(somethingWasChanged){
			return finalMerge(dbManager, toBeUpdated);
		}
		return true;
	}
	
	public static boolean updateClassInfo(Aclass toBeUpdated){
		try {
			System.err.println("updating class | "+ toBeUpdated.getMonthlyFee() + " | "+ toBeUpdated.getHourlyFee());
			
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			try {
				// shedule
				if (toBeUpdated.getSchedule() != null) {
					// lesson
					for (WeeklyLesson lesson : toBeUpdated.getSchedule().getwLessons()) {
						if (!Utils.isValidID(lesson.getId())) {
							em2.persist(lesson);
						} else {
							em2.merge(lesson);
						}
					}
					for (LocalDate today : toBeUpdated.getSchedule().getAllELessons().keySet()) {
						ListOfELessons todaysList = toBeUpdated.getSchedule().getAllELessons().get(today);
						for (Lesson aLesson : todaysList.getAllLessons()) {
							Map<Long, Billing> billings = aLesson.getBilling();
							
							for(Entry<Long, Billing> anEntry : billings.entrySet()){
								billings.get(anEntry.getKey()).updateFullAmount(toBeUpdated, aLesson);
								em2.merge(anEntry.getValue());
							}
							
							if (!Utils.isValidID(aLesson.getId())) {
								em2.persist(aLesson);
							} else {
								em2.merge(aLesson);
							}
							
							for(Attendance attend : aLesson.getAttendance().values()){
								if (!Utils.isValidID(aLesson.getId())) {
									em2.persist(aLesson);
								} else {
									em2.merge(aLesson);
								}
							}
							
						}
						if (!Utils.isValidID(todaysList.getId())) {
							em2.persist(todaysList);
						} else {
							em2.merge(todaysList);
						}
					}
					
					for (LocalDate today : toBeUpdated.getSchedule().getAllWLessons().keySet()) {
						ListOfWLessons todaysList = toBeUpdated.getSchedule().getAllWLessons().get(today);
						for (Lesson aLesson : todaysList.getAllLessons()) {
							Map<Long, Billing> billings = aLesson.getBilling();
							for(Entry<Long, Billing> anEntry : billings.entrySet()){
								billings.get(anEntry.getKey()).updateFullAmount(toBeUpdated, aLesson);
								em2.merge(anEntry.getValue());
							}
							
							for(Attendance attend : aLesson.getAttendance().values()){
								if (!Utils.isValidID(aLesson.getId())) {
									em2.persist(aLesson);
								} else {
									em2.merge(aLesson);
								}
							}
							
							if (!Utils.isValidID(aLesson.getId())) {
								em2.persist(aLesson);
							} else {
								em2.merge(aLesson);
							}
							
						}
						if (!Utils.isValidID(todaysList.getId())) {
							em2.persist(todaysList);
						} else {
							em2.merge(todaysList);
						}
					}
					
					
					if (!Utils.isValidID(toBeUpdated.getSchedule().getId())) {
						em2.persist(toBeUpdated.getSchedule());
					} else {
						em2.merge(toBeUpdated.getSchedule());
					}
					toBeUpdated.getBillingMonths().stream().forEach((BillingMonth bMonth) -> {
						if (!Utils.isValidID(bMonth.getId())) {
							for(ListOfStudentsPayments listOfPayments : bMonth.getAllPayments().values()){
								for(PaymentDetails payment :listOfPayments.getPayments()){
									if (!Utils.isValidID(payment.getId())) {
										em2.persist(payment);
									}
									else {
										em2.merge(payment);
									}
								}
								if (!Utils.isValidID(listOfPayments.getId())) {
									em2.persist(listOfPayments);
								}
								else {
									em2.merge(listOfPayments);
								}
							}
							em2.persist(bMonth);
						} else {
							em2.merge(bMonth);
						}
					});
					
				}
			} catch (RuntimeException e) {
				throw e;
			}catch (Exception ex) {
				System.out.println("Error while saving -1-");
				ex.printStackTrace(System.out);
				return false;
			}
			try {
				if (toBeUpdated.getTeacher() != null) {
					if (!toBeUpdated.getTeacher().getClasses().contains(toBeUpdated)) {
						toBeUpdated.getTeacher().getClasses().add(toBeUpdated);
						// merge chages to teacher of class
						em2.merge(toBeUpdated.getTeacher());
					}
				} else {
					Aclass copyInDB = em2.find(Aclass.class, toBeUpdated.getId());
					if (copyInDB.getTeacher() != null) {
						copyInDB.getTeacher().getClasses().remove(copyInDB);
						em2.merge(copyInDB);
					} else {
						System.out.println("class being updated doesn't have teacer AND didn't had also - no worries");
					}
				}
			} catch (Exception e) {
				System.out.println("Error while saving -2-");
				e.printStackTrace(System.out);
				return false;
			}
			return finalMerge(em2, toBeUpdated);
		} catch (Exception e) {
			System.err.println("error while updating class Info");
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	
	private static boolean finalMerge(EntityManager dbManager, Object toBeUpdated){
		try{
			dbManager.merge(toBeUpdated);
			dbManager.getTransaction().commit();
			dbManager.close();
		}catch(Exception e){
			System.out.println("Error while saving "+toBeUpdated.getClass().getSimpleName() +" -22.2-");
			e.printStackTrace(System.out);
			return false;
		}
		return true;
	}
	private static boolean closeConnection(EntityManager dbManager){
		try{
			dbManager.getTransaction().commit();
			dbManager.close();
		}catch(Exception e){
			System.out.println("Error while closing connection -2.2.2-");
			e.printStackTrace(System.out);
			return false;
		}
		return true;
	}
	
	
	public static boolean updateListOfWLessons(List<ListOfWLessons> toBeUpdatedList){
		
		EntityManager em2 = null;
		try{
			System.out.println("Merging "+toBeUpdatedList.size()+" lists of W lessons");
			em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			
			for(ListOfWLessons list :toBeUpdatedList){
				em2.merge(list);
			}
			
			em2.getTransaction().commit();
			em2.close();
			
		}catch(Exception e){
			System.err.println("Error while updateListOfWLessons");
			e.printStackTrace(System.err);
			if(em2 != null){
				em2.close();
			}
			return false;
		}
		return true;
	}
	
	
	public static boolean updateListOfLessonsAndDeleteLessons(List<ListOfELessons> toBeUpdatedEList, List<ListOfWLessons> toBeUpdatedWList, List <Lesson> lessons){
		EntityManager em2 = null;
		try{
			System.out.println("Merging "+toBeUpdatedEList.size()+" lists of E lessons");
			System.out.println("Merging "+toBeUpdatedWList.size()+" lists of W lessons");
			em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			
			for(ListOfELessons list :toBeUpdatedEList){
				em2.merge(list);
			}
			for(ListOfWLessons list :toBeUpdatedWList){
				em2.merge(list);
			}
			
			
			for(Lesson toDelete: lessons){
				toDelete.setTheClass(null);
				for(Attendance attendance : toDelete.getAttendance().values()){
					attendance = em2.getReference(Attendance.class, attendance.getId());
					em2.remove(attendance);
				}
				toDelete.setAttendance(null);
				
				for(Billing theBilling : toDelete.getBilling().values()){
					theBilling = em2.getReference(Billing.class, theBilling.getId());
					em2.remove(theBilling);
				}
				toDelete.setBilling(null);
				em2.remove(em2.merge(toDelete));
			}
			
			em2.getTransaction().commit();
			em2.close();
			
		}catch(Exception e){
			System.err.println("Error while updateListOfELessons");
			e.printStackTrace(System.err);
			if(em2 != null){
				em2.close();
			}
			return false;
		}
		return true;
	}
}
