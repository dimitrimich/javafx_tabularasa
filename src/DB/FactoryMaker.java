/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package DB;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Singleton to initialize Database Factory only once
 * it is supposed to be thread-safe, but who knows ? :P 
 * example was here http://crunchify.com/thread-safe-and-a-fast-singleton-implementation-in-java/
 * @author dimitris
 */
public class FactoryMaker {
	
	private final static EntityManagerFactory instance = Persistence.createEntityManagerFactory("tabula");
	
	private FactoryMaker() {
	}
	
	public static EntityManagerFactory getInstance() {
		return instance;
	}
}
