/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB;

import DB.objects.Aclass;
import DB.objects.PaymentDetails;
import DB.objects.Student;
import DB.objects.Teacher;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author d.michaelides
 */
public class DBTableUtils {
	public static ObservableList<Student> getAllStudents(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Student c");
		// do we have entries?
		List<Student> entries = q.getResultList();
		em2.close();
		return FXCollections.observableList(entries);
	}
	
	public static ObservableList<Aclass> getAllClasses(){
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			System.out.println("begin");
			if(em2.getTransaction() == null){
				System.out.println("transaction is null");
			}
			// read the existing entries
			Query q = em2.createQuery("select c from Aclass c");
			// do we have entries?
			List<Aclass> entries = q.getResultList();
			System.out.println("entries.size() " +entries.size());
			em2.close();
			return FXCollections.observableList(entries);
		}catch(Exception e){
			System.err.println("Error error ");
			e.printStackTrace(System.err);
		}
		return FXCollections.observableArrayList();
	}
	
	public static ObservableList<Teacher> getAllTeachers(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Teacher c");
		// do we have entries?
		List<Teacher> entries = q.getResultList();
		em2.close();
		return FXCollections.observableList(entries);
	}
	
	public static List<Student> getAllStudentsList(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Student c");
		// do we have entries?
		List<Student> entries = q.getResultList();
		em2.close();
		return (entries);
	}
	
	public static List<Aclass> getAllClassesList(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Aclass c");
		// do we have entries?
		List<Aclass> entries = q.getResultList();
		em2.close();
		return (entries);
	}
	
	public static List<Teacher> getAllTeachersList(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Teacher c");
		// do we have entries?
		List<Teacher> entries = q.getResultList();
		em2.close();
		return (entries);
	}
	public static ObservableList<PaymentDetails> getAllPayments(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from PaymentDetails c");
		// do we have entries?
		List<PaymentDetails> entries = q.getResultList();
		em2.close();
		return FXCollections.observableList(entries);
	}
}
