/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB;

import DB.objects.Aclass;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfWLessons;
import javafx.concurrent.Task;
import javax.persistence.EntityManager;

/**
 *
 * @author dimitris
 */
public class LessonsPersister {
    public void moveWeeklyLesson(Lesson l, ListOfWLessons oldList,  ListOfWLessons newList, Aclass theClass, boolean persistNewList){
        try {
            MoveWeeklyLessonTask task = new MoveWeeklyLessonTask(l, oldList, newList, theClass, persistNewList);
            task.call();
        } catch (Exception exception) {
            System.err.println("Exception while moving weekly lesson -12300-");
            exception.printStackTrace(System.err);
        }
    }
    private class MoveWeeklyLessonTask extends Task {
        public MoveWeeklyLessonTask(Lesson l, ListOfWLessons oldList,  ListOfWLessons newList, Aclass theClass, Boolean persist ){
            this.newList = newList;
            this.oldList = oldList;
            this.theClass = theClass;
            this.l = l;
            this.persistList = persist;
        }
        private final ListOfWLessons newList;
        private final ListOfWLessons oldList;
        private final Aclass theClass;
        private final Lesson l;
        private final boolean persistList;
        @Override
        protected Object call() throws Exception {
            EntityManager manager = FactoryMaker.getInstance().createEntityManager();
            manager.getTransaction().begin();
            manager.merge(l);
            manager.merge(oldList);
            if(persistList){
                manager.persist(newList);
            }else{
                manager.merge(newList);
            }
            manager.merge(theClass.getSchedule());
            manager.merge(theClass);
            manager.getTransaction().commit();
            manager.close();
            return null;
        }
    }
    
    public void moveExtraLesson(Lesson l, ListOfELessons oldList,  ListOfELessons newList, Aclass theClass, boolean persistNewList){
        try {
            MoveExtraLessonTask task = new MoveExtraLessonTask(l, oldList, newList, theClass, persistNewList);
            task.call();
        } catch (Exception exception) {
            System.err.println("Exception while moving weekly lesson -12300-");
            exception.printStackTrace(System.err);
        }
    }
    private class MoveExtraLessonTask extends Task {
        public MoveExtraLessonTask(Lesson l, ListOfELessons oldList,  ListOfELessons newList, Aclass theClass, Boolean persist ){
            this.newList = newList;
            this.oldList = oldList;
            this.theClass = theClass;
            this.l = l;
            this.persistList = persist;
        }
        private final ListOfELessons newList;
        private final ListOfELessons oldList;
        private final Aclass theClass;
        private final Lesson l;
        private final boolean persistList;
        @Override
        protected Object call() throws Exception {
            EntityManager manager = FactoryMaker.getInstance().createEntityManager();
            manager.getTransaction().begin();
            manager.merge(l);
            manager.merge(oldList);
            if(persistList){
                manager.persist(newList);
            }else{
                manager.merge(newList);
            }
            manager.merge(theClass.getSchedule());
            manager.merge(theClass);
            manager.getTransaction().commit();
            manager.close();
            return null;
        }
    }
    
    public void addExtraLesson(Lesson l, ListOfELessons list, Aclass theClass, boolean persistNewList){
        try {
            AddExtraLessonTask task = new AddExtraLessonTask(l, list, theClass, persistNewList);
            task.call();
        } catch (Exception exception) {
            System.err.println("Exception while moving weekly lesson -12300-");
            exception.printStackTrace(System.err);
        }
    }
    private class AddExtraLessonTask extends Task {
        public AddExtraLessonTask(Lesson l, ListOfELessons list, Aclass theClass, Boolean persist ){
            this.theList = list;
            this.theClass = theClass;
            this.l = l;
            this.persistList = persist;
        }
        private final ListOfELessons theList;
        private final Aclass theClass;
        private final Lesson l;
        private final boolean persistList;
        @Override
        protected Object call() throws Exception {
            EntityManager manager = FactoryMaker.getInstance().createEntityManager();
            manager.getTransaction().begin();
            for(Long studentid : l.getAttendance().keySet()){
                manager.persist(l.getAttendance().get(studentid));
                manager.persist(l.getBilling().get(studentid));
            }
            manager.persist(l);
            if(persistList){
                manager.persist(theList);
            }else{
                manager.merge(theList);
            }
            manager.merge(theClass.getSchedule());
            manager.merge(theClass);
            manager.getTransaction().commit();
            manager.close();
            return null;
        }
    }
    
    public void saveLessonDetails(Lesson aLesson){
        try {
            SaveLessonEdits task = new SaveLessonEdits(aLesson);
            task.call();
        } catch (Exception exception) {
            System.err.println("Exception while SaveLessonEdits -12300+++- -");
            exception.printStackTrace(System.err);
        }
    }
    
    private class SaveLessonEdits extends Task {
        public SaveLessonEdits(Lesson less){
            this.lesson = less;
        }
        private final Lesson lesson;
        @Override
        protected Object call() throws Exception {
            EntityManager manager = FactoryMaker.getInstance().createEntityManager();
            manager.getTransaction().begin();
            for(Long studentid : lesson.getAttendance().keySet()){
                manager.merge(lesson.getAttendance().get(studentid));
                manager.merge(lesson.getBilling().get(studentid));
            }
            manager.merge(lesson);
            manager.getTransaction().commit();
            manager.close();
            return null;
        }
    }
}
