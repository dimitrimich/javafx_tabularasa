/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package DB;

import DB.objects.Billing;
import DB.objects.BillingMonth;
import DB.objects.PaymentDetails;
import DB.objects.Student;
import controllers.BillingEditPaymentController.SEARCH_CRITERIA;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import javafx.concurrent.Task;
import javax.persistence.EntityManager;
import utils.Utils;

/**
 *
 * @author dimitris
 */
public class BillingPersister {
	
	public static void saveBillingEdits(Billing billingInfo){
		try {
			SaveBillingEdits task = new SaveBillingEdits(billingInfo);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while SaveLessonEdits -12-=300+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	public static void savePaymentAndBilling(PaymentDetails payment, Billing billing) {
		try {
			SavePaymentAndBilling task = new SavePaymentAndBilling(payment, billing);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while saving both 2 -12-00+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	private static class SavePaymentAndBilling extends Task {
		public SavePaymentAndBilling(PaymentDetails pay, Billing bill){
			this.billing = bill;
			this.newPayment = pay;
		}
		private final Billing billing;
		private final PaymentDetails newPayment;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			if(Utils.isValidID(newPayment.getId())){
				manager.merge(newPayment);
			}else{
				manager.persist(newPayment);
			}
			manager.merge(billing);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	public static void deleteAllPaymentsAndSaveBilling(Billing billing) {
		try {
			DeleteAllPaymentsAndSaveBilling task = new DeleteAllPaymentsAndSaveBilling(billing);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while deleting 2 -12-00+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	private static class DeleteAllPaymentsAndSaveBilling extends Task {
		public DeleteAllPaymentsAndSaveBilling(Billing bill){
			this.billing = bill;
		}
		private final Billing billing;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			for(PaymentDetails payment : billing.getPayments()){
				manager.remove(manager.getReference(PaymentDetails.class, payment.getId()));
			}
			manager.merge(billing);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	
	
	
	private static class SaveBillingEdits extends Task {
		public SaveBillingEdits(Billing billingInf){
			this.billingInfo = billingInf;
		}
		private final Billing billingInfo;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			manager.merge(billingInfo);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	
	public static void saveBillingMonthEdits(BillingMonth billingMonth){
		try {
			SaveBillingMonthEdits task = new SaveBillingMonthEdits(billingMonth);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while SaveLessonEdits -12-00+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	private static class SaveBillingMonthEdits extends Task {
		public SaveBillingMonthEdits(BillingMonth billingM){
			this.billingMonth = billingM;
		}
		private final BillingMonth billingMonth;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			manager.merge(billingMonth);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	public static void saveNewPayment(PaymentDetails payment){
		try {
			SaveNewPayment task = new SaveNewPayment(payment);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while SaveLessonEdits -12-00+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	private static class SaveNewPayment extends Task {
		public SaveNewPayment(PaymentDetails pay){
			this.payment = pay;
		}
		private final PaymentDetails payment;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			manager.persist(payment);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	public static void saveNewPaymentAndBillingMonthEdits(PaymentDetails pay, BillingMonth billMonth, Student payingStudent){
		try {
			SaveNewPaymentAndBillingMonthEdits task = new SaveNewPaymentAndBillingMonthEdits(pay, billMonth, payingStudent);
			task.call();
		} catch (Exception exception) {
			System.err.println("Exception while SaveLessonEdits -1245450+++- -");
			exception.printStackTrace(System.err);
		}
	}
	
	private static class SaveNewPaymentAndBillingMonthEdits extends Task {
		public SaveNewPaymentAndBillingMonthEdits(PaymentDetails pay, BillingMonth billingM, Student payingStu){
			this.billingMonth = billingM;
			this.paymentDetails = pay;
			this.payingStudent = payingStu;
		}
		private final PaymentDetails paymentDetails;
		private final BillingMonth billingMonth;
		private final Student payingStudent;
		
		@Override
		protected Object call() throws Exception {
			EntityManager manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			manager.persist(paymentDetails);
			manager.merge(billingMonth.getAllPayments().get(payingStudent.getId()));
			manager.merge(billingMonth);
			manager.getTransaction().commit();
			manager.close();
			return null;
		}
	}
	
	
	public static List<PaymentDetails> getPayments(LocalDate paymentDate, String receiptOrName, SEARCH_CRITERIA criterion){
		List<PaymentDetails> payments = null;
		EntityManager manager = null;
		try{
			manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			
			switch(criterion){
				case date:
					payments = manager.createQuery("SELECT OBJECT(payment) FROM PaymentDetails payment WHERE payment.paymentDate = :date")
							.setParameter("date", paymentDate).getResultList();
					break;
				case paidBy:
					payments = manager.createQuery(
							"SELECT OBJECT(payment)"
									+ " FROM PaymentDetails payment "
									+ "WHERE lower(payment.paymentBy) = :paidBy")
							.setParameter("paidBy", receiptOrName.toLowerCase()).getResultList();
					break;
				case paidFor:
					payments = manager.createQuery(
							"SELECT OBJECT(payment)"
									+ " FROM PaymentDetails payment "
									+ "WHERE lower(payment.paymentFor) = :paymentFor")
							.setParameter("paymentFor", receiptOrName.toLowerCase()).getResultList();
					break;
				case receipt:
					payments = manager.createQuery(
							"SELECT OBJECT(payment)"
									+ " FROM PaymentDetails payment "
									+ "WHERE lower(payment.receiptNo) = :receipt")
							.setParameter("receipt", receiptOrName.toLowerCase()).getResultList();
					break;
			}
			
			manager.close();
		}catch(Exception e){
			if(manager != null){
				manager.close();
			}
			System.err.println("Error while retrieving paymentDetails");
			e.printStackTrace(System.err);
		}
		return payments;
	}
	
	
	private static class GetPaymentsTask extends Task<List<PaymentDetails>> {
		public GetPaymentsTask(LocalDate paymentDate, String receiptNo, SEARCH_CRITERIA criterion){
			this.paymentDate = paymentDate;
			this.receiptNo = receiptNo;
			criteria = criterion;
		}
		private final LocalDate paymentDate;
		private final String receiptNo;
		private final  SEARCH_CRITERIA criteria;
		
		@Override
		protected List<PaymentDetails> call() throws Exception {
			List<PaymentDetails> payments = null;
			EntityManager manager = null;
			try{
				manager = FactoryMaker.getInstance().createEntityManager();
				manager.getTransaction().begin();
				
				if(this.paymentDate != null){
					payments = manager.createQuery("SELECT OBJECT(payment) FROM PaymentDetails payment WHERE payment.paymentDate = :date")
							.setParameter("date", this.paymentDate).getResultList();
					
				}else if (Utils.isValidParam(this.receiptNo)){
					payments = manager.createQuery(
							"SELECT OBJECT(payment)"
									+ " FROM PaymentDetails payment "
									+ "WHERE payment.paymentDate = :date")
							.setParameter("date", this.paymentDate).getResultList();
				}
				manager.close();
			}catch(Exception e){
				if(manager != null){
					manager.close();
				}
				System.err.println("Error while retrieving paymentDetails");
				e.printStackTrace(System.err);
			}
			return payments;
		}
	}
	
	public static boolean makePaymentsZero(List<PaymentDetails> payments){
		EntityManager manager = null;
		try{
			manager = FactoryMaker.getInstance().createEntityManager();
			manager.getTransaction().begin();
			
			for(PaymentDetails payment : payments){
				System.out.println("making payment zero....");
				payment.setDiscount(BigDecimal.ZERO);
				payment.setPaidAmount(BigDecimal.ZERO);
				payment.setTotalAmount(BigDecimal.ZERO);
				manager.merge(payment);
			}
			manager.getTransaction().commit();
			manager.close();
		}catch(Exception e){
			if(manager != null){
				manager.close();
			}
			System.err.println("Error while retrieving paymentDetails");
			e.printStackTrace(System.err);
			return false;
		}
		return true;
	}
	
	
	
	
}
