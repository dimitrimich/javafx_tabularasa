/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @OneToOne
    private BasicInformation basicInfo;
    @OneToOne
    private ContactInformation contactInfo;
    static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public BasicInformation getBasicInfo() {
        return basicInfo;
    }
    public void setBasicInfo(BasicInformation basicInfo) {
        this.basicInfo = basicInfo;
    }
    public ContactInformation getContactInfo() {
        return contactInfo;
    }
    public void setContactInfo(ContactInformation contactInfo) {
        this.contactInfo = contactInfo;
    }
    public Employee getEmployee(){
        return this;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Employee){
            return (Long.compare(getId(), ((Employee) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
