/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Schedule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @OneToOne
    private Aclass theClass;
    @OneToMany
    private List<WeeklyLesson> wLessons = new ArrayList<>();
    @OneToMany
    @MapKey(name = "theDate")
    private Map<LocalDate, ListOfWLessons> allWLessons = new HashMap<>();
    @OneToMany
    @MapKey(name = "theDate")
    private Map<LocalDate, ListOfELessons> allELessons = new HashMap<>();
    static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Aclass getTheClass() {
        return theClass;
    }
    public void setTheClass(Aclass theClass) {
        this.theClass = theClass;
    }
    public List<WeeklyLesson> getwLessons() {
        return wLessons;
    }
    public void setwLessons(List<WeeklyLesson> wLessons) {
        this.wLessons = wLessons;
    }
    public Map<LocalDate, ListOfELessons> getAllELessons() {
        return allELessons;
    }
    public void setAllELessons(Map<LocalDate, ListOfELessons> eLessons) {
        this.allELessons = eLessons;
    }
    public Map<LocalDate, ListOfWLessons> getAllWLessons() {
        return allWLessons;
    }    
    public void setAllWLessons(Map<LocalDate, ListOfWLessons> wLessons) {
        this.allWLessons = wLessons;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Schedule){
            return (Long.compare(getId(), ((Schedule) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
}
