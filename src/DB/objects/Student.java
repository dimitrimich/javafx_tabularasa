/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.sql.rowset.serial.SerialBlob;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Student implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	private SerialBlob picture;
	@OneToOne
	private BasicInformation basicInfo;
	@OneToOne
	private ContactInformation contactInfo;
	
	@ManyToMany(mappedBy="students")
	private List<Aclass> classes = new ArrayList<>();
	@ManyToMany(mappedBy="students")
	private List<Guardian> guardians = new ArrayList<>();
	static final long serialVersionUID = 1L;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Aclass> getClasses() {
		return classes;
	}
	public void setClasses(List<Aclass> classes) {
		this.classes = classes;
	}
	public BasicInformation getBasicInfo() {
		return basicInfo;
	}
	public void setBasicInfo(BasicInformation basicInfo) {
		this.basicInfo = basicInfo;
	}
	public ContactInformation getContactInfo() {
		return contactInfo;
	}
	public void setContactInfo(ContactInformation contactInfo) {
		this.contactInfo = contactInfo;
	}
	public List<Guardian> getGuardians() {
		return guardians;
	}
	public void setGuardians(List<Guardian> guardians) {
		this.guardians = guardians;
	}
	public SerialBlob getPicture() {
		return picture;
	}
	public void setPicture(SerialBlob picture) {
		this.picture = picture;
	}
	public Student getStudent(){
		return this;
	}
	public String getFullName(){
		return getBasicInfo().getFname()+" "+getBasicInfo().getLname();
	}
	@Override
	public String toString(){
		return getBasicInfo().getFname()+" "+getBasicInfo().getLname();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Student){
			return (Long.compare(getId(), ((Student) obj).getId())) == 0;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.id);
		return hash;
	}
	public String getAttendaceTableInfo(){
		StringBuilder sb = new StringBuilder(this.getBasicInfo().getFname());
		sb.append(" ");
		sb.append(this.getBasicInfo().getLname());
		if(contactInfo != null && contactInfo.getPhone1() != null){
			sb.append(" ");
			sb.append(contactInfo.getPhone1());
		}
		return sb.toString();
	}
	public boolean containsKeyWordNotCaseSensitive(String keyWord){
		if(keyWord == null ){throw new IllegalArgumentException("Keyword for search is null"); }
		if(keyWord.isEmpty()) { return false; }
		
		keyWord = keyWord.toUpperCase();
		// basic Info checks
		if (basicInfo.containsKeyWordNotCaseSensitive(keyWord)) { return true; }
		// contact Info:
		if(contactInfo != null){
			if (contactInfo.containsKeyWordNotCaseSensitive(keyWord)) { return true; }
		}
		return false;
	}
}
