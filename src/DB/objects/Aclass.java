package DB.objects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import utils.WeeklyLessonsEditor;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Aclass implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	private String name;
	private String codeName;
	private String description;
	private String color;
	private LocalDate startDate;
	private LocalDate endDate;
	@Column (precision = 8, scale = 2)
	private BigDecimal monthlyFee;
	@Column (precision = 8, scale = 2)
	private BigDecimal hourlyFee;
	private boolean paidPerMonth;
	static final long serialVersionUID = 1L;
	
	@OneToOne
	private Teacher teacher;
	@OneToOne
	private Schedule schedule;
	@ManyToMany
	private List<Student> students = new ArrayList<>();
	
	
	@ManyToMany
	private List<BillingMonth> billingMonths = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	public Aclass getAclass(){
		return this;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) { this.color = color; }
	public LocalDate getStartDate() { return startDate; }
	public void setStartDate(LocalDate startDate) {this.startDate = startDate;}
	public LocalDate getEndDate() { return endDate; }
	public void setEndDate(LocalDate endDate) { this.endDate = endDate; }
	public BigDecimal getMonthlyFee() { return monthlyFee; }
	public void setMonthlyFee(BigDecimal monthlyFee) { this.monthlyFee = monthlyFee; }
	public BigDecimal getHourlyFee() { return hourlyFee; }
	public void setHourlyFee(BigDecimal hourlyFee) { this.hourlyFee = hourlyFee; }
	public String getCodeName() { return codeName; }
	public void setCodeName(String codeName) {this.codeName = codeName;}
	public String getNameAndCode(){
		if(codeName == null || codeName.isEmpty()){
			return name;
		}
		return new StringBuilder(name).append(" | ").append(codeName).toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Aclass){
			return (Long.compare(getId(), ((Aclass) obj).getId())) == 0;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 89 * hash + Objects.hashCode(this.id);
		return hash;
	}
	
	public boolean isActive(){
		return (LocalDate.now().isBefore(endDate) || LocalDate.now().isEqual(endDate));
	}
	
	public void createEditBillingMonths(){
		try{
			Month startMonth = startDate.getMonth();
			Month endMOnth = endDate.getMonth();
			Year currentYear = Year.of(startDate.getYear());
			while(!startMonth.equals(endMOnth)) {
				if(!hasSuchBillingMonth(startMonth, currentYear)){
					BillingMonth currentMonth = new BillingMonth();
					currentMonth.setBillingMonth(startMonth);
					currentMonth.setBillingYear(currentYear);
					currentMonth.setAmountFull(this.getMonthlyFee());
					currentMonth.setClassId(this.getId());
					// Create the list of payments for each month
					for(Student stu : getStudents()){
						ListOfStudentsPayments list = new ListOfStudentsPayments();
						list.setStudentID(stu.getId());
						currentMonth.getAllPayments().put(stu.getId(), list);
					}
					System.out.println("Adding "+currentMonth.getBillingMonth());
					billingMonths.add(currentMonth);
				}
				if(startMonth.equals(Month.DECEMBER)){
					currentYear = currentYear.plusYears(1);
				}
				startMonth = startMonth.plus(1);
			}
			if(!hasSuchBillingMonth(startMonth, currentYear)){
				BillingMonth currentMonth = new BillingMonth();
				currentMonth.setBillingMonth(startMonth);
				currentMonth.setBillingYear(currentYear);
				currentMonth.setAmountFull(this.getMonthlyFee());
				currentMonth.setClassId(this.getId());
				// Create the list of payments for each month
				for(Student stu : getStudents()){
					ListOfStudentsPayments list = new ListOfStudentsPayments();
					list.setStudentID(stu.getId());
					currentMonth.getAllPayments().put(stu.getId(), list);
				}
				System.out.println("Adding "+currentMonth.getBillingMonth());
				billingMonths.add(currentMonth);
			}
		}catch(Exception e){
			System.err.println("EROR while creating billing months");
			e.printStackTrace(System.err);
		}
	}
	public boolean hasSuchBillingMonth(Month monthInQuestion, Year year){
		for(BillingMonth bMonth : getBillingMonths()){
			if(bMonth.getBillingMonth().equals(monthInQuestion) && bMonth.getBillingYear().equals(year)){
				return true;
			}
		}
		return false;
	}
	
	public BillingMonth getBillingMonth(Month monthInQuestion, Year year){
		for(BillingMonth bMonth : getBillingMonths()){
			if(bMonth.getBillingMonth().equals(monthInQuestion) && bMonth.getBillingYear().equals(year) ){
				return bMonth;
			}
		}
		return null;
	}
	
	public void createAllLessons(){
		try{
			WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
			Iterator<WeeklyLesson> it = this.getSchedule().getwLessons().iterator();
			while(it.hasNext()){
				editor.addNewWeeklyLesson(this, it.next(), getStartDate(), getEndDate());
			}
		}catch(Exception e){
			System.err.println("EROR while creating schedule");
			e.printStackTrace(System.err);
		}
	}
	public ArrayList<Lesson> getLessonsForAMonth(LocalDate theDate){
		return getLessonsForAMonth(theDate.getMonth(), Year.of(theDate.getYear()));
	}
	public ArrayList<Lesson> getLessonsForAMonth(Month thisMonth, Year thisYear){
		ArrayList<Lesson> theMonthsLessons = new ArrayList<>();
		Map<LocalDate, ListOfELessons> extraLessons = getSchedule().getAllELessons();
		for(Entry<LocalDate, ListOfELessons> anEntry : extraLessons.entrySet()){
			if(anEntry.getKey().getMonth().compareTo(thisMonth) == 0 && Year.of(anEntry.getKey().getYear()).compareTo(thisYear) == 0){
				theMonthsLessons.addAll(anEntry.getValue().getAllLessons());
			}
		}
		
		Map<LocalDate, ListOfWLessons> weeklyLessons = getSchedule().getAllWLessons();
		for(Entry<LocalDate, ListOfWLessons> anEntry : weeklyLessons.entrySet()){
			if(anEntry.getKey().getMonth().compareTo(thisMonth) == 0 && Year.of(anEntry.getKey().getYear()).compareTo(thisYear) == 0){
				theMonthsLessons.addAll(anEntry.getValue().getAllLessons());
			}
		}
		return theMonthsLessons;
	}
	
	
	public boolean isPaidPerMonth() { return paidPerMonth; }
	public void setPaidPerMonth(boolean paidPerMonth) { this.paidPerMonth = paidPerMonth; }
	public List<BillingMonth> getBillingMonths() {  return billingMonths;}
	public void setBillingMonths(List<BillingMonth> billingMonths) {  this.billingMonths = billingMonths; }
	
	/**
	 * was to calculate remaining amount
	 * not tested
	 * @deprecated
	 */
	public BigDecimal calculateRemainingAmount(Month currentMonth, Year currentYear, PaymentDetails newPayment) {
		if(isPaidPerMonth()){
			BillingMonth monthOfPayment = getBillingMonth(currentMonth, currentYear);
			return monthOfPayment.getAmountFull().subtract(
					monthOfPayment.getTotalAmountPaid().add(monthOfPayment.getTotalDiscount())
							.add(newPayment.getDiscount()).add(newPayment.getPaidAmount()));
		}else{
			throw new UnsupportedOperationException("method not supported to hourly class");
		}
	}
	public boolean containsKeyWordNotCaseSensitive(String keyWord){
		if(keyWord == null ){throw new IllegalArgumentException("Keyword for search is null"); }
		if(keyWord.isEmpty()) { return false; }
		
		keyWord = keyWord.toUpperCase();
		if(name != null){
			if (name.toUpperCase().contains(keyWord)) { return true; }
		}if(description != null){
			if (description.toUpperCase().contains(keyWord)) { return true; }
		}if(codeName != null){
			if (codeName.toUpperCase().contains(keyWord)) { return true; }
		}
		return false;
	}
}
