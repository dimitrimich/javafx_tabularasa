/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import utils.Constants;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Lesson implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private int dayOfTheWeek;
    private int startingHour;
    private int startingMinute;
    private int duration;
    private String description;
    private LocalDate theDate;
    private boolean extraLesson;
    private int indexNumber;
    @OneToOne
    private Aclass theClass;
    static final long serialVersionUID = 1L;
    
    @OneToMany
    @MapKey(name = "studentID")
    private Map<Long, Attendance> attendance = new HashMap<>();
    @OneToMany
    @MapKey(name = "studentID")
    private Map<Long, Billing> billing = new HashMap<>();
    
    public Lesson(){
        this.canceled = false;
        this.studentWhoCanceledID = Long.valueOf("0");
        this.excuseForCancel = "";
		this.theDate = Constants.UNKOWN_DATE;
	}
	public Lesson(Lesson toCopy){
	this.canceled = toCopy.canceled;
        this.studentWhoCanceledID = toCopy.studentWhoCanceledID;
        this.excuseForCancel = toCopy.excuseForCancel;
		this.theDate = toCopy.theDate;
		
	
	}
    public Lesson(WeeklyLesson wL){
		this();
        this.dayOfTheWeek = wL.getDayOfTheWeek();
        this.description = wL.getDescription();
        this.duration = wL.getDuration();
        this.startingHour = wL.getStartingHour();
        this.startingMinute = wL.getStartingMinute();
        this.indexNumber = wL.getIndexNumber();
        System.out.println("Set INdex Number = "+ wL.getIndexNumber());
    }
    public Lesson(WeeklyLesson wL, LocalDate thisDate){
        this(wL);
        this.theDate = thisDate;
    }
    public Lesson(WeeklyLesson wL, LocalDate thisDate, Aclass theLessonsClass){
        this(wL,thisDate);
        this.theClass = theLessonsClass;
        createAttendance();
        createBilling();
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getDayOfTheWeek() {
        return dayOfTheWeek;
    }
    public int getDayOfTheWeekForLocalDate(){
        switch(dayOfTheWeek){
            case 1: return 7;
            case 2: return 1;
            case 3: return 2;
            case 4: return 3;
            case 5: return 4;
            case 6: return 5;
            case 7: return 6;
            default:return 7;
        }
    }
    public void setDayOfTheWeek(int dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }
    public int getStartingHour() {
        return startingHour;
    }
    public void setStartingHour(int startingHour) {
        this.startingHour = startingHour;
    }
    public int getStartingMinute() {
        return startingMinute;
    }
    public void setStartingMinute(int startingMinute) {
        this.startingMinute = startingMinute;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public LocalDate getTheDate() {
        return theDate;
    }
    public void setTheDate(LocalDate theDate) {
        this.theDate = theDate;
    }
    public boolean isExtraLesson() {
        return extraLesson;
    }
    public void setExtraLesson(boolean isExtraLesson) {
        this.extraLesson = isExtraLesson;
    }
    public int getIndexNumber() {
        return indexNumber;
    }
    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }
    public String getStartingTime(){
        try{
            String time = Integer.toString(startingHour) +":"+ Integer.toString(startingMinute); 
            if(Integer.toString(startingMinute).equals("0")){
                return ("0:0".equals(time)) ?"N/A":time+"0";
            }
            return ("0:0".equals(time)) ?"N/A":time;
        }catch(Exception e){
            return "N/A";
        }
    }
    public Map<Long, Attendance> getAttendance() {
        return attendance;
    }
    public void setAttendance(HashMap<Long, Attendance> attendance) {
        this.attendance = attendance;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Lesson){
            return (Long.compare(getId(), ((Lesson) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    public boolean isFullyScheduled(){
        return (dayOfTheWeek != 0) && (startingHour != 0) && (duration != 0);
    }
    public Aclass getTheClass() {
        return theClass;
    }
    public void setTheClass(Aclass theClass) {
        this.theClass = theClass;
    }

    public final void createAttendance() {
        for(Student aStudent : theClass.getStudents()){
            Attendance a = new Attendance(aStudent);
            a.setStatus(Attendance.UNKNOWN);
            attendance.put(aStudent.getId(), a);
        }
    }
    public final void createBilling() {
        for(Student aStudent : theClass.getStudents()){
            Billing a = new Billing(aStudent, this);
            a.setStatus(Billing.STATUS_NOT_PAID);
            billing.put(aStudent.getId(), a);
        }
    }
    /* ************************** Canceled **************************************************************************/
    
    public static final int BY_TEACHER = 1;
    public static final int BY_STUDENT = 2;
    private boolean canceled;
    private int canceledBy;
    private Long studentWhoCanceledID;
    private String excuseForCancel;

    public boolean isCanceled() {
        return canceled;
    }
    public void setCanceled(boolean isCanceled) {
        this.canceled = isCanceled;
    }
    public int getCanceledBy() {
        return canceledBy;
    }
    public void setCanceledBy(int canceledBy) {
        this.canceledBy = canceledBy;
    }
    public Long getStudentWhoCanceledID() {
        return studentWhoCanceledID;
    }
    public void setStudentWhoCanceledID(Long studentWhoCanceledID) {
        this.studentWhoCanceledID = studentWhoCanceledID;
    }
    public String getExcuseForCancel() {
        return excuseForCancel;
    }
    public void setExcuseForCancel(String excuseForCancel) {
        this.excuseForCancel = excuseForCancel;
    }

    /* ************************** Billing **************************************************************************/
    public Map<Long, Billing> getBilling() {
        return billing;
    }
	public Billing getBillingForStudent(Student stu){
		return this.getBilling().get(stu.getId());
	}
    public void setBilling(Map<Long, Billing> billing) {
        this.billing = billing;
    }
	public BigDecimal getRemainingAmountForStudent(Student stu){
		return getBillingForStudent(stu).getRemainingAmount();
	}
}
