/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author d.michaelides
 */
@Entity
public class DBProperties implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private final List<String> colors = Arrays.asList("aqua", "seagreen", "cadetblue", "blue", "chartreuse", "darkgray", "cyan", "darkgreen", "darkorange", "darkviolet", "darkslateblue", "firebrick", "goldenrod", "indianred", "greenyellow", "maroon", "olive", "yellow", "red", "orangered", "saddlebrown", "purple", "gold", "sandybrown");
    private final List<String> letters = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    
	
	private int currentColor = 0;
    static final long serialVersionUID = 1L;
	private String paymentReceiptNo = "A-999";
	private long paymentNo = Long.valueOf("999");
	private int currentLetter = 0;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

	public String getPaymentReceiptNo() { return generateNextReceiptNo(); }
	private String generateNextReceiptNo() {
		if(paymentNo == Long.valueOf("9999999")){
			paymentNo = Long.valueOf("1000");
			if(currentLetter == letters.size() - 1){
				currentLetter = 0;
			}else{
				currentLetter++;
			}
		}else{
			paymentNo ++;
		}
		return new StringBuilder().append(letters.get(currentLetter)).append('-').append(paymentNo).toString();
	}
	
    public String getNextColor(){
        if(currentColor == colors.size() - 1){
            currentColor = 0;
        }else{
            currentColor++;
        }
        return colors.get(currentColor);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof DBProperties){
            return (Long.compare(getId(), ((DBProperties) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    
}
