/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author d.michaelides
 */
@Entity
public class ListOfELessons implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private LocalDate theDate;
    @OneToMany
    private List<Lesson> allLessons = new ArrayList<>();
    static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public List<Lesson> getAllLessons() {
        return allLessons;
    }
    public LocalDate getTheDate() {
        return theDate;
    }
    public void setTheDate(LocalDate theDate) {
        this.theDate = theDate;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ListOfELessons){
            return (Long.compare(getId(), ((ListOfELessons) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
}
