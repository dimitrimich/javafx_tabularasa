/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package DB.objects;

import customObjects.MyDecimalFormater;
import java.math.BigDecimal;
import java.time.LocalDate;
import utils.Constants;
import utils.LanguageUtils;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CASH;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CHECK;
import java.io.Serializable;
import java.time.Month;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import utils.LocalDateConverter;

/**
 *
 * @author dimitris
 */
@Entity
public class PaymentDetails implements Serializable{
	public PaymentDetails(){
		discount = BigDecimal.ZERO;
		paidAmount = BigDecimal.ZERO;
		totalAmount = BigDecimal.ZERO;
		paymentMethod = PAYMENT_METHOD.DEFAULT;
		checkNo = "";
		paymentDate = Constants.UNKOWN_DATE;
		paymentBy = "";
		paymentFor = "";
		receiptNo = "";
		className = "";
		billingMonth = null;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	
	private String receiptNo;
	private String className;
	private Month billingMonth;
	
	/**
	  to print on receipt
	  * @deprecated  not implemented yet for Paid-per-lesson
	 */
	private boolean paidInFullForReceipt;
	/**
		@deprecated not working yet for Paid-per-lesson
	*/
	public boolean isPaidInFullForReceipt() { return paidInFullForReceipt; }
	/**
		@deprecated not working yet for Paid-per-lesson
	*/public void setPaidInFullForReceipt(boolean paidInFullForReceipt) { this.paidInFullForReceipt = paidInFullForReceipt; }
	 
			
	static final long serialVersionUID = 1L;
	
	public enum PAYMENT_METHOD{
		DEFAULT, CHECK, CASH;
		
		public static String getStringValue(PAYMENT_METHOD pm) {
			if(pm.compareTo(DEFAULT) == 0){
				return LanguageUtils.getInstance().getCurrentLanguage().getString("payment.dialog.default");
			}
			if(pm.compareTo(CASH) == 0) {
				return LanguageUtils.getInstance().getCurrentLanguage().getString("payment.dialog.cash");
			}
			return LanguageUtils.getInstance().getCurrentLanguage().getString("payment.dialog.check");
		}
	};
	@Column (precision = 8, scale = 2)
	private BigDecimal discount;
	@Column (precision = 8, scale = 2)
	private BigDecimal paidAmount;
	@Column (precision = 8, scale = 2)
	private BigDecimal totalAmount;
	@Enumerated(EnumType.STRING)
	private PAYMENT_METHOD paymentMethod;
	private String checkNo;
	
	@Convert(converter = LocalDateConverter.class)
	private LocalDate paymentDate;
	private String paymentBy;
	private String paymentFor;
	private Long paymentForId;
	
	public BigDecimal getDiscount() { return discount; }
	public void setDiscount(BigDecimal discount) { this.discount = discount; }
	public BigDecimal getPaidAmount() { return paidAmount; }
	public void setPaidAmount(BigDecimal paidAmount) { this.paidAmount = paidAmount; }
	public PAYMENT_METHOD getPaymentMethod() { return paymentMethod; }
	public void setPaymentMethod(PAYMENT_METHOD paymentMethod) { this.paymentMethod = paymentMethod; }
	public String getCheckNo() { return checkNo; }
	public void setCheckNo(String checkNo) {  this.checkNo = checkNo; }
	public LocalDate getPaymentDate() { return paymentDate; }
	public void setPaymentDate(LocalDate paymentDate) { this.paymentDate = paymentDate; }
	public BigDecimal getTotalAmount() { return totalAmount; }
	public void setTotalAmount(BigDecimal totalAmount) { this.totalAmount = totalAmount; }
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	public String getPaymentBy() { return paymentBy; }
	public void setPaymentBy(String paymentBy) { this.paymentBy = paymentBy; }
	public String getPaymentFor() { return paymentFor; }
	public void setPaymentFor(String paymentFor) { this.paymentFor = paymentFor; }
	public Long getPaymentForId() { return paymentForId; }
	public void setPaymentForId(Long paymentForid) { this.paymentForId = paymentForid; }
	public String getClassName() { return className; }
	public void setClassName(String className) { this.className = className; }
	public Month getBillingMonth() { return billingMonth; }
	public void setBillingMonth(Month month) { this.billingMonth = month; }
	
	@Override
	public String toString(){
		MyDecimalFormater format = new MyDecimalFormater();
		StringBuilder sb = new StringBuilder();
		sb.append("Paid: ").append(format.numberToString(paidAmount)).append(" | Discount: ")
				.append(format.numberToString(discount));
		
		if(! paymentDate.equals(Constants.UNKOWN_DATE)){
			sb.append(" | On: ").append(paymentDate.toString());
		}
		switch(paymentMethod){
			case CASH :
				sb.append(" | Using: ");
				sb.append(" ").append(PAYMENT_METHOD.getStringValue(CASH));
				break;
			case CHECK:
				sb.append(" | Using: ");
				sb.append(" ").append(PAYMENT_METHOD.getStringValue(CHECK)).append(": ").append(checkNo);
				break;
			case DEFAULT:
				break;
				
		}
		return sb.toString();
	}
	
	public PaymentDetails withDiscount(BigDecimal disc){
		this.setDiscount(disc);
		return this;
	}
	public PaymentDetails withAmountPaid(BigDecimal paid){
		this.setPaidAmount(paid);
		return this;
	}
	public PaymentDetails withDate(LocalDate date){
		this.setPaymentDate(date);
		return this;
	}
	public PaymentDetails withMethod(PAYMENT_METHOD method){
		this.setPaymentMethod(method);
		return this;
	}
	public PaymentDetails withCheckNo(String checkNo){
		this.setCheckNo(checkNo);
		return this;
	}
	public PaymentDetails withTotalAmount(BigDecimal total){
		this.setTotalAmount(total);
		return this;
	}
	
	public void increaseDiscount(BigDecimal increase){
		this.discount = this.discount.add(increase);
	}
	public void decreaseDiscount(BigDecimal decrease){
		this.discount = this.discount.subtract(decrease);
	}
	public void increasePaidAmount(BigDecimal increase){
		System.out.println("increasePaidAmount -->" + paidAmount +" + "+increase);
		this.paidAmount = this.paidAmount.add(increase);
	}
	public void decreasePaidAmount(BigDecimal decrease){
		this.paidAmount = this.paidAmount.subtract(decrease);
	}
	public void increaseTotalAmount(BigDecimal increase){
		this.totalAmount = this.totalAmount.add(increase);
	}
	public void decreaseTotalAmount(BigDecimal decrease){
		this.totalAmount = this.totalAmount.subtract(decrease);
	}
	public String getReceiptNo() { return receiptNo;}
	public void setReceiptNo(String receiptNo) { this.receiptNo = receiptNo; }
	
	public PaymentDetails copy(){
		PaymentDetails clone = new PaymentDetails();
		clone.checkNo = this.checkNo;
		clone.discount = this.discount;
		clone.paidAmount = this.paidAmount;
		clone.paymentDate = this.paymentDate;
		clone.paymentMethod = this.paymentMethod;
		clone.totalAmount = this.totalAmount;
		clone.receiptNo = this.receiptNo;
		clone.paymentBy = this.paymentBy;
		clone.billingMonth = this.billingMonth;
		clone.className = this.className;
		clone.paymentFor = this.paymentFor;
		clone.paymentForId = this.paymentForId;
		return clone;
	}
	public void copyToExceptAmounts(PaymentDetails copyTo){
		copyTo.checkNo = this.checkNo;
		copyTo.paymentBy = this.paymentBy;
		copyTo.paymentDate = this.paymentDate;
		copyTo.paymentMethod = this.paymentMethod;
		copyTo.receiptNo = this.receiptNo;
		copyTo.billingMonth = this.billingMonth;
		copyTo.className = this.className;
		copyTo.paymentFor = this.paymentFor;
		copyTo.paymentForId = this.paymentForId;
	}
	public void addAmounts(PaymentDetails newPayment) {
		if(this.totalAmount == BigDecimal.ZERO){
			this.totalAmount = newPayment.getTotalAmount();
		}
		this.increaseDiscount(newPayment.getDiscount());
		this.increasePaidAmount(newPayment.getPaidAmount());
	}
	
	public boolean isPaidInFull(){
		return totalAmount.compareTo( discount.add(paidAmount) ) == 0 ;
	}
	
	/**
	 * @return discount != 0 && paid != 0
	 */
	public boolean isValidPayment(){
		return discount.compareTo(BigDecimal.ZERO) != 0 || paidAmount.compareTo(BigDecimal.ZERO) != 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PaymentDetails){
			return (Long.compare(getId(), ((PaymentDetails) obj).getId())) == 0;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int hash = 2;
		hash = 13 * hash + Objects.hashCode(this.id);
		return hash;
	}
}