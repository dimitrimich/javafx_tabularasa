/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
@Entity
public class ContactInformation implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	@OneToOne
	private Address address1;
	@OneToOne
	private Address address2;
	private String phone1;
	private String phone2;
	private String email1;
	private String email2;
	static final long serialVersionUID = 1L;
	@Transient
	private SimpleStringProperty phone;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Address getAddress1() {
		return address1;
	}
	public void setAddress1(Address address1) {
		this.address1 = address1;
	}
	public Address getAddress2() {
		return address2;
	}
	public void setAddress2(Address address2) {
		this.address2 = address2;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
		if(this.phone == null){
			this.phone = new SimpleStringProperty(phone1);
		}else{
			this.phone.set(phone1);
		}
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	
	public SimpleStringProperty getPhone() {
		return phone;
	}
	public void setPhone(SimpleStringProperty phone) {
		this.phone = phone;
		this.phone1 = phone.get();
	}
	
	
	public boolean containsKeyWordNotCaseSensitive(String keyWord){
		
		if(address1 != null){
			if(Utils.isValidParam(address1.toString())){
				if(address1.toString().toUpperCase().contains(keyWord)){ return true;}
			}
		}
		if(address2 != null){
			if(Utils.isValidParam(address2.toString())){
				if(address2.toString().toUpperCase().contains(keyWord)){ return true;}
			}
		}
		if(Utils.isValidParam(email1)){
			if(email1.toUpperCase().contains(keyWord)){ return true;}
		}
		if(Utils.isValidParam(email2)){
			if(email2.toUpperCase().contains(keyWord)){ return true;}
		}
		if(Utils.isValidParam(phone1)){
			if(phone1.toUpperCase().contains(keyWord)){ return true;}
		}
		if(Utils.isValidParam(phone2)){
			if(phone2.toUpperCase().contains(keyWord)){ return true;}
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ContactInformation){
			return (Long.compare(getId(), ((ContactInformation) obj).getId())) == 0;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 61 * hash + Objects.hashCode(this.id);
		return hash;
	}
	
}
