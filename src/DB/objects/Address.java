/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String streetNum;
    private String streetInfo1;
    private String streetInfo2;
    private String postCode;
    private String area;
    private String city;
    private String country;

    static final long serialVersionUID = 1L;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getStreetNum() {
        return streetNum;
    }
    public void setStreetNum(String streetNum) {
        this.streetNum = streetNum;
    }
    public String getStreetInfo1() {
        return streetInfo1;
    }
    public void setStreetInfo1(String streetInfo1) {
        this.streetInfo1 = streetInfo1;
    }
    public String getStreetInfo2() {
        return streetInfo2;
    }
    public void setStreetInfo2(String streetInfo2) {
        this.streetInfo2 = streetInfo2;
    }
    public String getPostCode() {
        return postCode;
    }
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        if(streetNum != null){
            sb.append(streetNum);
        }
        sb.append(" ");
        if(streetInfo1 != null)
            sb.append(streetInfo1);
        sb.append(" ");
        if(streetInfo2 != null)
            sb.append(streetInfo2);
        sb.append(" ");
        if(postCode != null)
            sb.append(postCode);
        sb.append(" ");
        if(area != null)
            sb.append(area);
        sb.append(" ");
        if(area != null)
            sb.append(area);
        sb.append(" ");
        if(city != null)
            sb.append(city);
        if(Utils.isValidParam(sb.toString()) && country != null){
            sb.append(", ");
            sb.append(country);
        }
		if(sb.toString().isEmpty()) return "";
        return sb.toString();
    
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Address){
            return (Long.compare(getId(), ((Address) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
