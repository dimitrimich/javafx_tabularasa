/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author d.michaelides
 */
@Entity
public class WeeklyLesson implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private int dayOfTheWeek;
    private int startingHour;
    private int startingMinute;
    private int duration;
    private String description;
    private int indexNumber;
    static final long serialVersionUID = 1L;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getDayOfTheWeek() {
        return dayOfTheWeek;
    }
    public void setDayOfTheWeek(int dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }
    public int getStartingHour() {
        return startingHour;
    }
    public void setStartingHour(int startingHour) {
        this.startingHour = startingHour;
    }
    public int getStartingMinute() {
        return startingMinute;
    }
    public void setStartingMinute(int startingMinute) {
        this.startingMinute = startingMinute;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getIndexNumber() {
        return indexNumber;
    }
    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }
    
    public String getStartingTime(){
        try{
            String time = Integer.toString(startingHour) +":"+ Integer.toString(startingMinute); 
            if(Integer.toString(startingMinute).equals("0")){
                return ("0:0".equals(time)) ?"N/A":time+"0";
            }
            return ("0:0".equals(time)) ?"N/A":time;
        }catch(Exception e){
            return "N/A";
        }
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof WeeklyLesson){
            return (Long.compare(getId(), ((WeeklyLesson) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    public boolean isFullyScheduled(){
        return (dayOfTheWeek != 0) && (startingHour != 0) && (duration != 0);
    }
}
