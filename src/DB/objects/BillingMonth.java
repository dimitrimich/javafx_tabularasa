/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package DB.objects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author dimitris
 */
@Entity
public class BillingMonth implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private Long classId;
    private Month billingMonth;
    private Year billingYear;
    @Column (precision = 8, scale = 2)
    private BigDecimal amountFull;
    
    @OneToMany
    @MapKey(name = "studentID")
    private Map<Long, ListOfStudentsPayments> allPayments = new HashMap<>();
	
    static final long serialVersionUID = 2L;
    
    @Transient
    public static final boolean STATUS_PAID = true;
    @Transient
    public static final boolean STATUS_NOT_PAID = false;
    
    public BillingMonth() {
        this.amountFull = BigDecimal.ZERO;
    }
    
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id;}
    public BigDecimal getAmountFull() {return amountFull; }
    public void setAmountFull(BigDecimal amountFull) { this.amountFull = amountFull; }
    public Long getClassId() { return classId; }
    public void setClassId(Long classId) { this.classId = classId;}
    public Month getBillingMonth() { return billingMonth; }
    public void setBillingMonth(Month billingMonth) { this.billingMonth = billingMonth; }
	public List<PaymentDetails> getPaymentsForStudent(Long studentID) { return allPayments.get(studentID).getPayments(); }
	public Map<Long, ListOfStudentsPayments> getAllPayments() { return allPayments; }
	public void setAllPayments(Map<Long, ListOfStudentsPayments> allPayments) { this.allPayments = allPayments; }
	
	public Year getBillingYear() {return billingYear; }
	public void setBillingYear(Year billingYear) { this.billingYear = billingYear; }
	
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BillingMonth){
            return (Long.compare(getId(), ((BillingMonth) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 11;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }
	private boolean isPaidInFullByStudent(Long stuID){
		return (allPayments.get(stuID).getAmountPaidAndDiscount().compareTo(amountFull) == 0);
	}
	public BigDecimal getTotalDiscountForStudent(Long stuID){
		return allPayments.get(stuID).getDiscount();
	}
	public BigDecimal getTotalDiscount(){
		BigDecimal discountSoFar = BigDecimal.ZERO;
		for(ListOfStudentsPayments payments: allPayments.values()){
			discountSoFar = discountSoFar.add(payments.getDiscount());
		}
		return discountSoFar;
	}
	public BigDecimal getTotalPaidForStudent(Long stuID){
		return allPayments.get(stuID).getAmountPaid();
	}
	
	public BigDecimal getTotalAmountPaid(){
		BigDecimal paidSoFar = BigDecimal.ZERO;
		for(ListOfStudentsPayments payments: allPayments.values()){
			paidSoFar = paidSoFar.add(payments.getAmountPaid());
		}
		return paidSoFar;
	}
	
	public BigDecimal getRemainingAmountForStudent(Student stu){
		return this.amountFull.subtract(getTotalDiscountForStudent(stu.getId()).add(getTotalPaidForStudent(stu.getId())));
	}
}
