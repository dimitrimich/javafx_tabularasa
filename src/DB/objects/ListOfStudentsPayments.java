/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB.objects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author d.michaelides
 */
@Entity
public class ListOfStudentsPayments implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	private Long studentID;
	@OneToMany
	private List<PaymentDetails> payments = new ArrayList<>();
	static final long serialVersionUID = 1L;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<PaymentDetails> getPayments() {
		return payments;
	}
	public Long getStudentID() {
		return studentID;
	}
	public void setStudentID(Long stuID) {
		this.studentID = stuID;
	}
	public BigDecimal getAmountPaid(){
		BigDecimal paid = BigDecimal.ZERO;
		for(PaymentDetails payment : payments){
			paid = paid.add(payment.getPaidAmount());
		}
		return paid;
	}
	public BigDecimal getDiscount(){
		BigDecimal paid = BigDecimal.ZERO;
		for(PaymentDetails payment : payments){
			paid = paid.add(payment.getDiscount());
		}
		return paid;
	}
	
	public BigDecimal getAmountPaidAndDiscount(){
		return getDiscount().add(getAmountPaid());
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ListOfStudentsPayments){
			return (Long.compare(getId(), ((ListOfStudentsPayments) obj).getId())) == 0;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int hash = 69;
		hash = 48 * hash + Objects.hashCode(this.id);
		return hash;
	}
	
}
