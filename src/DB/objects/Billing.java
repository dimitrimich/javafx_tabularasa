/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB.objects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import utils.Constants;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Billing implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	private Long studentID;
	private String studentDetails;
	private LocalDate lessonsDate;
	private boolean status;
	@Transient
	public static final boolean STATUS_PAID = true;
	@Transient
	public static final boolean STATUS_NOT_PAID = false;
	@OneToMany
	private List<PaymentDetails> payments;
	
	@Column (precision = 8, scale = 2)
	private BigDecimal amountFull;
	static final long serialVersionUID = 1L;
	
	public Billing(){
		studentDetails = "";
		status = STATUS_NOT_PAID;
		payments = new ArrayList<>();
		this.studentDetails = "";
		this.lessonsDate = Constants.UNKOWN_DATE;
		this.amountFull = BigDecimal.ZERO;
	}
	public Billing(Student s, Lesson l){
		this();
		this.lessonsDate = l.getTheDate();
		studentID = s.getId();
		studentDetails = s.getAttendaceTableInfo();
//        l.getTheClass().isPaidPerMonth()
		if(!l.getTheClass().isPaidPerMonth()){
			this.amountFull = ((l.getTheClass().getHourlyFee().multiply(BigDecimal.valueOf(l.getDuration()))).divide(BigDecimal.valueOf(60), 2, RoundingMode.HALF_UP));
		}
	}
	
	public void updateFullAmount(Aclass theClass, Lesson l){
		if(theClass.getMonthlyFee().compareTo(BigDecimal.ZERO) != 0){
			// calculate based on monthly fee
			this.amountFull = BigDecimal.ZERO;
		}else{
			// calculate based on HOURLY fee
			this.amountFull = ((l.getTheClass().getHourlyFee().multiply(
					new BigDecimal(l.getDuration())
			))
					.divide(new BigDecimal(60), 2, RoundingMode.HALF_UP));
		}
	}
	
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	public Long getStudentID() {return studentID;}
	public void setStudentID(Long studentID) {this.studentID = studentID;}
	public boolean getStatus() {return status;}
	public void setStatus(boolean status) {this.status = status; }
	public LocalDate getLessonsDate() { return lessonsDate; }
	public void setLessonsDate(LocalDate lessonsDate) { this.lessonsDate = lessonsDate; }
	public BigDecimal getAmountFull() { return amountFull; }
	public void setAmountFull(BigDecimal amountFull) { this.amountFull = amountFull; }
	public String getStudentDetails() {
		return studentDetails;
	}
	public void setStudentDetails(String studentDetails) {
		this.studentDetails = studentDetails;
	}
	
	public boolean isPaidInFull(){
		if(status == STATUS_PAID || (isAllLessonsPaid())){
			return true;
		}
		return false;
	}
	public boolean isAllLessonsPaid(){
		BigDecimal paidAmount = BigDecimal.ZERO;
		for(PaymentDetails payment : payments){
			paidAmount = paidAmount.add(payment.getDiscount()).add(payment.getPaidAmount());
		}
		return (this.getAmountFull().compareTo(paidAmount) == 0);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Billing){
			return (Long.compare(getId(), ((Billing) obj).getId())) == 0;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 67 * hash + Objects.hashCode(this.id);
		return hash;
	}
	public void addPayment(PaymentDetails payment){
		payments.add(payment);
		if(isAllLessonsPaid()){
			this.status = STATUS_PAID;
		}
	}
	public List<PaymentDetails> getPayments() { return payments; }
	public void setPayments(List<PaymentDetails> payments) {
		this.payments = payments;
		if(isAllLessonsPaid()){
			this.status = STATUS_PAID;
		}
	}
	public BigDecimal getTotalDiscount(){
		BigDecimal total = BigDecimal.ZERO;
		for(PaymentDetails payment : payments){
			total = total.add(payment.getDiscount());
		}
		return total;
	}
	public BigDecimal getTotalPaidAmount(){
		BigDecimal total = BigDecimal.ZERO;
		for(PaymentDetails payment : payments){
			total = total.add(payment.getPaidAmount());
		}
		return total;
	}
	public PaymentDetails convertToPaymentDetails(){
		PaymentDetails payment = new PaymentDetails();
		payment.setTotalAmount(amountFull);
		payment.setDiscount(this.getTotalDiscount());
		payment.setPaidAmount(this.getTotalPaidAmount());
		return payment;
	}
	public PaymentDetails getRemainingPayment(){
		PaymentDetails payment = new PaymentDetails();
		payment.setTotalAmount(amountFull.subtract( getTotalDiscount().add(getTotalPaidAmount()) ));
		return payment;
	}
	public BigDecimal getRemainingAmount(){
		return amountFull.subtract( getTotalDiscount().add(getTotalPaidAmount()));
	}
	
}
