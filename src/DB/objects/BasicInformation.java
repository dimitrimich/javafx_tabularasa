/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author d.michaelides
 */
@Entity
public class BasicInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String fname;
    private String middleName;
    private String lname;
    static final long serialVersionUID = 1L;
    @Transient
    private SimpleStringProperty firstName;
    @Transient
    private SimpleStringProperty lastName;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
        if(this.firstName == null){
            this.firstName = new SimpleStringProperty(fname);
        }else{
            this.firstName.set(fname);
        }
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getLname() {
        return lname;
    }
    public void setLname(String lname) {
        this.lname = lname;
        if(this.lastName == null){
            this.lastName = new SimpleStringProperty(lname);
        }else{
            this.lastName.set(lname);
        }
    }

    public SimpleStringProperty getFirstName() {
        return firstName;
    }
    public void setFirstName(SimpleStringProperty firstName) {
        this.firstName = firstName;
        this.fname = firstName.get();
    }
    public SimpleStringProperty getLastName() {
        return lastName;
    }
    public void setLastName(SimpleStringProperty lastName) {
        this.lastName = lastName;
        this.lname = lastName.get();
    }
    
    
	public boolean containsKeyWordNotCaseSensitive(String keyWord){
		if((fname).toUpperCase().contains(keyWord)){ return true;}
		if((lname).toUpperCase().contains(keyWord)){ return true;}
		if(middleName != null){
			if((middleName).toUpperCase().contains(keyWord)){ return true;}
		}
		return false;
	}
	
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BasicInformation){
            return (Long.compare(getId(), ((BasicInformation) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
}
