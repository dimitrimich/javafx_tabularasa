/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB.objects;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author d.michaelides
 */
@Entity
public class Attendance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private Long studentID;
    private String reason;
    private String studentDetails;
    private int status;
    public static final int YES = 1;
    public static final int NO = 2;
    public static final int UNKNOWN = 3;
    static final long serialVersionUID = 1L;
    
    @Transient
    private SimpleStringProperty studentDetailsProp;
    @Transient
    private SimpleStringProperty reasonProp;
    @Transient
    private SimpleIntegerProperty statusProp;
    
    public Attendance(){
        reason = "";
        studentDetails ="";
        reasonProp = new SimpleStringProperty(reason);
        status = UNKNOWN;
    }
    public Attendance(Student s){
        studentID = s.getId();
        reason = "";
        studentDetails = s.getAttendaceTableInfo();
        studentDetailsProp = new SimpleStringProperty(studentDetails);
        reasonProp = new SimpleStringProperty(reason);
        status = UNKNOWN;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStudentID() {
        return studentID;
    }
    public void setStudentID(Long studentID) {
        this.studentID = studentID;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    
    public String getReasonProp() {
        if(reasonProp == null){
            reasonProp = new SimpleStringProperty(reason);
        }else{
            reasonProp.setValue(reason);
        }
        return reasonProp.get();
    }

    public void setReasonProp(String reasonProp) {
        if(this.reasonProp == null){
            this.reasonProp = new SimpleStringProperty(reasonProp);
            this.reasonProp.setValue(reasonProp);
        }else{
            this.reasonProp.setValue(reasonProp);
        }
    }

    public SimpleIntegerProperty getStatusProp() {
        if(this.statusProp == null){
            this.statusProp = new SimpleIntegerProperty(status);
            this.statusProp.setValue(status);
        }else{
            this.statusProp.setValue(status);
        }
        return statusProp;
    }

    public void setStatusProp(int statusProp) {
        if(this.statusProp == null){
            this.statusProp = new SimpleIntegerProperty(statusProp);
            this.statusProp.setValue(statusProp);
        }else{
            this.statusProp.setValue(statusProp);
        }
        this.status = statusProp;
    }
    public String getStudentDetailsProp() {
        if(studentDetailsProp == null){
            studentDetailsProp = new SimpleStringProperty();
            studentDetailsProp.setValue(studentDetails);
        }else{
            studentDetailsProp.setValue(studentDetails);
        }
        return studentDetailsProp.get();
    }
    public void setStudentDetailsProp(SimpleStringProperty studentDetailsProp) {
        this.studentDetailsProp = studentDetailsProp;
    }
    public String getStudentDetails() {
        return studentDetails;
    }
    public void setStudentDetails(String studentDetails) {
        this.studentDetails = studentDetails;
        if(studentDetailsProp  == null){
            studentDetailsProp = new SimpleStringProperty(studentDetails);
        }else{
            studentDetailsProp.setValue(studentDetails);
        }
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Attendance){
//			if(getId() == null && ((Attendance) obj).getId() == null){
//				return true;
//			}
            return (Long.compare(getId(), ((Attendance) obj).getId())) == 0;
        }
        return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
