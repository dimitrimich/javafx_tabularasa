/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package DB;

import DB.objects.Aclass;
import DB.objects.BillingMonth;
import DB.objects.DBProperties;
import DB.objects.Guardian;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfWLessons;
import DB.objects.Schedule;
import DB.objects.Student;
import DB.objects.Teacher;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author d.michaelides
 */
public class DButils {
	
	
	
	public static boolean saveObject(Object o){
		printName(o);
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			em2.persist(o);
			em2.getTransaction().commit();
			em2.close();
		}catch(Exception e){
			System.out.println("SAVING failed - Exception");
			System.err.println(e.getClass().getSimpleName()+" - "+ e.getMessage());
			return false;
		}
		return true;
	}
	/**
	 * GET OBJECT FROM DB0
	 */
	public static Object getObjectBasedOnID(Class c, Long id){
		Object foundObject;
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			foundObject = em2.find(c, id);
			em2.close();
		}catch(Exception e){
			System.out.println("find failed - Exception");
			System.err.println(e.getClass().getSimpleName()+" - "+ e.getMessage());
			return null;
		}
		return foundObject;
	}
	
	public static Object updateObject(Object toBeUpdated){
		boolean result = false;
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			toBeUpdated = em2.merge(toBeUpdated);
			em2.getTransaction().commit();
			em2.close();
		}catch(Exception e){
			System.err.println("Update failed - Exception");
			System.err.println(e.getClass().getSimpleName()+" - "+ e.getMessage());
			return null;
		}
		return toBeUpdated;
	}
	
	
	
	private static void printName(Object o){
		switch(o.getClass().getSimpleName()){
			case "Student":
				System.out.println("Saving student: "+((Student)o).getBasicInfo().getFname() + " "+((Student)o).getBasicInfo().getLname());
				System.out.println("Saving student: "+((Student)o).getId());
				break;
			case "Aclass":
				System.out.println("Saving class: "+((Aclass)o).getName());
				break;
			case "Guardian":
				System.out.println("Saving Guardian: "+((Guardian)o).getBasicInfo().getFname());
				break;
			case "Schedule":
				if(((Schedule)o).getTheClass() != null){
					System.out.println("Saving Schedule of class: "+((Schedule)o).getTheClass().getName());
				}else{
					System.out.println("Saving Schedule");
				}
				break;
			case "Teacher":
				System.out.println("Saving Teacher: "+((Teacher)o).getBasicInfo().getFname());
				break;
			default:
				System.out.print("Saving something of unknown class - "+o.getClass().getSimpleName());
				break;
		}
	}
	
	public static boolean saveStudent(Student toBeSaved){
		EntityManager em2 = null;
		
		try {
			em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
		} catch (Exception e) {
			System.err.println("Error while saving -3242-");
			e.printStackTrace(System.err);
			return false;
		}
		try{
			em2.persist(toBeSaved.getBasicInfo());
			em2.persist(toBeSaved.getContactInfo().getAddress1());
			em2.persist(toBeSaved.getContactInfo().getAddress2());
			em2.persist(toBeSaved.getContactInfo());
		}catch(Exception e){
			System.out.println("Error while saving -1-");
			e.printStackTrace(System.out);
			return false;
		}
		try{
			em2.persist(toBeSaved);
			em2.getTransaction().commit();
			em2.close();
		}catch(Exception e){
			System.out.println("Error while saving -2-");
			e.printStackTrace(System.out);
			return false;
		}
		return true;
	}
	public static boolean saveClass(Aclass toBeSaved){
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			try{
				toBeSaved.getSchedule().getwLessons().stream().forEach((lesson) -> {
					em2.persist(lesson);
				});
				System.out.println("Saving Lessons");
				int i = 0;
				Map<LocalDate, ListOfWLessons> theWMap = toBeSaved.getSchedule().getAllWLessons();
				for(Entry<LocalDate, ListOfWLessons> anEntry: theWMap.entrySet()){
					ListOfWLessons listOfLessons = anEntry.getValue();
					for(Lesson theLesson : listOfLessons.getAllLessons()){
						for(Long theID : theLesson.getAttendance().keySet()){
							em2.persist(theLesson.getAttendance().get(theID));
						}
						em2.persist(theLesson);
						i++;
					}
					em2.persist(listOfLessons);
				}
				
				System.out.println("Saved "+ i + " Weekly lessons");
				
				i = 0;
				Map<LocalDate, ListOfELessons> theEMap = toBeSaved.getSchedule().getAllELessons();
				if(theEMap != null && !theEMap.isEmpty()){
					
					for(Entry<LocalDate, ListOfELessons> anEntry : theEMap.entrySet()){
						ListOfELessons listOfLessons = anEntry.getValue();
						for(Lesson theLesson : listOfLessons.getAllLessons()){
							for(Long theID : theLesson.getAttendance().keySet()){
								em2.persist(theLesson.getAttendance().get(theID));
							}
							em2.persist(theLesson);
							i++;
						}
						em2.persist(listOfLessons);
					}
					System.out.println("Saved "+ i + " Extra lessons");
				}else{
					System.out.println("Add empty list to be accepted");
					System.out.println("Not adding");
					//                ListOfELessons emptyList = new ListOfELessons();
					//                em2.persist(emptyList);
					//                emptyList.setTheDate(LocalDate.now());
					//                theEMap.put(LocalDate.now(), emptyList);
				}
			} catch (RuntimeException e) {
				throw e;
			}catch(Exception e){
				System.out.println("Error while saving -1-");
				e.printStackTrace(System.out);
				return false;
			}
			
			try{
				System.out.println("saving schedule");
				em2.persist(toBeSaved.getSchedule());
			}catch(Exception e){
				System.out.println("Error while saving -1.5-");
				e.printStackTrace(System.out);
				return false;
			}
			try{
				int i = 0;
				for(BillingMonth bMonth : toBeSaved.getBillingMonths()){
					i++;
					em2.persist(bMonth);
				}
				System.out.println("Saved "+i+" billing Months");
				
				
			}catch(Exception e){
				System.err.println("Error while saving -1.1.5-");
				e.printStackTrace(System.err);
				return false;
			}
			
			try{
				em2.persist(toBeSaved);
				if(toBeSaved.getTeacher() != null){
					System.out.println("toBeSaved.getTeacher() != null");
					toBeSaved.getTeacher().getClasses().add(toBeSaved);
					System.out.println("Save teacher of Class");
					em2.merge(toBeSaved.getTeacher());
				}else{
					System.out.println("toBeSaved.getTeacher() == null");
					
				}
				em2.getTransaction().commit();
				em2.close();
			}catch(Exception e){
				System.out.println("Error while saving -2-");
				e.printStackTrace(System.out);
				return false;
			}
			return true;
		}catch(Exception e){
			System.err.println("Error while saving class - 000 -");
			e.printStackTrace(System.err);
			return false;
		}
	}
	public static boolean saveTeacher(Teacher toBeSaved){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		try{
			em2.persist(toBeSaved.getBasicInfo());
			em2.persist(toBeSaved.getContactInfo().getAddress1());
			em2.persist(toBeSaved.getContactInfo().getAddress2());
			em2.persist(toBeSaved.getContactInfo());
		}catch(Exception e){
			System.out.println("Error while saving -1-");
			e.printStackTrace(System.out);
			return false;
		}
		try{
			em2.persist(toBeSaved);
			em2.getTransaction().commit();
			em2.close();
		}catch(Exception e){
			System.out.println("Error while saving -2-");
			e.printStackTrace(System.out);
			return false;
		}
		return true;
	}
	public static void printAllStudents(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Student c");
		// do we have entries?
		List<Student> entries = q.getResultList();
		System.out.println("Students:");
		int i = 0;
		for (Student student : entries) {
			i++;
			System.out.println("   "+i+". "+student.getId()+" -> "+student.getBasicInfo().getFname() +" "+student.getBasicInfo().getLname());
		}
		System.out.println("done");
	}
	public static void printAllClasses(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Aclass c");
		// do we have entries?
		List<Aclass> entries = q.getResultList();
		System.out.println("Classes:");
		int i = 0;
		for (Aclass theClass : entries) {
			i++;
			System.out.println("   "+i+". "+theClass.getName());
			if(theClass.getSchedule() != null){
				if(theClass.getSchedule().getwLessons() != null){
					System.out.println("   "+i+". "+theClass.getSchedule().getwLessons().size());
				}
			}
		}
		System.out.println("done");
	}
	
	public static void printAllTeachers(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Teacher c");
		// do we have entries?
		List<Teacher> entries = q.getResultList();
		System.out.println("Teachers:");
		int i = 0;
		for (Teacher teach : entries) {
			i++;
			System.out.println("   "+i+". "+teach.getBasicInfo().getFname());
		}
		System.out.println("done");
	}
	
	public static String getNextColor(){
		DBProperties props;
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from DBProperties c");
		// do we have entries?
		List<DBProperties> entries = q.getResultList();
		if(entries.isEmpty()){
			props = new DBProperties();
			em2.persist(props);
		}else{
			props = entries.get(0);
		}
		String nextColor = props.getNextColor();
		em2.merge(props);
		
		
		em2.getTransaction().commit();
		em2.close();
		return nextColor;
	}
	public static String getNextReceiptId(){
		DBProperties props;
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from DBProperties c");
		// do we have entries?
		List<DBProperties> entries = q.getResultList();
		if(entries.isEmpty()){
			props = new DBProperties();
			em2.persist(props);
		}else{
			props = entries.get(0);
		}
		String nextReceipt = props.getPaymentReceiptNo();
		em2.merge(props);
		
		em2.getTransaction().commit();
		em2.close();
		return nextReceipt;
	}
	
	public static Student findStudentWithName(String name) {
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		Student student = (Student)em2.createQuery("SELECT OBJECT(stu) FROM Student stu WHERE stu.basicInfo.fname = :studentName")
				.setParameter("studentName", name).getSingleResult();
		em2.close();
		return student;
	}
	public static Aclass findClassWithCode(String code) {
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		Aclass theClass = (Aclass)em2.createQuery("SELECT OBJECT(theClass) FROM Aclass theClass WHERE theClass.codeName = :code")
				.setParameter("code", code).getSingleResult();
		em2.close();
		return theClass;
	}
	
	public static Student getAllLessons(Long classId, Month month) {
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		Student student = (Student)em2.createQuery(
				
				"SELECT less "
						+ "FROM Lesson less, Aclass theClass"
						+ " WHERE theClass.id = :classId")
				.setParameter("classId", classId).getSingleResult();
		
		
		/*
		
		ArrayList<Lesson> theMonthsLessons = new ArrayList<>();
		Map<LocalDate, ListOfELessons> extraLessons = getSchedule().getAllELessons();
		for(Entry<LocalDate, ListOfELessons>anEntry : extraLessons.entrySet()){
		if(anEntry.getKey().getMonth().compareTo(theDate.getMonth()) == 0){
		theMonthsLessons.addAll(anEntry.getValue().getAllLessons());
		}
		}
		
		Map<LocalDate, ListOfWLessons> weeklyLessons = getSchedule().getAllWLessons();
		
		for(Entry<LocalDate, ListOfWLessons>anEntry : weeklyLessons.entrySet()){
		if(anEntry.getKey().getMonth().compareTo(theDate.getMonth()) == 0){
		theMonthsLessons.addAll(anEntry.getValue().getAllLessons());
		}
		}
		
		*/
		
		
		em2.close();
		return student;
	}
	
//    public Collection findProjectsWithName(String name) {
//        ExpressionBuilder builder = new ExpressionBuilder();
//        JpaEntityManager jpaEm = JpaHelper.getEntityManager(em);
//        Query query = jpaEm.createQuery(builder.get("name").equals(builder.getParameter("projectName")), Project.class);
//        query.setParameter("projectName", name);
//        Collection projects = query.getResultList();
//        return projects;
//    }
	
	public static Student getFirstStudent(){
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
		em2.getTransaction().begin();
		// read the existing entries
		Query q = em2.createQuery("select c from Student c");
		// do we have entries?
		List<Student> entries = q.getResultList();
		em2.close();
		return entries.get(0);
	}
	
}
