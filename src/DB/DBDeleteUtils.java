/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package DB;

import DB.objects.Aclass;
import DB.objects.Address;
import DB.objects.Attendance;
import DB.objects.BasicInformation;
import DB.objects.Billing;
import DB.objects.BillingMonth;
import DB.objects.ContactInformation;
import DB.objects.Guardian;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfStudentsPayments;
import DB.objects.ListOfWLessons;
import DB.objects.PaymentDetails;
import DB.objects.Schedule;
import DB.objects.Student;
import DB.objects.Teacher;
import DB.objects.WeeklyLesson;
import java.time.LocalDate;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class DBDeleteUtils {
	
	public static boolean updateStudentInfo(Student toBeUpdated){
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			if(toBeUpdated.getContactInfo() != null){
				if(toBeUpdated.getContactInfo().getAddress1() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress1().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress1());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress1());
					}
				}
				
				if(toBeUpdated.getContactInfo().getAddress2() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress2().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress2());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress2());
					}
				}
				
				if(Utils.isValidID(toBeUpdated.getContactInfo().getId())){
					em2.merge(toBeUpdated.getContactInfo());
				}else{
					em2.persist(toBeUpdated.getContactInfo());
				}
			}
			em2.merge(toBeUpdated.getBasicInfo());
			em2.merge(toBeUpdated);
			em2.getTransaction().commit();
			em2.close();
			return true;
		}catch(Exception e){
			System.err.println("Error while updating object");
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	public static boolean updateTeacherInfo(Teacher toBeUpdated){
		try{
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			if(toBeUpdated.getContactInfo() != null){
				if(toBeUpdated.getContactInfo().getAddress1() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress1().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress1());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress1());
					}
				}
				
				if(toBeUpdated.getContactInfo().getAddress2() != null){
					if(Utils.isValidID(toBeUpdated.getContactInfo().getAddress2().getId())){
						em2.merge(toBeUpdated.getContactInfo().getAddress2());
					}else{
						em2.persist(toBeUpdated.getContactInfo().getAddress2());
					}
				}
				
				if(Utils.isValidID(toBeUpdated.getContactInfo().getId())){
					em2.merge(toBeUpdated.getContactInfo());
				}else{
					em2.persist(toBeUpdated.getContactInfo());
				}
			}
			em2.merge(toBeUpdated.getBasicInfo());
			em2.merge(toBeUpdated);
			em2.getTransaction().commit();
			em2.close();
			return true;
		}catch(Exception e){
			System.err.println("Error while updating object");
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	public static boolean deleteLesson(Lesson toDelete){
		
		EntityManager em = null;
		
		try{
			em = FactoryMaker.getInstance().createEntityManager();
			em.getTransaction().begin();
			
			toDelete.setTheClass(null);
			for(Attendance attendance : toDelete.getAttendance().values()){
				em.remove(em.getReference(Attendance.class, attendance.getId()));
			}
			for(Billing billing : toDelete.getBilling().values()){
				for(PaymentDetails payment : billing.getPayments()){
					em.remove(em.getReference(PaymentDetails.class, payment.getId()));
				}
				em.remove(em.getReference(Billing.class, billing.getId()));
			}
			toDelete.setAttendance(null);
			toDelete.setBilling(null);
			
			em.merge(toDelete);
			em.remove(em.getReference(Lesson.class, toDelete.getId()));
			
			em.getTransaction().commit();
			em.close();
		}catch(Exception e){
			System.err.println(" ERROR while deleting lesson");
			e.printStackTrace(System.err);
			if(em != null){
				em.close();
			}
			return false;
		}
		return true;
		
		
	}
	
	
	public static boolean deleteStudentObject(Student toBeDeleted){
		try{
			EntityManager em = FactoryMaker.getInstance().createEntityManager();
			em.getTransaction().begin();
			
			try{
				// delete basic info
				em.remove(em.getReference(BasicInformation.class, toBeDeleted.getBasicInfo().getId()));
				
				if(toBeDeleted.getContactInfo() != null){
					// delete Address 1
					if(toBeDeleted.getContactInfo().getAddress1() != null){
						em.remove(em.getReference(Address.class, toBeDeleted.getContactInfo().getAddress1().getId()));
					}
					// delete Address 2
					if(toBeDeleted.getContactInfo().getAddress2() != null){
						em.remove(em.getReference(Address.class, toBeDeleted.getContactInfo().getAddress2().getId()));
					}
					// delete Contact info
					em.remove(em.getReference(ContactInformation.class, toBeDeleted.getContactInfo().getId()));
				}
				// remove reference from class -> student
				if(toBeDeleted.getClasses() != null){
					for(Aclass aClass : toBeDeleted.getClasses()){
						aClass.getStudents().remove(toBeDeleted);
						em.merge(aClass);
					}
				}
				// remove reference from guardian -> student
				if(toBeDeleted.getGuardians() != null){
					for(Guardian guardian : toBeDeleted.getGuardians()){
						guardian.getStudents().remove(toBeDeleted);
						em.merge(guardian);
					}
				}
			}catch(Exception e){
				System.out.println("Exception "+e);
				return false;
			}
			
			try{
				toBeDeleted.setBasicInfo(null);
				toBeDeleted.setContactInfo(null);
				toBeDeleted.setClasses(null);
				toBeDeleted.setGuardians(null);
				em.remove(em.merge(toBeDeleted));
				em.getTransaction().commit();
				em.close();
				System.out.println(" !!! DBDeleteUtils.deleted !!!");
				return true;
			}catch(Exception e){
				System.out.println("Exception "+e);
				return false;
			}
		}
		catch(Exception e){
			System.err.println("Exception while deleting Stu");
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	public static boolean deleteTeacherObject(Teacher toBeDeleted){try {
		
		EntityManager em = FactoryMaker.getInstance().createEntityManager();
		em.getTransaction().begin();
		
		try {
			// delete basic info
			em.remove(em.getReference(BasicInformation.class, toBeDeleted.getBasicInfo().getId()));
			
			if (toBeDeleted.getContactInfo() != null) {
				// delete Address 1
				if (toBeDeleted.getContactInfo().getAddress1() != null) {
					em.remove(em.getReference(Address.class, toBeDeleted.getContactInfo().getAddress1().getId()));
				}
				// delete Address 2
				if (toBeDeleted.getContactInfo().getAddress2() != null) {
					em.remove(em.getReference(Address.class, toBeDeleted.getContactInfo().getAddress2().getId()));
				}
				// delete Contact info
				em.remove(em.getReference(ContactInformation.class, toBeDeleted.getContactInfo().getId()));
			}
			// remove reference from class -> teacher
			if (toBeDeleted.getClasses() != null) {
				toBeDeleted.getClasses().stream().forEach((aClass) -> {
					em.merge(aClass);
				});
			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
			return false;
		}
		
		try {
			toBeDeleted.setBasicInfo(null);
			toBeDeleted.setContactInfo(null);
			toBeDeleted.setClasses(null);
			em.remove(em.merge(toBeDeleted));
			em.getTransaction().commit();
			em.close();
			return true;
		} catch (Exception e) {
			System.out.println("Exception " + e);
			return false;
		}
		
	} catch (Exception e) {
		System.err.println("Exception while deleting teacher");
		e.printStackTrace(System.err);
		return false;
	}
	}
	
	public static boolean deleteClassObject(Aclass toBeDeleted){
		try {
			EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
			em2.getTransaction().begin();
			// remove weekly lessons
			for (WeeklyLesson lesson : toBeDeleted.getSchedule().getwLessons()) {
				em2.remove(em2.getReference(WeeklyLesson.class, lesson.getId()));
			}
			//remove extra lessons
			Map<LocalDate, ListOfELessons> theMap = toBeDeleted.getSchedule().getAllELessons();
			for(Entry<LocalDate, ListOfELessons> anEntry : theMap.entrySet()){
				ListOfELessons todaysList = theMap.get(anEntry.getKey());
				for(Lesson aLesson: anEntry.getValue().getAllLessons()){
					for(Billing bill : aLesson.getBilling().values()){
						for(PaymentDetails payment : bill.getPayments()){
							em2.remove(em2.getReference(PaymentDetails.class, payment.getId()));
						}
						em2.remove(em2.getReference(Billing.class, bill.getId()));
					}
					em2.remove(em2.getReference(Lesson.class, aLesson.getId()));
				}
				em2.remove(em2.getReference(ListOfELessons.class, todaysList.getId()));
			}
			Map<LocalDate, ListOfWLessons> theWMap = toBeDeleted.getSchedule().getAllWLessons();
			for(Entry<LocalDate, ListOfWLessons> anEntry : theWMap.entrySet()){
				ListOfWLessons todaysList = theWMap.get(anEntry.getKey());
				for(Lesson aLesson: anEntry.getValue().getAllLessons()){
					for(Billing bill : aLesson.getBilling().values()){
						for(PaymentDetails payment : bill.getPayments()){
							em2.remove(em2.getReference(PaymentDetails.class, payment.getId()));
						}
						em2.remove(em2.getReference(Billing.class, bill.getId()));
					}
					em2.remove(em2.getReference(Lesson.class, aLesson.getId()));
				}
				em2.remove(em2.getReference(ListOfWLessons.class, todaysList.getId()));
			}
			
			// remove reference from students
			for (Student student : toBeDeleted.getStudents()) {
				student.getClasses().remove(toBeDeleted);
				em2.merge(student);
			}
			
			// remove reference from teacher
			if (toBeDeleted.getTeacher() != null && toBeDeleted.getTeacher().getClasses() != null) {
				System.out.println("teacher stuff result = " + toBeDeleted.getTeacher().getClasses().remove(toBeDeleted));
				em2.merge(toBeDeleted.getTeacher());
			}
			toBeDeleted.getSchedule().setTheClass(null);
			em2.remove(em2.getReference(Schedule.class, toBeDeleted.getSchedule().getId()));
			toBeDeleted.getBillingMonths().stream().forEach((bMonth) -> {
				
				bMonth.getAllPayments().values().stream().forEach((listOfStudentPayments) -> {
					listOfStudentPayments.getPayments().stream().forEach((aPayment) -> {
						em2.remove(em2.getReference(PaymentDetails.class, aPayment.getId()));
					});
					listOfStudentPayments.getPayments().clear();
					em2.remove(em2.getReference(ListOfStudentsPayments.class, listOfStudentPayments.getId()));
				});
				bMonth.getAllPayments().clear();
				em2.remove(em2.getReference(BillingMonth.class, bMonth.getId()));
			});
			
			toBeDeleted.setBillingMonths(null);
			toBeDeleted.setTeacher(null);
			toBeDeleted.setSchedule(null);
			toBeDeleted.setStudents(null);
			
			try {
				em2.remove(em2.merge(toBeDeleted));
				em2.getTransaction().commit();
				em2.close();
			} catch (Exception e) {
				System.out.println("Error while saving -2-");
				e.printStackTrace(System.out);
				return false;
			}
			return true;
		}  catch (RuntimeException e) {
			throw e;
		}catch (Exception e) {
			System.err.println("Exception while deleting");
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	public static boolean deleteLesson(ListOfWLessons todaysLessons, Lesson lesson) {
		
		EntityManager em = null;
		try{
			em = FactoryMaker.getInstance().createEntityManager();
			em.getTransaction().begin();
			
			lesson.setTheClass(null);
			for(Attendance attendance : lesson.getAttendance().values()){
				em.remove(em.getReference(Attendance.class, attendance.getId()));
			}
			for(Billing billing : lesson.getBilling().values()){
				for(PaymentDetails payment : billing.getPayments()){
					em.remove(em.getReference(PaymentDetails.class, payment.getId()));
				}
				em.remove(em.getReference(Billing.class, billing.getId()));
			}
			lesson.setAttendance(null);
			lesson.setBilling(null);
			
			em.merge(lesson);
			em.remove(em.getReference(Lesson.class, lesson.getId()));
			em.merge(todaysLessons);
			
			em.getTransaction().commit();
			em.close();
		}catch(Exception e){
			System.err.println(" ERROR while deleting lesson");
			e.printStackTrace(System.err);
			if(em != null){
				em.close();
			}
			return false;
		}
		return true;
		
		
		
		
	}
}
