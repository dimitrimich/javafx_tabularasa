/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import DB.DBTableUtils;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 *
 * @author dimitris
 */
public class GetAllClasses<ObservableList> extends Service {
    
        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            theTask.setOnSucceeded(workerStateEvent -> {
            });
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
                return DBTableUtils.getAllClasses();
            }
        }
    }
