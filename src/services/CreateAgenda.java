/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import jfxtras.agenda.AgendaBuilder;

/**
 *
 * @author dimitris
 */
public class CreateAgenda extends Service<AgendaBuilder> {
    
        @Override
        protected Task createTask() {
            CreateAgendaTask theTask = new CreateAgendaTask();
            return theTask;
        }
        private class CreateAgendaTask extends Task<AgendaBuilder> {
            @Override
            protected AgendaBuilder call() throws Exception {
                return new AgendaBuilder();
            }
        }
    }
