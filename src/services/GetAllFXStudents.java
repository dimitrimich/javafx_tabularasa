/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import DB.DBTableUtils;
import DB.objects.Student;
import fxObjects.StudentFX;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * returns and ObservableList<StudentFX> list
 * @author dimitris
 */
public class GetAllFXStudents extends Service<ObservableList <StudentFX>>{

	@Override
	protected Task<ObservableList<StudentFX>> createTask() {
        GetAllFXStudentsTask theTask = new GetAllFXStudentsTask();
        return theTask;
	}
	
    private class GetAllFXStudentsTask extends Task <ObservableList<StudentFX>>{
		@Override
		protected ObservableList<StudentFX> call() throws Exception {
            ObservableList<Student> students = DBTableUtils.getAllStudents();
			ObservableList<StudentFX> studentsFX = FXCollections.observableArrayList();
			for(Student stu : students){
				studentsFX.add(new StudentFX(stu));
			}
			return studentsFX;
		}
    }
}
