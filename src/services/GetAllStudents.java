/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import DB.DBTableUtils;
import DB.objects.Aclass;
import DB.objects.Student;
import DB.objects.Teacher;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 *
 * @author dimitris
 * @param <ObservableList>
 */
public class GetAllStudents<ObservableList> extends Service {
    @Override
    protected Task createTask() {
        GetAllStudentsTask theTask = new GetAllStudentsTask();
        return theTask;
    }
    private class GetAllStudentsTask extends Task {
        @Override
        protected Object call() throws Exception {
            return DBTableUtils.getAllStudents();
        }
    }
    
    public static class GetAllStudentsOfTheTeacher<ObservableList> extends Service {
        private final Teacher theTeacher;
        public GetAllStudentsOfTheTeacher(Teacher t){
            super();
            theTeacher = t;
        }

        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
                ArrayList<Student>allStudents = new ArrayList<>();
                if(theTeacher.getClasses() != null){
                    for(Aclass aClass : theTeacher.getClasses()){
                        if(aClass.getStudents() != null){
                            allStudents.addAll(aClass.getStudents());
                        }
                    }
                }

                return FXCollections.observableArrayList(allStudents);
            }
        }
    }
}
