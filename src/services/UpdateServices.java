/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import DB.DBUpdateUtils;
import DB.objects.Aclass;
import DB.objects.Student;
import DB.objects.Teacher;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 *
 * @author dimitris
 */
public class UpdateServices  {
    @Deprecated
    public static class UpdateTeachersClassesService  extends Service{
        private final Teacher toBeUpdated;
        public UpdateTeachersClassesService(Teacher toBeU){
            super();
            toBeUpdated = toBeU;
        }

        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            theTask.setOnSucceeded(workerStateEvent -> {
            });
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
                return DBUpdateUtils.updateTeachersClasses(toBeUpdated);
            }
        }
    }
    @Deprecated
    public static class UpdateStudentsClassesService  extends Service{
        private final Student toBeUpdated;
        public UpdateStudentsClassesService(Student toBeU){
            super();
            toBeUpdated = toBeU;
        }

        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            theTask.setOnSucceeded(workerStateEvent -> {
            });
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
                return DBUpdateUtils.updateStudentsClasses(toBeUpdated);
            }
        }
    }
    @Deprecated
    public static class UpdateClassesStudentsService  extends Service{
        private final Aclass toBeUpdated;
        public UpdateClassesStudentsService(Aclass toBeU){
            super();
            toBeUpdated = toBeU;
        }

        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            theTask.setOnSucceeded(workerStateEvent -> {
            });
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
//                return DBUpdateUtils.updateClassesStudents(toBeUpdated);
                return false;
            }
        }
    }
    
    
    
    
}
