/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import DB.DBTableUtils;
import DB.objects.Aclass;
import DB.objects.Student;
import DB.objects.Teacher;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 *
 * @author dimitris
 */
public class GetAllTeachers<ObservableList> extends Service {

    @Override
    protected Task createTask() {
        GetAllTeachersTask theTask = new GetAllTeachersTask();
        return theTask;
    }
    private class GetAllTeachersTask extends Task {
        @Override
        protected Object call() throws Exception {
            return DBTableUtils.getAllTeachers();
        }
    }
    public static class GetAllTeachersOfTheStudent<ObservableList> extends Service {
        private final Student theStudent;
        public GetAllTeachersOfTheStudent(Student t){
            super();
            theStudent = t;
        }

        @Override
        protected Task createTask() {
            GetAllTeachersTask theTask = new GetAllTeachersTask();
            return theTask;
        }
        private class GetAllTeachersTask extends Task {
            @Override
            protected Object call() throws Exception {
                ArrayList<Teacher>allTeachers = new ArrayList<>();
                if(theStudent.getClasses() != null){
                    for(Aclass aClass : theStudent.getClasses()){
                        if(aClass.getStudents() != null){
                            if(aClass.getTeacher() != null){
                                allTeachers.add(aClass.getTeacher());
                            }
                        }
                    }
                }

                return FXCollections.observableArrayList(allTeachers);
            }
        }
    }

}
