/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package services;

import DB.objects.Student;
import fxObjects.MonthFX;
import fxObjects.MonthFXUtil;
import fxObjects.StudentFX;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * returns and ObservableList<StudentFX> list
 * @author dimitris
 */
public class GetAllFXMonths extends Service<ObservableList <MonthFX>>{
	
	private Student student;
	public GetAllFXMonths(StudentFX studentFX){
		super();
		student = studentFX.getStudent();
	}
	public GetAllFXMonths(){
		super();
	}
	public void setStudent(StudentFX studentfx){
		this.student = studentfx.getStudent();
	}
	
	@Override
	protected Task<ObservableList<MonthFX>> createTask() {
		GetAllFXMonthsTask theTask = new GetAllFXMonthsTask();
		return theTask;
	}
	
	private class GetAllFXMonthsTask extends Task <ObservableList<MonthFX>>{
		@Override
		protected ObservableList<MonthFX> call() throws Exception {
			ObservableList<MonthFX> returnValue = null;
			try{
				returnValue = MonthFXUtil.getMonthFXListForStudent(student);
			}
			catch(Exception e){
				System.err.println("Exception in service");
				e.printStackTrace(System.err);
			}
			return returnValue;
		}
	}
}
