/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package services;

import DB.objects.Student;
import fxObjects.MonthFX;
import fxObjects.MonthFXUtil;
import fxObjects.StudentFX;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * returns and ObservableList<StudentFX> list
 * @author dimitris
 */
public class GetAllFXMonthsSorted extends Service<SortedList <MonthFX>>{
	
	private Student student;
	public GetAllFXMonthsSorted(StudentFX studentFX){
		super();
		student = studentFX.getStudent();
	}
	public GetAllFXMonthsSorted(){
		super();
	}
	public void setStudent(StudentFX studentfx){
		this.student = studentfx.getStudent();
	}
	
	@Override
	protected Task<SortedList<MonthFX>> createTask() {
		GetAllFXMonthsTask theTask = new GetAllFXMonthsTask();
		return theTask;
	}
	
	private class GetAllFXMonthsTask extends Task <SortedList<MonthFX>>{
		@Override
		protected SortedList<MonthFX> call() throws Exception {
			SortedList<MonthFX> returnValue = null;
			try{
				returnValue = MonthFXUtil.getSortedMonthFXListForStudent(student);
			}
			catch(Exception e){
				System.err.println("Exception in service");
				e.printStackTrace(System.err);
			}
			return returnValue;
		}
	}
}
