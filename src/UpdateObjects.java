
import DB.DBTableUtils;
import DB.DButils;
import DB.FactoryMaker;
import DB.objects.Address;
import DB.objects.ContactInformation;
import DB.objects.Student;
import java.util.List;
import javax.persistence.EntityManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author d.michaelides
 */
public class UpdateObjects {
//    public static void main(String [] args){
    public static void wasToBeMain(){
        System.out.println("---------------------------------------------------------------------------");
       
        List <Student> students = DBTableUtils.getAllStudentsList();
        
		EntityManager em2 = FactoryMaker.getInstance().createEntityManager();
        em2.getTransaction().begin();
        
        for (Student student : students) {
            if(student.getContactInfo() == null){
                ContactInformation cInfo = new ContactInformation();
                Address a1 = new Address();
                Address a2 = new Address();
                cInfo.setAddress1(a1);
                cInfo.setAddress2(a2);
                student.setContactInfo(cInfo);
                    em2.persist(a1);
                    em2.persist(a2);
                    em2.persist(cInfo);
                    em2.merge(student);
            }else{
                if(student.getContactInfo().getAddress1() == null){
                    Address a1 = new Address();
                        em2.persist(a1);
                    student.getContactInfo().setAddress1(a1);
                        em2.merge(student.getContactInfo());
                }
                if(student.getContactInfo().getAddress2() == null){
                    Address a2 = new Address();
                        em2.persist(a2);
                    student.getContactInfo().setAddress2(a2);
                        em2.merge(student.getContactInfo());
                }
            }
        }
        
        
        System.out.println("---------------------------------------------------------------------------");
//        List <Aclass> classes = DBTableUtils.getAllClassesList();
        
        
        
        System.out.println("---------------------------------------------------------------------------");
        DButils.printAllTeachers();
        
        System.out.println("---------------------------------------------------------------------------");
        
        
        
        
        
        
    }
}
