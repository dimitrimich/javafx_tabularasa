package jfxtras.agenda;

import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.util.Callback;


public class AgendaBuilder  {
	public AgendaBuilder() {
		agenda = new Agenda();
		
		// setup appointment groups
		// initial dates
		Calendar lFirstDayOfWeekCalendar = getFirstDayOfWeekCalendar(agenda.getLocale(), agenda.getDisplayedCalendar());
		int lFirstDayOfWeekYear = lFirstDayOfWeekCalendar.get(Calendar.YEAR);
		int lFirstDayOfWeekMonth = lFirstDayOfWeekCalendar.get(Calendar.MONTH);
		int FirstDayOfWeek = lFirstDayOfWeekCalendar.get(Calendar.DATE);
		Calendar lToday = agenda.getDisplayedCalendar();
		int lTodayYear = lToday.get(Calendar.YEAR);
		int lTodayMonth = lToday.get(Calendar.MONTH);
		int lTodayDay = lToday.get(Calendar.DATE);
		
		
		
		for (String lId : lAppointmentGroupMap.keySet())
		{
			Agenda.AppointmentGroup lAppointmentGroup = lAppointmentGroupMap.get(lId);
			lAppointmentGroup.setDescription(lId);
			agenda.appointmentGroups().add(lAppointmentGroup);
		}
		
		// accept new appointments
		agenda.createAppointmentCallbackProperty().set(new Callback<Agenda.CalendarRange, Agenda.Appointment>()
		{
			@Override
			public Agenda.Appointment call(Agenda.CalendarRange calendarRange)
			{
				return null;
			}
		});
		
		
		
		
		agenda.editAppointmentCallbackProperty().set(new Callback<Agenda.Appointment, Void>()
		{
			@Override
			public Void call(Agenda.Appointment param) {
				return null;
			}
		});
		
		
	}
	final private Agenda agenda;
	public Agenda getAgenda(){
		return agenda;
	}
	final Map<String, Agenda.AppointmentGroup> lAppointmentGroupMap = new TreeMap<String, Agenda.AppointmentGroup>();
	
	
	public String getSampleName() {
		return this.getClass().getSimpleName();
	}
	
	public String getSampleDescription() {
		return "Basic Agenda usage";
	}
	
	public Node getPanel(Stage stage) {
		return agenda;
	}
	private customControls.CalendarPicker  myCalendarText;
	public customControls.CalendarPicker getMyCalendarTextField(){
		return myCalendarText;
	}
	
	public Node getMyControlPanel() {
		// the result
		GridPane lGridPane = new GridPane();
		lGridPane.setVgap(2.0);
		lGridPane.setHgap(2.0);
		
		// setup the grid so all the labels will not grow, but the rest will
		ColumnConstraints lColumnConstraintsAlwaysGrow = new ColumnConstraints();
		lColumnConstraintsAlwaysGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints lColumnConstraintsNeverGrow = new ColumnConstraints();
		lColumnConstraintsNeverGrow.setHgrow(Priority.NEVER);
		lGridPane.getColumnConstraints().addAll(lColumnConstraintsNeverGrow, lColumnConstraintsAlwaysGrow);
		int lRowIdx = 0;
		
		// week
		
		myCalendarText  = new customControls.CalendarPicker();
		lGridPane.add(myCalendarText, new GridPane.C().row(lRowIdx).col(0));
		myCalendarText.calendarProperty().bindBidirectional(agenda.getDisplayedCalendarProperty());
		
		
		lRowIdx++;
		
		// done
		return lGridPane;
	}
	
	
	
	/**
	 * get the calendar for the first day of the week
	 */
	static private Calendar getFirstDayOfWeekCalendar(Locale locale, Calendar c)
	{
		// result
		int lFirstDayOfWeek = Calendar.getInstance(locale).getFirstDayOfWeek();
		int lCurrentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		int lDelta = 0;
		if (lFirstDayOfWeek <= lCurrentDayOfWeek)
		{
			lDelta = -lCurrentDayOfWeek + lFirstDayOfWeek;
		}
		else
		{
			lDelta = -lCurrentDayOfWeek - (7-lFirstDayOfWeek);
		}
		c = ((Calendar)c.clone());
		c.add(Calendar.DATE, lDelta);
		return c;
	}
	
	public Node getMyCalendarNode() throws Exception {
		return getMyControlPanel();
	}
}
