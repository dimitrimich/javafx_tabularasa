/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fxObjects;

import DB.objects.Aclass;
import DB.objects.BillingMonth;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfWLessons;
import DB.objects.Student;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import utils.LanguageUtils;

/**
 *
 * @author dimitris
 */
public class MonthFXUtil {
	public static ObservableList<MonthFX> getMonthFXListForStudent(Student student){
		Map<Year, Map<Month, MonthFX>> map = getAllFXMonthsForStudent(student);
		List<MonthFX> list = new ArrayList<>();
		for(Entry<Year, Map<Month, MonthFX>> entry : map.entrySet()){
			Map<Month, MonthFX> values = entry.getValue();
			for(MonthFX month : values.values()){
				month.finilizeProperties();
				list.add(month);
			}
		}
//		System.out.println("------ Print the list:  ----------");
//		for(MonthFX inList : list){
//			System.out.println("   "+inList.getMonth()+" >"+inList.getRemainingAmount());
//		}
		return FXCollections.observableArrayList(list);
	}
	
	public static SortedList<MonthFX> getSortedMonthFXListForStudent(Student student){
		Map<Year, Map<Month, MonthFX>> map = getAllFXMonthsForStudent(student);
		List<MonthFX> list = new ArrayList<>();
		for(Entry<Year, Map<Month, MonthFX>> entry : map.entrySet()){
			Map<Month, MonthFX> values = entry.getValue();
			for(MonthFX month : values.values()){
				month.finilizeProperties();
				list.add(month);
			}
		}
//		System.out.println("------ Print the list:  ----------");
//		for(MonthFX inList : list){
//			System.out.println("   "+inList.getMonth()+" >"+inList.getRemainingAmount());
//		}
		return createSortedList(list);
	}
	
	
	private static SortedList<MonthFX> createSortedList(List<MonthFX> unsortedList) {
		SortedList<MonthFX> sortedList = new SortedList<>( FXCollections.observableArrayList(unsortedList),
				(MonthFX monthA, MonthFX monthB) -> {
					if( monthA.getYear().isBefore(monthB.getYear() )) {
						return -1;
					} else if( monthA.getYear().isAfter(monthB.getYear() )) {
						return 1;
					} else {
						return monthA.getMonth().compareTo(monthB.getMonth());
					}
				});
		return sortedList;
	}
	
	/**
	 * Get MonthsFX map for all the lessons of all the classes of given student
	 * @return map of all MonthsFX
	 */
	public static Map<Year, Map<Month, MonthFX>> getAllFXMonthsForStudent(Student student){
		
		List <Aclass> classes = student.getClasses();
		Map<Year, Map<Month, MonthFX>> allTheMonths = new HashMap<>();
		for(Aclass aClass : classes){
			// monthly paid class
			if(aClass.isPaidPerMonth()){
				List<BillingMonth> billings = aClass.getBillingMonths();
				System.err.println("   Monthly Class Billings : ");
				for(BillingMonth aBilling : billings){
					BigDecimal currentRemaining = aBilling.getRemainingAmountForStudent(student);
					// add the amount if there is something left
					if(BigDecimal.ZERO.compareTo(currentRemaining) != 0){
						Month currentMonth = aBilling.getBillingMonth();
						Year currentYear = aBilling.getBillingYear();
						// billing Month is in duration of class because duration might change
						// but we do not remove billing MOnths
						LocalDate compareToStart = LocalDate.of(currentYear.getValue(), currentMonth, currentMonth.length(currentYear.isLeap()));
						LocalDate compareToEnd = LocalDate.of(currentYear.getValue(), currentMonth, 1);
						System.err.println("     "+aClass.getStartDate()+" < " +compareToStart +" | "+aClass.getEndDate() +" > "+compareToEnd);
						if(aClass.getStartDate().isBefore(compareToStart) && aClass.getEndDate().isAfter(compareToEnd)){
							System.out.println(" adding Month "+ aBilling.getBillingMonth());
							addBilling(currentRemaining, currentMonth, currentYear, allTheMonths);
						}
//						if(aClass.getStartDate().getYear() <= currentYear.getValue()){
//							if(aClass.getStartDate().getMonth().compareTo(currentMonth) == -1 || aClass.getStartDate().getMonth().compareTo(currentMonth) == 0){
//								if(aClass.getEndDate().getYear() >= currentYear.getValue()){
//									if(aClass.getEndDate().getMonth().compareTo(currentMonth) == 1 || aClass.getEndDate().getMonth().compareTo(currentMonth) == 0){
//
//										System.out.println("  yeap. Added ");
//										addBilling(currentRemaining, currentMonth, currentYear, allTheMonths);
//									}
//								}
//							}
//						}
					}
				}
			}else{ // class is paid per lesson
				Map<LocalDate, ListOfELessons> extraLessonsMap = aClass.getSchedule().getAllELessons();
				Map<LocalDate, ListOfWLessons> weeklyLessonsMap = aClass.getSchedule().getAllWLessons();
				// Extra Lessons
				for(ListOfELessons todayList : extraLessonsMap.values()){
					Month lessonMonth = todayList.getTheDate().getMonth();
					Year lessonYear = Year.of(todayList.getTheDate().getYear());
					List<Lesson> lessons = todayList.getAllLessons();
					for(Lesson lesson : lessons){
						System.out.println("month "+ lessonMonth);
						BigDecimal remainingAmnt= lesson.getRemainingAmountForStudent(student);
						System.out.println("   append amount "+remainingAmnt);
						
						addBilling(lesson.getRemainingAmountForStudent(student), lessonMonth, lessonYear, allTheMonths);
					}
				}
				// Scheduled Lessons
				for(ListOfWLessons todayList : weeklyLessonsMap.values()){
					Month lessonMonth = todayList.getTheDate().getMonth();
					Year lessonYear = Year.of(todayList.getTheDate().getYear());
					List<Lesson> lessons = todayList.getAllLessons();
					for(Lesson lesson : lessons){
						System.out.println("month "+ lessonMonth);
						BigDecimal remainingAmnt= lesson.getRemainingAmountForStudent(student);
						System.out.println("   append amount "+remainingAmnt);
						addBilling(lesson.getRemainingAmountForStudent(student), lessonMonth, lessonYear, allTheMonths);
					}
				}
			}
		}
		return allTheMonths;
	}
	
	/**
	 * add to the map given the year the map and the amount
	 */
	public static void addBilling(BigDecimal currentRemaining, Month currentMonth, Year currentYear, Map<Year, Map<Month, MonthFX>> allTheMonths){
		Map<Month, MonthFX> currentYearMap = allTheMonths.get(currentYear);
		if(currentYearMap != null){// it exists
			MonthFX currentMonthFX = currentYearMap.get(currentMonth);
			if(currentMonthFX != null){ // it exists
				System.out.println(" Year "+currentYear+ " exists"+ ", month "+currentMonth+" exists also, current amount = "+currentMonthFX.getRemainingAmount());
				currentMonthFX.increaseRemainingAmount(currentRemaining);
			}else{ // create currentMonthFX
				System.out.println(" Year "+currentYear+ " exists"+ ", month "+currentMonth+" does NOT exist");
				currentMonthFX = new MonthFX();
				currentMonthFX.setMonthAndYear(currentMonth, currentYear);
				currentMonthFX.setRemainingAmount(currentRemaining);
				
				currentYearMap.put(currentMonth, currentMonthFX);
			}
		}else{
			System.out.println(" Year "+currentYear+ " does NOT exist");
			Map<Month, MonthFX> newYear = new HashMap<>();
			
			MonthFX newMonth = new MonthFX();
			newMonth.setMonthAndYear(currentMonth, currentYear);
			newMonth.setRemainingAmount(currentRemaining);
			
			newYear.put(currentMonth, newMonth);
			allTheMonths.put(currentYear, newYear);
		}
	}
	
	
	
	
	public static void initMonthFXTable(TableView<MonthFX> table){
		TableColumn<MonthFX, BigDecimal> dueAmountColumn = new TableColumn<>(LanguageUtils.getString("billing.entry.due"));
		TableColumn<MonthFX, Year> yearColumn = new TableColumn<>(LanguageUtils.getString("billing.entry.year"));
		TableColumn<MonthFX, Month> monthColumn = new TableColumn<>(LanguageUtils.getString("billing.entry.month"));
		dueAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getRemainingAmountToPayProperty());
		yearColumn.setCellValueFactory(cellData -> cellData.getValue().getYearProperty());
		monthColumn.setCellValueFactory(cellData -> cellData.getValue().getMonthProperty());
		dueAmountColumn.setComparator((BigDecimal value1, BigDecimal value2) -> value1.compareTo(value2));
		yearColumn.setComparator((Year value1, Year value2) -> {
			if(value1.isBefore(value2)) return -1;
			if(value1.isAfter(value2)) return 1;
			return 0;
		});
		monthColumn.setComparator((Month value1, Month value2) -> value1.compareTo(value2));
		
		table.getColumns().setAll(monthColumn, yearColumn, dueAmountColumn);
	}
}
