/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fxObjects;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.time.format.TextStyle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import utils.LanguageUtils;

/**
 *
 * @author dimitris
 */
public class MonthFX {
	public MonthFX(){
		remainingAmountToPayProperty = new SimpleObjectProperty<>();
		yearProperty = new SimpleObjectProperty<>();
		monthProperty = new SimpleObjectProperty<>();
	}
	
	private StringProperty name;
	private BigDecimal remainingAmount;
	private Year year;
	private Month month;
	private ObjectProperty<BigDecimal> remainingAmountToPayProperty;
	private ObjectProperty<Year> yearProperty;
	private ObjectProperty<Month> monthProperty;
	
	public StringProperty getNameProperty() { return name; }
	public void setNameProperty(StringProperty name) { this.name = name; }
	public String getName() { return name.get(); }
	public void setName(String name) { this.name.set(name); }
	public BigDecimal getRemainingAmount() { return remainingAmount; }
	public void setRemainingAmount(BigDecimal remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	public Year getYear() { return year; }
	public Month getMonth() { return month; }
	public void setMonthAndYear(Month month, Year year) {
		this.month = month; this.year = year;
		initNameProperty();
	}
	public void increaseRemainingAmount(BigDecimal additionalAmount){
		if(additionalAmount == null) {additionalAmount = BigDecimal.ZERO;}
		this.remainingAmount = this.remainingAmount.add(additionalAmount);
	}
	public void initNameProperty(){
		name = new SimpleStringProperty(
				new StringBuilder()
						.append(month.getDisplayName(TextStyle.SHORT, LanguageUtils.getInstance().getCurrentLanguage().getLocale()))
						.append(" / ")
						.append(year)
						.toString());
	}
	public void finilizeProperties(){
		name.set(new StringBuilder(name.get()).append(" | ").append(remainingAmount).toString());
		remainingAmountToPayProperty.set(remainingAmount);
		yearProperty.set(year);
		monthProperty.set(month);
	}
	
	public ObjectProperty<BigDecimal> getRemainingAmountToPayProperty() {
		return remainingAmountToPayProperty;
	}
	public void setRemainingAmountToPayProperty(ObjectProperty<BigDecimal> remainingAmountToPay) {
		this.remainingAmountToPayProperty = remainingAmountToPay;
	}
	public void decreaseRemainingAmountToPayProperty(BigDecimal difference){
		this.remainingAmount = this.remainingAmount.subtract(difference);
		this.remainingAmountToPayProperty.set(remainingAmount);
	}
	
	public ObjectProperty<Year> getYearProperty() {
		return yearProperty;
	}
	public void setYearProperty(ObjectProperty<Year> yearProperty) {
		this.yearProperty = yearProperty;
	}
	public ObjectProperty<Month> getMonthProperty() {
		return monthProperty;
	}
	public void setMonthProperty(ObjectProperty<Month> monthProperty) {
		this.monthProperty = monthProperty;
	}
}
