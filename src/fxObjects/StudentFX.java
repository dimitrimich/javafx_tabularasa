/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fxObjects;

import DB.objects.Student;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * FX class to show student's name in a tableView
 * @author dimitris
 */
public class StudentFX {
	
	private StringProperty name;
	private Student student;
	
	public StudentFX(Student stu){
		this.student = stu;
		if(stu.getContactInfo().getPhone1() != null){
			this.name = new SimpleStringProperty(stu.getFullName()+" | "+stu.getContactInfo().getPhone1());
		}else{
			this.name = new SimpleStringProperty(stu.getFullName()+" | XX - XXXXXX");
		}
	}
	
	public StringProperty getNameProperty() { return name; }
	public void setNameProperty(StringProperty name) { this.name = name; }
	public String getName() { return name.get(); }
	public void setName(String name) { this.name.set(name); }
	public Student getStudent() { return student; }
	public void setStudent(Student student) { this.student = student; }
	
	
}
