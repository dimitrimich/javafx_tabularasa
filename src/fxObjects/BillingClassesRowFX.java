/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fxObjects;

import DB.objects.Aclass;
import DB.objects.PaymentDetails;
import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.time.format.TextStyle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import utils.LanguageUtils;
import utils.dialogs.BillingClassesRow;

/**
 *
 * @author dimitris
 */
public class BillingClassesRowFX {
	
	private StringProperty name;
	private StringProperty monthProperty;
	private IntegerProperty numberOfLessons;
	private LongProperty durationOfLessons;
	private IntegerProperty canceledLessons;
	private IntegerProperty extraLessons;
	private ObjectProperty<BigDecimal> remainingAmountToPay;
	private ObjectProperty<BigDecimal> fullAmountOfLessons;
	private ObjectProperty<BigDecimal> discountOfLessons;
	private ObjectProperty<BigDecimal> paidAmountOfLessons;
	private BooleanProperty selected;
	private Aclass theClass;
	private Month month;
	private Year year;
	
	public BillingClassesRowFX(BillingClassesRow row){
		name = new SimpleStringProperty(row.getName());
		numberOfLessons = new SimpleIntegerProperty(row.getNumberOfLessons());
		durationOfLessons = new SimpleLongProperty(row.getDurationOfLessons());
		canceledLessons = new SimpleIntegerProperty(row.getCanceledLessons());
		extraLessons = new SimpleIntegerProperty(row.getExtraLessons());
		fullAmountOfLessons = new SimpleObjectProperty<>(row.getFullAmountOfLessons());
		discountOfLessons = new SimpleObjectProperty<>(row.getDiscountOfLessons());
		paidAmountOfLessons = new SimpleObjectProperty<>(row.getPaidAmountOfLessons());
		BigDecimal remainingAmount = row.getFullAmountOfLessons().subtract(row.getPaidAmountOfLessons().add(row.getDiscountOfLessons()));
		remainingAmountToPay = new SimpleObjectProperty<>(remainingAmount);
		monthProperty = new SimpleStringProperty(row.getMonth().getDisplayName(TextStyle.FULL, LanguageUtils.getCurrentLocale()));
		month = row.getMonth();
		year = row.getYear();
		selected = new SimpleBooleanProperty(false);
		theClass = row.getTheClass();
	}
	
	public String getName() {
		return name.get();
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	public int getNumberOfLessons() {
		return numberOfLessons.get();
	}
	
	public void setNumberOfLessons(int numberOfLessons) {
		this.numberOfLessons.set(numberOfLessons);
	}
	
	public long getDurationOfLessons() {
		return durationOfLessons.get();
	}
	
	public void setDurationOfLessons(long durationOfLessons) {
		this.durationOfLessons.set(durationOfLessons);
	}
	
	public int getCanceledLessons() {
		return canceledLessons.get();
	}
	
	public void setCanceledLessons(int canceledLessons) {
		this.canceledLessons.set(canceledLessons);
	}
	
	public int getExtraLessons() {
		return extraLessons.get();
	}
	
	public void setExtraLessons(int extraLessons) {
		this.extraLessons.set(extraLessons);
	}
	
	public BigDecimal getFullAmountOfLessons() {
		return fullAmountOfLessons.get();
	}
	
	public void setFullAmountOfLessons(BigDecimal fullAmountOfLessons) {
		this.fullAmountOfLessons.set(fullAmountOfLessons);
	}
	
	public BigDecimal getDiscountOfLessons() {
		return discountOfLessons.get();
	}
	
	public void setDiscountOfLessons(BigDecimal discountOfLessons) {
		this.discountOfLessons.set(discountOfLessons);
	}
	
	public BigDecimal getPaidAmountOfLessons() {
		return paidAmountOfLessons.get();
	}
	
	public void setPaidAmountOfLessons(BigDecimal paidAmountOfLessons) {
		this.paidAmountOfLessons.set(paidAmountOfLessons);
	}
	
	public StringProperty getNameProperty() {
		return name;
	}
	
	public void setNameProperty(StringProperty name) {
		this.name = name;
	}
	
	public IntegerProperty getNumberOfLessonsProperty() {
		return numberOfLessons;
	}
	
	public void setNumberOfLessonsProperty(IntegerProperty numberOfLessons) {
		this.numberOfLessons = numberOfLessons;
	}
	
	public LongProperty getDurationOfLessonsProperty() {
		return durationOfLessons;
	}
	
	public void setDurationOfLessonsProperty(LongProperty durationOfLessons) {
		this.durationOfLessons = durationOfLessons;
	}
	
	public IntegerProperty getCanceledLessonsProperty() {
		return canceledLessons;
	}
	
	public void setCanceledLessonsProperty(IntegerProperty canceledLessons) {
		this.canceledLessons = canceledLessons;
	}
	
	public IntegerProperty getExtraLessonsProperty() {
		return extraLessons;
	}
	
	public void setExtraLessonsProperty(IntegerProperty extraLessons) {
		this.extraLessons = extraLessons;
	}
	
	public ObjectProperty<BigDecimal> getFullAmountOfLessonsProperty() {
		return fullAmountOfLessons;
	}
	
	public void setFullAmountOfLessonsProperty(ObjectProperty<BigDecimal> fullAmountOfLessons) {
		this.fullAmountOfLessons = fullAmountOfLessons;
	}
	
	public ObjectProperty<BigDecimal> getDiscountOfLessonsProperty() {
		return discountOfLessons;
	}
	
	public void setDiscountOfLessonsProperty(ObjectProperty<BigDecimal> discountOfLessons) {
		this.discountOfLessons = discountOfLessons;
	}
	
	public ObjectProperty<BigDecimal> getPaidAmountOfLessonsProperty() {
		return paidAmountOfLessons;
	}
	
	public void setPaidAmountOfLessonsProperty(ObjectProperty<BigDecimal> paidAmountOfLessons) {
		this.paidAmountOfLessons = paidAmountOfLessons;
	}
	
	public BooleanProperty getSelectedProperty() {
		return selected;
	}
	public void setSelectedProperty(BooleanProperty selected) {
		this.selected = selected;
	}
	public boolean getSelected() {
		return selected.get();
	}
	public void setSelected(boolean selected) {
		this.selected.set(selected);
	}

	public StringProperty getMonthProperty() { return monthProperty; }
	public void setMonthProperty(StringProperty monthProperty) { this.monthProperty = monthProperty; }
	
	public ObjectProperty<BigDecimal> getRemainingAmountToPayProperty() { return remainingAmountToPay; }
	public void setRemainingAmountToPayProperty(ObjectProperty<BigDecimal> remainingAmountToPay) { this.remainingAmountToPay = remainingAmountToPay; }
	public BigDecimal getRemainingAmountToPay() { return remainingAmountToPay.get(); }
	public void setRemainingAmountToPay(BigDecimal remainingAmountToPay) { this.remainingAmountToPay.set(remainingAmountToPay);}
	
	public Aclass getTheClass() { return theClass; }
	public void setTheClass(Aclass theClass) { this.theClass = theClass; }

	public Month getMonth() { return month; }
	public void setMonth(Month month) { this.month = month; }
	public Year getYear() { return year; }
	public void setYear(Year year) { this.year = year; }
	
	public void increaseTotalDiscount(BigDecimal amount){
		this.discountOfLessons.set(this.discountOfLessons.get().add(amount));
	}
	public void increaseTotalPaidAmount(BigDecimal amount){
		this.paidAmountOfLessons.set(this.paidAmountOfLessons.get().add(amount));
	}
	public void increaseTotalAmount(BigDecimal amount){
		this.fullAmountOfLessons.set(this.fullAmountOfLessons.get().add(amount));
	}
	public void decreaseTotalDiscount(BigDecimal amount){
		this.discountOfLessons.set(this.discountOfLessons.get().subtract(amount));
	}
	public void decreaseTotalPaidAmount(BigDecimal amount){
		this.paidAmountOfLessons.set(this.paidAmountOfLessons.get().subtract(amount));
	}
	public void decreaseTotalAmount(BigDecimal amount){
		this.fullAmountOfLessons.set(this.fullAmountOfLessons.get().subtract(amount));
	}
	public void refreshDueAmount(){
		BigDecimal remainingAmount = getFullAmountOfLessons().subtract(getPaidAmountOfLessons().add(getDiscountOfLessons()));
		remainingAmountToPay.set(remainingAmount);
	}
	public void addPayment(PaymentDetails newPayment){
		this.increaseTotalDiscount(newPayment.getDiscount());
		this.increaseTotalPaidAmount(newPayment.getPaidAmount());
		this.refreshDueAmount();
		
		if(remainingAmountToPay.get().compareTo(BigDecimal.ZERO) == 0 ){
			this.selected.set(false);
		}
	}
}
