/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import images.Images;
import images.icons.Icons;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.scene.input.DataFormat;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;

/**
 *
 * @author d.michaelides
 */
public class TimeTable {
    
    
    public static ObservableList<TableColumn> initializeTable(TableView theTable){
//        ArrayList<TableColumn<ClassScheduleTeacher, ClassScheduleTeacher>> columnList = new ArrayList<>();
//        columnList.ensureCapacity(22);
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> nameCol = new TableColumn<>("Name");
//        nameCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        nameCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher, ClassScheduleTeacher>, TableCell<ClassScheduleTeacher,ClassScheduleTeacher>>() {
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> t) {
//                return new TableCell<ClassScheduleTeacher, ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher t, boolean empty){
//                        if(t != null){
//                            setText(t.getTheClass().getName());
//                        }
//                    }
//                };
//            }
//        });
//        nameCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getTheClass().getName(), t2.getTheClass().getName());
//            }
//        });
//        columnList.add(nameCol);
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> descrCol = new TableColumn<>("Description");
//        descrCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        descrCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher,ClassScheduleTeacher>, TableCell<ClassScheduleTeacher, ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher, ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getTheClass().getDescription());
//                        }
//                    }
//                };
//            }
//        });
//        descrCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getTheClass().getDescription(), t2.getTheClass().getDescription());
//            }
//        });
//        columnList.add(descrCol);
//        
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> teacherCol = new TableColumn<>("Teacher");
//        teacherCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        teacherCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher,ClassScheduleTeacher>, TableCell<ClassScheduleTeacher,ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher,ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getTheTeacher().getFirstname()+" "+p.getTheTeacher().getLastname());
//                        }
//                    }
//                };
//            }
//        });
//        teacherCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getTheTeacher().getLastname(), t2.getTheTeacher().getLastname());
//            }
//        });
//        columnList.add(teacherCol);
//        
//        for(int i = 0; i < 8; i++){
//            final int day = i;
//            TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> dayCol = new TableColumn<>("On "+utils.Utils.getDayOfWeek(i));
//            dayCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//            dayCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher, ClassScheduleTeacher>, TableCell<ClassScheduleTeacher, ClassScheduleTeacher>>() {
//
//                @Override
//                public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                    return new TableCell<ClassScheduleTeacher, ClassScheduleTeacher>(){
//                        @Override
//                        public void updateItem(ClassScheduleTeacher p, boolean empty){
//                            if(p != null){
//                                boolean isThere = false;
//                                for(int j = 0; j < 12; j++){
//                                
//                                }
//                                
//                                setText(p.getAddress2());
//                            }
//                        }
//                    };
//                }
//            });
//            dayCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//                @Override
//                public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                    return Collator.getInstance().compare(t1.getAddress2(), t2.getAddress2());
//                }
//            });
//            
//            columnList.add(dayCol);
//
//        }
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> monCol = new TableColumn<>("On Monday");
//        monCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        monCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher, ClassScheduleTeacher>, TableCell<ClassScheduleTeacher, ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher, ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getAddress2());
//                        }
//                    }
//                };
//            }
//        });
//        monCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getAddress2(), t2.getAddress2());
//            }
//        });
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> emailCol = new TableColumn<>("Email");
//        emailCol.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        emailCol.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher,ClassScheduleTeacher>, TableCell<ClassScheduleTeacher,ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher,ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getEmail());
//                        }
//                    }
//                };
//            }
//        });
//        emailCol.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                if(t1.getEmail() != null && t1.getEmail() != null){
//                    return Collator.getInstance().compare(t1.getEmail(), t2.getEmail());
//                }else{
//                    return 1;
//                }
//            }
//        });
//        
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> phone1Col = new TableColumn<>("Phone 1");
//        phone1Col.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        phone1Col.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher,ClassScheduleTeacher>, TableCell<ClassScheduleTeacher,ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher,ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getPhone1());
//                        }
//                    }
//                };
//            }
//        });
//        phone1Col.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getPhone1(), t2.getPhone1());
//            }
//        });
//        TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> phone2Col = new TableColumn<>("Phone 2");
//        phone2Col.setCellValueFactory(new PropertyValueFactory<ClassScheduleTeacher, ClassScheduleTeacher>("ClassScheduleTeacher"));
//        phone2Col.setCellFactory(new Callback<TableColumn<ClassScheduleTeacher,ClassScheduleTeacher>, TableCell<ClassScheduleTeacher,ClassScheduleTeacher>>() {
//
//            @Override
//            public TableCell<ClassScheduleTeacher, ClassScheduleTeacher> call(TableColumn<ClassScheduleTeacher, ClassScheduleTeacher> p) {
//                return new TableCell<ClassScheduleTeacher,ClassScheduleTeacher>(){
//                    @Override
//                    public void updateItem(ClassScheduleTeacher p, boolean empty){
//                        if(p != null){
//                            setText(p.getPhone2());
//                        }
//                    }
//                };
//            }
//        });
//        phone2Col.setComparator(new Comparator<ClassScheduleTeacher>() {
//            @Override
//            public int compare(ClassScheduleTeacher t1, ClassScheduleTeacher t2) {
//                return Collator.getInstance().compare(t1.getPhone2(), t2.getPhone2());
//            }
//        });
//        
//        
//        theTable.getColumns().setAll( nameCol, descrCol, teacherCol, monCol, emailCol, phone1Col, phone2Col);
//        
//        ArrayList<TableColumn> columns = new ArrayList<>(8);
//        
////        columns.add(imageCol);
//        columns.add(nameCol);
//        columns.add(descrCol);
//        columns.add(teacherCol);
//        columns.add(monCol);
//        columns.add(emailCol);
//        columns.add(phone1Col);
//        columns.add(phone2Col);
//        return FXCollections.observableArrayList(columns);
        return null;
    }
}
