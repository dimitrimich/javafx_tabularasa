/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import DB.objects.Student;
import java.text.Collator;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author d.michaelides
 */
public class StudentsTable {
    
    
    public static ObservableList<TableColumn> initializeStudentsTable(TableView theTable){
        
        
        TableColumn<Student, Student> imageCol = new TableColumn<>("Image");
        imageCol.setCellValueFactory(new PropertyValueFactory<>("Student"));
        imageCol.setCellFactory((TableColumn<Student, Student> t) -> {
            final ImageView imageV = new ImageView();
            {
//                    ByteArrayInputStream stream = new ByteArrayInputStream(t.g)
//                    ImageIO.read("").
//                    Image img = new Image
                
            }
            return new TableCell<Student, Student>(){
                @Override
                public void updateItem(Student t, boolean empty){
                    if(t != null){
                        try{
                            if( t.getPicture() != null){
                                Image img = new Image(t.getPicture().getBinaryStream(), 40, 40, true, false);
                                imageV.setImage(img);
                                setGraphic(imageV);
                            }
                            
                        }catch(Exception e){
                            e.printStackTrace(System.err);
                        }
                    }else{
                        setGraphic(null);
                        setText(null);
                    }
                }
            };
        });
        imageCol.setSortable(false);
        
        TableColumn<Student, Student> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("Student"));
        firstNameCol.setCellFactory((TableColumn<Student, Student> t) -> new TableCell<Student, Student>(){
            @Override
            public void updateItem(Student t, boolean empty){
                if(t != null){
                    setText(t.getBasicInfo().getFname());
                }else{
                        setGraphic(null);
                        setText(null);
                    }
            }
        });
        firstNameCol.setComparator((Student t1, Student t2) -> Collator.getInstance().compare(t1.getBasicInfo().getFname(), t2.getBasicInfo().getFname()));
        
        TableColumn<Student, Student> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("Student"));
        lastNameCol.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student, Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    setText(p.getBasicInfo().getLname());
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        lastNameCol.setComparator((Student t1, Student t2) -> Collator.getInstance().compare(t1.getBasicInfo().getLname(), t2.getBasicInfo().getLname()));
        
        
        TableColumn<Student, Student> address1Col = new TableColumn<>("Address 1");
        address1Col.setCellValueFactory(new PropertyValueFactory<>("Student"));
        address1Col.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student,Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    if(p.getContactInfo() != null){
                        if(p.getContactInfo().getAddress1() != null){
                            setText(p.getContactInfo().getAddress1().toString());
                        }
                    }
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        address1Col.setComparator((Student t1, Student t2) -> 
                {
                if(t1.getContactInfo() != null && t2.getContactInfo() != null ){
                    if(t1.getContactInfo().getAddress1() != null && t1.getContactInfo().getAddress1() != null ){

                    return Collator.getInstance().compare(t1.getContactInfo().getAddress1().getStreetInfo1(), 
                            t2.getContactInfo().getAddress1().getStreetInfo1());
                    }
                }
                return 1;
        });
        
        TableColumn<Student, Student> address2Col = new TableColumn<>("Address 2");
        address2Col.setCellValueFactory(new PropertyValueFactory<>("Student"));
        address2Col.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student, Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    if(p.getContactInfo() != null){
                        if(p.getContactInfo().getAddress2() != null){
                            setText(p.getContactInfo().getAddress2().toString());
                        }
                    }
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        address2Col.setComparator((Student t1, Student t2) -> {
                if(t1.getContactInfo() != null && t2.getContactInfo() != null ){
                    if(t1.getContactInfo().getAddress2() != null && t1.getContactInfo().getAddress2() != null ){
                        return Collator.getInstance().compare(t1.getContactInfo().getAddress2().getStreetInfo1(), 
                            t2.getContactInfo().getAddress2().getStreetInfo1());
                    }
                }
                return 1;
                }
        );
        
        TableColumn<Student, Student> emailCol = new TableColumn<>("Email");
        emailCol.setCellValueFactory(new PropertyValueFactory<>("Student"));
        emailCol.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student,Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    if(p.getContactInfo() != null){
                        if(p.getContactInfo().getEmail1() != null){
                            setText(p.getContactInfo().getEmail1());
                        }
                    }
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        emailCol.setComparator((Student t1, Student t2) -> {
            if(t1.getContactInfo().getEmail1() != null && t1.getContactInfo().getEmail1() != null){
                return Collator.getInstance().compare(t1.getContactInfo().getEmail1(), t2.getContactInfo().getEmail1());
            }else{
                return 1;
            }
        });
        
        TableColumn<Student, Student> phone1Col = new TableColumn<>("Phone 1");
        phone1Col.setCellValueFactory(new PropertyValueFactory<Student, Student>("Student"));
        phone1Col.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student,Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    if(p.getContactInfo() != null){
                        if(p.getContactInfo().getPhone1() != null){
                            setText(p.getContactInfo().getPhone1());
                        }
                    }
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        phone1Col.setComparator((Student t1, Student t2) -> Collator.getInstance().compare(t1.getContactInfo().getPhone1(), t2.getContactInfo().getPhone1()));
        TableColumn<Student, Student> phone2Col = new TableColumn<>("Phone 2");
        phone2Col.setCellValueFactory(new PropertyValueFactory<Student, Student>("Student"));
        phone2Col.setCellFactory((TableColumn<Student, Student> p) -> new TableCell<Student,Student>(){
            @Override
            public void updateItem(Student p, boolean empty){
                if(p != null){
                    if(p.getContactInfo() != null){
                        if(p.getContactInfo().getPhone1() != null){
                            setText(p.getContactInfo().getPhone1());
                        }
                    }
                }else{
                    setGraphic(null);
                    setText(null);
                }
            }
        });
        phone2Col.setComparator((Student t1, Student t2) -> {
            if(t1.getContactInfo().getPhone2() != null && t2.getContactInfo().getPhone2() != null){
                return Collator.getInstance().compare(t1.getContactInfo().getPhone2(), t2.getContactInfo().getPhone2());
            }
            return 1;
        }
        );
        
        
        theTable.getColumns().setAll(imageCol, firstNameCol, lastNameCol, address1Col, address2Col, emailCol, phone1Col, phone2Col);
        
        ArrayList<TableColumn> columns = new ArrayList<>(8);
        
        columns.add(imageCol);
        columns.add(firstNameCol);
        columns.add(lastNameCol);
        columns.add(address1Col);
        columns.add(address2Col);
        columns.add(emailCol);
        columns.add(phone1Col);
        columns.add(phone2Col);
        return FXCollections.observableArrayList(columns);
    }
}
