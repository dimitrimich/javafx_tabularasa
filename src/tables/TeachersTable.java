/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import DB.objects.Teacher;
import java.text.Collator;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.sql.rowset.serial.SerialException;

/**
 *
 * @author d.michaelides
 */
public class TeachersTable {
    
    
    public static ObservableList<TableColumn> initializeTeachersTable(TableView theTable){
        
        
        
        TableColumn<Teacher, Teacher> imageCol = new TableColumn<>("Image");
        imageCol.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        imageCol.setCellFactory((TableColumn<Teacher, Teacher> t) -> {
            final ImageView imageV = new ImageView();
            return new TableCell<Teacher, Teacher>(){
                @Override
                public void updateItem(Teacher t, boolean empty){
                    if(t != null){
                        try{
                            if( t.getPicture() != null){
                                Image img = new Image(t.getPicture().getBinaryStream(), 40, 40, true, false);
                                imageV.setImage(img);
                                setGraphic(imageV);
                            }
                            
                        }catch(SerialException e){
                            e.printStackTrace(System.err);
                        }
                    }else{
                        setGraphic(null);
                        setText(null);
                    }
                }
            };
        });
        imageCol.setSortable(false);
        
        TableColumn<Teacher, Teacher> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        firstNameCol.setCellFactory((TableColumn<Teacher, Teacher> t) -> new TableCell<Teacher, Teacher>(){
            @Override
            public void updateItem(Teacher t, boolean empty){
                if(t != null){
                    setText(t.getBasicInfo().getFname());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        firstNameCol.setComparator((Teacher t1, Teacher t2) -> Collator.getInstance().compare(t1.getBasicInfo().getFname(), t2.getBasicInfo().getFname()));
        
        TableColumn<Teacher, Teacher> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        lastNameCol.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher, Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getBasicInfo().getLname());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        lastNameCol.setComparator((Teacher t1, Teacher t2) -> Collator.getInstance().compare(t1.getBasicInfo().getLname(), t2.getBasicInfo().getLname()));
        
        
        TableColumn<Teacher, Teacher> address1Col = new TableColumn<>("Address 1");
        address1Col.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        address1Col.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher,Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getContactInfo().getAddress1().toString());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        address1Col.setComparator((Teacher t1, Teacher t2) -> 
                {
                if(t1.getContactInfo() != null && t2.getContactInfo() != null ){
                    if(t1.getContactInfo().getAddress1() != null && t1.getContactInfo().getAddress1() != null ){

                    return Collator.getInstance().compare(t1.getContactInfo().getAddress1().getStreetInfo1(), 
                            t2.getContactInfo().getAddress1().getStreetInfo1());
                    }
                }
                return 1;
        });
        
        TableColumn<Teacher, Teacher> address2Col = new TableColumn<>("Address 2");
        address2Col.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        address2Col.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher, Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getContactInfo().getAddress2().toString());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        
        address2Col.setComparator((Teacher t1, Teacher t2) -> 
                {
                if(t1.getContactInfo() != null && t2.getContactInfo() != null ){
                    if(t1.getContactInfo().getAddress2() != null && t1.getContactInfo().getAddress2() != null ){

                    return Collator.getInstance().compare(t1.getContactInfo().getAddress2().getStreetInfo1(), 
                            t2.getContactInfo().getAddress2().getStreetInfo1());
                    }
                }
                return 1;
        });
        TableColumn<Teacher, Teacher> emailCol = new TableColumn<>("Email");
        emailCol.setCellValueFactory(new PropertyValueFactory<Teacher, Teacher>("Teacher"));
        emailCol.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher,Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getContactInfo().getEmail1());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        emailCol.setComparator((Teacher t1, Teacher t2) -> {
            if(t1.getContactInfo().getEmail1() != null && t1.getContactInfo().getEmail1() != null){
                return Collator.getInstance().compare(t1.getContactInfo().getEmail1(), t2.getContactInfo().getEmail1());
            }else{
                return 1;
            }
        });
        
        TableColumn<Teacher, Teacher> phone1Col = new TableColumn<>("Phone 1");
        phone1Col.setCellValueFactory(new PropertyValueFactory<Teacher, Teacher>("Teacher"));
        phone1Col.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher,Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getContactInfo().getPhone1());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        phone1Col.setComparator((Teacher t1, Teacher t2) -> {
            if(t1.getContactInfo().getPhone1() != null && t2.getContactInfo().getPhone1() != null ){
                return Collator.getInstance().compare(t1.getContactInfo().getPhone1(), t2.getContactInfo().getPhone1());
                }
            return 1;
            }
        );
        
        TableColumn<Teacher, Teacher> phone2Col = new TableColumn<>("Phone 2");
        phone2Col.setCellValueFactory(new PropertyValueFactory<>("Teacher"));
        phone2Col.setCellFactory((TableColumn<Teacher, Teacher> p) -> new TableCell<Teacher,Teacher>(){
            @Override
            public void updateItem(Teacher p, boolean empty){
                if(p != null){
                    setText(p.getContactInfo().getPhone2());
                }else{
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        phone2Col.setComparator((Teacher t1, Teacher t2) -> {
            if(t1.getContactInfo().getPhone2() != null && t2.getContactInfo().getPhone2() != null ){
                return Collator.getInstance().compare(t1.getContactInfo().getPhone2(), t2.getContactInfo().getPhone2());
                }
            return 1;
            }
        );
        
        
        theTable.getColumns().setAll( imageCol, firstNameCol, lastNameCol, address1Col, address2Col, emailCol, phone1Col, phone2Col);
        
        ArrayList<TableColumn> columns = new ArrayList<>(8);
        
        columns.add(imageCol);
        columns.add(firstNameCol);
        columns.add(lastNameCol);
        columns.add(address1Col);
        columns.add(address2Col);
        columns.add(emailCol);
        columns.add(phone1Col);
        columns.add(phone2Col);
        return FXCollections.observableArrayList(columns);
    }
}
