/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tables;

import customObjects.StudentBillingYear;
import java.text.Collator;
import java.time.Month;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author dimitris
 */
public class BillingStudentsTable {
    
    public static ObservableList<TableColumn> initializeTable(TableView<StudentBillingYear> theTable){
    
        TableColumn<StudentBillingYear, StudentBillingYear> fullNameCol = new TableColumn<>("Full Name");
        fullNameCol.setCellValueFactory(new PropertyValueFactory<>("StudentBillingYear"));
        fullNameCol.setCellFactory((TableColumn<StudentBillingYear, StudentBillingYear> t) -> new TableCell<StudentBillingYear, StudentBillingYear>(){
            @Override
            public void updateItem(StudentBillingYear t, boolean empty){
                if(getTableRow() != null){
                    if(getTableRow().getItem() != null){
                        System.out.println("t != null");
                        setText(((StudentBillingYear)getTableRow().getItem()).getBillingStudent().getFullName());
                    }else{
                        System.out.println("t ==== null");
                        setGraphic(null);
                        setText(null);
                    }
                }
            }
        });
        fullNameCol.setComparator((StudentBillingYear t1, StudentBillingYear t2) -> Collator.getInstance().compare(t1.getBillingStudent().getFullName(), t2.getBillingStudent().getFullName()));
    
        
        
        TableColumn<StudentBillingYear, StudentBillingYear> augustCol = new TableColumn<>("August");
        augustCol.setCellValueFactory(new PropertyValueFactory<>("StudentBillingYear"));
        augustCol.setCellFactory((TableColumn<StudentBillingYear, StudentBillingYear> t) -> new TableCell<StudentBillingYear, StudentBillingYear>(){
            @Override
            public void updateItem(StudentBillingYear t, boolean empty){
                if(getTableRow() != null){
                    if(getTableRow().getItem() != null){
                        StudentBillingYear.IsPaidAndAmount theDetails = ((StudentBillingYear)getTableRow().getItem()).getYearsBillingInfo().get(Month.OCTOBER);
                        if(theDetails.isPaidInFull()){
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-good");
                        }else{
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-bad");
                        }
                        setText(theDetails.getPaidAmount() + " / "+theDetails.getDueAmount());
                    }else{
                        System.out.println("t ==== null");
                        setGraphic(null);
                        setText(null);
                    }
                }
            }
        });
        
        
        
        theTable.getColumns().setAll(fullNameCol, augustCol);
        
        ArrayList<TableColumn> columns = new ArrayList<>(1);
        
        columns.add(fullNameCol);
        columns.add(augustCol);
        return FXCollections.observableArrayList(columns);
    }
    
    
    
}
