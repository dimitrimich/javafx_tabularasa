/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import customObjects.Person;
import images.Images;
import images.icons.Icons;
import java.text.Collator;
import java.util.Comparator;
import java.util.Iterator;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 *
 * @author d.michaelides
 */
public class TableUtils_buttons {
    
    
    public static void initializePersonsTable(TableView theTable){
        
        TableColumn<Person, Person> indexCol = new TableColumn<>("#");
        indexCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        indexCol.setCellFactory((TableColumn<Person, Person> p) -> new TableCell<Person,Person>(){
            @Override
            public void updateItem(Person p, boolean empty){
                if(p != null){
                    setText(Integer.toString(p.getIndex()));
                }
            }
        });
        indexCol.setComparator((Person p1, Person p2) -> (p1.getIndex() > p2.getIndex()) ? +1 : (p1.getIndex() < p2.getIndex()) ? -1 : 0);
        
        TableColumn<Person, Person> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        firstNameCol.setCellFactory((TableColumn<Person, Person> p) -> new TableCell<Person,Person>(){
            @Override
            public void updateItem(Person p, boolean empty){
                if(p != null){
                    setText(p.getFn());
                }
            }
        });
        firstNameCol.setComparator((Person p1, Person p2) -> Collator.getInstance().compare(p1.getFn(), p2.getFn()));
        
        TableColumn<Person, Person> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        lastNameCol.setCellFactory((TableColumn<Person,Person> p) -> new TableCell<Person,Person>(){
            @Override
            public void updateItem(Person p, boolean empty){
                if(p != null){
                    setText(p.getlName());
                }
            }
        });
        lastNameCol.setComparator((Person p1, Person p2) -> Collator.getInstance().compare(p1.getlName(), p2.getlName()));
        
        
        TableColumn<Person, Person> allCol = new TableColumn<>("Currency");
        allCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        
        allCol.setCellFactory((TableColumn<Person,Person> p) -> {
            final VBox v = new VBox();
            final ImageView image = new ImageView();
            {
                v.setAlignment(Pos.CENTER);
                v.getChildren().add(image);
            }
            
            return new TableCell<Person, Person>(){
                @Override
                public void updateItem(Person p, boolean empty){
                    if(p != null){
                        Image img;
                        if(p.getCurrency() == 0){
                            img = new Image(Images.class.getResource("profile_2.png").toString(),14,14,true,true);
                        }else{
                            img = new Image(Images.class.getResource("default_profile.png").toString(),16,16,true,true);
                        }
                        image.setImage(img);
                        setGraphic(v);
                    }
                }
            };
        });
        
        
        TableColumn<Person, Person> checkCol = new TableColumn<>("CheckBox");
        checkCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        
        checkCol.setCellFactory((TableColumn<Person,Person> p) -> {
            final VBox v = new VBox();
            final CheckBox check = new CheckBox();
            {
                v.setAlignment(Pos.CENTER);
                v.getChildren().add(check);
            }
            
            return new TableCell<Person,Person>(){
                @Override
                public void updateItem(Person p, boolean empty){
                    if(p != null){
                        check.setSelected(p.getInvited());
                        setGraphic(v);
                    }else{
                    }
                }
            };
        });
        indexCol.setSortable(false);
        firstNameCol.setSortable(false);
        lastNameCol.setSortable(false);
        allCol.setSortable(false);
        checkCol.setSortable(false);
        theTable.getColumns().setAll(indexCol, firstNameCol, lastNameCol, allCol, checkCol);
    }
}
