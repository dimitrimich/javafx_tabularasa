/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import customObjects.Person;
import images.Images;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author d.michaelides
 */
public class TableUtils {
    
    
    public static void initializePersonsTable(TableView theTable){
        
        TableColumn<Person, Person> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        firstNameCol.setCellFactory((TableColumn<Person, Person> p) -> new TableCell<Person,Person>(){
            @Override
            public void updateItem(Person p, boolean empty){
                if(p != null){
                    setText(p.getFn());
                }
            }
        });
        
        TableColumn<Person, Person> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        lastNameCol.setCellFactory((TableColumn<Person,Person> p) -> new TableCell<Person,Person>(){
            @Override
            public void updateItem(Person p, boolean empty){
                if(p != null){
                    setText(p.getlName());
                }
            }
        });
        
        
        TableColumn<Person, Person> allCol = new TableColumn<>("Currency");
        allCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        
        allCol.setCellFactory((TableColumn<Person,Person> p) -> {
            final VBox v = new VBox();
            final ImageView image = new ImageView();
            {
                v.setAlignment(Pos.CENTER);
                v.getChildren().add(image);
            }
            
            return new TableCell<Person, Person>(){
                @Override
                public void updateItem(Person p, boolean empty){
                    if(p != null){
                        Image img;
                        if(p.getCurrency() == 0){
                            img = new Image(Images.class.getResource("profile_2.png").toString(),14,14,true,true);
                        }else{
                            img = new Image(Images.class.getResource("default_profile.png").toString(),16,16,true,true);
                        }
                        image.setImage(img);
                        setGraphic(v);
                        
                        
                    }else{
                    }
                    
                }
                
                
            };
        });
        
        
        TableColumn<Person, Person> checkCol = new TableColumn<>("CheckBox");
        checkCol.setCellValueFactory(new PropertyValueFactory<Person, Person>("Person"));
        
        checkCol.setCellFactory((TableColumn<Person,Person> p) -> {
            final VBox v = new VBox();
            final CheckBox check = new CheckBox();
            {
                v.setAlignment(Pos.CENTER);
                v.getChildren().add(check);
            }
            
            return new TableCell<Person,Person>(){
                @Override
                public void updateItem(Person p, boolean empty){
                    if(p != null){
                        check.setSelected(p.getInvited());
                        setGraphic(v);
                    }else{
                    }
                }
            };
        });
        theTable.getColumns().setAll(firstNameCol, lastNameCol, allCol, checkCol);
    }
    
        }
