/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import customObjects.ClassForTests;
import images.Images;
import images.icons.Icons;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.scene.input.DataFormat;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;

/**
 *
 * @author d.michaelides
 */
public class ForTestsTable {
    
    
    public static ObservableList<TableColumn> initTable(TableView theTable){
        
        
        ArrayList<TableColumn> columns = new ArrayList<>(20);
        
        TableColumn<ClassForTests, ClassForTests> firstNameCol = new TableColumn<>("Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<ClassForTests, ClassForTests>("ClassForTests"));
        firstNameCol.setCellFactory((TableColumn<ClassForTests, ClassForTests> t) -> new TableCell<ClassForTests, ClassForTests>(){
            @Override
            public void updateItem(ClassForTests t, boolean empty){
                if(t != null){
                    setText(t.getName());
                }
            }
        });
        firstNameCol.setComparator((ClassForTests t1, ClassForTests t2) -> Collator.getInstance().compare(t1.getName(), t2.getName()));
        columns.add(firstNameCol);
        
        TableColumn<ClassForTests, ClassForTests> descrCol = new TableColumn<>("Description");
        descrCol.setCellValueFactory(new PropertyValueFactory<ClassForTests, ClassForTests>("ClassForTests"));
        descrCol.setCellFactory((TableColumn<ClassForTests, ClassForTests> p) -> new TableCell<ClassForTests, ClassForTests>(){
            @Override
            public void updateItem(ClassForTests p, boolean empty){
                if(p != null){
                    setText(p.getDescription());
                }
            }
        });
        descrCol.setComparator((ClassForTests t1, ClassForTests t2) -> Collator.getInstance().compare(t1.getDescription(), t2.getDescription()));
        columns.add(descrCol);
        
        
        TableColumn<ClassForTests, ClassForTests> teacherCol = new TableColumn<>("Teacher");
        teacherCol.setCellValueFactory(new PropertyValueFactory<ClassForTests, ClassForTests>("ClassForTests"));
        teacherCol.setCellFactory((TableColumn<ClassForTests, ClassForTests> p) -> new TableCell<ClassForTests,ClassForTests>(){
            @Override
            public void updateItem(ClassForTests p, boolean empty){
                if(p != null){
                    setText(p.getTeacher());
                }
            }
        });
        teacherCol.setComparator((ClassForTests t1, ClassForTests t2) -> Collator.getInstance().compare(t1.getTeacher(), t2.getTeacher()));
        columns.add(teacherCol);
        
        for(int i = 0; i < 8; i++){
                    
            final String day = Integer.toString(i);
            TableColumn<ClassForTests, ClassForTests> dayCol = new TableColumn<>("On "+ utils.Utils.getDayOfWeek(i));
            dayCol.setCellValueFactory(new PropertyValueFactory<ClassForTests, ClassForTests>("ClassForTests"));
            dayCol.setCellFactory((TableColumn<ClassForTests, ClassForTests> p) -> new TableCell<ClassForTests, ClassForTests>(){
                @Override
                public void updateItem(ClassForTests p, boolean empty){
                    if(p != null){
                        boolean found = false;
                        for(String d : p.getDays()){
                            if(day.equals(d)){
                                found = true;
                            }
                        }
                        if(found){
                            setText("YES");
//                                    if(!isSelected()){
//                                        setStyle("-fx-text-fill:green;");
//                                    }
                        }else{
                            setText("no");
//                                    if(!isSelected()){
//                                        setStyle("-fx-text-fill:red;");
//                                    }
                        }
                    }
                }
            });
            dayCol.setSortable(false);
            columns.add(dayCol);
        }
        
        theTable.getColumns().setAll( columns );
        return FXCollections.observableArrayList(columns);
    }
}
