/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import images.icons.Icons;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * Replaced by method theTable.setTableMenuButtonVisible(true);
 * @author d.michaelides
 */
public class ColumnHider {
    public void initHider2(final ObservableList<TableColumn> columnsList, ComboBox <Node> columnsVisibility){
        
        columnsVisibility.getItems().clear();
        final ImageView img = new ImageView(new Image(Icons.class.getResource("eye.png").toString(),14,14,true,true));
        columnsVisibility.getItems().add(img);
        final ArrayList<CheckBox> checkBoxes = new ArrayList<>(columnsList.size());
        for(TableColumn t : columnsList){
            CheckBox check = new CheckBox();
            check.setText(t.getText());
            check.setSelected(true);
            check.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            checkBoxes.add(check);
        }
        columnsVisibility.getItems().addAll(checkBoxes);
        
        columnsVisibility.setCellFactory(
                (ListView<Node> p) -> new ListCell<Node>() {
                    private final CheckBox check;
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                check = new CheckBox();
                check.addEventHandler(MouseEvent.MOUSE_RELEASED, (MouseEvent t) -> {
                    check.setSelected(!check.isSelected());
                });
            }

            @Override
            protected void updateItem(Node item, boolean empty) {
                if (item != null && !empty) {
                    if (getIndex() == 0) {
                        final ImageView img1 = new ImageView(new Image(Icons.class.getResource("eye.png").toString(),14,14,true,true));
                        setGraphic(img1);
                        setMouseTransparent(true);
                    } else {
                        check.setText(columnsList.get(getIndex()-1).getText());
                        columnsList.get(getIndex()-1).visibleProperty().bind(check.selectedProperty());
                        check.setSelected(true);
                        setGraphic(check);
                    }
                }
            }
        });
        
        columnsVisibility.getSelectionModel().selectFirst();
    }
    
    public static void initHider(final ObservableList<TableColumn> columnsList, ComboBox <Node> columnsVisibility){
        
        columnsVisibility.getItems().clear();
        final ImageView img = new ImageView(new Image(Icons.class.getResource("eye.png").toString(),14,14,true,true));
        columnsVisibility.getItems().add(img);
        final ArrayList<CheckBox> checkBoxes = new ArrayList<>(columnsList.size());
        for(TableColumn t : columnsList){
            CheckBox check = new CheckBox();
            check.setText(t.getText());
            check.setSelected(true);
            check.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            checkBoxes.add(check);
        }
        columnsVisibility.getItems().addAll(checkBoxes);
        
        columnsVisibility.setCellFactory(
                (ListView<Node> p) -> new ListCell<Node>() {
                    private final CheckBox check;
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                check = new CheckBox();
                check.addEventHandler(MouseEvent.MOUSE_RELEASED, (MouseEvent t) -> {
                    check.setSelected(!check.isSelected());
                });
            }

            @Override
            protected void updateItem(Node item, boolean empty) {
                if (item != null && !empty) {
                    if (getIndex() == 0) {
                        final ImageView img1 = new ImageView(new Image(Icons.class.getResource("eye.png").toString(),14,14,true,true));
                        setGraphic(img1);
                        setMouseTransparent(true);
                    } else {
                        check.setText(columnsList.get(getIndex()-1).getText());
                        columnsList.get(getIndex()-1).visibleProperty().bind(check.selectedProperty());
                        check.setSelected(true);
                        setGraphic(check);
                    }
                }
            }
        });
        
        columnsVisibility.getSelectionModel().selectFirst();
    
    }
    
    public static void initHider3(final ObservableList<TableColumn> columnsList, final ComboBox <ToggleButton> columnsVisibility){
        columnsVisibility.getItems().clear();
        final ArrayList<ToggleButton> toggles = new ArrayList<>(columnsList.size());
        for(TableColumn t : columnsList){
            ToggleButton b = new ToggleButton();
            b.setText(t.getText());
            b.setSelected(true);
            toggles.add(b);
        }
        columnsVisibility.getItems().addAll(toggles);
        
        columnsVisibility.setCellFactory(
                (ListView<ToggleButton> p) -> new ToggleCell(columnsList));
        
        columnsVisibility.getSelectionModel().selectFirst();
    }
    
    static class ToggleCell extends ListCell<ToggleButton> {
        protected ToggleCell(ObservableList<TableColumn> c){
            columnsList = c;
        }
        private final ObservableList<TableColumn> columnsList;
    
        private final ToggleButton toggle;
        {
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 
            toggle = new ToggleButton();
            toggle.addEventHandler(MouseEvent.MOUSE_RELEASED, (MouseEvent t) -> {
                toggle.setSelected(!toggle.isSelected());
            });
        }
        @Override protected void updateItem(ToggleButton item, boolean empty) {

            if (item != null && !empty) {
                System.out.println("   >>> -22-   ");
                toggle.setText(columnsList.get(getIndex()).getText());
                columnsList.get(getIndex()).visibleProperty().bind(toggle.selectedProperty());
                toggle.setSelected(true);
                setGraphic(toggle);
            }else{
                setGraphic(null);
            }
        }
    
    }

    
}
