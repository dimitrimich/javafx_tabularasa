/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tables;

import DB.objects.Aclass;
import DB.objects.WeeklyLesson;
import java.text.Collator;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class ClassesTable {
    
    
    public static ObservableList<TableColumn> initializeClassesTable(TableView theTable){
        
        TableColumn<Aclass, Aclass> firstNameCol = new TableColumn<>("Class Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        firstNameCol.setCellFactory((TableColumn<Aclass, Aclass> t) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null){
                        setText(cl.getName());
                    }else{
                        setText("");
                    }
                }
            };
        });
        firstNameCol.setComparator((Aclass t1, Aclass t2) -> Collator.getInstance().compare(t1.getName(), t2.getName()));
		
        TableColumn<Aclass, Aclass> codeNameCol = new TableColumn<>("Class Code");
        codeNameCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        codeNameCol.setCellFactory((TableColumn<Aclass, Aclass> t) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null){
                        setText(cl.getCodeName());
                    }else{
                        setText("");
                    }
                }
            };
        });
        codeNameCol.setComparator((Aclass t1, Aclass t2) -> Collator.getInstance().compare(t1.getCodeName(), t2.getCodeName()));
        
        TableColumn<Aclass, Aclass> descriptionCol = new TableColumn<>("Description");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        descriptionCol.setCellFactory((TableColumn<Aclass, Aclass> p) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null && Utils.isValidParam(cl.getDescription())){
                        setText(cl.getDescription());
                    }else{
                        setText("");
                    }
                }
            };
        });
        descriptionCol.setComparator((Aclass t1, Aclass t2) -> 
            {
            if(Utils.isValidParam(t1.getDescription()) && Utils.isValidParam(t2.getDescription())){
                return Collator.getInstance().compare(t1.getDescription(),  t2.getDescription());
            }
            return 1;
        });
        
        TableColumn<Aclass, Aclass> teacherCol = new TableColumn<>("Teacher");
        teacherCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        teacherCol.setCellFactory((TableColumn<Aclass, Aclass> p) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null && cl.getTeacher() != null){
                        setText(cl.getTeacher().getBasicInfo().getFname() +" "+ cl.getTeacher().getBasicInfo().getLname());
                    }else{
                        setText("");
                    }
                }
            };
        });
        teacherCol.setComparator((Aclass t1, Aclass t2) -> 
            {
            if(t1.getTeacher() != null && t2.getTeacher() != null){
                return Collator.getInstance().compare(t1.getTeacher().getBasicInfo().getFname(),  t2.getTeacher().getBasicInfo().getFname());
            }
            return 1;
        });
        
        TableColumn<Aclass, Aclass> numOfLessons = new TableColumn<>("# Weekly Lessons");
        numOfLessons.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        numOfLessons.setCellFactory((TableColumn<Aclass, Aclass> p) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null){
                        if(cl.getSchedule() != null && cl.getSchedule().getwLessons() != null){
                            setText(Integer.toString(cl.getSchedule().getwLessons().size()));
                        }else{
                            setText("");
                        }
                    }else{
                        setText("");
                    }
                }
            };
        });
        numOfLessons.setComparator((Aclass t1, Aclass t2) -> 
            {
            if(t2.getSchedule() != null && t1.getSchedule() != null
                && t2.getSchedule().getwLessons() != null && t1.getSchedule().getwLessons() != null){
                return Integer.compare(t1.getSchedule().getwLessons().size(), t2.getSchedule().getwLessons().size());
            }
            return 1;
        });
        
        
        TableColumn<Aclass, Aclass> lessonsCol = new TableColumn<>("Days of Lessons");
        lessonsCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        lessonsCol.setCellFactory((TableColumn<Aclass, Aclass> p) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null){
                        if(cl.getSchedule() != null && cl.getSchedule().getwLessons() != null){
                            StringBuilder sb = new StringBuilder();
                            if(cl.getSchedule().getwLessons().size() > 0){
                                for(WeeklyLesson lesson : cl.getSchedule().getwLessons()){
                                    sb.append(Utils.getDayOfWeekShort(lesson.getDayOfTheWeek()));
                                    sb.append(" | ");
                                }
                            }
                            setText(sb.toString());
                        }else{
                            setText("");
                        }
                    }else{
                        setText("");
                    }
                }
            };
        });
        lessonsCol.setSortable(false);
        
        TableColumn<Aclass, Aclass> lessonsTimeCol = new TableColumn<>("Times of Lessons");
        lessonsTimeCol.setCellValueFactory(new PropertyValueFactory<>("Aclass"));
        lessonsTimeCol.setCellFactory((TableColumn<Aclass, Aclass> p) -> {
            return new TableCell<Aclass, Aclass>(){
                @Override
                public void updateItem(Aclass cl, boolean empty){
                    if(cl != null){
                        if(cl.getSchedule() != null && cl.getSchedule().getwLessons() != null){
                            StringBuilder sb = new StringBuilder();
                            if(cl.getSchedule().getwLessons().size() > 0){
                                for(WeeklyLesson lesson : cl.getSchedule().getwLessons()){
                                    sb.append(lesson.getStartingTime());
                                    sb.append(" | ");
                                }
                            }
                            setText(sb.toString());
                        }else{
                            setText("");
                        }
                    }else{
                        setText("");
                    }
                }
            };
        });
        lessonsTimeCol.setSortable(false);
        
        theTable.getColumns().setAll( firstNameCol, codeNameCol, descriptionCol, teacherCol, numOfLessons, lessonsCol, lessonsTimeCol );
        
        ArrayList<TableColumn> columns = new ArrayList<>(3);
        
//        columns.add(imageCol);
        columns.add(firstNameCol);
        columns.add(codeNameCol);
        columns.add(descriptionCol);
        columns.add(teacherCol);
        columns.add(numOfLessons);
        columns.add(lessonsCol);
        columns.add(lessonsTimeCol);
        return FXCollections.observableArrayList(columns);
    }
}
