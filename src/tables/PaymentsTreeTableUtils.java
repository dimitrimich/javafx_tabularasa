/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package tables;

import DB.objects.PaymentDetails;
import customObjects.FullTreeMap;
import java.math.BigDecimal;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import utils.LanguageUtils;

/**
 * initializes the PaymentDetails TreeTableView
 * @author dimitris
 */
public class PaymentsTreeTableUtils {
	
	public static void initPaymentsTreeTable(TreeTableView table){
		Root theRootItem = new Root();
		theRootItem.setinfo("Payments");
		TreeItem<Root> root = new TreeItem<>(theRootItem);
		root.setExpanded(true);
		table.setRoot(root);
		
		TreeTableColumn<Root, String> infoColumn = new TreeTableColumn<>("Info");
		infoColumn.setPrefWidth(120);
		infoColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Root, String> param) -> {
			if(param.getValue().getValue() instanceof Root){
				return new ReadOnlyStringWrapper(param.getValue().getValue().getinfo());
			}
			else{
				return new ReadOnlyStringWrapper("");
			}
		});
		
		TreeTableColumn<PaymentFX, String> receiptColumn = new TreeTableColumn<>("Receipt");
		receiptColumn.setPrefWidth(120);
		receiptColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, String> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				if(!param.getValue().getValue().getReceipt().isEmpty()){
					return new ReadOnlyStringWrapper(param.getValue().getValue().getReceipt());
				}
			}
			return new ReadOnlyStringWrapper("");
		});
		
		TreeTableColumn<PaymentFX, String> paidByColumn = new TreeTableColumn<>("Paid By");
		paidByColumn.setPrefWidth(120);
		paidByColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, String> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				if(param.getValue().getValue().getPaidFor() != null){
					if(!param.getValue().getValue().getPaidBy().isEmpty()){
						return new ReadOnlyStringWrapper(param.getValue().getValue().getPaidBy());
					}
				}
			}
			return new ReadOnlyStringWrapper("");
		});
		TreeTableColumn<PaymentFX, String> paidForColumn = new TreeTableColumn<>("Paid For");
		paidForColumn.setPrefWidth(120);
		paidForColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, String> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				if(param.getValue().getValue().getPaidFor() != null){
					if(!param.getValue().getValue().getPaidFor().isEmpty()){
						return new ReadOnlyStringWrapper(param.getValue().getValue().getPaidFor());
					}
				}
			}
			return new ReadOnlyStringWrapper("");
		});
		
		TreeTableColumn<PaymentFX, String> classNameColumn = new TreeTableColumn<>("Class Name");
		classNameColumn.setPrefWidth(125);
		classNameColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, String> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				if(!param.getValue().getValue().getClassName().isEmpty()){
					return new ReadOnlyStringWrapper(param.getValue().getValue().getClassName());
				}
			}
			return new ReadOnlyStringWrapper("");
		});
		TreeTableColumn<PaymentFX, String> monthColumn = new TreeTableColumn<>("Month");
		monthColumn.setPrefWidth(120);
		monthColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, String> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				if(!param.getValue().getValue().getBillingMonth().isEmpty()){
					return new ReadOnlyStringWrapper(param.getValue().getValue().getBillingMonth());
				}
			}
			return new ReadOnlyStringWrapper("");
		});
		TreeTableColumn<PaymentFX, BigDecimal> amountPaidColumn = new TreeTableColumn<>("Paid Amount");
		amountPaidColumn.setPrefWidth(100);
		amountPaidColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, BigDecimal> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				return new ReadOnlyObjectWrapper<>(param.getValue().getValue().getAmountPaid());
			}
			return new ReadOnlyObjectWrapper("");
		});
		
		TreeTableColumn<PaymentFX, BigDecimal> amountDiscountColumn = new TreeTableColumn<>("Discount");
		amountDiscountColumn.setPrefWidth(100);
		amountDiscountColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<PaymentFX, BigDecimal> param) -> {
			if(param.getValue().getValue() instanceof PaymentFX){
				return new ReadOnlyObjectWrapper<>(param.getValue().getValue().getAmountDiscount());
			}
			return new ReadOnlyObjectWrapper("");
		});
		
		table.getColumns().setAll(infoColumn, receiptColumn, paidForColumn, paidByColumn, monthColumn, classNameColumn, amountPaidColumn,  amountDiscountColumn);
		infoColumn.setSortable(false); receiptColumn.setSortable(false); monthColumn.setSortable(false); classNameColumn.setSortable(false);
		amountPaidColumn.setSortable(false);  amountDiscountColumn.setSortable(false);
		paidForColumn.setSortable(false); paidByColumn.setSortable(false);
	}
	
	public static class Root{
		private StringProperty info;
		public StringProperty infoProperty(){
			if (info == null) { info = new SimpleStringProperty(this, "info"); }
			return info;
		}
		public String getinfo() { return infoProperty().get(); }
		public void setinfo(String info) { this.infoProperty().set(info); }
	}
	
	public static class PaymentFX{
		private StringProperty receipt;
		private StringProperty paidBy;
		private StringProperty paidFor;
		private ObjectProperty<BigDecimal> amountPaid;
		private ObjectProperty<BigDecimal> amountDiscount;
		private StringProperty billingMonth;
		private StringProperty className;
		
		public PaymentFX (){
			receipt = new SimpleStringProperty();
			paidBy = new SimpleStringProperty();
			paidFor = new SimpleStringProperty();
			billingMonth = new SimpleStringProperty();
			className = new SimpleStringProperty();
			amountPaid = new SimpleObjectProperty<>(BigDecimal.ZERO);
			amountDiscount = new SimpleObjectProperty<>(BigDecimal.ZERO);
		}
		public void addPayment(PaymentDetails newPayment){
			this.receipt.set(newPayment.getReceiptNo());
			this.amountPaid.set(this.amountPaid.get().add(newPayment.getPaidAmount()));
			this.amountDiscount.set(this.amountDiscount.get().add(newPayment.getDiscount()));
			this.paidBy.set(newPayment.getPaymentBy());
			this.paidFor.set(newPayment.getPaymentFor());
			if(newPayment.getBillingMonth() != null){
				this.billingMonth.set(newPayment.getBillingMonth().getDisplayName(TextStyle.FULL_STANDALONE, LanguageUtils.getCurrentLocale()));
			}
			else{
				this.billingMonth.set("");
			}
			this.className.set(newPayment.getClassName());
		}
		
		public StringProperty receiptProperty(){
			if (receipt == null) { receipt = new SimpleStringProperty(this, "receipt"); }
			return receipt;
		}
		public StringProperty paidByProperty(){
			if (paidBy == null) { paidBy = new SimpleStringProperty(this, "paidBy"); }
			return paidBy;
		}
		public StringProperty paidForProperty(){
			if (paidFor == null) { paidFor = new SimpleStringProperty(this, "paidFor"); }
			return paidFor;
		}
		public StringProperty billingMonthProperty(){
			if (billingMonth == null) { billingMonth = new SimpleStringProperty(this, "billingMonth"); }
			return billingMonth;
		}
		public StringProperty classNameProperty(){
			if (className == null) { className = new SimpleStringProperty(this, "className"); }
			return className;
		}
		public ObjectProperty<BigDecimal> amountPaidProperty(){
			if (amountPaid == null) { amountPaid = new SimpleObjectProperty(this, "amountPaid"); }
			return amountPaid;
		}
		public ObjectProperty<BigDecimal> amountDiscountProperty(){
			if (amountDiscount == null) { amountDiscount = new SimpleObjectProperty(this, "amountDiscount"); }
			return amountDiscount;
		}
		public String getReceipt() { return receipt.get(); }
		public void setReceipt(String receipt) { this.receipt.set(receipt); receiptProperty().set(receipt);}
		public String getPaidBy() { return paidBy.get(); }
		public void setPaidBy(String paidBy) { this.paidBy.set(paidBy); }
		public String getPaidFor() { return paidFor.get(); }
		public void setPaidFor(String paidFor) { this.paidFor.set(paidFor); }
		public BigDecimal getAmountPaid() { return amountPaid.get();}
		public void setAmountPaid(BigDecimal amountPaid) { this.amountPaid.set(amountPaid); }
		public BigDecimal getAmountDiscount() { return amountDiscount.get(); }
		public void setAmountDiscount(BigDecimal amountDiscount) { this.amountDiscount.set(amountDiscount); }
		public String getBillingMonth() { return billingMonth.get(); }
		public void setBillingMonth(String billingMonth) { this.billingMonth.set(billingMonth); }
		public String getClassName() { return className.get(); }
		public void setClassName(String className) { this.className.set(className); }
	}
	
	
	public static class SetTreeTableItemsService extends Service<List<TreeItem<PaymentFX>>>{
		public SetTreeTableItemsService(List<PaymentDetails> payments, TreeTableView table){
			this.payments = payments;
			this.table = table;
		}
		private List<PaymentDetails>  payments;
		private TreeTableView  table;
		
		@Override
		protected Task<List<TreeItem<PaymentFX>>> createTask() {
			SetTreeTableItemsTask theTask = new SetTreeTableItemsTask();
			return theTask;
		}
		
		private class SetTreeTableItemsTask extends Task<List<TreeItem<PaymentFX>>> {
			@Override
			protected List<TreeItem<PaymentFX>> call() throws Exception {
				SortedList<TreeItem<PaymentFX>> treeItems = null;
				
				try{
					// group payments based on receipt
					HashMap<String, PaymentDetails> paymentsMap = new HashMap();
					
					FullTreeMap theTreeMap = new FullTreeMap();
					System.out.println("All regular Payments "+ payments.size());
					for(PaymentDetails payment : payments){
						System.out.println("    f="+payment.getTotalAmount()+" | p="+payment.getPaidAmount()+" | d="+payment.getDiscount());
						String receipt = payment.getReceiptNo();
						if(paymentsMap.containsKey(receipt)){
							paymentsMap.get(receipt).addAmounts(payment);
						}else{
							paymentsMap.put(receipt, payment);
						}
						System.out.println("theTreeMap.addPayment: "+payment.getPaidAmount() +" | "+payment.getDiscount());
						theTreeMap.addPayment(payment);
					}
					
					// create the PaymentFX entities
					System.out.println("--------------------------------------\nCREATE paymentsFX:");
					List<PaymentFX> paymentsFX = new ArrayList<>();
					for(Entry<String, PaymentDetails> entry : paymentsMap.entrySet()){
						PaymentFX paymentFX = new PaymentFX();
						paymentFX.addPayment(entry.getValue());
						System.out.println("   > adding -->"+entry.getValue().getBillingMonth() +" | "+entry.getValue().getDiscount()+" "+entry.getValue().getPaidAmount());
						System.out.println("   > paymentFX -->"+paymentFX.getBillingMonth() +" | "+paymentFX.getAmountDiscount() +" "+paymentFX.getAmountPaid());
						
						paymentsFX.add(paymentFX);
					}
					
					ArrayList<TreeItem<PaymentFX>> treeItemsTemp = new ArrayList<>();
					for(PaymentFX fxItem : paymentsFX){
						fxItem.setClassName("");
						fxItem.setBillingMonth("");
						TreeItem<PaymentFX> treeItem = new TreeItem<>(fxItem);
						treeItem.getChildren().addAll(theTreeMap.getMonthsForPayment(fxItem.getReceipt()));
						treeItemsTemp.add(treeItem);
					}
					treeItems = theTreeMap.createSortedPaymentList(treeItemsTemp);
					System.out.println("returning ....");
				}catch(Exception e){
					e.printStackTrace(System.err);
				}
				return treeItems;
			}
		}
	}
	
}
