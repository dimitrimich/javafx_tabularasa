/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package utils;

import DB.DBDeleteUtils;
import DB.DBUpdateUtils;
import DB.objects.Aclass;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfWLessons;
import DB.objects.WeeklyLesson;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import static utils.ScheduleUtils.getDayOfTheWeekForLocalDate;

/**
 *
 * @author dimitris
 */
public class WeeklyLessonsEditor {
	private static final WeeklyLessonsEditor editor = new WeeklyLessonsEditor();
	
	private WeeklyLessonsEditor(){}
	public static WeeklyLessonsEditor getInstance(){
		return editor;
	}
	
	/**
	 * move all lessons that have the same index to new Day/time specified in newDateTime
	 * @param newDateTime The new Day/Time for the lessons (we don't care for Date only day+time)
	 * @param schedule the Aclasses' schedule
	 * @param classIndex Index of the weekly lesson
	 */
	public void moveWeeklyLessons(LocalDateTime newDateTime, Map<LocalDate, ListOfWLessons> schedule, int classIndex, int newDuration, boolean isFullyScheduled){
		reAddRemovedLessons(removeAllLessons(newDateTime, schedule, classIndex, newDuration, isFullyScheduled), schedule);
	}
	
	private ArrayList<Lesson> removeAllLessons(LocalDateTime newDateTime, Map<LocalDate, ListOfWLessons> schedule, int classIndex, int newDuration, boolean isFullyScheduled){
		
		System.out.println("Removing lessons");
		
		ArrayList<Lesson> toBeRemoved = new ArrayList<>();
		
		Long count = schedule.values().stream().filter((ListOfWLessons t) -> {
			boolean found = false;
			Iterator <Lesson> it = t.getAllLessons().iterator();
			while(it.hasNext()){
				Lesson l = it.next();
				if (l.getIndexNumber() == classIndex){
					if(isFullyScheduled){
						l.setTheDate(LocalDate.of(newDateTime.getYear(), l.getTheDate().getMonth(), l.getTheDate().getDayOfMonth()));
						LocalDate newDate = moveToNewDay(l.getTheDate(), newDateTime);
						System.out.println("NewDate --3-3= "+ newDate.toString());
						System.out.println("newDateTime --3-3= "+ newDateTime.toString());
						System.out.println("Fully scheduled - not 3000");
						l.setTheDate(newDate);
						l.setDayOfTheWeek(getDayOfTheWeekFromLocalDate(newDate));
						l.setStartingHour(newDateTime.getHour());
						l.setStartingMinute(newDateTime.getMinute());
					}else{
						System.out.println("NOT Fully scheduled - 3000");
						LocalDate temp = previousSunday(l.getTheDate());
						l.setTheDate(LocalDate.of(3000, temp.getMonth(), temp.getDayOfMonth()));
						l.setDayOfTheWeek(0);
						l.setStartingHour(0);
						l.setStartingMinute(0);
					}
					l.setDuration(newDuration);
					it.remove();
					toBeRemoved.add(l);
					found = true;
				}
			}
			return found;
		}).count();
		
		System.out.println("Removed "+toBeRemoved.size() +" lessons");
		return toBeRemoved;
	}
	/**
	 * add back to the schedule all the removed lessons
	 * @param schedule the schedule
	 * @param removedLessons the lessons that were removed previously
	 */
	private void reAddRemovedLessons(ArrayList<Lesson> removedLessons, Map<LocalDate, ListOfWLessons> schedule){
		System.out.println("Adding them back");
		for(Lesson lesson : removedLessons){
			if(schedule.containsKey(lesson.getTheDate())){
				System.out.println("Adding to (0): "+lesson.getTheDate());
				schedule.get(lesson.getTheDate()).getAllLessons().add(lesson);
			}else{
				System.out.println("Adding to (1): "+lesson.getTheDate());
				ListOfWLessons newDay = new ListOfWLessons();
				newDay.setTheDate(lesson.getTheDate());
				newDay.getAllLessons().add(lesson);
				schedule.put(lesson.getTheDate(), newDay);
			}
		}
	}
	
	/**
	 * checks if lesson is scheduled or not
	 */
	public void addNewWeeklyLesson(Aclass selectedClass, WeeklyLesson wlesson, LocalDate startDate, LocalDate endDate){
		if(wlesson.isFullyScheduled()){
			addFullyScheduledLesson(selectedClass, wlesson, startDate, endDate);
		}else{
			addNotScheduledLesson(selectedClass, wlesson, startDate, endDate);
		}
	}
	
	private void addFullyScheduledLesson(Aclass selectedClass, WeeklyLesson wlesson, LocalDate startDate, LocalDate endDate){
//		System.out.println("--->Adding 1 weekly lesson");
		
//		System.out.println(" startDate = "+ startDate + " | "+startDate.isBefore(endDate) +" | end = "+endDate);
		for (LocalDate thisDay = startDate; (thisDay.isBefore(endDate) || thisDay.isEqual(endDate)); thisDay = thisDay.plusDays(1)) {
//			System.out.println("  Today = "+ thisDay.toString());
			ListOfWLessons listOfLessons = new ListOfWLessons();
			// weekly Lesson Monday = 2 but DayOfWeek.Monday = 1
//			System.out.println(thisDay.getDayOfWeek()+" =?= "+ DayOfWeek.of(getDayOfTheWeekForLocalDate(wlesson.getDayOfTheWeek()))
//			+ " "+(thisDay.getDayOfWeek() == DayOfWeek.of(getDayOfTheWeekForLocalDate(wlesson.getDayOfTheWeek()))));
			
			if (thisDay.getDayOfWeek() == DayOfWeek.of(getDayOfTheWeekForLocalDate(wlesson.getDayOfTheWeek()))) {
//				System.out.println(" fully scheduled ? " + wlesson.isFullyScheduled());
				if(wlesson.isFullyScheduled()){
					listOfLessons.setTheDate(thisDay);
					listOfLessons.getAllLessons().add(new Lesson(wlesson, thisDay, selectedClass));
					System.out.println("  Adding a lesson - "+thisDay.toString());
				}
			}
			if (!listOfLessons.getAllLessons().isEmpty()){
				selectedClass.getSchedule().getAllWLessons().put(thisDay, listOfLessons);
			}
		}
	}
	
	private void addNotScheduledLesson(Aclass selectedClass, WeeklyLesson wlesson, LocalDate startDate, LocalDate endDate){
		System.out.println("Adding 1 Not Scheduled lesson");
		
		// Lessons that are missing Day or Time
		LocalDate fromSunday = previousSunday(startDate);
		LocalDate toSunday = previousSunday(endDate);
		for (LocalDate thisSunday = fromSunday; !(thisSunday.equals(toSunday.plusDays(7))); thisSunday = thisSunday.plusDays(7)){
			LocalDate addTo = LocalDate.of(3000, thisSunday.getMonthValue(), thisSunday.getDayOfMonth());
			System.out.println("Add lesson in date - "+ addTo.toString());
			ListOfWLessons listOfLessons = selectedClass.getSchedule().getAllWLessons().get(addTo);
			if(listOfLessons == null){
				listOfLessons = new ListOfWLessons();
				listOfLessons.setTheDate(addTo);
			}
			listOfLessons.getAllLessons().add(new Lesson(wlesson, addTo, selectedClass));
			selectedClass.getSchedule().getAllWLessons().put(addTo, listOfLessons);
		}
	}
	private LocalDate moveToNewDay(LocalDate oldDate, Object newDate ){
		if(newDate instanceof LocalDate){
			return moveToNewDayWithDayParameter(oldDate, ((LocalDate) newDate).getDayOfWeek());
		}else if(newDate instanceof LocalDateTime){
			return moveToNewDayWithDayParameter(oldDate, ((LocalDateTime) newDate).getDayOfWeek());
		}else{
			throw new IllegalArgumentException(" Parameter "+newDate +" is not a Date");
		}
	}
	
	private LocalDate moveToNewDayWithDayParameter(LocalDate oldDate, DayOfWeek newDay ){
		switch(oldDate.getDayOfWeek()){
			case MONDAY:
				switch(newDay){
					case MONDAY:
						return oldDate;
					case TUESDAY:
						return oldDate.plusDays(1);
					case WEDNESDAY:
						return oldDate.plusDays(2);
					case THURSDAY:
						return oldDate.plusDays(3);
					case FRIDAY:
						return oldDate.plusDays(4);
					case SATURDAY:
						return oldDate.plusDays(5);
					case SUNDAY:
						return oldDate.minusDays(1);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case TUESDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.minusDays(1);
					case TUESDAY:
						return oldDate;
					case WEDNESDAY:
						return oldDate.plusDays(1);
					case THURSDAY:
						return oldDate.plusDays(2);
					case FRIDAY:
						return oldDate.plusDays(3);
					case SATURDAY:
						return oldDate.plusDays(4);
					case SUNDAY:
						return oldDate.minusDays(2);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case WEDNESDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.minusDays(2);
					case TUESDAY:
						return oldDate.minusDays(1);
					case WEDNESDAY:
						return oldDate;
					case THURSDAY:
						return oldDate.plusDays(1);
					case FRIDAY:
						return oldDate.plusDays(2);
					case SATURDAY:
						return oldDate.plusDays(3);
					case SUNDAY:
						return oldDate.minusDays(3);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case THURSDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.minusDays(3);
					case TUESDAY:
						return oldDate.minusDays(2);
					case WEDNESDAY:
						return oldDate.minusDays(1);
					case THURSDAY:
						return oldDate;
					case FRIDAY:
						return oldDate.plusDays(1);
					case SATURDAY:
						return oldDate.plusDays(2);
					case SUNDAY:
						return oldDate.minusDays(4);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case FRIDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.minusDays(4);
					case TUESDAY:
						return oldDate.minusDays(3);
					case WEDNESDAY:
						return oldDate.minusDays(2);
					case THURSDAY:
						return oldDate.minusDays(1);
					case FRIDAY:
						return oldDate;
					case SATURDAY:
						return oldDate.plusDays(1);
					case SUNDAY:
						return oldDate.minusDays(5);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case SATURDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.minusDays(5);
					case TUESDAY:
						return oldDate.minusDays(4);
					case WEDNESDAY:
						return oldDate.minusDays(3);
					case THURSDAY:
						return oldDate.minusDays(2);
					case FRIDAY:
						return oldDate.minusDays(1);
					case SATURDAY:
						return oldDate;
					case SUNDAY:
						return oldDate.minusDays(6);
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			case SUNDAY:
				switch(newDay){
					case MONDAY:
						return oldDate.plusDays(1);
					case TUESDAY:
						return oldDate.plusDays(2);
					case WEDNESDAY:
						return oldDate.plusDays(3);
					case THURSDAY:
						return oldDate.plusDays(4);
					case FRIDAY:
						return oldDate.plusDays(5);
					case SATURDAY:
						return oldDate.plusDays(6);
					case SUNDAY:
						return oldDate;
					default:
						throw new AssertionError(oldDate.getDayOfWeek().name());
				}
			default:
				throw new AssertionError(newDay.name());
		}
	}
	private int getDayOfTheWeekFromLocalDate(LocalDate aDate){
		switch(aDate.getDayOfWeek()){
			case SUNDAY: return 1;
			case MONDAY: return 2;
			case TUESDAY: return 3;
			case WEDNESDAY: return 4;
			case THURSDAY: return 5;
			case FRIDAY: return 6;
			case SATURDAY: return 7;
			default:throw new IllegalArgumentException("UNKNOWN DAY");
		}
	}
	private DayOfWeek getDayOfTheWeekFromLocalDate(int calendarDay){
		switch(calendarDay){
			case 1: return DayOfWeek.SUNDAY;
			case 2: return DayOfWeek.MONDAY;
			case 3: return DayOfWeek.TUESDAY;
			case 4: return DayOfWeek.WEDNESDAY;
			case 5: return DayOfWeek.THURSDAY;
			case 6: return DayOfWeek.FRIDAY;
			case 7: return DayOfWeek.SATURDAY;
			default: throw new IllegalArgumentException("UNKNOWN DAY");
		}
	}
	private LocalDate previousSunday(LocalDate toHere){
		switch(toHere.getDayOfWeek()){
			case MONDAY:
				return toHere.minusDays(1);
			case TUESDAY:
				return toHere.minusDays(2);
			case WEDNESDAY:
				return toHere.minusDays(3);
			case THURSDAY:
				return toHere.minusDays(4);
			case FRIDAY:
				return toHere.minusDays(5);
			case SATURDAY:
				return toHere.minusDays(6);
			case SUNDAY:
				return toHere;
			default:
				throw new AssertionError(toHere.getDayOfWeek().name());
		}
	}
	
	public void setNewStartDate(Aclass selectedClass, LocalDate newDate){
		if(newDate.isBefore(selectedClass.getStartDate())){
			// add more weekly lessons
			System.out.println("New Date = "+ newDate.toString());
			moveStartDateEarlier(selectedClass, newDate);
			
		}else{
			// remove existing weekly lessons
			moveStartDateLater(selectedClass, newDate);
		}
	}
	
	private void moveStartDateEarlier(Aclass selectedClass, LocalDate newDate){
		for(WeeklyLesson lesson : selectedClass.getSchedule().getwLessons()){
			addNewWeeklyLesson(selectedClass, lesson, newDate, selectedClass.getStartDate().minusDays(1));
		}
	}
	private void moveStartDateLater(Aclass selectedClass, LocalDate newDate){
		deleteLessonsBeforeNewDate(selectedClass, newDate);
		for(LocalDate start = selectedClass.getStartDate(); start.isBefore(newDate); start = start.plusDays(1)){
			selectedClass.getSchedule().getAllWLessons().remove(start);
		}
	}
	public void setNewEndDate(Aclass selectedClass, LocalDate newDate){
		if(newDate.isBefore(selectedClass.getEndDate())){
			// remove existing weekly lessons
			moveEndDateEarlier(selectedClass, newDate);
		}else{
			// add more weekly lessons
			System.out.println("----> Add more lessons");
			moveEndDateLater(selectedClass, newDate);
		}
	}
	private void moveEndDateEarlier(Aclass selectedClass, LocalDate newDate){
		deleteLessonsAfterEndDate(selectedClass, newDate);
		System.out.println("Delete dates: ");
		for(LocalDate start = newDate.plusDays(1); start.isBefore(selectedClass.getEndDate().plusDays(1)); start = start.plusDays(1)){
			System.out.println("   "+start);
			selectedClass.getSchedule().getAllWLessons().remove(start);
		}
	}
	
	private void moveEndDateLater(Aclass selectedClass, LocalDate newDate){
		for(WeeklyLesson lesson : selectedClass.getSchedule().getwLessons()){
			addNewWeeklyLesson(selectedClass, lesson, selectedClass.getEndDate().plusDays(1), newDate);
		}
	}
	
	/**
	 * Go through all the dates and remove Lessons if they correspond to the given Weekly Lesson
	 */
	public void removeWeeklyLesson(Aclass selectedClass, WeeklyLesson lesson){
		System.out.println("removeWeeklyLesson");
		if(!lesson.isFullyScheduled()){
			removeNOTfulyScheduledLesson(selectedClass, lesson);
			return;
		}
		for(LocalDate today = selectedClass.getStartDate(); today.isBefore(selectedClass.getEndDate().plusDays(1)); today = today.plusDays(1)){
			if(selectedClass.getSchedule().getAllWLessons().containsKey(today)){
				ListOfWLessons todaysLessons = selectedClass.getSchedule().getAllWLessons().get(today);
				Iterator <Lesson> it = todaysLessons.getAllLessons().iterator();
				while(it.hasNext()){
					Lesson l = it.next();
					if(l.getIndexNumber() == lesson.getIndexNumber()){
						it.remove();
						DBDeleteUtils.deleteLesson(todaysLessons, l);
					}
				}
			}
		}
	}
	
	private void removeNOTfulyScheduledLesson(Aclass selectedClass, WeeklyLesson lesson){
		System.out.println("Not fully scheduled ");
		// Lessons that are missing Day or Time
		LocalDate fromSunday = previousSunday(selectedClass.getStartDate());
		LocalDate toSunday = previousSunday(selectedClass.getEndDate());
		Map<LocalDate, ListOfWLessons> theMap = selectedClass.getSchedule().getAllWLessons();
		
		for (LocalDate thisSunday = fromSunday; !(thisSunday.equals(toSunday.plusDays(7))); thisSunday = thisSunday.plusDays(7)){
			LocalDate removeFrom = LocalDate.of(3000, thisSunday.getMonthValue(), thisSunday.getDayOfMonth());
			System.out.println("Future sunday = "+removeFrom);
			
			ListOfWLessons listOfLessons = selectedClass.getSchedule().getAllWLessons().get(removeFrom);
			if(listOfLessons != null){
				Iterator<Lesson> it = listOfLessons.getAllLessons().iterator();
				while(it.hasNext()){
					Lesson l = it.next();
					System.out.println("index "+lesson.getIndexNumber() +"== "+l.getIndexNumber());
					if(lesson.getIndexNumber() == l.getIndexNumber()){
						it.remove();
						DBDeleteUtils.deleteLesson(listOfLessons, l);
					}
				}
			}
		}
	}
	
	public LocalDateTime getADate(LocalDate startDate, int day, int hour, int minute){
		System.out.println("Day "+day+" | hour = "+ hour+" | min = "+minute);
		System.out.println("StartDate = "+ startDate);
		DayOfWeek localDateDay = getDayOfTheWeekFromLocalDate(day);
		LocalDate specificDay = moveToNewDayWithDayParameter(startDate, localDateDay);
		return LocalDateTime.of(specificDay.getYear(),specificDay.getMonth(), specificDay.getDayOfMonth(), hour, minute);
	}
	
	private void deleteLessonsBeforeNewDate(Aclass selectedClass, LocalDate newDate) {
		System.out.println("deleteLessonsBeforeNewDate()");
		
		List<Lesson> allLessonsToDelete = new ArrayList<>();
		List<ListOfELessons> extraLessonsLists = new ArrayList<>();
		List<ListOfWLessons> weeklyLessonsLists = new ArrayList<>();
		Map<LocalDate, ListOfELessons> theMap = selectedClass.getSchedule().getAllELessons();
		for(Map.Entry<LocalDate, ListOfELessons> anEntry : theMap.entrySet()){
			Iterator<Lesson> it = anEntry.getValue().getAllLessons().iterator();
			while(it.hasNext()){
				Lesson aLesson = it.next();
				if(newDate.isAfter(aLesson.getTheDate())){
					it.remove();
					allLessonsToDelete.add(aLesson);
					extraLessonsLists.add(anEntry.getValue());
				}
			}
		}
		
		Map<LocalDate, ListOfWLessons> theWMap = selectedClass.getSchedule().getAllWLessons();
		for(Map.Entry<LocalDate, ListOfWLessons> anEntry : theWMap.entrySet()){
			Iterator<Lesson> it = anEntry.getValue().getAllLessons().iterator();
			while(it.hasNext()){
				Lesson aLesson = it.next();
				if(newDate.isAfter(aLesson.getTheDate())){
					it.remove();
					allLessonsToDelete.add(aLesson);
					weeklyLessonsLists.add(anEntry.getValue());
				}
			}
		}
		System.out.println("Deleting "+allLessonsToDelete.size()+" lessons");
		
		DBUpdateUtils.updateListOfLessonsAndDeleteLessons(extraLessonsLists, weeklyLessonsLists, allLessonsToDelete);
	}
	
	private void deleteLessonsAfterEndDate(Aclass selectedClass, LocalDate newDate) {
		System.out.println("deleteLessonsAfterEndDate()");
		List<Lesson> allLessonsToDelete = new ArrayList<>();
		List<ListOfELessons> extraLessonsLists = new ArrayList<>();
		List<ListOfWLessons> weeklyLessonsLists = new ArrayList<>();
		
		Map<LocalDate, ListOfELessons> theMap = selectedClass.getSchedule().getAllELessons();
		for(Map.Entry<LocalDate, ListOfELessons> anEntry : theMap.entrySet()){
			Iterator<Lesson> it = anEntry.getValue().getAllLessons().iterator();
			while(it.hasNext()){
				Lesson aLesson = it.next();
				if(newDate.isBefore(aLesson.getTheDate())){
					it.remove();
					allLessonsToDelete.add(aLesson);
					extraLessonsLists.add(anEntry.getValue());
				}
			}
		}
		Map<LocalDate, ListOfWLessons> theWMap = selectedClass.getSchedule().getAllWLessons();
		for(Map.Entry<LocalDate, ListOfWLessons> anEntry : theWMap.entrySet()){
			Iterator<Lesson> it = anEntry.getValue().getAllLessons().iterator();
			while(it.hasNext()){
				Lesson aLesson = it.next();
				if(newDate.isBefore(aLesson.getTheDate())){
					it.remove();
					allLessonsToDelete.add(aLesson);
					weeklyLessonsLists.add(anEntry.getValue());
				}
			}
		}
		System.out.println("Deleting "+allLessonsToDelete.size()+" lessons");
		DBUpdateUtils.updateListOfLessonsAndDeleteLessons(extraLessonsLists, weeklyLessonsLists, allLessonsToDelete);
	}
}
