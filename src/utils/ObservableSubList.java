/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.*;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

/**
 *
 * @author d.michaelides
 */
public class ObservableSubList <E extends Object> implements ObservableList{
    private ObservableList<E> fullList;
    private int start;
    private int end;
    
    public ObservableSubList(ObservableList<E> p){
        fullList = p;
        start = 0;
        end = p.size();
    }
    
    

    @Override
    public void addListener(ListChangeListener ll) {
        fullList.addListener(ll);
    }

    @Override
    public void removeListener(ListChangeListener ll) {
        fullList.removeListener(ll);
    }

    @Override
    public boolean setAll(Object... es) {
        try{
            ArrayList<E> list = new ArrayList<>();
        for(Object e :es){
            E ee = (E)e;
            list.add(ee);
        }
        fullList.setAll(list);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public boolean setAll(Collection clctn) {
        try{
            ArrayList<E> list = new ArrayList<>();
        for(Object e :clctn){
            E ee = (E)e;
            list.add(ee);
        }
        fullList.setAll(list);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Object... es) {
        
        try{
            ArrayList<E> list = new ArrayList<>();
        for(Object e :es){
            E ee = (E)e;
            list.add(ee);
        }
        fullList.removeAll(list);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(Object... es) {
        
        try{
            ArrayList<E> list = new ArrayList<>();
        for(Object e :es){
            E ee = (E)e;
            list.add(ee);
        }
        fullList.retainAll(list);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public void remove(int i, int i1) {
        fullList.remove(i, i1);
    }

    @Override
    public int size() {
        return fullList.size();
    }

    @Override
    public boolean isEmpty() {
        return fullList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return fullList.subList(start, end).contains(o);
    }

    @Override
    public Iterator iterator() {
        return fullList.iterator();
    }

    @Override
    public Object[] toArray() {
        return fullList.toArray();
    }

    @Override
    public Object[] toArray(Object[] a) {
        return fullList.toArray(a);
    }

    @Override
    public boolean add(Object e) {
        return fullList.add((E)e);
    }

    @Override
    public boolean remove(Object o) {
        return fullList.remove((E)o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return fullList.containsAll(c);
    }

    @Override
    public boolean addAll(Collection c) {
        return fullList.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return fullList.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return fullList.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection c) {
        return fullList.retainAll(c);
    }

    @Override
    public void clear() {
        fullList.subList(start, end).clear();
    }

    @Override
    public Object get(int index) {
        return fullList.get(index);
    }

    @Override
    public Object set(int index, Object element) {
        return fullList.subList(start, end).set(index, (E)element);
    }

    @Override
    public void add(int index, Object element) {
        fullList.subList(start, end).add(index, (E)element);
    }

    @Override
    public Object remove(int index) {
        return fullList.subList(start, end).remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return fullList.subList(start, end).indexOf((E)o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return fullList.subList(start, end).lastIndexOf((E)o);
    }

    @Override
    public ListIterator listIterator() {
        return fullList.subList(start, end).listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return fullList.subList(start, end).listIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        start = fromIndex;
        end = toIndex;
        return fullList.subList(start, end).subList(fromIndex, toIndex);
    }

    @Override
    public void addListener(InvalidationListener il) {
        fullList.addListener(il);
    }

    @Override
    public void removeListener(InvalidationListener il) {
        fullList.removeListener(il);
    }
    
    public ObservableList<E> showFirstX(int value){
        System.out.println("showFirstX - 2");
        start = 0;
        if(value > fullList.size()){
            value = fullList.size();
        }
        end = value;
        
        ObservableList<E> itemsAtTheStart = FXCollections.observableArrayList();
        for(int i = start; i < end; i++){
            itemsAtTheStart.add(fullList.get(i));
        }
        return itemsAtTheStart;
    }
    public ObservableList<E> nextX(int value){
        System.out.println("nextX - 2");
      
        if((start + value) >= fullList.size()){
            start = fullList.size() - value;
        }else{
            start +=  value;
        }
        
        if((end + value) >= fullList.size()){
            end = fullList.size();
        }else{
            end += value;
        }
        
        System.out.println(start+" -> "+ end );
        List <E> subList = fullList.subList(start, end);
        return FXCollections.observableList(subList);
    }
    public ObservableList<E> previousX(int value){
        System.out.println("previousX - 2");
        if(value >= start){
            start = 0;
        }else{
            start = (start-value);
        }
        if(value >= end){
            end = value;
        }else{
            end = (end-value);
        }
        System.out.println(start+" -> "+ end );
        List <E> subList = fullList.subList(start, end);
        return FXCollections.observableList(subList);
    }

    @Override
    public boolean addAll(Object... es) {
        try{
        for(Object e :es){
            E ee = (E)es;
            fullList.add(ee);
        }
        }catch(Exception e){
            return false;
        }
        return true;
    }
    
    public ObservableList<E> getFullList(){
       start = 0;
       end = fullList.size();
       return fullList;
    }
    
    
    
}
