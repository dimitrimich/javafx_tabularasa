
package utils.pdf;
import java.io.File;
import java.io.IOException;

class OpenFile implements Runnable {
	
	private File arquivo;
	
	public OpenFile(File arquivo) {
		this.arquivo = arquivo;
	}
	
	private void abrirArquivo(File arquivo) throws IOException {
		
		if (arquivo != null) {
			java.awt.Desktop.getDesktop().open(arquivo);
		}
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			abrirArquivo(arquivo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}