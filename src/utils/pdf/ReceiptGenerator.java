/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.pdf;

import DB.objects.PaymentDetails;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import customObjects.MyDecimalFormater;
import java.awt.Color;
import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;
import utils.LanguageUtils;

/**
 *
 * @author DIMITRIM
 */
public class ReceiptGenerator {
	
//	static String SOURCE_logo = "image.jpg";
	static String SOURCE_logo = "image.jpg";
	private PaymentDetails payment;
	private List <String> classesAndMonths;
	
	
	
	public void createPdfReceipt() throws IOException, DocumentException{
		try{
			PdfReader waterMarkReader = WaterMark.addWaterMark();
			
			System.out.println("-------------------------------------------- createPdfReceipt -0 ");
			Document document = new Document(PageSize.A5);
			
			File thePDF = File.createTempFile("receipt", ".pdf");
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(thePDF));
			document.open();
			PdfContentByte canvas = writer.getDirectContent();
			
			PdfImportedPage page;
			page = writer.getImportedPage(waterMarkReader, 1);
			canvas.addTemplate(page, 1f, 0, 0, 1, 0, 0);
			
			if(payment == null){
				throw new Exception ("Don't have a payment to generate report");
			}
			absText(writer, "ANGELINA PAPAGEORGIOU TABULA RASA AP", 115, 540, false);
			absText(writer, "Ανδρέα Ζάκου 555, Μέσα Γειτονία", 140, 530, false);
			
			absText(writer, "76347826337N", 285.5f, 496f, false);
			absText(writer, payment.getReceiptNo(), 285.5f, 481.2f, false);
			
			absText(writer, payment.getPaymentBy(), 135, 453.8f, false);
			
			absText(writer, GreekNumberToWords.convert(payment.getPaidAmount().intValue()), 135, 441f, false);
//			absText(writer, Integer.toString(payment.getPaidAmount().intValue()), 135, 441f, false);
			MyDecimalFormater format = new MyDecimalFormater();
			BigInteger decimal =  payment.getPaidAmount().remainder(BigDecimal.ONE).movePointRight(payment.getPaidAmount().scale()).abs().toBigInteger();
//			absText(writer, decimal.toString(), 135, 427f, false);
			absText(writer, GreekNumberToWords.convert(decimal.intValue()), 135, 427f, false);
			
			
			if(payment.getPaymentMethod().equals(PaymentDetails.PAYMENT_METHOD.CASH)){
				absText(writer, "Μετρητά", 135, 400.5f, false);
				absText(writer, "----", 135, 386.5f, false);
			}else if(payment.getPaymentMethod().equals(PaymentDetails.PAYMENT_METHOD.CHECK)){
				absText(writer, "Επιταγή", 135, 400.5f, false);
				absText(writer, payment.getCheckNo(), 135, 386.5f, false);
			}
			
			float currentHeight = 349;
			if(classesAndMonths != null){
				for(String line : classesAndMonths){
					absText(writer, line, 135, currentHeight, false);
					currentHeight -= 10f;
				}
			}
			absText(writer, format.numberToString(payment.getDiscount().add(payment.getPaidAmount())), 340, 400, false);
			absText(writer, format.numberToString(payment.getDiscount()), 340, 386.5f, false);
			absText(writer, format.numberToString(payment.getPaidAmount()), 340, 373.5f, false);
			
			absText(writer, payment.getPaymentDate().format(DateTimeFormatter.ofPattern("dd/MMM/uuuu", LanguageUtils.getCurrentLocale())), 135, (float)115.5, false);
			
			document.close();
			
			thePDF.setExecutable(true);
			thePDF.deleteOnExit();
			
			if (Desktop.isDesktopSupported()) {
				System.err.println("Will open the PDF");
				OpenFile openFile = new OpenFile(thePDF);
				Thread threadOpenFile = new Thread(openFile);
				threadOpenFile.start();
			}else{
				System.err.println("Could not open pdf file - No Desktop");
			}
		}catch(Exception e){
			
			System.err.println("Exception while creating PDF");
			e.printStackTrace(System.err);
		}
	}
	private static BaseFont getFont(){
		try{
			Font font = FontFactory.getFont("/utils/pdf/GentiumPlus-R.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 0.8f, Font.NORMAL);
			BaseFont baseFont = font.getBaseFont();
			return baseFont;
		}catch(Exception e){
			e.printStackTrace(System.err);
		}
		return null;
	}
	private static void absText(PdfWriter writer, String text, float x, float y, boolean isRed) {
		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf;
			if(false){
				bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			}else{
				bf = getFont();
			}
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setColorFill(Color.black);
			if(isRed){
				cb.setColorFill(Color.red);
			}
			cb.setFontAndSize(bf, 8);
			cb.showText(text);
			cb.endText();
			cb.restoreState();
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Image getImage(){
		try {
			Image img = Image.getInstance(getBytes());
			return img;
		} catch (BadElementException | IOException ex) {
			
			System.err.println("EXCEPTION ");
			ex.printStackTrace(System.err);
		}
		
		return null;
	}
	
	private static byte[] getBytes(){
		File file = new File(SOURCE_logo);
		Path path = Paths.get(file.getAbsolutePath());
		ByteArrayInputStream is = null;
		try {
			byte[] data = Files.readAllBytes(path);
			return data;
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException ex) {
					ex.printStackTrace(System.err);
				}
			}
		}
		return null;
	}
	
	public PaymentDetails getPayment() { return payment; }
	public void setPayment(PaymentDetails payment) { this.payment = payment; }
	public List<String> getClassesAndMonths() { return classesAndMonths; }
	public void setClassesAndMonths(List<String> classesAndMonths) { this.classesAndMonths = classesAndMonths; }
	
	
}
