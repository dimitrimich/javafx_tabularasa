package utils.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import utils.LanguageUtils;
import static utils.Utils.toByteArray;
import static utils.pdf.ReceiptGenerator.SOURCE_logo;

public class WaterMark {
	
	public static PdfReader addWaterMark(){
		try {
			PdfReader Read_PDF_To_Watermark = null;
			if(LanguageUtils.getCurrentLocale().equals(new Locale("el"))){
				Read_PDF_To_Watermark = new PdfReader(WaterMark.class.getResource("receiptTemplate_A5gr.pdf").toExternalForm());
			}else if (LanguageUtils.getCurrentLocale().equals(Locale.ENGLISH)){
				Read_PDF_To_Watermark = new PdfReader(WaterMark.class.getResource("receiptTemplate_A5en.pdf").toExternalForm());
			}else{
				Read_PDF_To_Watermark = new PdfReader(WaterMark.class.getResource("receiptTemplate_A5en.pdf").toExternalForm());
			}
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, baos);
			Image watermark_image = Image.getInstance(getBytes());
			
			PdfContentByte add_watermark;
			PdfGState state = new PdfGState();
			state.setFillOpacity(0.15f);
			float widthOfPage = Read_PDF_To_Watermark.getPageSize(1).getWidth();
			float heightOfImage = watermark_image.getHeight();
			float correctScale = (250 / heightOfImage) * 100;
			float imageNewWidth = watermark_image.getWidth() * (correctScale/100);
			float xPosition = (widthOfPage - imageNewWidth) / 2;
			watermark_image.setAbsolutePosition(xPosition, (float)209.76);
			watermark_image.scalePercent(correctScale);
			add_watermark = stamp.getUnderContent(1);
			add_watermark.setGState(state);
			add_watermark.addImage(watermark_image);
			
			stamp.close();
			PdfReader toReturn = new PdfReader(baos.toByteArray());
			return toReturn;
		}
		catch (IOException | DocumentException i1) {
			i1.printStackTrace(System.err);
		}
		
		System.out.println("returning null _===");
		return null;
		
	}
	
	private static byte[] getBytes(){
		
		InputStream stream = WaterMark.class.getResourceAsStream(SOURCE_logo);
		try {
			byte[] data = toByteArray(stream);
			return data;
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}
		return null;
	}
}