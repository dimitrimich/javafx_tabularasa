package utils.pdf;

import java.text.DecimalFormat;
/**
 * @author  http://www.rgagnon.com/javadetails/java-0426.html
 */
public class GreekNumberToWords {
	
	private static final String[] tensNames = {
		"",
		" δέκα", " είκοσι", " τριάντα", " σαράντα",
		" πενήντα",
		" εξήντα",
		" εβδομήντα",
		" ογδόντα",
		" ενενήντα"
	};
	
	private static final String[] numNames = {
		"",
		" ένα", " δύο", " τρία", " τέσσερα", " πέντε",
		" έξι", " εφτά", " οκτώ", " εννέα", " δέκα",
		" έντεκα", " δώδεκα", " δεκατρία", " δεκατέσσερα", " δεκαπέντε",
		" δεκαέξι", " δεκαεφτά", " δεκαοκτώ", " δεκαεννία"
	};
	private static final String[] hundreds = {
		"",
		" εκατό", " διακόσια", " τριακόσια", " τετρακόσια",
		" πεντακόσια",
		" εξακόσια",
		" εφτακόσια",
		" οκτακόσια",
		" εννιακόσια"
	};
	
	private GreekNumberToWords() {}
	
	private static String convertLessThanOneThousand(int number) {
		String soFar;
		
		if (number % 100 < 20){
			soFar = numNames[number % 100];
			number /= 100;
		}
		else {
			soFar = numNames[number % 10];
			number /= 10;
			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0) return soFar;
		
		return hundreds[number] + soFar;
	}
	
	
	public static String convert(long number) {
		// 0 to 999 999 999 999
		if (number == 0) { return "μηδέν"; }
		
		String snumber = Long.toString(number);
		
		// pad with "0"
		String mask = "000000000000";
		DecimalFormat df = new DecimalFormat(mask);
		snumber = df.format(number);
		
		// XXXnnnnnnnnn
		int billions = Integer.parseInt(snumber.substring(0,3));
		// nnnXXXnnnnnn
		int millions  = Integer.parseInt(snumber.substring(3,6));
		// nnnnnnXXXnnn
		int hundredThousands = Integer.parseInt(snumber.substring(6,9));
		// nnnnnnnnnXXX
		int thousands = Integer.parseInt(snumber.substring(9,12));
		
		String tradBillions;
		switch (billions) {
			case 0:
				tradBillions = "";
				break;
			case 1 :
				tradBillions = convertLessThanOneThousand(billions)
						+ " δισεκατομμύριο(α) ";
				break;
			default :
				tradBillions = convertLessThanOneThousand(billions)
						+ " δισεκατομμύριο(α) ";
		}
		String result =  tradBillions;
		
		String tradMillions;
		switch (millions) {
			case 0:
				tradMillions = "";
				break;
			case 1 :
				tradMillions = convertLessThanOneThousand(millions)
						+ " εκατομμύριο/α ";
				break;
			default :
				tradMillions = convertLessThanOneThousand(millions)
						+ " εκατομμύριο/α ";
		}
		result =  result + tradMillions;
		
		String tradHundredThousands;
		switch (hundredThousands) {
			case 0:
				tradHundredThousands = "";
				break;
			case 1 :
				tradHundredThousands = "χίλια ";
				break;
			default :
				tradHundredThousands = convertLessThanOneThousand(hundredThousands)
						+ " χιλιάδες ";
		}
		result =  result + tradHundredThousands;
		
		String tradThousand;
		tradThousand = convertLessThanOneThousand(thousands);
		result =  result + tradThousand;
		
		// remove extra spaces!
		return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	}
	
	/**
	 * testing
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("*** " + GreekNumberToWords.convert(0));
		System.out.println("*** " + GreekNumberToWords.convert(1));
		System.out.println("*** " + GreekNumberToWords.convert(16));
		System.out.println("*** " + GreekNumberToWords.convert(100));
		System.out.println("*** " + GreekNumberToWords.convert(118));
		System.out.println("*** " + GreekNumberToWords.convert(200));
		System.out.println("*** " + GreekNumberToWords.convert(219));
		System.out.println("*** " + GreekNumberToWords.convert(800));
		System.out.println("*** " + GreekNumberToWords.convert(801));
		System.out.println("*** " + GreekNumberToWords.convert(1316));
		System.out.println("*** " + GreekNumberToWords.convert(1000000));
		System.out.println("*** " + GreekNumberToWords.convert(2000000));
		System.out.println("*** " + GreekNumberToWords.convert(3000200));
		System.out.println("*** " + GreekNumberToWords.convert(700000));
		System.out.println("*** " + GreekNumberToWords.convert(9000000));
		System.out.println("*** " + GreekNumberToWords.convert(9001000));
		System.out.println("*** " + GreekNumberToWords.convert(123456789));
		System.out.println("*** " + GreekNumberToWords.convert(2147483647));
		System.out.println("*** " + GreekNumberToWords.convert(3000000010L));
		
		/*
		*** zero
		*** one
		*** sixteen
		*** one hundred
		*** one hundred eighteen
		*** two hundred
		*** two hundred nineteen
		*** eight hundred
		*** eight hundred one
		*** one thousand three hundred sixteen
		*** one million
		*** two millions
		*** three millions two hundred
		*** seven hundred thousand
		*** nine millions
		*** nine millions one thousand
		*** one hundred twenty three millions four hundred
		**      fifty six thousand seven hundred eighty nine
		*** two billion one hundred forty seven millions
		**      four hundred eighty three thousand six hundred forty seven
		*** three billion ten
		**/
	}
}