/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import customControls.TimeCell;
import customControls.TimeTableCell;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author d.michaelides
 */
public class TimeTableUtils {
    public static ArrayList<Node> createEmptyDay(VBox day, int startingHour, int hours, final TextArea selectedClass){
        
        System.out.println("createEmptyDay() - Thred name "+Thread.currentThread().getName());
        try{
        ArrayList<Node> list = new ArrayList<>();
        for(int hour = startingHour; hour < (startingHour+hours); hour++){
            for(int minute = 0; minute < 60; minute += 5 ){
                TimeTableCell timeSlot =  new TimeTableCell();
                initializeTimeTableCell(timeSlot, selectedClass);
                list.add(timeSlot);
            }
        }
        return list;
        }catch(Exception e){
            e.printStackTrace(System.err);
            return null;
        }
    }
    
    public static ArrayList<Node> createTimes(VBox times, int startingHour, int hours){
        ArrayList<Node> list = new ArrayList<>();
        for(int hour = startingHour; hour < (startingHour+hours); hour++){
            TimeCell startingMinute = new TimeCell(hour, 0);
            TimeCell halfHour = new TimeCell(hour, 30);
            list.add(startingMinute);
            list.add(halfHour);
        }
        return list;
    }
    
    public static void swapClassesOfTheSameDay(VBox day, TimeTableCell class1, TimeTableCell class2){
        TimeTableCell temp1 = new TimeTableCell();
        TimeTableCell temp2 = new TimeTableCell();
        FXCollections.replaceAll(day.getChildren(), class1, temp1);
        FXCollections.replaceAll(day.getChildren(), class2, temp2);
        FXCollections.replaceAll(day.getChildren(), temp1, class2);
        FXCollections.replaceAll(day.getChildren(), temp2, class1);
    }
    
    public static void switchCells(VBox v, int newPosition, int oldPosition){
        Collections.swap(v.getChildren(), newPosition, oldPosition);
    }
    
    public static void moveClass(VBox targetDay, TimeTableCell cellToBeAdded, TimeTableCell target, final TextArea showDetails, boolean fromStartingList){
        int positionToAdd = targetDay.getChildren().indexOf(target);
        VBox sourceDay = ((VBox)cellToBeAdded.getParent());
        int sourceClassIndex = sourceDay.getChildren().indexOf(cellToBeAdded);
        int amountToRemove = ((Integer.parseInt(cellToBeAdded.getDuration()) / 30) * 6) - 1;
        
        if(targetDay.getChildren().contains(cellToBeAdded)){
            // moving a class to different hour in the same day
            TimeTableCell temp1 = new TimeTableCell();
            initializeTimeTableCell(temp1, showDetails);
            FXCollections.replaceAll(targetDay.getChildren(), cellToBeAdded, temp1);
            FXCollections.replaceAll(targetDay.getChildren(), target, cellToBeAdded);
            
            if(positionToAdd > sourceClassIndex){
                // Remove That were replaced by moved class
                int toRemoveIndex = positionToAdd + 1;
                if(!"not".equals(targetDay.getId())){
                    for(int i = 0; i < amountToRemove; i++){
                        // index is always the same because when you remove index becomes smaller
                        targetDay.getChildren().remove(toRemoveIndex);
                    }
                }
                if(!"not".equals(targetDay.getId())){
                    // add slots to previous position of moved class
                    for(int i = sourceClassIndex; i < (sourceClassIndex + amountToRemove); i++){
                        TimeTableCell toAdd = new TimeTableCell();
                        initializeTimeTableCell(toAdd, showDetails);
                        sourceDay.getChildren().add(sourceClassIndex, toAdd);
                    }
                }
            }
            return;
        }
        // moving a class to different day
        targetDay.getChildren().set(positionToAdd, cellToBeAdded);
        
        // remove slots replaced by dropped Class
        int toRemoveIndex = (targetDay.getChildren().indexOf(cellToBeAdded) + 1);
        
        if(!"not".equals(targetDay.getId())){
            for(int i = 0; i < amountToRemove; i++){
                // index is always the same because when you remove index becomes smaller
                targetDay.getChildren().remove(toRemoveIndex);
            }
        }

        if("not".equals(sourceDay.getId())){
           // add 1 slot
           TimeTableCell toAdd = new TimeTableCell();
           initializeTimeTableCell(toAdd, showDetails);
           sourceDay.getChildren().add(toAdd);
        }else{
            // add slots to source day
            for(int i = sourceClassIndex; i < (sourceClassIndex + amountToRemove + 1); i++){
                TimeTableCell toAdd = new TimeTableCell();
                initializeTimeTableCell(toAdd, showDetails);
                sourceDay.getChildren().add(sourceClassIndex, toAdd);
            }
        }
    }
    
    
    public static void initializeTimeTableCells(VBox day, TextArea showDetails){
        Iterator <Node> it = (Iterator)day.getChildren().iterator();
        
        while(it.hasNext()){
            Node n = it.next();
            TimeTableCell l = (TimeTableCell)n;
            TimeTableHandlers.setOnDragDetected(l);
            l.setOnDragDone(TimeTableHandlers.getOnDragDoneHandler(l));
            TimeTableHandlers.setOnDragOver(l);
            TimeTableHandlers.setOnDragEntered(l);
            TimeTableHandlers.setOnDragExited(l);
            TimeTableHandlers.setOnDragDropped(l, showDetails);
            TimeTableHandlers.setOnRightClick(l);
            TimeTableHandlers.setOnClicked(l, showDetails);
        }
    }
    public static void initializeTimeTableCell(TimeTableCell cell, TextArea showDetails){
        TimeTableHandlers.setOnDragDetected(cell);
        cell.setOnDragDone(TimeTableHandlers.getOnDragDoneHandler(cell));
        TimeTableHandlers.setOnDragOver(cell);
        TimeTableHandlers.setOnDragEntered(cell);
        TimeTableHandlers.setOnDragExited(cell);
        TimeTableHandlers.setOnDragDropped(cell, showDetails);
        TimeTableHandlers.setOnRightClick(cell);
        TimeTableHandlers.setOnClicked(cell, showDetails);
    }
    
    
    public static void initializeCells(GridPane table){
//        Iterator <Node> it = (Iterator)table.getChildren().iterator();
//        
//        while(it.hasNext()){
//            Node n = it.next();
//            Label l = (Label)n;
//            DragNDropHandlers_Cells.setOnDragDetected(l);
//            l.setOnDragDone(DragNDropHandlers_Cells.getOnDragDoneHandler(l));
//            DragNDropHandlers_Cells.setOnDragOver(l);
//            DragNDropHandlers_Cells.setOnDragEntered(l);
//            DragNDropHandlers_Cells.setOnDragExited(l);
//            DragNDropHandlers_Cells.setOnDragDropped(l);
//        }
    }
    
    
    public static void initializeTextAreas(VBox table){
//        Iterator <Node> it = (Iterator)table.getChildren().iterator();
//        
//        while(it.hasNext()){
//            Node n = it.next();
//            TextArea l = (TextArea)n;
//            DragNDropHandlers_New.setOnDragDetected(l);
//            l.setOnDragDone(DragNDropHandlers_New.getOnDragDoneHandler(l));
//            DragNDropHandlers_New.setOnDragOver(l);
//            DragNDropHandlers_New.setOnDragEntered(l);
//            DragNDropHandlers_New.setOnDragExited(l);
//            DragNDropHandlers_New.setOnDragDropped(l);
//        }
    }
    
    public static int getFirstIndexNotOccupied(VBox day){
        int i =  0;
        for(Node n : day.getChildren()){
            if(n instanceof TimeTableCell){
                if("".equals(((TimeTableCell)n).getText())){
                    return i;
                }
            }
            i++;
        }
        return -1;
    }
    
    public static boolean doesItFit(VBox day, TimeTableCell cellToAdd, TimeTableCell target){
        
        if(day.getId().equals("not")){
            return !(target.isClassAssigned());
        }
        int timeUntilNext = 0;
        int positionToAdd = day.getChildrenUnmodifiable().indexOf(target);
        for(int i = positionToAdd; i < day.getChildrenUnmodifiable().size(); i++){
            if(((TimeTableCell)day.getChildrenUnmodifiable().get(i)).isClassAssigned() && (((TimeTableCell)day.getChildrenUnmodifiable().get(i)) != cellToAdd)){
                break;
            }
            if((((TimeTableCell)day.getChildrenUnmodifiable().get(i)) == cellToAdd)){
                timeUntilNext += cellToAdd.getPrefHeight();
            }else{
                timeUntilNext += 5;
            }
            
        }
        return (Integer.parseInt(cellToAdd.getDuration()) <= timeUntilNext);
    }
    
    public static void swapClassesFromDifferentDays(VBox sourceDay, TimeTableCell source, TimeTableCell target, TextArea showDetails){
            
            //1. create temporary with value of target
            TimeTableCell targetCopied = new TimeTableCell(target);
            //1.a    add listeners to temporary
            TimeTableUtils.initializeTimeTableCell(targetCopied, showDetails);
            //1.b   change positionInDay of temporary
//            targetCopied.setPositionInDay(source.getPositionInDay());
            //2. change source positionInDay
//            source.setPositionInDay(target.getPositionInDay());
            //2.a replace target with source
            Collections.replaceAll(((VBox) target.getParent()).getChildren(), target, source);
            if("Does not fit".equals(targetCopied.getText())){
                targetCopied.setText(targetCopied.getName());
            }
            //3. Add temporary in position source was before
//            sourceDay.getChildren().add(source.getPositionInDay(), targetCopied);
            FXCollections.replaceAll(sourceDay.getChildren(), source, targetCopied);
//            sourceDay.getChildren().replaceAll(=targetCopied);//source.getPositionInDay(), targetCopied);
    
    
    }
    
}
