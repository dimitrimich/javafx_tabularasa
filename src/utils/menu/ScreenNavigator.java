/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package utils.menu;

import controllers.HomePageController;
import controllers.StageController;
import java.io.IOException;
import java.util.HashMap;
import javafx.stage.Stage;
import utils.Constants;

/**
 *
 * @author d.michaelides
 */
public class ScreenNavigator {
	private final Stage stage;
	private static int currentScreen = -1;
	private static String currentScreenName = "";
	private static final HashMap<String, StageController> controllers = new HashMap<>();
	private static ScreenNavigator singleton;
	
	private ScreenNavigator(Stage s){
		stage = s;
	}
	
	public static ScreenNavigator getInstance(Stage s){
		if(singleton == null){
			singleton = new ScreenNavigator(s);
		}
		return singleton;
	}
	public static HashMap<String, StageController> getControllersMap(){
		return controllers;
	}
	
	
	public void goToScreen(String screenName) throws IOException{
		
		if(Constants.screens.get(screenName) == Constants.SCREEN_HOME){
			currentScreen = Constants.SCREEN_HOME;
			currentScreenName = screenName;
			stage.setScene(null);
			stage.setScene(controllers.get(screenName).getScene());
		}else{
			if(currentScreen != Constants.screens.get(screenName)){
				controllers.get(currentScreenName).unload();
				currentScreen = Constants.screens.get(screenName);
				((HomePageController)controllers.get("HomePage")).goToScreen(screenName);
				controllers.get(screenName).onLoad();
				currentScreenName = screenName;
			}
		}
	}
}
