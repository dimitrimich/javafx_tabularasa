/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.menu;

import java.io.IOException;
import java.io.InputStream;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import utils.LanguageUtils;

/**
 * Builds a tree menu !! not used at the moment
 * @author d.michaelides
 */
public class TreeMenuBuilder {
    private static TreeView theMenu;
    
    public static TreeItem<String> getTheMenuRoot(){
        return theMenu.getRoot();
    }
    
    public static void buildTheMenu(){
        if(theMenu == null){
            theMenu = new TreeView();
            try{
                TreeItem<String> root = new TreeItem<> (LanguageUtils.getString("menu.home"), getIcon("home.png"));
                root.setExpanded(true);
                TreeItem<String> students = new TreeItem<> (LanguageUtils.getString("menu.students"), getIcon("student.png"));
                    TreeItem<String> students_create = new TreeItem<> (LanguageUtils.getString("menu.create"));         
                    students.getChildren().add(students_create);
                    TreeItem<String> item2 = new TreeItem<> (LanguageUtils.getString("menu.view.all"));         
                    students.getChildren().add(item2);

                TreeItem<String> teachers = new TreeItem<> (LanguageUtils.getString("menu.teachers"), getIcon("teacher.png"));
                    TreeItem<String>teachers_create = new TreeItem<> (LanguageUtils.getString("menu.create"));         
                    teachers.getChildren().add(teachers_create);
                    TreeItem<String> teachers2 = new TreeItem<> (LanguageUtils.getString("menu.view.all"));         
                    teachers.getChildren().add(teachers2);

                TreeItem<String> classes = new TreeItem<> (LanguageUtils.getString("menu.classes"), getIcon("class.png"));
                    TreeItem<String>class_create = new TreeItem<> (LanguageUtils.getString("menu.create"));         
                    classes.getChildren().add(class_create);
                    TreeItem<String> classes2 = new TreeItem<> (LanguageUtils.getString("menu.view.all"));
                    classes.getChildren().add(classes2);
                TreeItem<String> billingEntry = new TreeItem<> (LanguageUtils.getString("menu.billings"), getIcon("dollar_16px.png"));
                    TreeItem<String>billing_entry = new TreeItem<> (LanguageUtils.getString("menu.billings.new"), getIcon("new_payment_16px.png"));
                    TreeItem<String>billing_edit = new TreeItem<> (LanguageUtils.getString("menu.billings.cancel"), getIcon("remove_payment_16px.png"));
                    billingEntry.getChildren().add(billing_entry);
                    billingEntry.getChildren().add(billing_edit);
//                TreeItem<String> other = new TreeItem<> ("Other", getIcon("other.png"));
//                    TreeItem<String>billing = new TreeItem<> ("Billing");         
//                    TreeItem<String>billingStudents = new TreeItem<> ("Billing Students");         
//                    TreeItem<String>other_table = new TreeItem<> ("Table");         
//                    TreeItem<String>other_time_table = new TreeItem<> ("Time Table");     
//                    TreeItem<String>other_time_tableNew = new TreeItem<> ("Time Table New");         
//                    other.getChildren().add(billing);       
//                    other.getChildren().add(billingStudents);
//                    other.getChildren().add(other_table);      
//                    other.getChildren().add(other_time_table);
//                    other.getChildren().add(other_time_tableNew);
                    
                TreeItem<String> settings = new TreeItem<> (LanguageUtils.getString("menu.settings"), getIcon("settings_icon.png"));

                root.getChildren().add(students);
                root.getChildren().add(teachers);
                root.getChildren().add(classes);
                root.getChildren().add(billingEntry);
//                root.getChildren().add(other);
TreeItem<String> temp = new TreeItem<> ("Temp", getIcon("settings_icon.png"));

                root.getChildren().add(settings);
                root.getChildren().add(temp);
                
                theMenu.setRoot(root);
            }catch(Exception e){
                e.printStackTrace(System.out);
            }
        }
    }
    
    private static ImageView getIcon(String iconName){
        InputStream is = null;
        try{
            is = ClassLoader.getSystemResourceAsStream("images/icons/"+iconName);
            ImageView icon =  new ImageView(new Image(is,16,16,true,true));
            return icon;
        }
        finally{
            if(is != null){
                try {
                    is.close();
                } catch (IOException iOException) {
                    System.err.println("44444 "+iOException.toString());
                }
            }
        }
    }
}
