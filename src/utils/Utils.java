package utils;

import DB.objects.Aclass;
import DB.objects.Schedule;
import DB.objects.Student;
import DB.objects.Teacher;
import customObjects.ClassForTests;
import customObjects.Person;
import images.Images;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;

/**
 *
 * @author d.michaelides
 */
public class Utils {
	
	private static InnerShadow red;
	public static boolean notEqual(TextField tField, String text){
//        boolean fieldIsValid = isValidParam(tField.getText());
//        boolean textIsValid = isValidParam(text);
//
//        if(fieldIsValid && !textIsValid){
//            System.out.println("Compare -1- notEqual");
//            return true;
//        }
//        if(!fieldIsValid && textIsValid){
//            System.out.println("Compare -2- notEqual");
//            return true;
//        }
//        if(!fieldIsValid && !textIsValid){
//            System.out.println("Compare -3- Equal");
//            return false;
//        }
		return !(notNull(tField.getText()).trim().equals(notNull(text).trim()));
	}
	
	public static boolean isValidParam(String param) {
		if (param == null) {
			return false;
		}
		return !param.trim().isEmpty();
	}
	
	public static String notNull(String param) {
		if (param == null) {
			return "";
		}
		return param;
	}
	
	public static String getDayOfWeek(int i) {
		switch (i) {
			case 1:
				return "Sunday";
			case 2:
				return "Monday";
			case 3:
				return "Tuesday";
			case 4:
				return "Wednesday";
			case 5:
				return "Thursday";
			case 6:
				return "Friday";
			case 7:
				return "Saturday";
			default:
				return "N/A";
		}
	}
	public static String getDayOfWeekShort(int i) {
		switch (i) {
			case 1:
				return "Su";
			case 2:
				return "Mo";
			case 3:
				return "Tu";
			case 4:
				return "We";
			case 5:
				return "Th";
			case 6:
				return "Fr";
			case 7:
				return "Sa";
			default:
				return "N/A";
		}
	}
	
	public static int getDayOfWeek(String day) {
		switch (day) {
			case "Monday":
				return 2;
			case "Tuesday":
				return 3;
			case "Wednesday":
				return 4;
			case "Thursday":
				return 5;
			case "Friday":
				return 6;
			case "Saturday":
				return 7;
			case "Sunday":
				return 1;
			default:
				return -1;
		}
	}
	
	public static InnerShadow getRedShadow() {
		if (red == null) {
			red = new InnerShadow();
			red.setChoke(0);
			red.setHeight(10);
			red.setWidth(10);
			red.setBlurType(BlurType.THREE_PASS_BOX);
			red.setOffsetX(0);
			red.setOffsetY(0);
			red.setRadius(7);
			red.setColor(Color.hsb(0, 1, 1));
		}
		return red;
	}
	
	public static Tooltip getDefaultTooltip(String text) {
		Tooltip t = new Tooltip(text);
		t.setFont(Font.font("Garamond", FontWeight.SEMI_BOLD, 13));
		return t;
	}
	
	public static void turnNodeRed(Control node, String tooltipText) {
		node.setEffect(Utils.getRedShadow());
		if (isValidParam(tooltipText)) {
			node.setTooltip(Utils.getDefaultTooltip(tooltipText));
		}
		if (node instanceof TextField) {
			((TextField) node).textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
				node.setEffect(null);
				node.setTooltip(null);
			});
		}
		if (node instanceof ComboBox) {
			((ComboBox) node).focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
				if (oldValue == false && newValue == true) {
					node.setEffect(null);
					node.setTooltip(null);
				}
			});
			((ComboBox) node).disabledProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
				if (oldValue == false && newValue == true) {
					node.setEffect(null);
					node.setTooltip(null);
				}
			});
		}
	}
	
	public static ObservableList<Teacher> getTeachersList(int sizeOfArray) {
		ArrayList<Teacher> list = new ArrayList<>();
		list.ensureCapacity(sizeOfArray);
		
		Random rnd = new Random(new Date().getTime());
		for (int i = 0; i < sizeOfArray; i++) {
		}
		
		return FXCollections.observableArrayList(list);
	}
	
	public static ObservableList<ClassForTests> createClassesForTests(int sizeOfArray) {
		ArrayList<ClassForTests> list = new ArrayList<>();
		list.ensureCapacity(sizeOfArray);
		
		Random rnd = new Random(new Date().getTime());
		for (int i = 0; i < sizeOfArray; i++) {
			ClassForTests a = new ClassForTests(randomString(12), randomString(20), randomString(7) + " " + randomString(10),
					Integer.toString(rnd.nextInt(7)), Integer.toString(rnd.nextInt(7)), Integer.toString(rnd.nextInt(7)), Integer.toString(rnd.nextInt(7)),
					Integer.toString(rnd.nextInt(120)), Integer.toString(rnd.nextInt(60)), Integer.toString(rnd.nextInt(90)), Integer.toString(rnd.nextInt(120)),
					Integer.toString(rnd.nextInt(24)), Integer.toString(rnd.nextInt(24)), Integer.toString(rnd.nextInt(90)), Integer.toString(rnd.nextInt(24)),
					Integer.toString(rnd.nextInt(60)), Integer.toString(rnd.nextInt(60)), Integer.toString(rnd.nextInt(60)), Integer.toString(rnd.nextInt(60)));
			
			list.add(a);
		}
		return FXCollections.observableArrayList(list);
	}
	
	public static ObservableList<Aclass> createClasses(int sizeOfArray) {
		ArrayList<Aclass> list = new ArrayList<>();
		list.ensureCapacity(sizeOfArray);
		
		Random rnd = new Random(new Date().getTime());
		for (int i = 0; i < sizeOfArray; i++) {
		}
		return FXCollections.observableArrayList(list);
	}
	
	public static ObservableList<Schedule> createSchedules(int sizeOfArray) {
		ArrayList<Schedule> theList = new ArrayList<>(sizeOfArray);
		theList.ensureCapacity(sizeOfArray);
		Random rnd = new Random(new Date().getTime());
		
		for (int i = 0; i < sizeOfArray; i++) {
			Schedule s = null;
			int leftOver = i % 4;
			switch (leftOver) {
				case 0:
					if (rnd.nextBoolean()) {
						s = createSchedule2x2(0, -1);
					} else {
						s = createSchedule2x2(1, 4);
					}
					break;
				case 1:
					s = createSchedule1x2(rnd.nextInt(8));
					break;
				case 2:
					if (rnd.nextBoolean()) {
						s = createSchedule3x1(rnd.nextInt(8), -1, rnd.nextInt(8));
					} else {
						s = createSchedule3x1(rnd.nextInt(8), rnd.nextInt(8), rnd.nextInt(8));
					}
					break;
				case 3:
					if (rnd.nextBoolean()) {
						s = createSchedule3x1(rnd.nextInt(8), -1, rnd.nextInt(8));
					} else {
						s = createSchedule3x1(-1, rnd.nextInt(8), -1);
					}
					break;
			}
			theList.add(s);
		}
		
		return FXCollections.observableArrayList(theList);
	}
	
	public static Schedule createSchedule2x2(int day1, int day2) {
		Random rnd = new Random(new Date().getTime());
		return null;
	}
	
	public static Schedule createSchedule1x2(int day1) {
		return null;
	}
	
	public static Schedule createSchedule3x1(int day1, int day2, int day3) {
		return null;
	}
	
	public static ArrayList<Person> getPersonsList(int sizeOfArray) {
		ArrayList<Person> list = new ArrayList<>();
		list.ensureCapacity(sizeOfArray);
		
		Random rnd = new Random(new Date().getTime());
		for (int i = 0; i < sizeOfArray; i++) {
			Person p = new Person(i + 1, randomString(7), randomString(10), rnd.nextInt(2), randomDouble(), randomBoolean());
			list.add(p);
		}
		
		return list;
	}
	
	public static ArrayList<Student> getStudentsList(int sizeOfArray) {
		InputStream stream = null;
		InputStream stream2 = null;
		InputStream stream3 = null;
		
		try {
			
//            File file = new File(Images.class.getResource("default_profile.png").toURI());
			stream = Images.class.getResourceAsStream("default_profile.jpg");
			byte[] data = toByteArray(stream);
			SerialBlob thePic1 = new SerialBlob(data);
			
			stream2 = Images.class.getResourceAsStream("default_profile.png");
			byte[] data2 = toByteArray(stream2);
			SerialBlob thePic2 = new SerialBlob(data2);
			
			stream3 = Images.class.getResourceAsStream("profile_2.png");
			byte[] data3 = toByteArray(stream3);
			SerialBlob thePic3 = new SerialBlob(data3);
			
			ArrayList<Student> list = new ArrayList<>();
			list.ensureCapacity(sizeOfArray);
			
			return list;
		} catch (Exception ee) {
			System.err.println(" exception while creating images");
			ee.printStackTrace(System.err);
			return null;
		}finally{
			try{
				stream.close();
				stream2.close();
				stream3.close();
			}catch(Exception e){
				System.err.println("Exception on close()-12345");
			}
			
		}
	}
	
	private static String randomAddress() {
		Random rnd = new Random(new Date().getTime());
		// michael karaoli 12, Pareklishia, TK 4046, Lemesos, Kipros
		StringBuilder sb = new StringBuilder();
		sb.append(randomString(10));
		sb.append(" ");
		sb.append(randomString(3));
		sb.append(" ");
		sb.append(rnd.nextInt(2));
		sb.append(", ");
		sb.append("Perioxi");
		sb.append(",  ");
		sb.append("TK: ");
		sb.append(rnd.nextInt(4));
		sb.append(", ");
		sb.append("Lemesos, Kipros.");
		return sb.toString();
	}
	
	private static String randomEmail() {
		Random rnd = new Random(new Date().getTime());
		// michael karaoli 12, Pareklishia, TK 4046, Lemesos, Kipros
		StringBuilder sb = new StringBuilder();
		sb.append(randomString(10));
		sb.append("@");
		sb.append(randomString(7));
		sb.append(".com");
		return sb.toString();
	}
	
	private static String randomString(int len) {
		String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//        String AB = "ABCDEFGHIJKLMNOΑΣΔΦΓΗΞΚΛΖΧΨΩΒΝΡΥΘΙΟ";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}
	
	private static String randomPhone(boolean mobile) {
		String AB = "0123456789";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(8);
		if (mobile) {
			sb.append("99");
		} else {
			sb.append("25");
		}
		for (int i = 2; i < 8; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}
	
	private static double randomDouble() {
		Random rnd = new Random();
		return 100 + rnd.nextDouble();
	}
	
	private static boolean randomBoolean() {
		Random rnd = new Random();
		return rnd.nextBoolean();
	}
	
	public static byte[] toByteArray(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try{
			int reads = is.read();
			while (reads != -1) {
				baos.write(reads);
				reads = is.read();
			}
			return baos.toByteArray();
		}
		finally{
			try{
				baos.close();
			}
			catch(Exception er){
				System.err.println("close Exception 45");
			}
		}
	}
	
	public static byte[] toByteArray(Image anImage) throws Exception {
		BufferedImage bImage = SwingFXUtils.fromFXImage(anImage, null);
		
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		try{
			ImageIO.write(bImage, "png", outStream);
			outStream.flush();
			return outStream.toByteArray();
		}
		finally{
			try{
				outStream.close();
			}catch(Exception e){
				System.err.println("close errror 45");
			}
		}
	}
	
	public static byte[] readFully(InputStream input) throws IOException {
		byte[] buffer = new byte[8192];
		int bytesRead;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try{
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
			return output.toByteArray();
		}
		finally{
			try{
				output.close();
				
			}catch(Exception e){
				System.err.println("error close"+ e.getMessage());
			}
		}
	}
	
	// Returns the contents of the file in a byte array.
	public static byte[] getBytesFromFile(File file) throws IOException {
		byte[] bytes;
		// Get the size of the file
		InputStream is = null;
		
		try{
			is = new FileInputStream(file);
			// Get the size of the file
			long length = file.length();
			// You cannot create an array using a long type.
			// It needs to be an int type.
			// Before converting to an int type, check
			// to ensure that file is not larger than Integer.MAX_VALUE.
			if (length > Integer.MAX_VALUE) {
				// File is too large
			}   // Create the byte array to hold the data
			bytes = new byte[(int) length];
			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}   // Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
			
			return bytes;
		}
		finally{
			try {
				is.close();
			} catch (IOException iOException) {
				
				System.err.println("close exception - "+iOException.getMessage());
			}
		}
	}
	
	public static boolean isValidID(Long objectID){
		return (objectID != null && objectID != 0L);
	}
	
	public static List<String> colors = Arrays.asList("aqua", "seagreen", "cadetblue", "blue", "chartreuse", "darkgray", "cyan", "darkgreen", "darkorange", "darkviolet", "darkslateblue", "firebrick", "goldenrod", "indianred", "greenyellow", "maroon", "olive", "yellow", "red", "orangered", "saddlebrown", "purple", "gold", "sandybrown");
	
	public static String getRandomColor(){
		Random random = new Random(Calendar.getInstance().getTimeInMillis());
		return colors.get(random.nextInt(colors.size()));
		
		
		
//        File file = new File("system.properties");
//        FileInputStream input;
//        try {
//            input = new FileInputStream(file);
//            Properties properties = new Properties();
//            properties.load(input);
//            properties.isEmpty();
//        } catch (IOException ex) {
//            ex.printStackTrace(System.err);
//        }
		
	}
	
	public static boolean areEqual(String s1, String s2){
		if(!isValidParam(s1) && !isValidParam(s2)){
			return true;
		}
		if(isValidParam(s1)){
			if(s1.equals(s2)){
				return true;
			}
		}
		return s2.equals(s1);
	}
	
	public static BigDecimal[] divideBigDecimal(BigDecimal amount, BigDecimal divisor){
		if(BigDecimal.ZERO.compareTo(amount) == 0 || BigDecimal.ZERO.compareTo(divisor) == 0){
			System.err.println("Tried to divide by zero ");
			return new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ZERO};
		}
		
		BigDecimal result = amount.divide(divisor, 2, RoundingMode.FLOOR);
		BigDecimal remainder = amount.subtract(result.multiply(divisor));
		
		return new BigDecimal[]{result, remainder};
	}
	
	public int compareStrings(String arg1, String arg2){
		return levenshteinDistance(arg1.subSequence(0, arg1.length()), arg2.subSequence(0, arg2.length()));
		
	}
	public int levenshteinDistance (CharSequence lhs, CharSequence rhs) {
		int len0 = lhs.length() + 1;
		int len1 = rhs.length() + 1;
		
		// the array of distances
		int[] cost = new int[len0];
		int[] newcost = new int[len0];
		
		// initial cost of skipping prefix in String s0
		for (int i = 0; i < len0; i++) cost[i] = i;
		
		// dynamically computing the array of distances
		
		// transformation cost for each letter in s1
		for (int j = 1; j < len1; j++) {
			// initial cost of skipping prefix in String s1
			newcost[0] = j;
			
			// transformation cost for each letter in s0
			for(int i = 1; i < len0; i++) {
				// matching current letters in both strings
				int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;
				
				// computing cost for each transformation
				int cost_replace = cost[i - 1] + match;
				int cost_insert  = cost[i] + 1;
				int cost_delete  = newcost[i - 1] + 1;
				
				// keep minimum cost
				newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
			}
			
			// swap cost/newcost arrays
			int[] swap = cost; cost = newcost; newcost = swap;
		}
		
		// the distance is the cost for transforming all letters in both strings
		return cost[len0 - 1];
	}
}
