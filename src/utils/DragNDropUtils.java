/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import customObjects.Person;
import images.Images;
import java.text.Collator;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 *
 * @author d.michaelides
 */
public class DragNDropUtils {
    private TableView source;
    private TableView target;
    
    public DragNDropUtils(TableView s, TableView t){
        source = s;
        target = t;
    }
    
    public void initializeTables(){
        initializeSourceTable();
        initializeTargetTable();
    }
    
    
    public void initializeTargetTable(){
        
        utils.EventFilters.setOnDragEntered(target);
        utils.EventFilters.setOnDragOver(target);
        utils.EventFilters.setOnDragExited(target);
        utils.EventFilters.setOnDragDropped(target, source.getItems());
        
        TableColumn<Person, Person> indexCol = new TableColumn<>("#");
        indexCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        indexCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {
            @Override
            public TableCell<Person,Person> call(TableColumn<Person, Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(Integer.toString(p.getIndex()));
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }
                    }
                };
            }
        });
        indexCol.setComparator((Person p1, Person p2) -> (p1.getIndex() > p2.getIndex()) ? +1 : (p1.getIndex() < p2.getIndex()) ? -1 : 0);
        TableColumn<Person, Person> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        firstNameCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {
            @Override
            public TableCell<Person,Person> call(TableColumn<Person, Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(p.getFn());
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }
                    }
                };
            }
        });
        firstNameCol.setComparator(
                (Person p1, Person p2) -> Collator.getInstance().compare(p1.getFn(), p2.getFn()));
        
        TableColumn<Person, Person> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        lastNameCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person,Person> call(TableColumn<Person,Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(p.getlName());
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }
                    }
                };
            }
        });
        
        lastNameCol.setComparator(
                (Person p1, Person p2) -> Collator.getInstance().compare(p1.getlName(), p2.getlName()));
        
        TableColumn<Person, Person> allCol = new TableColumn<>("Currency");
        allCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        
        allCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person, Person> call(TableColumn<Person,Person> p) {
               final VBox v = new VBox();
               final ImageView image = new ImageView();
               {
                   v.setAlignment(Pos.CENTER);
                   v.getChildren().add(image);
               }
                      
                return new TableCell<Person, Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            Image img;
                            if(p.getCurrency() == 0){
                                img = new Image(Images.class.getResource("profile_2.png").toString(),14,14,true,true);
                            }else{
                                img = new Image(Images.class.getResource("default_profile.png").toString(),16,16,true,true);
                            }
                            image.setImage(img);
                            setGraphic(v);
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }else{
                        }
                        
                    }
                    
                    
                };
            }
        });
        
        
        TableColumn<Person, Person> checkCol = new TableColumn<>("CheckBox");
        checkCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        
        checkCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person,Person> call(TableColumn<Person,Person> p) {
               final VBox v = new VBox();
               final CheckBox check = new CheckBox();
               {
                   v.setAlignment(Pos.CENTER);
                   v.getChildren().add(check);
               }
                      
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            check.setSelected(p.getInvited());
                            setGraphic(v);
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }else{
                        }
                    }
                };
            }
        });
        target.getColumns().setAll(indexCol, firstNameCol, lastNameCol, allCol, checkCol);
    }
    
    
    public void initializeSourceTable(){
        
        utils.EventFilters.setOnDragEntered(source);
        utils.EventFilters.setOnDragOver(source);
        utils.EventFilters.setOnDragExited(source);
        utils.EventFilters.setOnDragDropped(source, target.getItems());
        
        
//        TableColumn<Person, Integer> indexCol = new TableColumn<>("#");
//        indexCol.setCellValueFactory(new Callback<CellDataFeatures<Person, Integer>, ObservableValue<Integer>>() {
//            public ObservableValue<Integer> call(CellDataFeatures<Person, Integer> p) {
//                return new ReadOnlyObjectWrapper(p.getValue().getIndex());
//            }
//        });
        
        TableColumn<Person, Person> indexCol = new TableColumn<>("#");
        indexCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        indexCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {
            @Override
            public TableCell<Person,Person> call(TableColumn<Person, Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(Integer.toString(p.getIndex()));
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(target));
                        }
                    }
                };
            }
        });
        indexCol.setComparator(
        (Person p1, Person p2) -> 
        {
            return (p1.getIndex() > p2.getIndex()) ? +1 : (p1.getIndex() < p2.getIndex()) ? -1 : 0;
        });
        
        
        
        TableColumn<Person, Person> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        firstNameCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {
            @Override
            public TableCell<Person,Person> call(TableColumn<Person, Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(p.getFn());
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(source));
                        }
                    }
            };
            }
        });
        firstNameCol.setComparator(
                (Person p1, Person p2) -> Collator.getInstance().compare(p1.getFn(), p2.getFn()));
        
        TableColumn<Person, Person> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        lastNameCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person,Person> call(TableColumn<Person,Person> p) {
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            setText(p.getlName());
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(source));
                        }
                    }
                };
            }
        });
        lastNameCol.setComparator(
                (Person p1, Person p2) -> Collator.getInstance().compare(p1.getlName(), p2.getlName()));
        
        TableColumn<Person, Person> allCol = new TableColumn<>("Currency");
        allCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        
        allCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person, Person> call(TableColumn<Person,Person> p) {
               final VBox v = new VBox();
               final ImageView image = new ImageView();
               {
                   v.setAlignment(Pos.CENTER);
                   v.getChildren().add(image);
               }
                      
                return new TableCell<Person, Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            Image img;
                            if(p.getCurrency() == 0){
                                img = new Image(Images.class.getResource("profile_2.png").toString(),14,14,true,true);
                            }else{
                                img = new Image(Images.class.getResource("default_profile.png").toString(),16,16,true,true);
                            }
                            image.setImage(img);
                            setGraphic(v);
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(source));
                        }else{
                        }
                    }
                };
            }
        });
        
        
        TableColumn<Person, Person> checkCol = new TableColumn<>("CheckBox");
        checkCol.setCellValueFactory(new PropertyValueFactory<>("Person"));
        
        checkCol.setCellFactory(new Callback<TableColumn<Person,Person>, TableCell<Person,Person>>() {

            @Override
            public TableCell<Person,Person> call(TableColumn<Person,Person> p) {
               final VBox v = new VBox();
               final CheckBox check = new CheckBox();
               {
                   v.setAlignment(Pos.CENTER);
                   v.getChildren().add(check);
               }
                      
                return new TableCell<Person,Person>(){
                    @Override
                    public void updateItem(Person p, boolean empty){
                        if(p != null){
                            check.setSelected(p.getInvited());
                            setGraphic(v);
                            setOnDragDetected(utils.EventFilters.getDragDetectedHandler(this, Integer.toString(getIndex())));
                            setOnDragDone(utils.EventFilters.getOnDragDoneHandler(source));
                        }else{
                        }
                    }
                };
            }
        });
        source.getColumns().setAll(indexCol, firstNameCol, lastNameCol, allCol, checkCol);
    }
    
}
