/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.net.URL;

/**
 *
 * @author d.michaelides
 */
public class PanelAndUrl {
    private String name;
    private URL location;
    public PanelAndUrl(String name, URL url){
        this.name = name;
        this.location = url;
    }
    
    public String getName(){
        return name;
    } 
    public URL getLocation(){
        return location;
    } 
    
}
