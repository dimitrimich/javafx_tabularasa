package utils.dialogs;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;

/**
 *
 * @author dimitris
 */
public class CustomDialogs {
    
    public static Optional<ButtonType> yesNoDialog(String title, String message, String yes, String no){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        ButtonType buttonTypeYes = new ButtonType(yes, ButtonData.YES);
        ButtonType buttonTypeNo = new ButtonType(no, ButtonData.NO);
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
        
        return alert.showAndWait();
    }
    
    public static Optional<ButtonType> retryClearCloseDialog(String title, String message, String retry, String clear, String close){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(null);
        
        ButtonType buttonTypeRetry = new ButtonType(retry);
        ButtonType buttonTypeClear = new ButtonType(clear);
        ButtonType buttonTypeClose = new ButtonType(close, ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(buttonTypeRetry, buttonTypeClear, buttonTypeClose);
        
        return alert.showAndWait();
    }
    public static Optional<ButtonType> clearCloseDialog(String title, String message, String clear, String close){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.setHeaderText(null);
        
        ButtonType buttonTypeClear = new ButtonType(clear);
        ButtonType buttonTypeClose = new ButtonType(close, ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(buttonTypeClear, buttonTypeClose);
        return alert.showAndWait();
        
    }
    
    public static void infoDialog(String title, String message){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    public static void errorDialog(String title, String message){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        
        alert.showAndWait();
    }
    
    
    
    
    
}
