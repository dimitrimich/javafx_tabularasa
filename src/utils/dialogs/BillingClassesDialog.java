/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.util.Map;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

/**
 *
 * @author dimitris
 */
public class BillingClassesDialog {
	private static Dialog <Map<Year, Map<Month, BigDecimal>>> instance;
	
	private BillingClassesDialog(){}
	
	public static Dialog <Map<Year, Map<Month, BigDecimal>>> getInstance(){
		if(instance != null){return instance;}
		
		instance = new Dialog<>();
		instance.setTitle("Payment Method");
		instance.setHeaderText(null);
		// Set the icon (must be included in the project).
		instance.setGraphic(null);
		
		// Set the button types.
		ButtonType okButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		instance.getDialogPane().getButtonTypes().addAll(okButtonType);
		
		instance.getDialogPane().setContent(BillingClassesDialogScene.getInstance());
		
		instance.setResultConverter(dialogButton -> {
			return BillingClassesDialogScene.getController().getPaymentsToReturn();
		});
		return instance;
	}
	
	
}