/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import DB.objects.Aclass;
import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;

/**
 *
 * @author dimitris
 */
public class BillingClassesRow {
    private String name;
    private int numberOfLessons;
    private long durationOfLessons;
    private int canceledLessons;
    private int extraLessons;
    private BigDecimal fullAmountOfLessons;
    private BigDecimal discountOfLessons;
    private BigDecimal paidAmountOfLessons;
    private Aclass theClass;
    private Month month;
    private Year year;
    
    public BillingClassesRow(){
	name = "";
	numberOfLessons = 0;
	durationOfLessons = 0L;
	canceledLessons = 0;
	extraLessons = 0;
	fullAmountOfLessons = BigDecimal.ZERO;
	discountOfLessons = BigDecimal.ZERO;
	paidAmountOfLessons = BigDecimal.ZERO;
	month = Month.JANUARY;
	year = Year.of(1990);
    }
    
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public int getNumberOfLessons() { return numberOfLessons; }
    public void setNumberOfLessons(int numberOfLessons) { this.numberOfLessons = numberOfLessons; }
    public long getDurationOfLessons() { return durationOfLessons; }
    public void setDurationOfLessons(long durationOfLessons) { this.durationOfLessons = durationOfLessons; }
    public int getCanceledLessons() { return canceledLessons; }
    public void setCanceledLessons(int canceledLessons) { this.canceledLessons = canceledLessons; }
    public int getExtraLessons() { return extraLessons; }
    public void setExtraLessons(int extraLessons) { this.extraLessons = extraLessons; }
    public BigDecimal getFullAmountOfLessons() {  return fullAmountOfLessons; }
    public void setFullAmountOfLessons(BigDecimal fullAmountOfLessons) { this.fullAmountOfLessons = fullAmountOfLessons; }
    public BigDecimal getDiscountOfLessons() { return discountOfLessons; }
    public void setDiscountOfLessons(BigDecimal discountOfLessons) { this.discountOfLessons = discountOfLessons; }
    public BigDecimal getPaidAmountOfLessons() { return paidAmountOfLessons; }
    public void setPaidAmountOfLessons(BigDecimal paidAmountOfLessons) { this.paidAmountOfLessons = paidAmountOfLessons; }
    public Month getMonth() { return month; }
    public void setMonth(Month month) { this.month = month; }
    public Year getYear() { return year; }
    public void setYear(Year year) { this.year = year; }
    
    public void increaseDiscountBy1(){
	this.discountOfLessons = this.discountOfLessons.add(BigDecimal.ONE);
    }
    public void increaseFullAmountBy1(){
	this.fullAmountOfLessons = this.fullAmountOfLessons.add(BigDecimal.ONE);
    }
    public void increasePaidAmountBy1(){
	this.paidAmountOfLessons = this.paidAmountOfLessons.add(BigDecimal.ONE);
    }
    public void increaseExtraLessonsBy1(){
	this.extraLessons++;
    }
    public void increaseCanceledLessonsBy1(){
	this.canceledLessons++;
    }
    public void increaseNumberOfLessonsBy1(){
	this.numberOfLessons++;
    }
    public void increaseDurationBy(long extraDuration){
	durationOfLessons += extraDuration;
    }
    public void increaseFullAmountOfLessons(BigDecimal extraAmount){
	fullAmountOfLessons = fullAmountOfLessons.add(extraAmount);
    }
    public void increaseDiscountOfLessons(BigDecimal extraAmount){
	discountOfLessons = discountOfLessons.add(extraAmount);
    }
    public void increasePaidAmountOfLessons(BigDecimal extraAmount){
	paidAmountOfLessons = paidAmountOfLessons.add(extraAmount);
    }
    public Aclass getTheClass() { return theClass; }
    public void setTheClass(Aclass theClass) { this.theClass = theClass; }
    
}
