/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import customObjects.MyDecimalFormater;
import java.math.BigDecimal;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;

/**
 *
 * @author dimitris
 */
public class BigDecimalCellFormatter extends TableCell<Object, BigDecimal> {
    private MyDecimalFormater formater = new MyDecimalFormater();
    private boolean boldText;
    public BigDecimalCellFormatter() {
    }
    public BigDecimalCellFormatter(boolean boldText) {
        super();
        this.boldText = boldText;
    }
    
    @Override
    protected void updateItem(BigDecimal item, boolean empty) {
        super.updateItem(item, empty);
        
        // If the row is not empty but the Double-value is null,
        // we will always display 0%
        if (!empty && null == item) {
            item = BigDecimal.ZERO;
        }
        
        // Here we set the displayed text to anything we want without changing the
        // real value behind it. We could also have used switch case or anything you
        // like.
        setText(item == null ? "" : formater.numberToString(item));
        if(boldText){
            if (item != null) {
                ObservableList<String> styles = this.getStyleClass();
                if(!styles.contains("bold_text")){
                    styles.add("bold_text");
                }
                
            }
        }
        // If the cell is selected, the text will always be white
        // (so that it can be read against the blue background),
        // if the value is 1 it will be green.
//        if (item != null) {
//            double value = item.doubleValue();
//            if (isFocused() || isSelected() || isPressed()) {
//                setTextFill(Color.WHITE);
//            } else if (value < 1) {
//                setTextFill(Color.BLACK);
//            } else {
//                setTextFill(Color.GREEN);
//            }
//        }
    }
}