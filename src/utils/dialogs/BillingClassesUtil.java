/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import fxObjects.BillingClassesRowFX;
import DB.objects.Lesson;
import DB.objects.PaymentDetails;
import DB.objects.Student;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Split the value that we get from a dialog equally among classes
 * @author dimitris
 */
public class BillingClassesUtil {
    
    public static void dividePayments(HashMap<BillingClassesRowFX, PaymentDetails> payments, PaymentDetails paymentFromDialog)
    {
	
	BigDecimal amountFromDialog = paymentFromDialog.getPaidAmount();
	BigDecimal discountFromDialog = paymentFromDialog.getDiscount();
	
	MathContext math = new MathContext(4, RoundingMode.FLOOR);
	BigDecimal[] amounts = amountFromDialog.divideAndRemainder(BigDecimal.valueOf(payments.size()), math);
	BigDecimal[] discounts = discountFromDialog.divideAndRemainder(BigDecimal.valueOf(payments.size()), math);
//		System.out.println("Full Amount:" + amountFromDialog + " | " + discountFromDialog);
	
	Iterator<PaymentDetails> it = payments.values().iterator();
	while(it.hasNext()){
	    PaymentDetails payment = it.next();
	    payment.setCheckNo(paymentFromDialog.getCheckNo());
	    payment.setPaymentDate(paymentFromDialog.getPaymentDate());
	    payment.setPaymentMethod(paymentFromDialog.getPaymentMethod());
	    payment.setDiscount(discounts[0]);
	    payment.setPaidAmount(amounts[0]);
	    if(!it.hasNext()){
		payment.increaseDiscount(discounts[1]);
		payment.increasePaidAmount(amounts[1]);
	    }
	}
	printMap(payments);
	
//		System.out.println("--------------- /init ------------------");
	
	calculatePayments(payments, amountFromDialog, discountFromDialog);
	
    }
    
    private static void calculatePayments(HashMap<BillingClassesRowFX, PaymentDetails> payments, BigDecimal amountFromDialog, BigDecimal discountFromDialog)
    {
	BigDecimal totalAmountRemaining = getAmountRemaining(payments);
//		System.out.println("leftovers after subtract = " + totalAmountRemaining);
	printMap(payments);
	
//		System.out.println("before remaining amount = " + totalAmountRemaining);
	totalAmountRemaining = addLeftoverAmount(payments, totalAmountRemaining);
//		System.out.println("after remaining amount = " + totalAmountRemaining);
	printMap(payments);
    }
    
    /**
     * @param totalAmountRemaining
     * @return
     */
    private static  BigDecimal addLeftoverAmount(HashMap<BillingClassesRowFX, PaymentDetails> payments, BigDecimal totalAmountRemaining)
    {
//		System.out.println("LeftOvers :");
	for(Entry<BillingClassesRowFX, PaymentDetails> entry : payments.entrySet())
	{
	    PaymentDetails newPayment = entry.getValue();
	    BigDecimal remainingAmountNow = entry.getKey().getRemainingAmountToPay();
	    
	    BigDecimal remainingAmountForClass = remainingAmountNow.subtract(newPayment.getDiscount().add(newPayment.getPaidAmount()));
	    
	    System.out.println("for class " + entry.getKey().getName() + " remaining amount for class= " + remainingAmountForClass);
	    System.out.println("   totalAmountRemaining = " + totalAmountRemaining);
	    if(remainingAmountForClass.compareTo(BigDecimal.ZERO) != 0)
	    {
		if(newPayment.getPaidAmount().compareTo(entry.getValue().getDiscount()) == 1){
//		System.out.println(newPayment.getPaidAmount() + " .__compareTo("+entry.getValue().getDiscount()+") ");
		    // total amount remaining to use is smaller that what the class need to be fully paid
		    if(totalAmountRemaining.compareTo(remainingAmountForClass) == -1){
			newPayment.increasePaidAmount(totalAmountRemaining);
		    }else{
			newPayment.increasePaidAmount(remainingAmountForClass);
		    }
		}else{
//		System.out.println(newPayment.getPaidAmount() + " .__compareTo("+entry.getValue().getDiscount()+") ");
		    // total amount remaining to use is smaller that what the class need to be fully paid
		    if(totalAmountRemaining.compareTo(remainingAmountForClass) == -1){
			newPayment.increaseDiscount(totalAmountRemaining);
		    }else{
			newPayment.increaseDiscount(remainingAmountForClass);
		    }
		}
		totalAmountRemaining = totalAmountRemaining.subtract(remainingAmountForClass);
		if(totalAmountRemaining.compareTo(BigDecimal.ZERO) == -1){
		    totalAmountRemaining = BigDecimal.ZERO;
		    break;
		}
	    }
	}
	return totalAmountRemaining;
    }
    
    private static BigDecimal getAmountRemaining(HashMap<BillingClassesRowFX, PaymentDetails> payments)
    {
//		System.out.println("Remaining Amount :");
	BigDecimal amount = BigDecimal.ZERO;
	
	for(Entry<BillingClassesRowFX, PaymentDetails> entry : payments.entrySet())
	{
//			System.out.println("for class " + entry.getKey().getName());
	    PaymentDetails newPayment = entry.getValue();
	    BigDecimal remainingAmountNow = entry.getKey().getRemainingAmountToPay();
//			System.out.println("   now = "+ remainingAmountNow);
//			System.out.println("   disc = "+ newPayment.getDiscount());
//			System.out.println("   pay = "+ newPayment.getPaidAmount());
	    BigDecimal remainingAmount = remainingAmountNow.subtract(newPayment.getDiscount().add(newPayment.getPaidAmount()));
//			System.out.println("   after = "+ remainingAmount);
	    
	    if(remainingAmount.compareTo(BigDecimal.ZERO) < 0)
	    {
//				System.out.println("   < 0 = "+ remainingAmount);
		if(newPayment.getPaidAmount().compareTo(entry.getValue().getDiscount()) == 1){
//					System.out.println(newPayment.getPaidAmount() + " .compareTo("+entry.getValue().getDiscount()+") ");
		    amount = amount.add(remainingAmount.abs());
		    newPayment.decreasePaidAmount(remainingAmount.abs());
		}else{
//					System.out.println(newPayment.getPaidAmount() + " .compareTo("+entry.getValue().getDiscount()+") ");
		    amount = amount.add(remainingAmount.abs());
		    newPayment.decreaseDiscount(remainingAmount.abs());
		}
	    }
	}
	return amount;
    }
    
    
    
    public static void printMap(HashMap<BillingClassesRowFX, PaymentDetails> payments)
    {
	for(Entry<BillingClassesRowFX, PaymentDetails> entry : payments.entrySet())
	{
	    System.out.println(entry.getKey().getName() + ": " + entry.getValue().getPaidAmount()+ " | " + entry.getValue().getDiscount());
	}
    }
    /*           ***                   **** LESSONS			***				****/
    
    public static void divideLessonPayments(Map<Lesson, PaymentDetails>payments, PaymentDetails paymentFromDialog, Student stu)
    {
	
	BigDecimal amountFromDialog = paymentFromDialog.getPaidAmount();
	BigDecimal discountFromDialog = paymentFromDialog.getDiscount();
	
	MathContext math = new MathContext(4, RoundingMode.FLOOR);
	BigDecimal[] amounts = amountFromDialog.divideAndRemainder(BigDecimal.valueOf(payments.size()), math);
	BigDecimal[] discounts = discountFromDialog.divideAndRemainder(BigDecimal.valueOf(payments.size()), math);
//		System.out.println("Full Amount:" + amountFromDialog + " | " + discountFromDialog);
	
	Iterator<PaymentDetails> it = payments.values().iterator();
	while(it.hasNext()){
	    PaymentDetails payment = it.next();
	    payment.setCheckNo(paymentFromDialog.getCheckNo());
	    payment.setPaymentDate(paymentFromDialog.getPaymentDate());
	    payment.setPaymentMethod(paymentFromDialog.getPaymentMethod());
	    payment.setDiscount(discounts[0]);
	    payment.setPaidAmount(amounts[0]);
	    if(!it.hasNext()){
		payment.increaseDiscount(discounts[1]);
		payment.increasePaidAmount(amounts[1]);
	    }
	}
	System.out.println("Lesson Payments 1:");
	printLessonsMap(payments);
	
//		System.out.println("--------------- /init ------------------");
	
	calculateLessonPayments(payments, amountFromDialog, discountFromDialog, stu);
	
    }
    
    private static void calculateLessonPayments(Map<Lesson, PaymentDetails> payments, BigDecimal amountFromDialog, BigDecimal discountFromDialog, Student stu)
    {
	BigDecimal totalAmountRemaining = getAmountRemainingForLesson(payments, stu);
//		System.out.println("leftovers after subtract = " + totalAmountRemaining);
	System.out.println("Lesson Payments 2:");
	printLessonsMap(payments);
	
//		System.out.println("before remaining amount = " + totalAmountRemaining);
	totalAmountRemaining = addLeftoverAmountForLessons(payments, totalAmountRemaining, stu);
//		System.out.println("after remaining amount = " + totalAmountRemaining);
	System.out.println("Lesson Payments 3:");
	printLessonsMap(payments);
    }
    
    /**
     * @param totalAmountRemaining
     * @return
     */
    private static  BigDecimal addLeftoverAmountForLessons(Map<Lesson, PaymentDetails> payments, BigDecimal totalAmountRemaining, Student stu)
    {
//		System.out.println("LeftOvers :");
	int i = 0;
	for(Entry<Lesson, PaymentDetails> entry : payments.entrySet())
	{
	    i++;
	    PaymentDetails newPayment = entry.getValue();
	    BigDecimal remainingAmountNow = entry.getKey().getBillingForStudent(stu).getRemainingAmount();
	    
	    BigDecimal remainingAmountForClass = remainingAmountNow.subtract(newPayment.getDiscount().add(newPayment.getPaidAmount()));
	    
//			System.out.println("for lesson " + i + " remaining amount = " + remainingAmountForClass);
	    if(remainingAmountForClass.compareTo(BigDecimal.ZERO) != 0)
	    {
		if(newPayment.getPaidAmount().compareTo(entry.getValue().getDiscount()) == 1){
		    // total amount remaining to use is smaller that what the class need to be fully paid
		    if(totalAmountRemaining.compareTo(remainingAmountForClass) == -1){
			newPayment.increasePaidAmount(totalAmountRemaining);
		    }else{
			newPayment.increasePaidAmount(remainingAmountForClass);
		    }
		}else{
		    // total amount remaining to use is smaller that what the class need to be fully paid
		    if(totalAmountRemaining.compareTo(remainingAmountForClass) == -1){
			newPayment.increaseDiscount(totalAmountRemaining);
		    }else{
			newPayment.increaseDiscount(remainingAmountForClass);
		    }
		}
		totalAmountRemaining = totalAmountRemaining.subtract(remainingAmountForClass);
		if(totalAmountRemaining.compareTo(BigDecimal.ZERO) == -1){
		    totalAmountRemaining = BigDecimal.ZERO;
		    break;
		}
	    }
	}
	return totalAmountRemaining;
    }
    
    private static BigDecimal getAmountRemainingForLesson(Map<Lesson, PaymentDetails> payments, Student stu)
    {
//		System.out.println("Remaining Amount :");
	BigDecimal amount = BigDecimal.ZERO;
	
	for(Entry<Lesson, PaymentDetails> entry : payments.entrySet())
	{
	    PaymentDetails newPayment = entry.getValue();
	    BigDecimal remainingAmountNow = entry.getKey().getBillingForStudent(stu).getRemainingAmount();
//			System.out.println("   now = "+ remainingAmountNow);
//			System.out.println("   disc = "+ newPayment.getDiscount());
//			System.out.println("   pay = "+ newPayment.getPaidAmount());
	    BigDecimal remainingAmount = remainingAmountNow.subtract(newPayment.getDiscount().add(newPayment.getPaidAmount()));
//			System.out.println("   after = "+ remainingAmount);
	    
	    if(remainingAmount.compareTo(BigDecimal.ZERO) < 0)
	    {
//				System.out.println("   < 0 = "+ remainingAmount);
		if(newPayment.getPaidAmount().compareTo(entry.getValue().getDiscount()) == 1){
//					System.out.println(newPayment.getPaidAmount() + " .compareTo("+entry.getValue().getDiscount()+") ");
		    amount = amount.add(remainingAmount.abs());
		    newPayment.decreasePaidAmount(remainingAmount.abs());
		}else{
//					System.out.println(newPayment.getPaidAmount() + " .compareTo("+entry.getValue().getDiscount()+") ");
		    amount = amount.add(remainingAmount.abs());
		    newPayment.decreaseDiscount(remainingAmount.abs());
		}
	    }
	}
	return amount;
    }
    
    
    
    public static void printLessonsMap(Map<Lesson, PaymentDetails> payments)
    {
	int i = 0;
	for(Entry<Lesson, PaymentDetails> entry : payments.entrySet())
	{
	    i++;
	    System.out.println( i+": " + entry.getValue().getPaidAmount()+ " | " + entry.getValue().getDiscount());
	}
    }
}
