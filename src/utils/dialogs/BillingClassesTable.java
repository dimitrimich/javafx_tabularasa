/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import fxObjects.BillingClassesRowFX;
import java.math.BigDecimal;
import java.time.Month;
import java.util.function.BiFunction;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

/**
 *
 * @author dimitris
 */
public class BillingClassesTable {
	
	public static void initTable(TableView<BillingClassesRowFX> table, BiFunction<BillingClassesRowFX, Boolean, Void> payClass){
		
		TableColumn<BillingClassesRowFX, Boolean> selectColumn = new TableColumn<>("Select");
		TableColumn<BillingClassesRowFX, String> nameColumn = new TableColumn<>("Name");
		TableColumn<BillingClassesRowFX, String> monthColumn = new TableColumn<>("Month");
		TableColumn<BillingClassesRowFX, BigDecimal> remainingAmountColumn = new TableColumn<>("Due Amount");
		TableColumn<BillingClassesRowFX, Integer> numOfLessonsColum = new TableColumn<>("No. Of Lessons");
		TableColumn<BillingClassesRowFX, Long> durationColumn = new TableColumn<>("Duration");
		TableColumn<BillingClassesRowFX, Integer> numOfCanceledColumn = new TableColumn<>("Canceled Lessons");
		TableColumn<BillingClassesRowFX, Integer> numOfExtraColumn = new TableColumn<>("Extra Lessons");
		TableColumn<BillingClassesRowFX, BigDecimal> fullAmountColumn = new TableColumn<>("Full Amount");
		TableColumn<BillingClassesRowFX, BigDecimal> discountColumn = new TableColumn<>("Discount");
		TableColumn<BillingClassesRowFX, BigDecimal> paidAmountColumn = new TableColumn<>("Paid Amount");
		
		
		setUpSelectColumn(selectColumn, payClass);
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		monthColumn.setCellValueFactory(cellData -> cellData.getValue().getMonthProperty());
		remainingAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getRemainingAmountToPayProperty());
		numOfLessonsColum.setCellValueFactory(cellData -> cellData.getValue().getNumberOfLessonsProperty().asObject());
		durationColumn.setCellValueFactory(cellData -> cellData.getValue().getDurationOfLessonsProperty().asObject());
		numOfCanceledColumn.setCellValueFactory(cellData -> cellData.getValue().getCanceledLessonsProperty().asObject());
		numOfExtraColumn.setCellValueFactory(cellData -> cellData.getValue().getExtraLessonsProperty().asObject());
		fullAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getFullAmountOfLessonsProperty());
		discountColumn.setCellValueFactory(cellData -> cellData.getValue().getDiscountOfLessonsProperty());
		paidAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getPaidAmountOfLessonsProperty());
		
		// In case we have multiple columns with percent-values it
		// might come in handy to store our formatter
		Callback<TableColumn<BillingClassesRowFX,BigDecimal>,TableCell<BillingClassesRowFX,BigDecimal>> percantageCellFactory =
				new Callback<TableColumn<BillingClassesRowFX,BigDecimal>,TableCell<BillingClassesRowFX,BigDecimal>>() {
					@Override
					public TableCell call(TableColumn p) {
						return new BigDecimalCellFormatter();
					}
				};
		Callback<TableColumn<BillingClassesRowFX,BigDecimal>,TableCell<BillingClassesRowFX,BigDecimal>> boldPercantageCellFactory =
				new Callback<TableColumn<BillingClassesRowFX,BigDecimal>,TableCell<BillingClassesRowFX,BigDecimal>>() {
					@Override
					public TableCell call(TableColumn p) {
						return new BigDecimalCellFormatter(true);
					}
				};
		
		// Now all we have to do is to apply it
		remainingAmountColumn.setCellFactory(boldPercantageCellFactory);
		fullAmountColumn.setCellFactory(percantageCellFactory);
		discountColumn.setCellFactory(percantageCellFactory);
		paidAmountColumn.setCellFactory(percantageCellFactory);
		
		remainingAmountColumn.setSortable(false);
		fullAmountColumn.setSortable(false);
		discountColumn.setSortable(false);
		paidAmountColumn.setSortable(false);
		nameColumn.setSortable(false);
		monthColumn.setSortable(false);
		selectColumn.setSortable(false);
		numOfCanceledColumn.setSortable(false);
		numOfExtraColumn.setSortable(false);
		numOfLessonsColum.setSortable(false);
		durationColumn.setSortable(false);
		
		table.setEditable(true);
		table.getColumns().addAll(selectColumn, nameColumn, monthColumn, remainingAmountColumn, numOfLessonsColum, durationColumn, numOfCanceledColumn, numOfExtraColumn, fullAmountColumn, discountColumn, paidAmountColumn);
	}
	
	private static void setUpSelectColumn(TableColumn<BillingClassesRowFX, Boolean> selectColumn, BiFunction<BillingClassesRowFX, Boolean, Void> payClass) {
		selectColumn.setCellValueFactory(cellData -> cellData.getValue().getSelectedProperty());
		selectColumn.setCellFactory( new Callback<TableColumn<BillingClassesRowFX,Boolean>, TableCell<BillingClassesRowFX,Boolean>>()
		{
			@Override
			public TableCell<BillingClassesRowFX,Boolean> call( TableColumn<BillingClassesRowFX,Boolean> param )
			{
				return new CheckBoxTableCell<BillingClassesRowFX, Boolean>()
				{
					{
						setAlignment( Pos.CENTER );
					}
					@Override
					public void updateItem( Boolean item, boolean empty )
					{
						if ( ! empty )
						{
							TableRow  row = getTableRow();
							if ( row != null )
							{
								int rowNo = row.getIndex();
								TableViewSelectionModel  sm = getTableView().getSelectionModel();
								
								if(row.getItem() instanceof BillingClassesRowFX){
									BillingClassesRowFX theRow = (BillingClassesRowFX)row.getItem();
									if(theRow.getRemainingAmountToPay().compareTo(BigDecimal.ZERO) == 0){
										System.out.println("class "+theRow.getName()+" is fully payed");
										setDisable(true);
										payClass.apply(((BillingClassesRowFX)row.getItem()), item);
										return;
									}else{
										setDisable(false);
									}
//										else{
									
//										setDisable(false);
//										payClass.apply(((BillingClassesRowFX)row.getItem()), item);
//									}
								}
								
								if ( item ){
									sm.select( rowNo );
								}
								else{
									sm.clearSelection( rowNo );
								}
								if(row.getItem() instanceof BillingClassesRowFX){
									payClass.apply(((BillingClassesRowFX)row.getItem()), item);
								}
							}
						}
						
						super.updateItem( item, empty );
					}
				};
			}
		} );
		selectColumn.setEditable( true );
	}
}
