/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import DB.objects.PaymentDetails;
import controllers.BillingDialogController;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.SingleSelectionModel;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CASH;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CHECK;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.DEFAULT;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author dimitris
 */
public class BillingDialog {
    public enum AUTO_COMPLETE_FLAG{AUTO_COMPLETE, NOT_AUTO_COMPLETE};
    
    private static Dialog <PaymentDetails> instance;
    private static BillingDialogController controller;
    
    private BillingDialog(){}
    
    public static Dialog <PaymentDetails> getInstance(AUTO_COMPLETE_FLAG options){
	
	if(instance != null){
	    controller.setAutoComplete(options);
	    return  instance;
	}
	
	instance = new Dialog<>();
	instance.setTitle("Payment Method");
	instance.setHeaderText(null);
	// Set the icon (must be included in the project).
	instance.setGraphic(null);
	
	// Set the button types.
	ButtonType okButtonType = new ButtonType("OK", ButtonData.OK_DONE);
	instance.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);
	
	AnchorPane scene = BillingDialogScene.getInstance(options);
	controller = BillingDialogScene.getController();
	controller.setAutoComplete(options);
	instance.getDialogPane().setContent(scene);
	
	instance.setResultConverter(dialogButton -> {
	    if (dialogButton == okButtonType) {
		PaymentDetails returnValue = new PaymentDetails();
		
		returnValue.setDiscount(controller.getAmountDiscountValue().getNumber());
		returnValue.setPaidAmount(controller.getAmountPaidValue().getNumber());
		returnValue.setPaymentDate(controller.getDate().getValue());
		returnValue.setPaymentBy(controller.getPaymentBy().getText());
		SingleSelectionModel<String> choice = controller.getPayMethodChoice().getSelectionModel();
		if(! choice.isEmpty()){
		    String selectedItem = choice.getSelectedItem();
		    if(selectedItem.equals(PaymentDetails.PAYMENT_METHOD.getStringValue(CASH))){
			returnValue.setPaymentMethod(CASH);
		    }else if(selectedItem.equals(PaymentDetails.PAYMENT_METHOD.getStringValue(CHECK))){
			returnValue.setPaymentMethod(CHECK);
			returnValue.setCheckNo(controller.getCheckNo().getText());
		    }else{
			returnValue.setPaymentMethod(DEFAULT);
		    }
		}
		return (returnValue);
	    }
	    return null;
	});
	return instance;
    }
    
    
}