/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import controllers.BillingConfirmationDialogController;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author dimitris
 */

public class BillingConfirmationDialogScene extends AnchorPane{
	
	private static AnchorPane instance = null;
	private static BillingConfirmationDialogController sceneController = null;
	private BillingConfirmationDialogScene() { /*Exists only to defeat instantiation. */ }
	
	public static AnchorPane getInstance() {
		if(instance == null) {
			sceneController = new BillingConfirmationDialogController("", "", null);
			instance = (AnchorPane)sceneController.getScene().getRoot();
		}
		return instance;
	}
	
	public static BillingConfirmationDialogController getController(){
		return sceneController;
	}
}
