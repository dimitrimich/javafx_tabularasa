/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Dialog;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author dimitris
 */
public class PopupLessonDialog {
	private static Dialog <Object> instance;
	
	private PopupLessonDialog(){}
	
	public static Dialog <Object> getInstance(){
		if(instance != null){return instance;}
		
		instance = new Dialog<>();
		instance.initStyle(StageStyle.UNDECORATED);
		System.err.println("TRANSPARENT_WINDOW = "  + Platform.isSupported(ConditionalFeature.TRANSPARENT_WINDOW));
		
		instance.setHeaderText(null);
		// Set the icon (must be included in the project).
		instance.setGraphic(null);
		instance.setResizable(false);
				
		// Set the button types.
//		ButtonType okButtonType = new ButtonType("OK", ButtonData.OK_DONE);
//		instance.getDialogPane().getButtonTypes().addAll(okButtonType);
		
		instance.getDialogPane().setContent(PopupLessonDialogScene.getInstance());
		instance.getDialogPane().getScene().setFill(Color.TRANSPARENT);
		System.out.println("set eventHandler");
		instance.getDialogPane().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
			if(t.getCode() == KeyCode.ESCAPE)
			{
				Stage sb = (Stage)instance.getDialogPane().getScene().getWindow();//use any one object
				PopupLessonDialogScene.getController().saveChanges();
				sb.close();
			}
		});
		
		instance.getDialogPane().getScene().getWindow().focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			System.out.println(""+oldValue +" | "+newValue );
			if(oldValue && !newValue){
				Stage sb = (Stage)instance.getDialogPane().getScene().getWindow();//use any one object
				PopupLessonDialogScene.getController().saveChanges();
				sb.close();
				
			}
		});
		instance.getDialogPane().focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			System.out.println(""+oldValue +" | "+newValue );
			if(oldValue && !newValue){
				Stage sb = (Stage)instance.getDialogPane().getScene().getWindow();//use any one object
				PopupLessonDialogScene.getController().saveChanges();
				sb.close();
			}
		});
		instance.setResultConverter(dialogButton -> {
			return null;
		});
		return instance;
	}
	
	
}