/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import controllers.PopUp_LessonController;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author dimitris
 */

public class PopupLessonDialogScene extends AnchorPane{
	
	private static VBox instance = null;
	private static PopUp_LessonController sceneController = null;
	private PopupLessonDialogScene() { /*Exists only to defeat instantiation. */ }
	
	public static VBox getInstance() {
		System.err.println("-------------------============---------------------");
		if(instance == null) {
			sceneController = new PopUp_LessonController("", "", null);
			instance = (VBox) ((ScrollPane) ((AnchorPane)sceneController.getScene().getRoot()).getChildren().get(0)).getContent();
		}
		return instance;
	}
	
	public static PopUp_LessonController getController(){
		return sceneController;
	}
}
