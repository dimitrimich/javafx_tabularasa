/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import controllers.BillingClassesDialogController;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author dimitris
 */

public class BillingClassesDialogScene extends AnchorPane{
	
	private static AnchorPane instance = null;
	private static BillingClassesDialogController sceneController = null;
	private BillingClassesDialogScene() { /*Exists only to defeat instantiation. */ }
	
	public static AnchorPane getInstance() {
		if(instance == null) {
			sceneController = new BillingClassesDialogController("", "", null);
			instance = (AnchorPane)sceneController.getScene().getRoot();
		}
		return instance;
	}
	
	public static BillingClassesDialogController getController(){
		return sceneController;
	}
}
