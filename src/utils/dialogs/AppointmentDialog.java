/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package utils.dialogs;

import DB.objects.Aclass;
import images.icons.Icons;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import org.controlsfx.control.decoration.Decorator;
import org.controlsfx.control.decoration.GraphicDecoration;
import org.controlsfx.control.decoration.StyleClassDecoration;
import utils.Utils;

/**
 *
 * @author dimitris
 */
public class AppointmentDialog <AppointmentDetails> extends Dialog{
	private final ArrayList<String> common_hours_24 = new ArrayList<>(
			Arrays.asList("13", "14", "15", "16", "17", "18",
					"19", "20",  "21",  "22"));
	
	private final ArrayList<String> common_minutes = new ArrayList<>(
			Arrays.asList("00", "15", "20", "30", "40", "45"));
	
	private final ArrayList<String> days_strings = new ArrayList<>(
			Arrays.asList("Sunday","Monday","Tuesday", "Wednesday",
					"Thursday", "Friday", "Saturday"));
	private final ArrayList<String> common_durations = new ArrayList<>(
			Arrays.asList("30","45", "60", "75", "90", "120"));
	
	
	private ChoiceBox <ClassKeyPair> classes  = new ChoiceBox();
	private ChoiceBox day  = new ChoiceBox();
	private ComboBox hour = new ComboBox();
	private ComboBox minute = new ComboBox();
	private ComboBox duration = new ComboBox();
	private Label dayLabel = new Label();
	private Label timeLabel = new Label();
	private Label durationLabel = new Label();
	
	private SimpleBooleanProperty dayProperty = new SimpleBooleanProperty(false);
	private SimpleBooleanProperty classProperty = new SimpleBooleanProperty(false);
	private SimpleBooleanProperty hourProperty = new SimpleBooleanProperty(false);
	private SimpleBooleanProperty minuteProperty = new SimpleBooleanProperty(false);
	private SimpleBooleanProperty durationProperty = new SimpleBooleanProperty(false);
	private MyBooleanBinding b;
	private ButtonType loginButtonType;
	
	
	public AppointmentDialog(Object owner, String title, List<Aclass> listOfClasses) {
		super();
		classes.setMinWidth(300);
		if(listOfClasses.size() == 1){
			classes.getItems().add(new ClassKeyPair(listOfClasses.get(0)));
			classes.getSelectionModel().selectFirst();
			classes.setDisable(true);
			classProperty.setValue(Boolean.TRUE);
		}else{
			Aclass defaultClass = new Aclass();
			defaultClass.setName("Select a Class");
			classes.getItems().add(new ClassKeyPair(defaultClass));
			classes.getItems().addAll(listOfClasses.stream().map(cl -> new ClassKeyPair(cl)).collect(Collectors.toList()));
			classes.getSelectionModel().selectFirst();
			classes.setDisable(false);
		}
		
		Region r = new Region();
		r.setMinHeight(0);
		r.setMinWidth(0);
		r.setMaxHeight(0);
		r.setMaxWidth(0);
		setHeaderText(null);
		loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
		
		b = new MyBooleanBinding();
		
		Node loginButton = getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);
		
		loginButton.disableProperty().bind(b.getTotalProperty());
		
		System.out.println("b.getTotalProperty() = "+ b.getTotalProperty().getValue());
	}
	
	
	public Optional<AppointmentDetails> showDialog(){
		if(classes.getItems().isEmpty()){
			throw new IllegalArgumentException("No classes passed as argument to dialog");
		}
		
		classes.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){classProperty.setValue(true);}
		});
		day.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){dayProperty.setValue(true);}
		});
		day.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){dayProperty.setValue(true);}
		});
		hour.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){hourProperty.setValue(true);}
		});
		minute.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){minuteProperty.setValue(true);}
		});
		duration.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue.intValue() >= 0){durationProperty.setValue(true);}
		});
		classProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			checkBValue();
		});
		dayProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			checkBValue();
		});
		hourProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			checkBValue();
		});
		minuteProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			checkBValue();
		});
		durationProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			checkBValue();
		});
		
		
		// main BorderPane
		BorderPane root = new BorderPane();
		root.setPrefHeight(100);
		root.setPrefWidth(400);
		// TOP
		Label title = new Label("Create Extra Lesson");
		BorderPane.setAlignment(title, Pos.CENTER);
		root.setTop(title);
		// CENTER
		GridPane mainContent = new GridPane();
		
		
		mainContent.setAlignment(Pos.CENTER);
		mainContent.setPrefWidth(400);
		mainContent.prefHeight(50);
		
		day.getItems().addAll(days_strings);
		day.setPrefWidth(130);
		dayLabel.setText("Day:");
		dayLabel.setPrefHeight(25);
		timeLabel.setContentDisplay(ContentDisplay.CENTER);
		timeLabel.setAlignment(Pos.CENTER);
		timeLabel.setPrefHeight(25);
		timeLabel.setPrefHeight(210);
		timeLabel.setText("Time");
		GridPane.setColumnIndex(timeLabel, 1);
		GridPane.setColumnSpan(timeLabel, 3);
		GridPane.setHalignment(timeLabel, HPos.CENTER);
		timeLabel.setPadding(new Insets(0, 55, 0, 0));
		
		durationLabel.setText("Duration");
		GridPane.setColumnIndex(durationLabel, 4);
		durationLabel.setPrefHeight(25);
		GridPane.setRowIndex(day, 1);
		hour.setEditable(true);
		hour.setMinWidth(65);
		hour.setMaxWidth(52);
		hour.getItems().addAll(common_hours_24);
		GridPane.setColumnIndex(hour, 1);
		GridPane.setRowIndex(hour, 1);
		GridPane.setHalignment(hour, HPos.RIGHT);
		minute.setEditable(true);
		minute.setMinWidth(75);
		minute.setMaxWidth(70);
		minute.setPrefWidth(75);
		minute.getItems().addAll(common_minutes);
		GridPane.setColumnIndex(minute, 3);
		GridPane.setRowIndex(minute, 1);
		duration.setEditable(true);
		duration.setMinWidth(65);
		duration.setMaxWidth(52);
		duration.setPrefWidth(52);
		duration.getItems().addAll(common_durations);
		GridPane.setColumnIndex(duration, 4);
		GridPane.setRowIndex(duration, 1);
		Label dots = new Label(":");
		GridPane.setColumnIndex(dots, 2);
		GridPane.setRowIndex(dots, 1);
		ColumnConstraints c1 = new ColumnConstraints();
		c1.setHgrow(Priority.SOMETIMES);
		c1.setMaxWidth(180);
		c1.setMinWidth(10);
		c1.setPrefWidth(180);
		ColumnConstraints c2 = new ColumnConstraints();
		c2.setHgrow(Priority.SOMETIMES);
		c2.setMaxWidth(190);
		c2.setMinWidth(10);
		c2.setPrefWidth(80);
		ColumnConstraints c3 = new ColumnConstraints();
		c3.setHgrow(Priority.SOMETIMES);
		c3.setMaxWidth(5);
		c3.setMinWidth(5);
		c3.setPrefWidth(5);
		ColumnConstraints c4 = new ColumnConstraints();
		c4.setHgrow(Priority.SOMETIMES);
		c4.setMaxWidth(315);
		c4.setMinWidth(10);
		c4.setPrefWidth(130);
		ColumnConstraints c5 = new ColumnConstraints();
		c5.setHgrow(Priority.SOMETIMES);
		c5.setMaxWidth(140);
		c5.setMinWidth(10);
		c5.setPrefWidth(102);
		ColumnConstraints c6 = new ColumnConstraints();
		c6.setHgrow(Priority.SOMETIMES);
		c6.setMaxWidth(140);
		c6.setMinWidth(10);
		c6.setPrefWidth(35);
		ObservableList <ColumnConstraints> columnsList = FXCollections.observableArrayList();
		columnsList.add(c1);columnsList.add(c2);columnsList.add(c3);
		columnsList.add(c4);columnsList.add(c5);columnsList.add(c6);
		mainContent.getColumnConstraints().addAll(columnsList);
		mainContent.getColumnConstraints().set(0, c1);
		mainContent.getColumnConstraints().set(1, c2);
		mainContent.getColumnConstraints().set(2, c3);
		mainContent.getColumnConstraints().set(3, c4);
		mainContent.getColumnConstraints().set(4, c5);
		mainContent.getColumnConstraints().set(5, c6);
		mainContent.getChildren().addAll( day, hour, minute, duration,
				dayLabel, timeLabel, durationLabel, dots);
		RowConstraints r1 = new RowConstraints();
		r1.setMaxHeight(25);
		r1.setVgrow(Priority.SOMETIMES);
		RowConstraints r2 = new RowConstraints();
		r2.setMaxHeight(25);
		r2.setVgrow(Priority.SOMETIMES);
		
		ObservableList <RowConstraints> rowList = FXCollections.observableArrayList();
		rowList.add(r1);rowList.add(r2);
		mainContent.getRowConstraints().addAll(rowList);
		mainContent.getRowConstraints().set(0, r1);
		mainContent.getRowConstraints().set(1, r2);
		
		HBox content = new HBox();
		content.getChildren().add(mainContent);
		HBox.setMargin(mainContent, new Insets(0, 0, 5, 0));
		
		root.setCenter(content);
		
		// bottom
		root.setBottom(classes);
		
		getDialogPane().setContent(root);
		
		setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				AppointmentDetails result = new AppointmentDetails();
				result.setTheClass(classes.getSelectionModel().getSelectedItem().getTheClass());
				result.setDay(day.getSelectionModel().getSelectedIndex());
				result.setHour(Integer.parseInt(hour.getEditor().getText()));
				result.setMinute(Integer.parseInt(minute.getEditor().getText()));
				result.setDuration(Integer.parseInt(duration.getEditor().getText()));
				return result;
			}else{
				return null;
			}
		});
		
		
		setUpComboBoxes();
		
		System.out.println("All content added");
		System.out.println("loading teachers");
		return showAndWait();
	}
	
	private void checkBValue(){
		b.get();
	}
	
	private boolean validateExtraLessonInput(){
		
		boolean thereIsError = false;
		if(day.getSelectionModel().isEmpty()){
			thereIsError = true;
			Decorator.addDecoration(day, new GraphicDecoration(createImageNode() ,Pos.CENTER_RIGHT));
			Decorator.addDecoration(day, new StyleClassDecoration("red-border"));
			day.requestFocus();
		}
		if(!Utils.isValidParam(hour.getEditor().getText())){
			thereIsError = true;
			Decorator.addDecoration(hour, new GraphicDecoration(createImageNode() ,Pos.CENTER_RIGHT));
			Decorator.addDecoration(hour, new StyleClassDecoration("red-border"));
			hour.requestFocus();
		}
		if(!Utils.isValidParam(minute.getEditor().getText())){
			thereIsError = true;
			Decorator.addDecoration(minute, new GraphicDecoration(createImageNode() ,Pos.CENTER_RIGHT));
			Decorator.addDecoration(minute, new StyleClassDecoration("red-border"));
			minute.requestFocus();
		}
		if(!Utils.isValidParam(duration.getEditor().getText())){
			thereIsError = true;
			Decorator.addDecoration(duration, new GraphicDecoration(createImageNode() ,Pos.CENTER_RIGHT));
			Decorator.addDecoration(duration, new StyleClassDecoration("red-border"));
			duration.requestFocus();
		}
		
		if(thereIsError){
			return false;
		}
		if(hour.getEditor().getText().equals("24") && (!minute.getEditor().getText().equals("00") ||!minute.getEditor().getText().equals("0"))){
			thereIsError = true;
			Decorator.addDecoration(minute, new GraphicDecoration(createImageNode() ,Pos.CENTER_RIGHT));
			Decorator.addDecoration(minute, new StyleClassDecoration("red-border"));
			minute.requestFocus();
		}
		return (!thereIsError);
	}
	
	private void setUpComboBoxes() {
		day.setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(day);
		});
		hour.setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(hour);
		});
		hour.getEditor().setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(hour);
		});
		minute.setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(minute);
		});
		minute.getEditor().setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(minute);
		});
		duration.setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(duration);
		});
		duration.getEditor().setOnMouseClicked((MouseEvent event) -> {
			Decorator.removeAllDecorations(duration);
		});
		
		day.getItems().clear();
		day.getItems().addAll(days_strings);
		hour.getItems().clear();
		hour.getItems().addAll(common_hours_24);
		minute.getItems().clear();
		minute.getItems().addAll(common_minutes);
		duration.getItems().clear();
		duration.getItems().addAll(common_durations);
		EventHandler<KeyEvent> handler = utils.EventFilters.getOnlyNumbersFilter();
		
		// filtering not numbers
		hour.addEventFilter(KeyEvent.KEY_RELEASED, handler);
		minute.addEventFilter(KeyEvent.KEY_RELEASED, handler);
		// text listener (hours)
		ChangeListener<String> lister = utils.EventFilters.getHoursListener(hour, 2, 24, hourProperty);
		hour.getEditor().addEventFilter(KeyEvent.KEY_TYPED, utils.EventFilters.getOnlyNumbersFilter());
		hour.getEditor().textProperty().addListener(lister);
		// text listener (minutes)
		ChangeListener<String> minutesListener = utils.EventFilters.getHoursListener(minute, 2, 60, minuteProperty);
		minute.getEditor().addEventFilter(KeyEvent.KEY_TYPED, utils.EventFilters.getOnlyNumbersFilter());
		minute.getEditor().textProperty().addListener(minutesListener);
		// duration listener (minutes)
		ChangeListener<String> durationsListener = utils.EventFilters.getHoursListener(duration, 3, 240, durationProperty);
		duration.getEditor().addEventFilter(KeyEvent.KEY_TYPED, utils.EventFilters.getOnlyNumbersFilter());
		duration.getEditor().textProperty().addListener(durationsListener);
	}
	
	private Node createImageNode() {
		Image image =  new Image(Icons.class.getResource("attentionSmall.png").toString(),16,16,true,true);
		ImageView imageView = new ImageView(image);
		return imageView;
	}
	
	
	class MyBooleanBinding extends BooleanBinding{
		{
			super.bind(dayProperty);
			super.bind(hourProperty);
			super.bind(minuteProperty);
			super.bind(durationProperty);
		}
		
		public MyBooleanBinding() {
			super();
		}
		
		private final SimpleBooleanProperty totalProperty = new SimpleBooleanProperty(true);
		@Override
		protected boolean computeValue() {
			System.out.println("computeValue ----------------__>");
			totalProperty.setValue(!(dayProperty.getValue() && hourProperty.getValue() && minuteProperty.getValue() && durationProperty.getValue() ));
			return !totalProperty.getValue();
		}
		public SimpleBooleanProperty getTotalProperty() {
			return totalProperty;
		}
	}
	public class AppointmentDetails{
		private Aclass theClass;
		private int day;
		private int hour;
		private int minute;
		private int duration;
		public Aclass getTheClass() {return theClass;}
		public void setTheClass(Aclass aClass) { theClass = aClass;}
		public int getDay() {return day;}
		public void setDay(int day) {this.day = day;}
		public int getHour() {return hour;}
		public void setHour(int hour) {this.hour = hour;}
		public int getMinute() {return minute;}
		public void setMinute(int minute) {this.minute = minute;}
		public int getDuration() {return duration;}
		public void setDuration(int duration) {this.duration = duration;}
	}
	/**
	 * to be used for the choiceList */
	public class ClassKeyPair{
		private final Aclass key;
		private final String value;
		
		public ClassKeyPair(Aclass theClass){
			key = theClass;
			value = theClass.getNameAndCode();
		}
		@Override
		public String toString(){ return value; }
		public Aclass getTheClass(){ return key; }
	}
}