/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import controllers.BillingDialogController;
import java.math.BigDecimal;
import javafx.scene.layout.AnchorPane;
import utils.dialogs.BillingDialog.AUTO_COMPLETE_FLAG;

/**
 *
 * @author dimitris
 */

public class BillingDialogScene extends AnchorPane{
    
    private static AnchorPane instance = null;
    private static BillingDialogController sceneController = null;
    private BillingDialogScene() { /*Exists only to defeat instantiation. */ }
    
    public static AnchorPane getInstance(AUTO_COMPLETE_FLAG options) {
        if(instance == null) {
//            instance = new DialogScene();
            sceneController = new BillingDialogController("", "", null, BigDecimal.valueOf(1000), options);
            instance = (AnchorPane)sceneController.getScene().getRoot();
        }
        return instance;
    }
    
    public static BillingDialogController getController(){
        return sceneController;
    }
}
