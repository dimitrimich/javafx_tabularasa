/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package utils.dialogs;

import java.math.BigDecimal;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

/**
 *
 * @author dimitris
 */
public class BillingConfirmationDialog {
	private static Dialog <Boolean> instance;
	
	private BillingConfirmationDialog(){}
	
	public static Dialog <Boolean> getInstance(){
		if(instance != null){return instance;}
		
		instance = new Dialog<>();
		instance.setTitle("Payment Successful");
		instance.setHeaderText(null);
		// Set the icon (must be included in the project).
		instance.setGraphic(null);
		
		// Set the button types.
		ButtonType okButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		instance.getDialogPane().getButtonTypes().addAll(okButtonType);
		instance.getDialogPane().setContent(BillingConfirmationDialogScene.getInstance());
		
		instance.setResultConverter(dialogButton -> {
			return BillingConfirmationDialogScene.getController().isGenerateReceipt();
		});
		return instance;
	}
	public static void initValues(BigDecimal received, BigDecimal discount){
		BillingConfirmationDialogScene.getController().setAmounts(received, discount);
		
	}
}