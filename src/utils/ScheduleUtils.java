/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package utils;

import DB.objects.Aclass;
import DB.objects.Lesson;
import DB.objects.ListOfWLessons;
import DB.objects.Schedule;
import DB.objects.WeeklyLesson;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author dimitris
 */
public class ScheduleUtils {
	@Deprecated
	public static void createAllLessons(Schedule schedule, LocalDate startDate, LocalDate endDate) {
		try {
			
			Map<LocalDate, ListOfWLessons> theMap = schedule.getAllWLessons();
			System.out.println("Start Date = " + startDate.toString() + " --> " + endDate.toString());
			System.out.println("Schedule = " + schedule.getwLessons().size());
			
			// For FULLY scheduled Lessons
			// go through dates startDate -> endDate
			for (LocalDate thisDay = startDate; (thisDay.isBefore(endDate) || thisDay.isEqual(endDate)); thisDay = thisDay.plusDays(1)) {
				ListOfWLessons listOfLessons = new ListOfWLessons();
				// traverse all weekly lessons
				for (WeeklyLesson thisLesson : schedule.getwLessons()) {
					// weekly Lesson Monday = 2 but DayOfWeek.Monday = 1
					if (thisDay.getDayOfWeek() == DayOfWeek.of(getDayOfTheWeekForLocalDate(thisLesson.getDayOfTheWeek()))) {
						if(thisLesson.isFullyScheduled()){
							listOfLessons.setTheDate(thisDay);
							listOfLessons.getAllLessons().add(new Lesson(thisLesson, thisDay));
						}
					}
				}
				if (!listOfLessons.getAllLessons().isEmpty()){
					theMap.put(thisDay, listOfLessons);
					System.out.println(thisDay.toString() +" has "+listOfLessons.getAllLessons().size()+" lessons");
				}
			}
			
			// Lessons that are missing Day or Time
			for (WeeklyLesson thisLesson : schedule.getwLessons()) {
				if(!thisLesson.isFullyScheduled()){
					LocalDate fromSunday = previousSunday(startDate);
					LocalDate toSunday = previousSunday(endDate);
					for (LocalDate thisSunday = fromSunday; !(thisSunday.equals(toSunday.plusDays(7))); thisSunday = thisSunday.plusDays(7)){
						LocalDate addTo = LocalDate.of(3000, thisSunday.getMonthValue(), thisSunday.getDayOfMonth());
						ListOfWLessons listOfLessons = theMap.get(addTo);
						if(listOfLessons == null){
							listOfLessons = new ListOfWLessons();
							listOfLessons.setTheDate(addTo);
						}
						listOfLessons.getAllLessons().add(new Lesson(thisLesson, addTo));
						System.out.println("not scheduled Lesson added to  = "+ addTo.toString());
						theMap.put(addTo, listOfLessons);
					}
				}
			}
		} catch (Exception e) {
			System.err.println("EXCEPTION");
			e.printStackTrace(System.err);
		}
	}
	
	/**
	 * All lessons that belong to the same weekly lesson will be moved to new Date
	 * Also change the original "parent" weekly lesson
	 * @param  newDateCal  the new date to move lessons to
	 * @param theClass the class to edit
	 * @param weeklyLessonIndex of which weekly lesson to move lessons
	 */
	@Deprecated
	public static void moveAllLessonsToNewDate(Calendar newDateCal, Aclass theClass, int weeklyLessonIndex){
		int year = newDateCal.get(Calendar.YEAR);
		int month = newDateCal.get(Calendar.MONTH); // January = 0 in calendar | 1 in LocalDate
		int date = newDateCal.get(Calendar.DATE);
		int day = newDateCal.get(Calendar.DAY_OF_WEEK); // Sunday = 1 in calendar | 7 in LocalDate
		int hour = newDateCal.get(Calendar.HOUR_OF_DAY);
		int minute = newDateCal.get(Calendar.MINUTE);
		
		for(WeeklyLesson parentLesson : theClass.getSchedule().getwLessons()){
			if(parentLesson.getIndexNumber() == weeklyLessonIndex){
				System.out.println("Setting Parent weekly lesson's details");
				parentLesson.setDayOfTheWeek(day);
				parentLesson.setStartingHour(hour);
				parentLesson.setStartingMinute(minute);
			}
		}
		Map<LocalDate, ListOfWLessons> theMap = theClass.getSchedule().getAllWLessons();
		
		Map<LocalDate, ListOfWLessons> theMapToAdd = new HashMap<>();
		
//        theMap.keySet().iterator()
		Iterator<LocalDate> itDate = theMap.keySet().iterator();
		while(itDate.hasNext()){
			LocalDate key = itDate.next();
//			List<Lesson> todaysLessons = theMap.get(key).getAllLessons();
			Iterator <Lesson> it = theMap.get(key).getAllLessons().iterator();
			while(it.hasNext()){
				Lesson aLesson = it.next();
				if(!aLesson.isExtraLesson()){
					if(aLesson.getIndexNumber() == weeklyLessonIndex){
						LocalDate oldDate = aLesson.getTheDate();
						System.out.println("Old Date = "+ oldDate.toString());
						// find new LocalDate from Calendar's Day
						LocalDate newDate = getDayInSameWeek(oldDate, day);
						System.out.println("New Date = "+ newDate.toString());
						ListOfWLessons newList;
						if(theMap.containsKey(newDate)){
							System.out.println("--- 1 ----");
							newList = theMap.get(newDate);
							System.out.println("--- 2 ----");
						}else{
							System.out.println("--- 3 ----");
							newList = new ListOfWLessons();
							System.out.println("--- 4 ----");
							newList.setTheDate(newDate);
							System.out.println("--- 5 ----");
						}
						aLesson.setTheDate(newDate);
						aLesson.setDayOfTheWeek(day);
						aLesson.setStartingHour(hour);
						aLesson.setStartingHour(minute);
						it.remove();
						newList.getAllLessons().add(aLesson);
						if(!theMap.containsKey(newDate)){
							theMapToAdd.put(newDate, newList);
						}
					}
				}
			}
		}
		if(!theMapToAdd.isEmpty()){
			theMap.putAll(theMapToAdd);
		}
	}
	
	
	
	private static LocalDate previousSunday(LocalDate toHere){
		switch(toHere.getDayOfWeek()){
			case MONDAY:
				return toHere.minusDays(1);
			case TUESDAY:
				return toHere.minusDays(2);
			case WEDNESDAY:
				return toHere.minusDays(3);
			case THURSDAY:
				return toHere.minusDays(4);
			case FRIDAY:
				return toHere.minusDays(5);
			case SATURDAY:
				return toHere.minusDays(6);
			case SUNDAY:
				return toHere;
			default:
				throw new AssertionError(toHere.getDayOfWeek().name());
		}
	}
	/** when we have a LocalDate to get our DayOfTheWeek (sunday = 1
	 * @param aDate)*/
	public static int getDayOfTheWeekFromLocalDate(LocalDate aDate){
		switch(aDate.getDayOfWeek().getValue()){
			case 7: return 1;
			case 1: return 2;
			case 2: return 3;
			case 3: return 4;
			case 4: return 5;
			case 5: return 6;
			case 6: return 7;
			default:return 7;
		}
	}
	
	/** when we have "our" day as int, convert to LocalDay.DayOfTheWeek*/
	public static int getDayOfTheWeekForLocalDate(int value){
		switch(value){
			case 1: return 7;
			case 2: return 1;
			case 3: return 2;
			case 4: return 3;
			case 5: return 4;
			case 6: return 5;
			case 7: return 6;
			default:return 7;
		}
	}
	
	/**
	 * @param today
	 * @param day Day as represented in Calendar
	 * get the LocalDate with day = day param of the same week
	 */
	private static LocalDate getDayInSameWeek(LocalDate today, int day){
		//day =  sunday, monday, tue ....
		//today = monday, tuesday wed...
		switch(today.getDayOfWeek()){
			case MONDAY:
				switch(day){
					case Calendar.MONDAY: return today;
					case Calendar.TUESDAY: return today.plusDays(1);
					case Calendar.WEDNESDAY: return today.plusDays(2);
					case Calendar.THURSDAY: return today.plusDays(3);
					case Calendar.FRIDAY: return today.plusDays(4);
					case Calendar.SATURDAY: return today.plusDays(5);
					case Calendar.SUNDAY: return today.minusDays(1);
				}
			case TUESDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(1);
					case Calendar.TUESDAY: return today;
					case Calendar.WEDNESDAY: return today.plusDays(1);
					case Calendar.THURSDAY: return today.plusDays(2);
					case Calendar.FRIDAY: return today.plusDays(3);
					case Calendar.SATURDAY: return today.plusDays(4);
					case Calendar.SUNDAY: return today.minusDays(2);
				}
			case WEDNESDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(2);
					case Calendar.TUESDAY: return today.minusDays(1);
					case Calendar.WEDNESDAY: return today;
					case Calendar.THURSDAY: return today.plusDays(1);
					case Calendar.FRIDAY: return today.plusDays(2);
					case Calendar.SATURDAY: return today.plusDays(3);
					case Calendar.SUNDAY: return today.minusDays(3);
				}
			case THURSDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(3);
					case Calendar.TUESDAY: return today.minusDays(2);
					case Calendar.WEDNESDAY: return today.minusDays(1);
					case Calendar.THURSDAY: return today;
					case Calendar.FRIDAY: return today.plusDays(1);
					case Calendar.SATURDAY: return today.plusDays(2);
					case Calendar.SUNDAY: return today.minusDays(3);
				}
			case FRIDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(4);
					case Calendar.TUESDAY: return today.minusDays(3);
					case Calendar.WEDNESDAY: return today.minusDays(2);
					case Calendar.THURSDAY: return today.minusDays(1);
					case Calendar.FRIDAY: return today;
					case Calendar.SATURDAY: return today.plusDays(1);
					case Calendar.SUNDAY: return today.minusDays(5);
				}
			case SATURDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(5);
					case Calendar.TUESDAY: return today.minusDays(4);
					case Calendar.WEDNESDAY: return today.minusDays(3);
					case Calendar.THURSDAY: return today.minusDays(2);
					case Calendar.FRIDAY: return today.minusDays(1);
					case Calendar.SATURDAY: return today;
					case Calendar.SUNDAY: return today.minusDays(6);
				}
			case SUNDAY:
				switch(day){
					case Calendar.MONDAY: return today.minusDays(6);
					case Calendar.TUESDAY: return today.minusDays(5);
					case Calendar.WEDNESDAY: return today.minusDays(4);
					case Calendar.THURSDAY: return today.minusDays(3);
					case Calendar.FRIDAY: return today.minusDays(2);
					case Calendar.SATURDAY: return today.minusDays(1);
					case Calendar.SUNDAY: return today;
				}
			default:
				throw new AssertionError(today.getDayOfWeek().name());
		}
	}
}
