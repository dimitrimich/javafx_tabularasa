package utils;

import customObjects.Person;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.effect.SepiaTone;
import javafx.scene.input.*;

/**
 * Will contain all filters to stop input of various characters
 * Filter to stop digits, maybe filter to stop other things, don't know
 * @author d.michaelides
 */
public class EventFilters {
    public static EventHandler getNoNumbersFilter(){
        javafx.event.EventHandler h = (EventHandler) (Event k) -> {
            if(k instanceof KeyEvent){
                KeyEvent key = (KeyEvent)k;
                if(key.getCharacter().matches("\\d")){
                    if(!key.isAltDown() && !key.isControlDown() ){
                        key.consume();
                    }
                }
            }
        };
        return h;
    }
    
    public static EventHandler<KeyEvent> getOnlyNumbersFilter_Red_Border(){
        final utils.BooleanWrapper isRed = new utils.BooleanWrapper(false);
        
        javafx.event.EventHandler<KeyEvent> h = (KeyEvent key) -> {
            if(!key.getCharacter().matches("[0-9]") && !key.getCharacter().equals("\b")){
                if(!key.isAltDown() && !key.isControlDown() ){
                    if(!isRed.value){
                        ((Control)key.getSource()).setStyle("-fx-border-color:red");
                        isRed.value = true;
                    }
                    key.consume();
                }
            }else{
                if(isRed.value){
                    isRed.value = false;
                    ((Control)key.getSource()).setStyle("-fx-border-color:null");
                }
            }
        };
        return h;
    }
    public static EventHandler<KeyEvent> getOnlyNumbersFilter(){
        
        javafx.event.EventHandler<KeyEvent> h = (KeyEvent key) -> {
            if(!key.getCharacter().matches("[0-9]") && !key.getCharacter().equals("\b")){
                if(!key.isAltDown() && !key.isControlDown() ){
                    key.consume();
                }
            }
        };
        return h;
    }
    
    
    public static void setEditableTextArea(final TextArea textarea){
        textarea.setEditable(false);
        textarea.setStyle("-fx-background-color:#EEEEEE;");
        textarea.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if(event.getClickCount() == 2){
                textarea.setStyle("-fx-border-color:#11FF11;-fx-cursor:text;-fx-background-color:#EEEEEE;");
                textarea.setEditable(true);
            }
        });
        textarea.focusedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
            if(oldValue == true && newValue == false){
                textarea.setStyle("-fx-border-color:null;-fx-background-color:#EEEEEE;");
                textarea.setEditable(false);
            }
        });
    }
            
    public static void setEditableTextField(final TextField textField){
        textField.setEditable(false);
        textField.setStyle("-fx-background-color:#EEEEEE;");
        textField.setCursor(Cursor.DEFAULT);
        
        textField.addEventHandler(MouseEvent.MOUSE_PRESSED, (MouseEvent event) -> {
            if(event.getClickCount() == 2){
                textField.setStyle("-fx-border-color:#11FF11;-fx-background-color:#EEEEEE;");
                textField.setCursor(Cursor.TEXT);
                textField.setEditable(true);
            }
        });
        textField.focusedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
            if(oldValue == true && newValue == false){
                textField.setStyle("-fx-border-color:null;-fx-background-color:#EEEEEE;");
                textField.setCursor(Cursor.DEFAULT);
                textField.setEditable(false);
            }
        });
        textField.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if(t.getCode() == KeyCode.ENTER){
                ((TextField)t.getSource()).getParent().requestFocus();
//                    theScroll2.requestFocus();
            }
        });
    
    
    }
     public static ChangeListener<String> getHoursListener(final ComboBox<String> comboBox, final int textLength, final int limit, SimpleBooleanProperty hasText){
        ChangeListener<String> listener = (final ObservableValue<? extends String> ov, final String oldValue, final String newValue) -> {
            if (comboBox.getEditor().getText().length() > textLength) {
                String s = comboBox.getEditor().getText().substring(0, textLength);
                comboBox.getEditor().setText(s);
                comboBox.getEditor().positionCaret(textLength);
                hasText.setValue(true);
            }
            if(comboBox.getEditor().getText() != null && !comboBox.getEditor().getText().isEmpty()){
                try{
                    int hour_int = Integer.parseInt(comboBox.getEditor().getText());
                    if(hour_int > limit){
                        comboBox.getEditor().setText(Integer.toString(limit));
                    }else if(hour_int < 0){
                        comboBox.getEditor().setText("0");
                    }
                }catch(NumberFormatException e){
                }
                hasText.setValue(true);
            }else{
                hasText.setValue(false);
            }
        };
        return listener;
    }
    
    
    public static ChangeListener<String> getHoursListener(final ComboBox<String> comboBox, final int textLength, final int limit){
        return getHoursListener(comboBox, textLength, limit, new SimpleBooleanProperty());
    }
    
    
    /**
     * 1. Copy in clipBoard the index of the person to copy to the other table
     * @param cell
     * @param index
     * @return 
     */
    public static EventHandler getDragDetectedHandler(final TableCell<Person, Person> cell, final String index){
        javafx.event.EventHandler h = (EventHandler) (Event k) -> {
            Dragboard db = cell.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.put(DataFormat.PLAIN_TEXT, index);
            db.setContent(content);
            k.consume();
        };
        return h;
    }
    
    
    /** 2. on drag is finishe
     * @param from
     * @return */
    public static EventHandler getOnDragDoneHandler(final TableView from){
        javafx.event.EventHandler h = (EventHandler<DragEvent>) (DragEvent event) -> {
            event.consume();
        };
        return h;
    }
    
     /**
      * 3.  for the Target Table
     * @param target
      */
    public static void setOnDragOver(final TableView target){
        target.setOnDragOver((DragEvent event) -> {
            if ((((TableCell<Person, Person>) event.getGestureSource()).getTableView()) != target && event.getDragboard().hasString()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        });
    
    }
    
     /**
      * 4. for the Target Table
     * @param target
      */
    public static void setOnDragEntered(final TableView target){
        target.setOnDragEntered((DragEvent event) -> {
            if ((((TableCell<Person, Person>) event.getGestureSource()).getTableView()) != target && event.getDragboard().hasString()) {
                SepiaTone sepiaTone = new SepiaTone();
                sepiaTone.setLevel(0.4);
                target.setEffect(sepiaTone);
            }
            event.consume();
        });
    }
     /**
      * 5. for the Target Table
     * @param target
      */
    public static void setOnDragExited(final TableView target){
         target.setOnDragExited((DragEvent event) -> {
             target.setEffect(null);
             event.consume();
         });
    }
    
     /**
      * 6. for the Target Table
      */
    public static void setOnDragDropped(final TableView target, final ObservableList<Person> sourceItems){
        
        target.setOnDragDropped((DragEvent event) -> {
            System.out.println(" Dropped item");
            /* data dropped */
            /* if there is a string data on dragboard, read it and use it */
            Dragboard db = event.getDragboard();
            boolean success = false;
            
            if ((((TableCell<Person, Person>) event.getGestureSource()).getTableView()) != target && db.hasString()) {
                success = true;
                int index;
                try{
                    index = Integer.parseInt(db.getString());
                }catch(Exception e){
                    event.consume();
                    return;
                }
                try{
//                        (((TableCell<Person, Person>) event.getGestureSource()).getTableView()).getItems().add(persons.get(index));
                       
//                        targetItems.add(((Person)source.getItems().get(index)));
//                        source.getItems().remove(index);
                    
                    target.getItems().add(sourceItems.get(index));
                    sourceItems.remove(index);
                    (((TableCell<Person, Person>) event.getGestureSource()).getTableView()).getItems().remove(index);
                }catch(Exception e){
                    System.out.println("Regular Exception "+e.getClass().getSimpleName()+", "+e.getMessage());
                }
                
            }
            /* let the source know whether the string was successfully
            * transferred and used */
            event.setDropCompleted(success);
            
            event.consume();
        });
    }
    
    
    
    
    
    
    
    
    
    
    
    }
    
   
    

