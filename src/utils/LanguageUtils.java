/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package utils;

import java.util.Locale;
import java.util.ResourceBundle;
import static utils.Constants.ENGLISH;

/**
 *
 * @author d.michaelides
 */
public class LanguageUtils {
	private static LanguageUtils language = new LanguageUtils( );
	
	/* A private Constructor prevents any other
	* class from instantiating.
	*/
	private LanguageUtils(){ }
	
	/* Static 'instance' method */
	public static LanguageUtils getInstance( ) {
		return language;
	}
	public static Locale getCurrentLocale(){
		return getInstance().getCurrentLanguage().getLocale();
	}
	
	public static String getString(String name){
		return getInstance().getCurrentLanguage().getString(name);
	}
	
	private static boolean turn = false;
	private final static ResourceBundle labelsGr = ResourceBundle.getBundle("Labels", new Locale("el"));
	private final static ResourceBundle labelsEn = ResourceBundle.getBundle("Labels",Locale.ENGLISH);
	private static ResourceBundle currentLabels = labelsGr;
	private static int currentLanguage = Constants.GREEK;
	
	public ResourceBundle switchLanguage(){
		turn = !turn;
		if(turn){
			currentLabels = labelsEn;
		}
		else{
			currentLabels = labelsGr;
		}
		return currentLabels;
	}
	public ResourceBundle switchLanguage(int language){
		switch(language){
			case Constants.ENGLISH:
				currentLabels = labelsEn;
				currentLanguage = Constants.ENGLISH;
				break;
			case Constants.GREEK:
				currentLabels = labelsGr;
				currentLanguage = Constants.GREEK;
				break;
		}
		return currentLabels;
	}
	public ResourceBundle getResourceBundle(int language){
		if(language == ENGLISH)
			return labelsEn;
		return labelsGr;
		
	}
	public ResourceBundle getCurrentLanguage(){
		return currentLabels;
	}
	public int getCurrentLanguageCode(){
		return currentLanguage;
	}
}
