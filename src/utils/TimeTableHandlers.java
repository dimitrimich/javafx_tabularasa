package utils;

import customControls.TimeTableCell;
import java.util.Collections;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.effect.SepiaTone;
import javafx.scene.input.*;
import javafx.scene.layout.VBox;

/**
 * Will contain all filters to stop input of various characters
 * Filter to stop digits, maybe filter to stop other things, don't know
 * @author d.michaelides
 */
public class TimeTableHandlers {
    public static VBox times;
    public static void setTimesVBox(VBox t){
        times = t;
    }
    public static int startingHour;
    public static void setStartingHour(int _startingHour){
        startingHour = _startingHour;
    }
    
    /**
     * 1. Copy in clipBoard the index of the person to copy to the other table
     */
    public static void setOnDragDetected(final TimeTableCell cell){
        cell.setOnDragDetected((MouseEvent k) -> {
            if(cell.isClassAssigned()){
                Dragboard db = cell.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                content.put(DataFormat.PLAIN_TEXT, cell.getText());
                db.setContent(content);
                k.consume();
            }
        });
    }
    
    /** 2. on drag is finished
     * @param from
     * @return */
    public static EventHandler getOnDragDoneHandler(final TimeTableCell from){
        javafx.event.EventHandler h = Event::consume;
        return h;
    }
     /**
      * 3.  for the Target Table
     * @param target
      */
    public static void setOnDragOver(final TimeTableCell target){
        target.setOnDragOver((DragEvent event) -> {
            if ( event.getGestureSource() != target && event.getDragboard().hasString()){// && !((target).getParent().getId().equals("not"))) {
                if(!TimeTableUtils.doesItFit(((VBox)target.getParent()), ((TimeTableCell) event.getGestureSource()), target)){
                    target.setText("Does not fit");
                    /* Don't allow drop */
                    event.acceptTransferModes(TransferMode.NONE);
                }else{
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
            }
            event.consume();
        });
    }
    
     /**
      * 4. for the Target Table
     * @param target
      */
    public static void setOnDragEntered(final TimeTableCell target){
        target.setOnDragEntered((DragEvent event) -> {
            if (event.getGestureSource() != target && event.getDragboard().hasString()){// && !((target).getParent().getId().equals("not"))) {
                SepiaTone sepiaTone = new SepiaTone();
                sepiaTone.setLevel(0.4);
                target.setEffect(sepiaTone);
                
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setEffect(sepiaTone);
                
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).getStylesheets().add("b");
//            ((TextArea)times.getChildren().get(target.getPositionInDay())).setStyle("-fx-border-color:red;");
                
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setText(
//                    "-->"+
//                    ((TextArea)times.getChildren().get(target.getPositionInDay())).getText()
//                );
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setScaleX(1.1);
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setScaleY(1);
            }
            event.consume();
        });
    }
     /**
      * 5. for the Target Table
     * @param target
      */
    public static void setOnDragExited(final TimeTableCell target){
         target.setOnDragExited((DragEvent event) -> {
//             Collections.replaceAll(((TextArea)times.getChildren().get(target.getPositionInDay())).getStylesheets(), "b", "a");
//            
//            ((TextArea)times.getChildren().get(target.getPositionInDay())).getStylesheets().set(0, "a");
//            ((TextArea)times.getChildren().get(target.getPositionInDay())).setStyle("-fx-border-color:null;");
//             
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setText(
//                    ((TextArea)times.getChildren().get(target.getPositionInDay())).getText().replaceAll("-->", "")
//                );
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setScaleX(1);
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setScaleY(0.3333);
//                ((TextArea)times.getChildren().get(target.getPositionInDay())).setEffect(null);
                if("Does not fit".equals(target.getText())){
                    target.setText(target.getName());
                }
             target.setEffect(null);
             event.consume();
         });
    }
    
     /**
      * 6. for the Target Table
     * @param target
     * @param showDetails
      */
    public static void setOnDragDropped(final TimeTableCell target, final TextArea showDetails){
        
        target.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            
            // not the same && was dragged
            if (event.getGestureSource() != target && db.hasString() ) {
//                if(!TimeTableUtils.doesItFit(((VBox)target.getParent()), ((TimeTableCell) event.getGestureSource()), target.getPositionInDay())){
//                    System.out.println("will NOT be added");
//                    return;
//                }

// remove from inital table
//                if(((TimeTableCell) event.getGestureSource() ).getParent().getId().equals("not")){
//                    ((VBox)((TimeTableCell) event.getGestureSource() ).getParent()).getChildren().remove((TimeTableCell)event.getGestureSource());
//                    DragNDropUtils_Cells.moveClass((VBox)target.getParent(), ((TimeTableCell) event.getGestureSource() ), target.getPositionInDay(), showDetails, true);
//                    return;
//                }
                
                // target and source have classes
                if(target.isClassAssigned() && ((TimeTableCell) event.getGestureSource()).isClassAssigned()){
                    if(target.getParent() == ((TimeTableCell) event.getGestureSource()).getParent()){
                        // classses of the same day
                        TimeTableUtils.swapClassesOfTheSameDay(((VBox)target.getParent()), target, ((TimeTableCell) event.getGestureSource()));
                        return;
                    }else{
                        // classes of different days
                        TimeTableUtils.swapClassesFromDifferentDays(((VBox)((TimeTableCell) event.getGestureSource()).getParent()), ((TimeTableCell) event.getGestureSource()), target, showDetails);
                        return;
                    }
                }
                if((target).getParent().getId().equals("not")){
//                    ((TimeTableCell) event.getGestureSource()).setPositionInDay(0);
                    int positionToMove = TimeTableUtils.getFirstIndexNotOccupied((VBox) target.getParent());
                    if(positionToMove >= 0){
                        TimeTableUtils.moveClass((VBox)target.getParent(), ((TimeTableCell) event.getGestureSource()), target, showDetails, false);
                    }else{
                        ((VBox)target.getParent()).getChildren().add(((TimeTableCell) event.getGestureSource()));
                    }
                } else {
                    TimeTableUtils.moveClass((VBox)target.getParent(), ((TimeTableCell) event.getGestureSource()), target, showDetails, false);
                }
                success = true;
            }
            
            /* let the source know whether the string was successfully
            * transferred and used */
            event.setDropCompleted(success);
            event.consume();
        });
    }
    
    
     /**
      * 3.  for the Target Table
     * @param target
      */
    public static void setOnRightClick(final TimeTableCell target){
        target.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if(t.getButton() == MouseButton.SECONDARY){
                if(target.isClassAssigned()){
                    target.setContextMenu(null);
                    ContextMenu cm = new ContextMenu();
                    MenuItem m1 = new MenuItem(target.getName());
                    MenuItem m2 = new MenuItem(target.getDescription());
                    MenuItem m3 = new MenuItem("                     ");
                    cm.getItems().add(m1);
                    cm.getItems().add(m2);
                    cm.getItems().add(m3);
                    target.setContextMenu(cm);
                }else{
                    ContextMenu cm = new ContextMenu();
                    MenuItem m1 = new MenuItem("                     ");
                    MenuItem m2 = new MenuItem("                     ");
                    MenuItem m3 = new MenuItem("                     ");
                    cm.getItems().add(m1);
                    cm.getItems().add(m2);
                    cm.getItems().add(m3);
                    target.setContextMenu(cm);
                }
            }
        });
    }
        
    public static void setOnClicked(final TimeTableCell target, final TextArea selected){
        target.setOnMouseClicked((MouseEvent t) -> {
            if(t.getButton() == MouseButton.PRIMARY){
                if(target.isClassAssigned()){
                    populateArea(target, selected);
                }
            }
        });
    }
    private static void populateArea(final TimeTableCell target, final TextArea selected){
        selected.setText(
                  " "+target.getName()+"\n"
                + " TeacherName  <--- \n"
                + " 8:00 -> 10:00\n"
                + " Monday");
    
    }
        
//        target.hoverProperty().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldV, Boolean newV) {
//                    target.getContextMenu().getItems().clear();
//                if(target.isClassAssigned()){
//                    if(!oldV && newV){
//                        target.get
//                        target.getContextMenu().getItems().add(new MenuItem(target.getName()));
//                        target.getContextMenu().getItems().add(new MenuItem(target.getDescription()));
////                        target.setTooltip(new Tooltip(target.getName()+ "\n"+target.getDescription()));
////                        target.setMinHeight(55);
////                        target.setMinWidth(150);
////                        target.setText(target.getName()+"\n"
////                                + ""+target.getDescription());
//                    }else{
//                            target.getContextMenu().getItems().clear();
////                        target.setTooltip(null);
////                        target.setMinHeight(35);
////                        target.setMinWidth(100);
////                        target.setText(target.getName());
//                    }
//                }
//            }
//        });
                
    
    
    
    
    
    
    
    
    
    
    }
    
   
    

