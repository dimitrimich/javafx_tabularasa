package utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.SepiaTone;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Will contain all filters to stop input of various characters
 * Filter to stop digits, maybe filter to stop other things, don't know
 * @author d.michaelides
 */
public class DragNDropHandlers_Cells {
    
    /**
     * 1. Copy in clipBoard the index of the person to copy to the other table
     * @param cell
     */
    public static void setOnDragDetected(final Label cell){
        
        cell.setOnDragDetected((MouseEvent k) -> {
            Dragboard db = cell.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.put(DataFormat.PLAIN_TEXT, cell.getText());
            db.setContent(content);
            k.consume();
        });
    }
    
    
    /** 2. on drag is finishe
     * @param fromd*/
    public static EventHandler getOnDragDoneHandler(final Label from){
        javafx.event.EventHandler h = (EventHandler<DragEvent>) (DragEvent event) -> {
            event.consume();
        };
        return h;
    }
    
     /**
      * 3.  for the Target Table
     * @param target
      */
    public static void setOnDragOver(final Label target){
        target.setOnDragOver((DragEvent event) -> {
            if ( event.getGestureSource() != target && event.getDragboard().hasString()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        });
    
    }
    
     /**
      * 4. for the Target Table
     * @param target
      */
    public static void setOnDragEntered(final Label target){
        target.setOnDragEntered((DragEvent event) -> {
            if (event.getGestureSource() != target && event.getDragboard().hasString()) {
                SepiaTone sepiaTone = new SepiaTone();
                sepiaTone.setLevel(0.4);
                target.setEffect(sepiaTone);
            }
            event.consume();
        });
    }
     /**
      * 5. for the Target Table
     * @param target
      */
    public static void setOnDragExited(final Label target){
         target.setOnDragExited((DragEvent event) -> {
             target.setEffect(null);
             event.consume();
         });
    }
    
     /**
      * 6. for the Target Table
     * @param target
      */
    public static void setOnDragDropped(final Label target){
        
        target.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            
            if (event.getGestureSource() != target && db.hasString()) {
                String temp = target.getText();
                target.setText(db.getString());
                ((Label) event.getGestureSource() ).setText(temp);
            }
            /* let the source know whether the string was successfully
            * transferred and used */
            event.setDropCompleted(success);
            event.consume();
        });
    }
    
    
    
    
    
    
    
    
    
    
    
    }
    
   
    

