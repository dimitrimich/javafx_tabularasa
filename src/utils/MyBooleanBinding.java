/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author dimitris
 */
public class MyBooleanBinding  extends BooleanBinding{
    public MyBooleanBinding(){
        super();
        dependancies = FXCollections.observableArrayList();
        listener = (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            theValue.setValue(computeValue());
        };
    }

    private final ChangeListener<Boolean> listener;
    private final ObservableList<BooleanProperty> dependancies;
    private SimpleBooleanProperty theValue = new SimpleBooleanProperty();
    public void reInitialize(){
        for(BooleanProperty prop : dependancies){
            prop.removeListener(listener);
        }
        dependancies.clear();
    }

    public SimpleBooleanProperty getTheValue() {
        return theValue;
    }
    public void setTheValue(SimpleBooleanProperty theValue) {
        this.theValue = theValue;
    }

    @Override
    protected final boolean computeValue() {
        for(BooleanProperty prop : dependancies){
            if(!prop.getValue()){
                return false;
            }
        }
        return true;
    }

    public void bind(BooleanProperty b){
        dependancies.add(b);
        b.addListener(listener);
    }
}
