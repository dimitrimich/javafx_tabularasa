/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import controllers.SaveObjectController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import screens.FXMLs;

/**
 *
 * @author dimitris
 */
public class MessageScreenHandler {
    private Stage parentStage;
    private AnchorPane parentRoot;
    private Image imageToDisplay;
    private String messageToDisplay;
    private String[] buttonLabels;
    private Stage mainStage;
    private Scene mainScene;
    private SaveObjectController controller;
    
    public MessageScreenHandler(Stage pStage, AnchorPane pRoot, Image imgToDisplay, String msgToDisplay, String [] btnLabels){
        parentStage = pStage;
        parentRoot = pRoot;
        imageToDisplay = imgToDisplay;
        messageToDisplay = msgToDisplay;
        buttonLabels = btnLabels;
        try{
            initScene();
        }catch(Exception e){
            System.err.println("Exception while showing message screen - "+ e.getMessage());
        }
    }
    
    private void  initScene() throws Exception{
//        final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("SaveObject.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
//        mainScene = new Scene((AnchorPane)myLoader.load());
//        controller = (SaveObjectController)myLoader.getController();
//        
//        controller.init(imageToDisplay, messageToDisplay, buttonLabels);
//        
//        parentStage.setOnCloseRequest((WindowEvent ev) -> {
//            mainScene.getWindow().fireEvent(new WindowEvent(parentStage, WindowEvent.WINDOW_CLOSE_REQUEST));
//        });
//        
//        mainStage.setScene(mainScene);
//        mainStage.toFront();
//        parentStage.setOpacity(0.6);
//        parentRoot.mouseTransparentProperty().bind(mainScene.getWindow().showingProperty());
//        parentRoot.mouseTransparentProperty().bind(mainScene.getWindow().showingProperty());
    }
    
    public void showMessageStage(){
        mainStage.show();
    }
    
    public SaveObjectController getController(){
        return controller;
    }
    
    public Stage getParentStage() {
        return parentStage;
    }
    public void setParentStage(Stage parentStage) {
        this.parentStage = parentStage;
    }
    public AnchorPane getParentRoot() {
        return parentRoot;
    }
    public void setParentRoot(AnchorPane parentRoot) {
        this.parentRoot = parentRoot;
    }
    public Image getImageToDisplay() {
        return imageToDisplay;
    }
    public Scene getMainScene() {
        return mainScene;
    }
    public String getMessageToDisplay() {
        return messageToDisplay;
    }
    public Stage getMainStage() {
        return mainStage;
    }

}