/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;

/**
 *
 * @author d.michaelides
 */
public abstract class Constants {
    public final static int ENGLISH = 1;
    public final static int GREEK = 2;
    public final static int STUDENT = 3;
    public final static int TEACHER = 4;
    public final static int CLASS = 5;
    
    public final static int SCREEN_HOME = 100;
    public final static int SCREEN_CREATE_STUDENT = 101;
    public final static int SCREEN_CREATE_TEACHER = 102;
    public final static int SCREEN_CREATE_CLASS = 103;
    public final static int SCREEN_VIEW_STUDENTS = 104;
    public final static int SCREEN_VIEW_TEACHERS = 105;
    public final static int SCREEN_VIEW_CLASSES = 106;
    
    public final static int SCREEN_OTHER_TABLES = 107;
    public final static int SCREEN_OTHER_TIME = 108;
    public final static int SCREEN_OTHER_TIME_NEW = 109;
    public final static int SCREEN_HOME_2 = 110;
    public final static int SCREEN_SETTINGS = 111;
    public final static int SCREEN_BILLING = 113;
    public final static int SCREEN_BILLING_STUDENTS = 114;
    public final static int SCREEN_BILLING_ENTRY = 115;
    public final static int SCREEN_BILLING_EDIT_PAYMENT = 116;
    public final static int SCREEN_TEMP = 117;
    public final static HashMap<String, Integer> screens = new HashMap<>(17); // increase it 
    static {
        screens.put("HomePage", SCREEN_HOME);
        screens.put("CreateStudent", SCREEN_CREATE_STUDENT);
        screens.put("CreateTeacher", SCREEN_CREATE_TEACHER);
        screens.put("CreateClass", SCREEN_CREATE_CLASS);
        screens.put("ViewStudents", SCREEN_VIEW_STUDENTS);
        screens.put("ViewTeachers", SCREEN_VIEW_TEACHERS);
        screens.put("ViewClasses", SCREEN_VIEW_CLASSES);
        screens.put("PersonsTable_button", SCREEN_OTHER_TABLES);
        screens.put("TimeTable", SCREEN_OTHER_TIME);
        screens.put("TimeTableNew", SCREEN_OTHER_TIME_NEW);
        screens.put("Home", SCREEN_HOME_2);
        screens.put("Settings", SCREEN_SETTINGS);
        screens.put("BillingInfo", SCREEN_BILLING);
        screens.put("BillingStudents", SCREEN_BILLING_STUDENTS);
        screens.put("BillingEntry", SCREEN_BILLING_ENTRY);
        screens.put("BillingEditPayment", SCREEN_BILLING_EDIT_PAYMENT);
        screens.put("Temp", SCREEN_TEMP);
    }
    
    public final static int FIELD_FNAME = 80;
    public final static int FIELD_LNAME = 81;
    public final static int FIELD_ADDRESS_1 = 82;
    public final static int FIELD_STREET_NO_1 = 83;
    public final static int FIELD_STREET_NAME_1 = 84;
    public final static int FIELD_STREET_AREA_1 = 85;
    public final static int FIELD_STREET_CITY_1 = 86;
    public final static int FIELD_STREET_ZIP_1 = 87;
    public final static int FIELD_ADDRESS_2 = 88;
    public final static int FIELD_STREET_NO_2 = 89;
    public final static int FIELD_STREET_NAME_2 = 90;
    public final static int FIELD_STREET_AREA_2 = 91;
    public final static int FIELD_STREET_CITY_2 = 92;
    public final static int FIELD_STREET_ZIP_2 = 93;
    public final static int FIELD_PHONE_1 = 94;
    public final static int FIELD_PHONE_2 = 95;
    public final static int FIELD_EMAIL = 96;
    public final static LocalDate UNKOWN_DATE = LocalDate.of(1900, Month.JANUARY, 1);
    public final static boolean INCREASE = true;
    public final static boolean DECREASE = false;
    
    
}
