package customControls;

import DB.objects.Aclass;
import DB.objects.WeeklyLesson;
import javafx.scene.control.TextArea;

/**
 *
 * @author d.michaelides
 */
public final class TimeTableCell extends TextArea{
    private boolean classAssigned = false;
    private String name;
    private String description;
    private String hour;
    private String minute;
    private String duration;
//    private int positionInDay = 0;
    
    public TimeTableCell(){
        setEditable(false);
//        setPrefHeight(30.0);
        setPrefHeight(5.0);
        setPrefWidth(100.0);
        setWrapText(true);
//        setStyle("-fx-background-color:lightGrey;");
        getStyleClass().add("class-cell");
    }
    
    public TimeTableCell(TimeTableCell c){
        setEditable(false);
        setPrefHeight(c.getPrefHeight());
        setPrefWidth(100.0);
        setWrapText(true);
        setStyle(c.getStyle());
//        setPositionInDay(c.getPositionInDay());
        setDescription(c.getDescription());
        setDescription(c.getHour());
        setClassAssigned(c.isClassAssigned());
        setDuration(c.getDuration());
        setText(c.getText());
        setName(c.getName());
    }
    
    public TimeTableCell(Aclass theClass, WeeklyLesson theLesson){
        setEditable(false);
        setPrefHeight(theLesson.getDuration());
        setPrefWidth(100.0);
        setWrapText(true);
        getStyleClass().add("class-cell");
        setStyle("-fx-background-color:"+theClass.getColor()+";");
//        setPositionInDay(c.getPositionInDay());
        setDescription(theClass.getDescription());
        setClassAssigned(true);
        setDuration(Integer.toString(theLesson.getDuration()));
        setName(theClass.getName());
    }

//    public int getPositionInDay() {
//        return positionInDay;
//    }
//    public void setPositionInDay(int p ) {
//        positionInDay = p;
//    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isClassAssigned() {
        return classAssigned;
    }

    public void setClassAssigned(boolean classAssigned) {
        this.classAssigned = classAssigned;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
