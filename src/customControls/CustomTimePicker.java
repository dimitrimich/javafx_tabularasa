/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package customControls;

import java.time.LocalTime;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import jfxtras.scene.control.LocalTimePicker;
import jfxtras.scene.layout.VBox;
import utils.LanguageUtils;

/**
 *
 * @author dimitris
 */
public class CustomTimePicker extends VBox{
    private LocalTimePicker picker = new LocalTimePicker();
    private ChoiceBox<String> dayChoice;
    private ComboBox<String> duration;
    public CustomTimePicker(ChoiceBox<String> dayChoice, ComboBox<String> dur){
        super();
        picker.setLocale(LanguageUtils.getInstance().getCurrentLanguage().getLocale());
        this.dayChoice = dayChoice;
        this.duration = dur;
        setMaxHeight(25);
        setPrefHeight(25);
        picker.setPrefHeight(25);
        picker.setMaxHeight(25);
        picker.setLocalTime(LocalTime.of(12, 00));
        picker.setDisable(true);
        picker.setOpacity(0.8);
        
//        setPadding(new Insets(30, 30, 30, 30));
        getChildren().addAll(picker);
        addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if(event.getButton() == MouseButton.SECONDARY){
                if(!duration.isDisabled()){
                    wasClicked();
                }
            }
        });
        dayChoice.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if(event.getButton() == MouseButton.SECONDARY){
                if(!duration.isDisabled()){
                    wasClicked();
                }
            }
        });
        addEventFilter(MouseEvent.ANY, (MouseEvent event) -> {
            if(event.getButton() == MouseButton.SECONDARY){
                event.consume();
            }
        });
    }
    
    public LocalTimePicker getTimePicker(){
        return picker;
    }
    
    
    private void wasClicked(){
        picker.setDisable(!picker.isDisabled());
        double opasity = (picker.isDisabled()) ? 0.8 : 1;
        picker.setOpacity(opasity);
        if(picker.isDisabled()){
            picker.setLocalTime(LocalTime.of(12, 0));
            this.dayChoice.setDisable(true);
            this.dayChoice.getSelectionModel().selectFirst();
        }else{
            this.dayChoice.setDisable(false);
        }
    }
}
