package customControls;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import static utils.Utils.isValidParam;

/**
 *
 * @author dimitris
 */
public class DoubleField extends TextField{
    private SimpleDoubleProperty valueProp;
    private boolean dotFound = false;
    private double maxValue;
    
    public DoubleField(Double theMaxValue){
        this(theMaxValue.doubleValue());
    }
    public DoubleField(double theMaxValue){
        super();
        maxValue = theMaxValue;
        if(maxValue == 0){ maxValue= Double.MAX_VALUE; }
        valueProp = new SimpleDoubleProperty(0.00);
        setMaxHeight(25);
        setMaxWidth(100);
        addEventFilter(KeyEvent.KEY_RELEASED, (KeyEvent key) -> {
            if(key.getCode() == KeyCode.ENTER){
                if(isValidParam(getText())){
                    double tempValue;
                    try{
                        tempValue = Double.parseDouble(getText());
                    }catch(Exception e){
                        System.err.println("Exception converting Double to string");
                        e.printStackTrace(System.err);
                        tempValue = 0.00;
                    }
                    if(tempValue > maxValue){
                        tempValue = maxValue;
                    }
                    if(tempValue == 0){
                        setText("0.00");
                    }else{
                        setText(Double.toString(tempValue));
                    }
                }
                key.consume();
            }
        });
        addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
            dotFound = (getText().contains("."));
            if(key.getCharacter().matches("\\d") || key.getCharacter().equals(".")){
                if(key.getCharacter().equals(".")){
                    if(dotFound){
                        key.consume();
                        return;
                    }
                }
                if(isValidParam(getText())){
                    if(dotFound){
                        String [] parts  = getText().split("\\.");
                        if(parts != null){
                            if(parts.length == 2){
                                if(parts[1].length() == 2){
                                    int caretPosition = getCaretPosition();
                                    if(caretPosition == getText().length() || caretPosition == getText().length()-1){
                                        String temp = getText();
                                        String lastDigit = temp.substring(temp.length()-2, temp.length()-1);
                                        temp = temp.substring(0, temp.length()-2);
                                        temp = temp + lastDigit + key.getCharacter();
                                        setText(temp);
                                        positionCaret(caretPosition+1);
                                        key.consume();
                                        return;
                                    }
                                    if(caretPosition == getText().length()-2){
                                        String temp = getText();
                                        String beforeLastDigit = temp.substring(temp.length()-2, temp.length()-1);
                                        temp = temp.substring(0, temp.length()-2);
                                        temp = temp + key.getCharacter() + beforeLastDigit;
                                        setText(temp);
                                        positionCaret(caretPosition+1);
                                        key.consume();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                key.consume();
            }
        });
       focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
           if(!newValue){
                if(isValidParam(getText())){
                    double tempValue;
                    try{
                        tempValue = Double.parseDouble(getText());
                    }catch(Exception e){
                        System.err.println("Exception converting Double to string-1-");
                        e.printStackTrace(System.err);
                        tempValue = 0.00;
                    }
                    
                    if(tempValue > maxValue){
                        tempValue = maxValue;
                    }
                    if(tempValue == 0 ){
                        setText("0.00");
                    }else{
                       setText(Double.toString(tempValue));
                    }
                }
           }
        });
       textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            double newValueDouble = 0.00;
        
           if(isValidParam(oldValue)){
                newValueDouble = Double.parseDouble(oldValue);
           }
           try {
               if(isValidParam(newValue)){
                    newValueDouble = Double.parseDouble(newValue);
                    if(newValueDouble > maxValue){
                        setText(oldValue);
                        return;
                    }
                    
               }else{
                   newValueDouble = 0.00;
               }
           } catch (NumberFormatException numberFormatException) {
               System.err.println("Error converting to double");
               
           }
           valueProp.set(newValueDouble);
        });
       
    }
    public SimpleDoubleProperty valueProperty(){
        return valueProp;
    }

    public double getMaxValue() { return maxValue; }
    public void setMaxValue(double maxValue) { this.maxValue = maxValue;}
}
