package customControls;

import javafx.scene.control.TextArea;

/**
 * TextArea that will be used as labels marking the time in the Time Table
 * @author d.michaelides
 */
public class TimeCell extends TextArea{
    private String originalTime;

    public String getOriginalTime() {
        return originalTime;
    }

    final public void setOriginalTime(String _originalTime) {
        this.originalTime = _originalTime;
    }
    
    public TimeCell(int hour, int minute){
        setMaxHeight(30);
        setMouseTransparent(true);
        setPrefHeight(30);
        if(minute == 30){
            getStyleClass().add("time-cell");
        }else{
            getStyleClass().add("time-cell2");
        }
        String minuteStr = Integer.toString(minute);
        if(minuteStr.length() == 1){
            minuteStr = "0" + minuteStr;
        }
        setText(Integer.toString(hour)+":"+minuteStr);
        setOriginalTime(Integer.toString(hour)+":"+minuteStr);
        setWrapText(true);
    }
}
