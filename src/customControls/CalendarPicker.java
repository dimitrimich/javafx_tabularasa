/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package customControls;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.DatePicker;
import javafx.util.StringConverter;

/**
 *  A <code>DatePicker</code> with Calendar Property
 * @author dimitris
 */
public final class CalendarPicker extends DatePicker {
    public ObjectProperty<Calendar> calendarProperty() {
        return calendarObjectProperty;
    }
    final private ObjectProperty<Calendar> calendarObjectProperty = new SimpleObjectProperty<>(this, "calendar", null);
    public Calendar getCalendar() {
        return calendarObjectProperty.getValue();
    }
    public void setCalendar(Calendar value) {
        calendarObjectProperty.setValue(value);
    }
	
    public CalendarPicker(){
        super();
        setValue(LocalDate.now());
        Calendar initValue = Calendar.getInstance();
        initValue.set(LocalDate.now().getYear(), (LocalDate.now().getMonthValue()-1), LocalDate.now().getDayOfMonth());
        
        setCalendar(initValue);
        valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            Calendar displayedCalendar = (Calendar)getCalendar().clone();
            displayedCalendar.set(newValue.getYear(), (newValue.getMonthValue()-1), newValue.getDayOfMonth());
            setCalendar(displayedCalendar);
            setDateInEditor(newValue);
        });
        initPattern();
    }

    private void setDateInEditor(LocalDate aDate){
        String pattern = "dd / MMMM / yyyy";
        DateTimeFormatter dateFormatter =  DateTimeFormatter.ofPattern(pattern);
        getEditor().setText(dateFormatter.format(aDate));
    }
    
    private void initPattern() {
        String pattern = "dd / MMMM / yyyy";
        StringConverter converter = new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = 
            DateTimeFormatter.ofPattern(pattern);
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        };             
        this.setConverter(converter);
    }
}
