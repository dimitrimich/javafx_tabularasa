/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package customControls;

import DB.objects.Billing;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
/**
 *
 * @author dimitris
 */
public class BillingTables extends HBox{
    DateTimeFormatter dateFormatter =  DateTimeFormatter.ofPattern("dd / MMMM / yyyy");
    private Label title;
    private TableView<Billing> tables [] = new TableView[3];
    
    private VBox tableVBoxes[] = new VBox[3];
    
    Label fullAmounts [] = new Label[3];
    Label paidAmounts [] = new Label[3];
    Label dueAmounts [] = new Label[3];
    CheckBox payAlls [] = new CheckBox[3];
    
    public BillingTables(ScrollPane parent){
        super();
        minWidthProperty().bind(parent.widthProperty().subtract(10));
        maxWidthProperty().bind(parent.widthProperty().subtract(10));
        setMinHeight(130);
        setPrefHeight(220);
        setPrefWidth(200);
        setSpacing(3);
        title = new Label();
        title.setPrefHeight(15);
        title.setPrefWidth(97);
        
        setUpTables();
        setUpVBoxes();
        
        getChildren().add(title);
        getChildren().add(tableVBoxes[0]);
        getChildren().add(tableVBoxes[1]);
        getChildren().add(tableVBoxes[2]);
        HBox.setHgrow(title, Priority.NEVER);
        HBox.setHgrow(tableVBoxes[0], Priority.ALWAYS);
        HBox.setHgrow(tableVBoxes[1], Priority.ALWAYS);
        HBox.setHgrow(tableVBoxes[2], Priority.ALWAYS);
        
    }
    // Getters and Setters
    public Label getTitle() {  return title; }
    public void setTitle(Label title) { this.title = title; }
    public TableView<Billing> getTable1() { return tables[0]; }
    public void setTable1(TableView<Billing> table1) { this.tables[0] = table1; }
    public TableView<Billing> getTable2() { return tables[1]; }
    public void setTable2(TableView<Billing> table2) { this.tables[1] = table2; }
    public TableView<Billing> getTable3() { return tables[2]; }
    public void setTable3(TableView<Billing> table3) { this.tables[2] = table3; }
    
    private void setUpVBoxes(){
        initVBox(0);
        initVBox(1);
        initVBox(2);
    }
    
    private void setUpTables(){
        initTable(0);
        initTable(1);
        initTable(2);
    }

    private void initTable(int index) {
        System.out.println("initTable");
        tables[index] = new TableView<>();
        
        /* * ******** Date of Lesson ****************/ 
        TableColumn dateCol = new TableColumn("Date");
        dateCol.setCellValueFactory(new PropertyValueFactory("dateProp"));
//        dateCol.setCellValueFactory(
//            new Callback<CellDataFeatures<Billing, String>, SimpleStringProperty>() {  
//            @Override  
//            public SimpleStringProperty call(CellDataFeatures<Billing, String> data) {
//                return new SimpleStringProperty(dateFormatter.format(data.getValue().getDateProp().getValue()));
//            }
//        });
        
        
        // old factory:
//        Callback<TableColumn<Billing, Billing>, TableCell<Billing, Billing>> checkBoxFactory
//                = (TableColumn<Billing, Billing> p) -> new CheckBoxCell();
        
//        Callback<TableColumn<Billing, Billing>, TableCell<Billing, Billing>> checkBoxFactory
//                = (TableColumn<Billing, Billing> p) -> new CheckBoxTableCell();
        
        
        TableColumn checkBoxCol = new TableColumn("Paid");
        checkBoxCol.setEditable(true);
        
        checkBoxCol.setCellValueFactory(new PropertyValueFactory("statusProp"));
//      
        
        Callback<TableColumn, TableCell> checkBoxFactory =  (TableColumn p) -> new CheckBoxTableCell();
        checkBoxCol.setCellFactory(checkBoxFactory);

        /* * ******** FULL amount ****************/ 
        TableColumn amountFullCol = new TableColumn("Full Amount");
        amountFullCol.setCellValueFactory(new PropertyValueFactory("amountFullProp"));

        /* * ******** Paid Amount ****************/ 
        TableColumn amountPaidCol = new TableColumn("Paid Amount");
        amountPaidCol.setCellValueFactory(new PropertyValueFactory("amountPaidProp"));
//        Callback<TableColumn, TableCell> doubleCellFactory =  (TableColumn p) -> new PaidAmountCell();
//        Callback<TableColumn, TableCell> doubleCellFactory =  (TableColumn p) -> new DoubleEditingCell();
//        amountPaidCol.setCellFactory(doubleCellFactory);

        /* * ******** Remaining Amount ****************/ 
        TableColumn amountRemainingCol = new TableColumn("Remaining Amount");
        amountRemainingCol.setCellValueFactory(new PropertyValueFactory("amountDueProp"));

        
        amountPaidCol.setEditable(true);
        amountPaidCol.setSortable(false);
        dateCol.setSortable(false);
        checkBoxCol.setSortable(false);
        tables[index].setEditable(true);
        amountFullCol.setEditable(false);
        amountFullCol.setSortable(false);
        dateCol.setEditable(false);
        amountRemainingCol.setEditable(false);
        amountRemainingCol.setSortable(false);
        checkBoxCol.setMinWidth(40);
        amountFullCol.setMinWidth(55);
        amountPaidCol.setMinWidth(55);
        amountRemainingCol.setMinWidth(55);
        dateCol.setMinWidth(120);
        tables[index].getColumns().addAll(dateCol, checkBoxCol, amountFullCol, amountPaidCol, amountRemainingCol);
    }

    private void initVBox(int index) {
        tableVBoxes[index] = new VBox(5);
        tableVBoxes[index].setPrefWidth(200);
        
        GridPane grid = new GridPane();
            grid.getStyleClass().add("with-border");
        
            RowConstraints row1 = 
                    new RowConstraints(10, 25, USE_PREF_SIZE, Priority.SOMETIMES, VPos.CENTER, false);
            grid.getRowConstraints().add(row1);
          
            Label fullAmountTitle = new Label("Full Amount");
            fullAmountTitle.getStyleClass().add("custom_label");
            Label paidAmountTitle = new Label("Paid Amount");
            paidAmountTitle.getStyleClass().add("custom_label");
            Label dueAmountTitle = new Label("Due Amount");
            dueAmountTitle.getStyleClass().add("custom_label");
            
            fullAmounts[index] = new Label("00.00");
            fullAmounts[index].getStyleClass().add("custom_label");
            fullAmounts[index].setPrefWidth(100);
            final Tooltip fullAmountsTitle = new Tooltip();
            fullAmountsTitle.setText("Total Amount\n  for the Month\n");
            fullAmounts[index].setTooltip(fullAmountsTitle);
            paidAmounts[index] = new Label("00.00");
            paidAmounts[index].getStyleClass().add("custom_label");
            paidAmounts[index].setPrefWidth(100);
            final Tooltip paidAmountsTitle = new Tooltip();
            paidAmountsTitle.setText("Paid Amount\n  for the Month\n");
            paidAmounts[index].setTooltip(paidAmountsTitle);
            dueAmounts[index] = new Label("00.00");
            dueAmounts[index].getStyleClass().add("custom_label");
            dueAmounts[index].setPrefWidth(100);
            final Tooltip dueAmountsTitle = new Tooltip();
            dueAmountsTitle.setText("Due Amount\n for the Month\n");
            dueAmounts[index].setTooltip(dueAmountsTitle);
            payAlls[index] = new CheckBox();
            final Tooltip checkBoxTitle = new Tooltip();
            checkBoxTitle.setText("Set whole month as paid\n");
            payAlls[index].setTooltip(checkBoxTitle);
            
            grid.add(payAlls[index], 0, 0);
            grid.add(fullAmounts[index], 1, 0);
            grid.add(paidAmounts[index], 2, 0);
            grid.add(dueAmounts[index], 3, 0);
            
            grid.setPrefHeight(25);
            grid.setMinHeight(25);
        tableVBoxes[index].getChildren().add(tables[index]);
        tableVBoxes[index].getChildren().add(grid);
    }
    
    //CheckBoxTableCell for creating a CheckBox in a table cell
    public static class CheckBoxTableCell<S, T> extends TableCell<S, T> {
        private final CheckBox checkBox;
        private ObservableValue<T> ov;
        public CheckBoxTableCell() {
            this.checkBox = new CheckBox();
            this.checkBox.setAlignment(Pos.CENTER);
            setAlignment(Pos.CENTER);
            setGraphic(checkBox);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(getTableRow() != null && getItem() != null){
                        if(newValue){
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-good");
                        }else{
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-bad");
                        }
                        getTableView().getSelectionModel().select(getTableRow().getIndex());
                    }
                }
            });
        } 

        @Override public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setGraphic(checkBox);
                ov = getTableColumn().getCellObservableValue(getIndex());
                if (ov instanceof BooleanProperty) {
                    System.out.println("is boolean Property");
                    checkBox.selectedProperty().bindBidirectional((BooleanProperty) ov);
                }
                else{
//                    if(ov instanceof ReadOnlyObjectWrapper){
//                    }
                    System.out.println("is NOT boolean Property - "+ ov.getClass().getSimpleName());
                
                }
            }
        }
    }

 
    private class CheckBoxCell extends TableCell<Billing, Billing> {
 
        private CheckBox check;
        {
            check = new CheckBox();
            check.setAllowIndeterminate(false);
            check.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(getTableRow() != null && getItem() != null){
//                        getItem().setStatusProp(newValue);
                        if(newValue){
//                            getItem().setAmountDueProp(BigDecimal.ZERO);
//                            getItem().setAmountPaidProp(getItem().getAmountFull());
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-good");
                        }else{
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-bad");
//                            getItem().setAmountPaidProp(BigDecimal.ZERO);
//                            getItem().setAmountDueProp(getItem().getAmountFull());
                        }
                        getTableView().getSelectionModel().select(getTableRow().getIndex());
                    }
                }
            });
        }
 
        public CheckBoxCell() {
        }
 
        @Override
        public void updateItem(Billing item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if(item != null){
                    if(getTableRow() == null){
                        setGraphic(null);
                        setText(null);
                    }else{
                        getTableView().getSelectionModel().select(getTableRow().getIndex());
                        setGraphic(check);
                        setText(null);
//                        check.selectedProperty().bindBidirectional(getItem().getStatusProp());
                        if(check.isSelected()){
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-good");
                        }else{
                            getTableRow().getStyleClass().clear();
                            getTableRow().getStyleClass().add("custom-row-bad");
                        }
                    }
                }else{
                    setText(null);setGraphic(null);
                }
            }
        }
        
    }
//    private class PaidAmountCell extends TableCell<Billing, Double> {
// 
//        private NumberTextField textField;
// 
//        public PaidAmountCell() {
//        }
// 
//        @Override
//        public void startEdit() {
//            if (!isEmpty()) {
//                super.startEdit();
//                createTextField();
//                setText(null);
//                setGraphic(textField);
//                textField.selectAll();
//                textField.requestFocus();
//            }
//        }
//
//        @Override
//        public void commitEdit(BigDecimal newValue) {
//            
//            throw new UnsupportedOperationException(" commitEdit not supported");
//            
////            super.commitEdit(newValue); //To change body of generated methods, choose Tools | Templates.
////            setText(newValue.toString());
////            setGraphic(null);
////            ((Billing) getTableView().getItems().get(getTableRow().getIndex())).setAmountPaidProp(newValue);
//////            ((Billing) getTableView().getItems().get(getTableRow().getIndex())).setAmountPaidProp(newValue.getValue());
////            ((Billing) getTableView().getItems().get(getTableRow().getIndex())).refreshAmountDueProp();
//        }
//        
// 
//        @Override
//        public void updateItem(BigDecimal item, boolean empty) {
//            
//            throw new UnsupportedOperationException(" updateItem not supported");
//            super.updateItem(item, empty);
// 
//            if (empty) {
//                setText(null);
//                setGraphic(null);
//            } else {
//                if (isEditing()) {
//                    if (textField != null) {
//                        textField.setText(getString());
//                    }
//                    setText(null);
//                    setGraphic(textField);
//                } else {
//                    setText(getString());
//                    setGraphic(null);
//                }
//            }
        }
 
//        private void createTextField() {
//            
//            throw new UnsupportedOperationException(" createTextField not supported");
//            textField = new NumberTextField(((Billing) getTableView().getItems().get(getTableRow().getIndex())).getAmountFullProp().get());
//            textField.setText(getString());
//            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()* 2);
//            textField.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
////                commitEdit(new SimpleDoubleProperty(newValue.doubleValue()));
//                commitEdit(newValue.doubleValue());
//           
//            });
//            textField.focusedProperty().addListener(
//                (ObservableValue<? extends Boolean> arg0, 
//                Boolean arg1, Boolean arg2) -> {
//                    if (!arg2) {
//                        commitEdit(textField.valueProperty().getValue());
////                        commitEdit(new SimpleDoubleProperty(textField.valueProperty().getValue()));
//                    }
//            });
//        }
// 
//        private String getString() {
//            return getItem() == null ? "" : getItem().toString();
//        }
//    }
//    
//    
//     public static class DoubleEditingCell extends TableCell<Billing, Double> {
//        private TextField textField;
//        public DoubleEditingCell() {
//        }
//
//        @Override public void startEdit() {
//            super.startEdit();
//
//            if (textField == null) {
//                createTextField();
//            }
//
//            setText(null);
//            setGraphic(textField);
//            textField.selectAll();
//        }
//        @Override public void cancelEdit() {
//            super.cancelEdit();
//            setText(getItem().toString());
//            setGraphic(null);
//        }
//
//        @Override public void updateItem(Double item, boolean empty) {
//            super.updateItem(item, empty);
//            if (empty) {
//                setText(null);
//                setGraphic(null);
//            } else {
//                if (isEditing()) {
//                    if (textField != null) {
//                        textField.setText(getString());
//                    }
//                    setText(null);
//                    setGraphic(textField);
//                } else {
//                    setText(getString());
//                    setGraphic(null);
//                }
//            }
//        }
//
//        private void createTextField() {
//            textField = new TextField(getString());
//            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
//            textField.setOnKeyReleased((KeyEvent t) -> {
//                if (t.getCode() == KeyCode.ENTER) {
//                    commitEdit(Double.parseDouble(textField.getText()));
//                } else if (t.getCode() == KeyCode.ESCAPE) {
//                    cancelEdit();
//                }
//            });
//        }
//
//        private String getString() {
//            return getItem() == null ? "" : getItem().toString();
//        }
//    } 
    
//}