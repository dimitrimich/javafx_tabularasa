/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package customControls;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author d.michaelides
 */
public class TimePicker{
    private HBox hbox;
    private VBox vbox;
    private Button up;
    private Button down;
    private Polygon upTriangle;
    private Polygon downTriangle;
    private TextField text;
    
    public HBox getHBox(){
        initHBox();
        initTextField();
        initVBox();
        hbox.getChildren().addAll(text, vbox);
        addListeners();
        return hbox;
    }
    
    private void initHBox(){
        hbox = new HBox();
        hbox.setMinHeight(36.0);
        hbox.setPrefHeight(36.0);
        hbox.setPrefWidth(209);
        hbox.setSpacing(2);
    }
    
    private void initTextField(){
        System.out.println(" 0 ");
        text = new TextField();
        text.setText("00");
        text.setMaxHeight(30);
        text.setMaxWidth(30);
        text.setMinHeight(25);
        text.setMinWidth(25);
        text.setPrefHeight(30);
        text.setPrefWidth(30);
    }
    private void initVBox(){
        vbox = new VBox();
        vbox.setSpacing(1);
        vbox.setMinHeight(27);
        vbox.setMinWidth(30);
        vbox.setPrefHeight(27);
        vbox.setPrefWidth(30);
        vbox.setPadding(new Insets(3, 0, 0, 0));
        initUpButton();
        initDownButton();
        vbox.getChildren().addAll(up, down);
    }
    
    private void initUpButton(){
        up = new Button();
        up.setContentDisplay(ContentDisplay.CENTER);
        up.setMaxHeight(12);
        up.setMaxWidth(12);
        up.setMinHeight(12);
        up.setMinWidth(12);
        up.setPrefHeight(12);
        up.setPrefWidth(12);
        up.setStyle("-fx-background-color:transparent;");
        up.setText("000000000");
        initTriangle(true);
        up.setGraphic(upTriangle);
    }
    private void initDownButton(){
        down = new Button();
        down.setContentDisplay(ContentDisplay.CENTER);
        down.setMaxHeight(12);
        down.setMaxWidth(12);
        down.setMinHeight(12);
        down.setMinWidth(12);
        down.setPrefHeight(12);
        down.setPrefWidth(12);
        down.setStyle("-fx-background-color:transparent;");
        down.setText("");
        initTriangle(false);
        down.setGraphic(downTriangle);
    }
    private void initTriangle(boolean isUp){
        if(isUp){
            upTriangle = new Polygon();
            upTriangle.getPoints().addAll(new Double[]{
                -50.0, 40.0,
                50.0, 40.0,
                0.0, -60.0 });
                 upTriangle.setScaleX(0.1);
                 upTriangle.setScaleY(0.1);
                 upTriangle.setStrokeType(StrokeType.INSIDE);
                 upTriangle.setStroke(Paint.valueOf("BLACK"));
            upTriangle.getStyleClass().add("triangle");
        }
        else{
            downTriangle = new Polygon();
            downTriangle.getPoints().addAll(new Double[]{
                -50.0, 40.0,
                50.0, 40.0,
                0.0, -60.0 });
                 downTriangle.setScaleX(0.1);
                 downTriangle.setScaleY(0.1);
                 downTriangle.setStrokeType(StrokeType.INSIDE);
                 downTriangle.setStroke(Paint.valueOf("BLACK"));
                     
            downTriangle.setRotate(180);
            downTriangle.getStyleClass().add("triangle");
        }
    }
    
    
    
    
    private void upPressed() {
        if(!text.getText().isEmpty()){
            text.setText(  Integer.toString(Integer.parseInt(text.getText()) + 1) );
        }
    }
    
    private void downPressed() {
        if(!text.getText().isEmpty()){
            text.setText(  Integer.toString(Integer.parseInt(text.getText()) - 1) );
        }
    }
    
    public void addListeners() {
        upTriangle.setOnMouseReleased((MouseEvent t) -> {
            upPressed();
        });
        
        downTriangle.setOnMouseReleased((MouseEvent t) -> {
            downPressed();
        });
        
        
        text.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
            if(!key.getCharacter().matches("[0-9]") && !key.getCharacter().equals("\b")){
                if(!key.isAltDown() && !key.isControlDown() ){
                    key.consume();
                }      
            }
        });
        
        text.textProperty().addListener((final ObservableValue<? extends String> ov, final String oldValue, final String newValue) -> {
            if (text.getText().length() > 2) {
                String s = text.getText().substring(0, 2);
                text.setText(s);
                text.positionCaret(2);
            }
            if(text.getText() != null && !text.getText().isEmpty()){
                int text_int = Integer.parseInt(text.getText());
                if(text_int > 24){
                    text.setText("24");
                }else if(text_int < 0){
                    text.setText("0");
                }
            }
        });
    }    
}
