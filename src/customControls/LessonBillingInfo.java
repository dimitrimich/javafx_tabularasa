/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customControls;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 *
 * @author dimitris
 */
public class LessonBillingInfo extends GridPane{
    private Label dateLbl;
    private Label canceledLbl;
    private Label attendanceLbl;
    private Label durationLbl;
    private Label classCodeLbl;
    private Button payBtn;
    private Label paymentDetailsLbl;
    private GridPane billingInfoGrid;
    private Label amountFullLbl;
    private Label amountFullValue;
    
    public LessonBillingInfo(){
    
        ColumnConstraints columnContstraints1 = new ColumnConstraints();
            columnContstraints1.setHgrow(Priority.SOMETIMES);
            columnContstraints1.setMinWidth(10);
            columnContstraints1.setPrefWidth(100);
        ColumnConstraints columnContstraints2 = new ColumnConstraints();
            columnContstraints2.setHgrow(Priority.SOMETIMES);
            columnContstraints2.setMinWidth(10);
            columnContstraints2.setPrefWidth(100);
        ColumnConstraints columnContstraints3 = new ColumnConstraints();
            columnContstraints3.setHgrow(Priority.SOMETIMES);
            columnContstraints3.setMinWidth(10);
            columnContstraints3.setPrefWidth(100);
        ColumnConstraints columnContstraints4 = new ColumnConstraints();
            columnContstraints4.setHgrow(Priority.SOMETIMES);
            columnContstraints4.setMinWidth(10);
            columnContstraints4.setPrefWidth(100);
        this.getColumnConstraints().addAll(columnContstraints1, columnContstraints2, columnContstraints3, columnContstraints4);
        
        RowConstraints rowConstr1 = new RowConstraints();
            rowConstr1.setMaxHeight(25);
            rowConstr1.setMinHeight(10);
            rowConstr1.setVgrow(Priority.SOMETIMES);
        
        RowConstraints rowConstr2 = new RowConstraints();
            rowConstr2.setMaxHeight(25);
            rowConstr2.setMinHeight(10);
            rowConstr2.setVgrow(Priority.SOMETIMES);
            
        RowConstraints rowConstr3 = new RowConstraints();
            rowConstr3.setMaxHeight(25);
            rowConstr3.setMinHeight(10);
            rowConstr3.setVgrow(Priority.SOMETIMES);
        this.getRowConstraints().addAll(rowConstr1, rowConstr2, rowConstr3);
        
        dateLbl = new Label();
            GridPane.setColumnSpan(dateLbl, 1);
            GridPane.setColumnIndex(dateLbl, 0);
            GridPane.setRowIndex(dateLbl, 0);
            GridPane.setMargin(dateLbl, new Insets(0, 1, 0, 0));
        
        canceledLbl = new Label();
            GridPane.setColumnSpan(canceledLbl, 1);
            GridPane.setColumnIndex(canceledLbl, 0);
            GridPane.setRowIndex(canceledLbl, 1);
            
        attendanceLbl = new Label();
            GridPane.setRowIndex(attendanceLbl, 2);
            GridPane.setColumnSpan(attendanceLbl, 1);
            GridPane.setColumnIndex(attendanceLbl, 0);
            
        classCodeLbl = new Label();
            GridPane.setRowIndex(classCodeLbl, 0);
            GridPane.setColumnIndex(classCodeLbl, 1);
            GridPane.setColumnSpan(classCodeLbl, 1);
        classCodeLbl.setText("CODE");
        durationLbl = new Label();
            GridPane.setRowIndex(durationLbl, 1);
            GridPane.setColumnIndex(durationLbl, 1);
            GridPane.setColumnSpan(durationLbl, 1);
        durationLbl.setText("100min");
            
        
        initInternalGrid();
        
        this.getChildren().setAll(dateLbl, canceledLbl, attendanceLbl,
                                classCodeLbl, durationLbl, billingInfoGrid);
        this.setMinHeight(75);
    }

    public Label getDateLbl() { return dateLbl;}
    public void setDateLbl(Label dateLbl) { this.dateLbl = dateLbl; }
    public Label getCanceledLbl() { return canceledLbl;  }
    public void setCanceledLbl(Label canceledLbl_) {this.canceledLbl = canceledLbl_; }
    public Label getAttendanceLbl() { return attendanceLbl;}
    public void setAttendanceLbl(Label attendanceLbl_) { this.attendanceLbl = attendanceLbl_; }
    public Label getDurationLbl() { return durationLbl; }
    public void setDurationLbl(Label durationLbl) { this.durationLbl = durationLbl; }
    public Label getClassCodeLbl() { return classCodeLbl; }
    public void setClassCodeLbl(Label classCodeLbl) { this.classCodeLbl = classCodeLbl; }
    public Button getPayBtn() { return payBtn; }
    public void setPayBtn(Button payBtn) { this.payBtn = payBtn; }
    public Label getAmountFullLbl() {return amountFullLbl; }
    public void setAmountFullLbl(Label amountDueLbl) { this.amountFullLbl = amountDueLbl; }
    public Label getAmountFullValue() { return amountFullValue;}
    public Label getPaymentDetailsLbl() { return paymentDetailsLbl; }
    public void setPaymentDetailsLbl(Label paymentDetailsLbl) { this.paymentDetailsLbl = paymentDetailsLbl; }
    
    private void initInternalGrid(){
        
        billingInfoGrid = new GridPane();
            billingInfoGrid.getStyleClass().add("with-border");
            GridPane.setColumnIndex(billingInfoGrid, 2);
            GridPane.setColumnSpan(billingInfoGrid, 2);
            GridPane.setRowSpan(billingInfoGrid, 3);
            ColumnConstraints internalGridColConstraints1 = new ColumnConstraints();
                internalGridColConstraints1.setHgrow(Priority.SOMETIMES);
                internalGridColConstraints1.setMinWidth(10);
                internalGridColConstraints1.setPrefWidth(100);
            ColumnConstraints internalGridColConstraints2 = new ColumnConstraints();
                internalGridColConstraints2.setHgrow(Priority.SOMETIMES);
                internalGridColConstraints2.setMinWidth(10);
                internalGridColConstraints2.setPrefWidth(100);
            billingInfoGrid.getColumnConstraints().addAll(internalGridColConstraints1, internalGridColConstraints2);
            
            RowConstraints internalGridRowConstraints1 = new RowConstraints();
                internalGridRowConstraints1.setMinHeight(10);
                internalGridRowConstraints1.setPrefHeight(30);
                internalGridRowConstraints1.setVgrow(Priority.SOMETIMES);

            RowConstraints internalGridRowConstraints2 = new RowConstraints();
                internalGridRowConstraints2.setMinHeight(10);
                internalGridRowConstraints2.setPrefHeight(30);
                internalGridRowConstraints2.setVgrow(Priority.SOMETIMES);
            RowConstraints internalGridRowConstraints3 = new RowConstraints();
                internalGridRowConstraints3.setMinHeight(10);
                internalGridRowConstraints3.setPrefHeight(30);
                internalGridRowConstraints3.setVgrow(Priority.SOMETIMES);            
            billingInfoGrid.getRowConstraints().addAll(internalGridRowConstraints1, internalGridRowConstraints2, internalGridRowConstraints3);
                
            amountFullLbl = new Label("Full Amount:");
                GridPane.setHalignment(amountFullLbl, HPos.RIGHT);
                GridPane.setMargin(amountFullLbl, new Insets(0, 1, 0, 0));
                
            amountFullValue = new Label();
                amountFullValue.setPrefHeight(15);
                amountFullValue.setPrefWidth(148);
                GridPane.setColumnIndex(amountFullValue, 1);           
                
            payBtn = new Button();
                GridPane.setRowIndex(payBtn, 1);
                GridPane.setColumnIndex(payBtn, 0);
                GridPane.setColumnSpan(payBtn, 2);
            payBtn.setText("Pay");
            GridPane.setHalignment(payBtn, HPos.CENTER);
            
                
            paymentDetailsLbl = new Label();
                GridPane.setRowIndex(paymentDetailsLbl, 2);
                GridPane.setColumnIndex(paymentDetailsLbl, 0);
                GridPane.setColumnSpan(paymentDetailsLbl, 2);
            paymentDetailsLbl.setText("PAID | DISCOUNT | DATE | METHOD");
            GridPane.setHalignment(paymentDetailsLbl, HPos.CENTER);
            
                
            billingInfoGrid.getChildren().addAll(amountFullLbl, payBtn, amountFullValue, paymentDetailsLbl);
            billingInfoGrid.setPadding(new Insets(1, 1, 1, 1));
            GridPane.setMargin(billingInfoGrid, new Insets(1, 1, 1, 1));
    }
    public GridPane getBillingInfoGrid() { return billingInfoGrid; }
    public void setBillingInfoGrid(GridPane billingInfoGrid) { this.billingInfoGrid = billingInfoGrid;  }
}
