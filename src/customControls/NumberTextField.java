package customControls;

import customObjects.MyDecimalFormater;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParsePosition;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * Textfield implementation that accepts formatted number and stores them in a
 * BigDecimal property The user input is formatted when the focus is lost or the
 * user hits RETURN.
 *
 * @author Thomas Bolz
 */
public class NumberTextField extends TextField {

    private BigDecimal maxValue = BigDecimal.valueOf(Double.MAX_VALUE);
    private final MyDecimalFormater formater;
    private ObjectProperty<BigDecimal> number = new SimpleObjectProperty<>();

    public final BigDecimal getNumber() {
        return number.get();
    }

    public final void setNumber(BigDecimal value) {
        value = value.setScale(2, RoundingMode.CEILING);
        number.set(value);
    }

    public ObjectProperty<BigDecimal> numberProperty() {
        return number;
    }

    public NumberTextField() {
        this(BigDecimal.ZERO);
    }

    public NumberTextField(BigDecimal value) {
        super();
        this.formater = new MyDecimalFormater();
        initHandlers();
        setNumber(value);
    }

    private void initHandlers() {

        addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
            if(!key.getCharacter().matches("\\d") && !key.getCharacter().equals(".")){
                key.consume();
            }
        });
        // try to parse when focus is lost or RETURN is hit
        setOnAction((ActionEvent arg0) -> {
            parseAndFormatInput();
        });

        focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                
                parseAndFormatInput();
            }
        });

        // Set text in field if BigDecimal property is changed from outside.
        numberProperty().addListener(
                (ObservableValue<? extends BigDecimal> obserable, BigDecimal oldValue, BigDecimal newValue) -> {
            setText(formater.numberToString(newValue));
        });
    }

    /**
     * Tries to parse the user input to a number according to the provided
     * NumberFormat
     */
    private void parseAndFormatInput() {
        try {
            String input = getText();
            if (input == null || input.length() == 0) {
                input = "0.00";
                setText(input);
            }
            Number parsedNumber = formater.parse(input, new ParsePosition(0));
            BigDecimal newValue = new BigDecimal(parsedNumber.toString());
            if(newValue.compareTo(maxValue) == 1){
                newValue = maxValue;
            }
            setNumber(newValue);
            selectAll();
        } catch (Exception ex) {
            // If parsing fails keep old number
            System.err.println("Parsing of NUmber failed");
            setText(formater.format(number.get()));
        }
    }

    public BigDecimal getMaxValue() { return maxValue; }
    public void setMaxValue(BigDecimal maxValue) { this.maxValue = maxValue;}
}