/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package customObjects;

/**
 *
 * @author d.michaelides
 */
public class Person {
    public Person(int index, String fName, String lName, int age, double testing, boolean testB){
        this.index = index;
        fn = fName;
        this.lName = lName;
        this.age = age;
        this.test = testing;
        this.testB = testB;
    }
    private int index;
    private String fn;
    private String lName;
    private int age;
    private double test;
    private boolean testB;

    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
    public String getFn() {
        return fn;
    }
    public void setFn(String fn) {
        this.fn = fn;
    }
    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getCurrency() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getTest() {
        return test;
    }

    public void setTest(double test) {
        this.test = test;
    }

    public boolean getInvited() {
        return testB;
    }

    public void setTestB(boolean testB) {
        this.testB = testB;
    }
    
    public Person getPerson(){
        return this;
    }
            
    
}
