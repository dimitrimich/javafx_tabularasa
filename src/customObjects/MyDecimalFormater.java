/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customObjects;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 *
 * @author dimitris
 */
public class MyDecimalFormater extends DecimalFormat{

    public MyDecimalFormater() {
        super("00.00");
        DecimalFormatSymbols dfs = this.getDecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        this.setDecimalFormatSymbols(dfs);
        this.setMinimumFractionDigits(2);
        this.setParseBigDecimal(true);
    }
    
    public String numberToString(BigDecimal number){
        number = number.setScale(2, RoundingMode.CEILING);
        return format(number);
    }
}
