package customObjects;

import java.util.ArrayList;

/**
 *
 * @author d.michaelides
 */
public class ClassForTests {
    
    public ClassForTests(String n, String d, String t,
            String d1, String d2, String d3, String d4,
            String h1, String h2, String h3, String h4,
            String m1, String m2, String m3, String m4,
            String dun1, String dun2, String dun3, String dun4){
        
        name =  n;  description =  d; teacher =  t;
        day1 =  d1; day2 =  d2; day3 =  d3; day4 =  d4;
        hour1 =  h1; hour2 =  h2; hour3 =  h3; hour4 = h4;
        minute1 =  m1; minute2 =  m2; minute3 =  m3; minute4 = m4;
        duration1 =  dun1; duration2 =  dun2; duration3 =  dun3; duration4 =  dun4;
        days  = new ArrayList<>(4);
        hours  = new ArrayList<>(4);
        minutes  = new ArrayList<>(4);
        durations  = new ArrayList<>(4);
        days.add(day1);  days.add(day2); days.add(day3); days.add(day4);
        hours.add(hour1);  hours.add(hour2); hours.add(hour3); hours.add(hour4);
        minutes.add(minute1);  minutes.add(minute2); minutes.add(minute3); minutes.add(minute4);
        durations.add(duration1);  durations.add(duration2); durations.add(duration3); durations.add(duration4);
    }
    
    public ArrayList<String> getDays(){
        return days;
    }
    public ArrayList<String> getHours(){
        return hours;
    }
    public ArrayList<String> getMinutes(){
        return minutes;
    }
    public ArrayList<String> getDurations(){
        return durations;
    }

    public String getDay1() {
        return day1;
    }

    public void setDay1(String day1) {
        this.day1 = day1;
    }

    public String getDay2() {
        return day2;
    }

    public void setDay2(String day2) {
        this.day2 = day2;
    }

    public String getDay3() {
        return day3;
    }

    public void setDay3(String day3) {
        this.day3 = day3;
    }

    public String getDay4() {
        return day4;
    }

    public void setDay4(String day4) {
        this.day4 = day4;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration1() {
        return duration1;
    }

    public void setDuration1(String duration1) {
        this.duration1 = duration1;
    }

    public String getDuration2() {
        return duration2;
    }

    public void setDuration2(String duration2) {
        this.duration2 = duration2;
    }

    public String getDuration3() {
        return duration3;
    }

    public void setDuration3(String duration3) {
        this.duration3 = duration3;
    }

    public String getDuration4() {
        return duration4;
    }

    public void setDuration4(String duration4) {
        this.duration4 = duration4;
    }

    public String getHour1() {
        return hour1;
    }

    public void setHour1(String hour1) {
        this.hour1 = hour1;
    }

    public String getHour2() {
        return hour2;
    }

    public void setHour2(String hour2) {
        this.hour2 = hour2;
    }

    public String getHour3() {
        return hour3;
    }

    public void setHour3(String hour3) {
        this.hour3 = hour3;
    }

    public String getHour4() {
        return hour4;
    }

    public void setHour4(String hour4) {
        this.hour4 = hour4;
    }

    public String getMinute1() {
        return minute1;
    }

    public void setMinute1(String minute1) {
        this.minute1 = minute1;
    }

    public String getMinute2() {
        return minute2;
    }

    public void setMinute2(String minute2) {
        this.minute2 = minute2;
    }

    public String getMinute3() {
        return minute3;
    }

    public void setMinute3(String minute3) {
        this.minute3 = minute3;
    }

    public String getMinute4() {
        return minute4;
    }

    public void setMinute4(String minute4) {
        this.minute4 = minute4;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
    
    private String name;
    private String description;
    private String teacher;
    private String day1;
    private String day2;
    private String day3;
    private String day4;
    private String hour1;
    private String hour2;
    private String hour3;
    private String hour4;
    private String minute1;
    private String minute2;
    private String minute3;
    private String minute4;
    private String duration1;
    private String duration2;
    private String duration3;
    private String duration4;
    private ArrayList<String> days;
    private ArrayList<String> minutes;
    private ArrayList<String> hours;
    private ArrayList<String> durations;
    
    
    
    
    public ClassForTests getClassForTests(){
        return this;
    }
    
    
}
