/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package customObjects;

import DB.objects.PaymentDetails;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javafx.collections.FXCollections;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TreeItem;
import tables.PaymentsTreeTableUtils.PaymentFX;

/**
 *
 * @author dimitris
 */
public class FullTreeMap {
	private HashMap<String, HashMap<Month, HashMap<String, PaymentDetails>>> fullTreeMap = new HashMap<>();
	
	/**
	 * add payment in the tree
	 * @param payment new payment to be added in the Tree
	 */
	public void addPayment(PaymentDetails payment){
		String receipt = payment.getReceiptNo();
		Month month = payment.getBillingMonth();
		String className = payment.getClassName();
		
		if(fullTreeMap.containsKey(receipt)){
			HashMap<Month, HashMap<String, PaymentDetails>> allMonthsTree = fullTreeMap.get(receipt);
			if(allMonthsTree.containsKey(month)){
				HashMap<String, PaymentDetails> allClassesTree = allMonthsTree.get(month);
				if(allClassesTree.containsKey(className)){
					// increament payment
					allClassesTree.get(className).addAmounts(payment);
				}else{
					// new Payment
					PaymentDetails copyOfPayment = payment.copy();
					allClassesTree.put(className, copyOfPayment);
				}
			}else{
				// new class --> new payment
				HashMap<String, PaymentDetails> allClassesTree = new HashMap<>();
				PaymentDetails copyOfPayment = payment.copy();
				allClassesTree.put(className, copyOfPayment);
				allMonthsTree.put(month, allClassesTree);
			}
		}else{
			// new month --> new class --> new payment
			HashMap<String, PaymentDetails> allClassesTree = new HashMap<>();
			PaymentDetails copyOfPayment = payment.copy();
			allClassesTree.put(className, copyOfPayment);
			HashMap<Month, HashMap<String, PaymentDetails>> allMonthsTree = new HashMap<>();
			allMonthsTree.put(month, allClassesTree);
			fullTreeMap.put(receipt, allMonthsTree);
		}
	}
	
	public List<TreeItem<PaymentFX>> getMonthsForPayment(String receipt){
		HashMap<Month, HashMap<String, PaymentDetails>> monthsMap = fullTreeMap.get(receipt);
		ArrayList<TreeItem<PaymentFX>> theTempList = new ArrayList<>();
		System.out.println("Create Month PaymentFX:");
		for(Entry <Month, HashMap<String, PaymentDetails>> entry : monthsMap.entrySet()){
			Month currentMonth = entry.getKey();
			// null --> default value (not actual month)
			if(currentMonth == null ){ continue; }
			System.out.println("  currentMonth = "+currentMonth);
			PaymentDetails totalPayment = new PaymentDetails();
			totalPayment.setBillingMonth(currentMonth);
			HashMap<String, PaymentDetails> payments = entry.getValue();
			// find total amounts
			// get list of payments per class
			ArrayList<TreeItem<PaymentFX>> theClassesListTemp = new ArrayList<>();
			for(Entry<String, PaymentDetails> classesEntry : payments.entrySet()){
				PaymentDetails payment = classesEntry.getValue();
				String className = classesEntry.getKey();
				totalPayment.addAmounts(payment);
				PaymentFX classRow  = new PaymentFX(); // ClassRowFX
				classRow.addPayment(payment);
				classRow.setClassName(className);
				System.out.println("   --> classname = "+className);
//				classRow.setClassName("- className -");
				classRow.setBillingMonth("");
				classRow.setReceipt("");
				classRow.setPaidBy("");
				classRow.setPaidFor("");
				theClassesListTemp.add(new TreeItem<>(classRow));
			}
			SortedList<TreeItem<PaymentFX>> theClassesList = createSortedClassesList(theClassesListTemp);
			
			PaymentFX monthRow = new PaymentFX();
			monthRow.addPayment(totalPayment);
			monthRow.setClassName("");
			monthRow.setReceipt("");
			monthRow.setPaidBy("");
			monthRow.setPaidFor("");
			TreeItem<PaymentFX> monthTreeItem = new TreeItem<>(monthRow);
			monthTreeItem.getChildren().addAll(theClassesList);
			theTempList.add(monthTreeItem);
		}
		
		SortedList<TreeItem<PaymentFX>> theList = createSortedMonthList(theTempList);
		return theList;
	}
	
	
	
	
	public HashMap<String, HashMap<Month, HashMap<String, PaymentDetails>>> getFullTreeMap() { return fullTreeMap; }
	public void setFullTreeMap(HashMap<String, HashMap<Month, HashMap<String, PaymentDetails>>> fullTreeMap) { this.fullTreeMap = fullTreeMap; }
	
	public SortedList<TreeItem<PaymentFX>> createSortedMonthList(ArrayList<TreeItem<PaymentFX>> tempList) {
		SortedList<TreeItem<PaymentFX>> sortedMonthsList = new SortedList<>( FXCollections.observableArrayList(tempList),
				(TreeItem<PaymentFX> monthA, TreeItem<PaymentFX> monthB) -> {
					return  monthA.getValue().getBillingMonth().compareTo(monthB.getValue().getBillingMonth());
				});
		return sortedMonthsList;
	}
	public SortedList<TreeItem<PaymentFX>> createSortedClassesList(ArrayList<TreeItem<PaymentFX>> theClassesListTemp) {
		SortedList<TreeItem<PaymentFX>> sortedClassesList = new SortedList<>( FXCollections.observableArrayList(theClassesListTemp),
				(TreeItem<PaymentFX> monthA, TreeItem<PaymentFX> monthB) -> {
					return  monthA.getValue().getClassName().compareTo(monthB.getValue().getClassName());
				});
		return sortedClassesList;
	}
	
//	public SortedList<TreeItem<MonthRowFX>> createSortedMonthList() {
//		SortedList<TreeItem<MonthRowFX>> sortedMonthsList = new SortedList<>( FXCollections.observableArrayList(),
//				(TreeItem<MonthRowFX> monthA, TreeItem<MonthRowFX> monthB) -> {
//					return  monthA.getValue().getBillingMonth().compareTo(monthB.getValue().getBillingMonth());
//				});
//		return sortedMonthsList;
//	}
//	public SortedList<TreeItem<ClassRowFX>> createSortedClassesList() {
//		SortedList<TreeItem<ClassRowFX>> sortedClassesList = new SortedList<>( FXCollections.observableArrayList(),
//				(TreeItem<ClassRowFX> monthA, TreeItem<ClassRowFX> monthB) -> {
//					return  monthA.getValue().getClassName().compareTo(monthB.getValue().getClassName());
//				});
//		return sortedClassesList;
//	}
	public SortedList<TreeItem<PaymentFX>> createSortedPaymentList(ArrayList<TreeItem<PaymentFX>> treeItemsTemp) {
		SortedList<TreeItem<PaymentFX>> sortedClassesList = new SortedList<>( FXCollections.observableArrayList(treeItemsTemp),
				(TreeItem<PaymentFX> monthA, TreeItem<PaymentFX> monthB) -> {
					return  monthA.getValue().getReceipt().compareTo(monthB.getValue().getReceipt());
				});
		return sortedClassesList;
	}
	
	
}
