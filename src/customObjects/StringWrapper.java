package customObjects;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author d.michaelides
 */
public class StringWrapper {
    public StringWrapper(String v){
        value = new SimpleStringProperty(v);
    }
    private final SimpleStringProperty value;

    public String getValue() {
        return value.get();
    }
}
