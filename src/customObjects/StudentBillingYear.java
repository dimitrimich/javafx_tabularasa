/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package customObjects;

import DB.objects.Aclass;
import DB.objects.Billing;
import DB.objects.Lesson;
import DB.objects.Student;
import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author dimitris
 */
public class StudentBillingYear {
    private Student billingStudent;
	private Year billingYear;

	public Year getBillingYear() { return billingYear; }
	public void setBillingYear(Year billingYear) { this.billingYear = billingYear; }
	
	
    private HashMap<Month, IsPaidAndAmount> yearsBillingInfo = new HashMap<>();
    
    public StudentBillingYear(Student bStudent, Year year){
        if(bStudent == null){
            throw new NullPointerException("Student given is Null");
        }
		this.billingYear = year;
        this.billingStudent = bStudent;
        init();
    }
    private void init(){
        yearsBillingInfo.put(Month.AUGUST, getAmountPaidForAllClassesForMonth(Month.AUGUST));
        yearsBillingInfo.put(Month.SEPTEMBER, getAmountPaidForAllClassesForMonth(Month.SEPTEMBER));
        yearsBillingInfo.put(Month.OCTOBER, getAmountPaidForAllClassesForMonth(Month.OCTOBER));
        yearsBillingInfo.put(Month.NOVEMBER, getAmountPaidForAllClassesForMonth(Month.NOVEMBER));
        yearsBillingInfo.put(Month.DECEMBER, getAmountPaidForAllClassesForMonth(Month.DECEMBER));
        yearsBillingInfo.put(Month.JANUARY, getAmountPaidForAllClassesForMonth(Month.JANUARY));
        yearsBillingInfo.put(Month.FEBRUARY, getAmountPaidForAllClassesForMonth(Month.FEBRUARY));
        yearsBillingInfo.put(Month.MARCH, getAmountPaidForAllClassesForMonth(Month.MARCH));
        yearsBillingInfo.put(Month.APRIL, getAmountPaidForAllClassesForMonth(Month.APRIL));
        yearsBillingInfo.put(Month.MAY, getAmountPaidForAllClassesForMonth(Month.MAY));
        yearsBillingInfo.put(Month.JUNE, getAmountPaidForAllClassesForMonth(Month.JUNE));
        yearsBillingInfo.put(Month.JULY, getAmountPaidForAllClassesForMonth(Month.JULY));
    }
    
    public Student getBillingStudent() { return billingStudent; }
    public void setBillingStudent(Student billingStudent) { this.billingStudent = billingStudent; }
    public HashMap<Month, IsPaidAndAmount> getYearsBillingInfo() { return yearsBillingInfo; }
    public void setYearsBillingInfo(HashMap<Month, IsPaidAndAmount> yearsBillingInfo) { this.yearsBillingInfo = yearsBillingInfo; }
    
    public IsPaidAndAmount getAmountPaidForClassForMonth(Aclass thisClass, Month calendarMonth){
        ArrayList<Lesson> monthsLessons = thisClass.getLessonsForAMonth(calendarMonth, billingYear);
        IsPaidAndAmount theDetails = new IsPaidAndAmount();
        theDetails.setPaidInFull(true);
        
        if(thisClass.isPaidPerMonth()){
            Iterator<Lesson> it = monthsLessons.iterator();
            boolean foundUnpaidLesson = false;
            // go through the lessons and see if one is not paid
            while(it.hasNext()){
                Lesson thisLesson = it.next();
                Billing thisBilling = thisLesson.getBilling().get(billingStudent.getId());
                if(! thisBilling.isPaidInFull()){
                    foundUnpaidLesson = true;
                    break;
                }
            }
            if(foundUnpaidLesson){
               theDetails.setPaidInFull(false);
               theDetails.setDueAmount(thisClass.getMonthlyFee());
               theDetails.setPaidAmount(BigDecimal.ZERO);
            }else{
                theDetails.setPaidInFull(true);
                theDetails.setDueAmount(BigDecimal.ZERO);
                theDetails.setPaidAmount(thisClass.getMonthlyFee());
            }
            
        }else{// Paid Per Lesson
            Iterator<Lesson> it = monthsLessons.iterator();
            while(it.hasNext()){
                Lesson thisLesson = it.next();
                Billing thisBilling = thisLesson.getBilling().get(billingStudent.getId());
                if(thisBilling.isPaidInFull()){
                    theDetails.setPaidAmount(theDetails.getPaidAmount().add(thisBilling.getAmountFull()));
                }else{
//                    theDetails.setDueAmount(theDetails.getDueAmount().add(thisBilling.getAmountFull().subtract(thisBilling.getAmountPaid())));
                    theDetails.setPaidInFull(false);
                }
            }
        }
        return theDetails;
    }
    
    public IsPaidAndAmount getAmountPaidForAllClassesForMonth(Month calendarMonth){
        
        IsPaidAndAmount monthTotals = new IsPaidAndAmount();
        for(Aclass thisClass : billingStudent.getClasses()){
            IsPaidAndAmount thisClassDetails = getAmountPaidForClassForMonth(thisClass, calendarMonth);
            monthTotals.setPaidInFull(thisClassDetails.isPaidInFull());
            monthTotals.setDueAmount(monthTotals.getDueAmount().add(thisClassDetails.getDueAmount()));
            monthTotals.setPaidAmount(monthTotals.getPaidAmount().add(thisClassDetails.getPaidAmount()));
        }
        
        return monthTotals;
    }
    
    public class IsPaidAndAmount{
        private BigDecimal paidAmount = BigDecimal.ZERO;
        private BigDecimal dueAmount = BigDecimal.ZERO;
        private boolean paidInFull;

        public BigDecimal getDueAmount() {  return dueAmount; }
        public void setDueAmount(BigDecimal dueAmount) { this.dueAmount = dueAmount; }
        public BigDecimal getPaidAmount() { return paidAmount; }
        public void setPaidAmount(BigDecimal amount) { this.paidAmount = amount; }
        public boolean isPaidInFull() { return paidInFull; }
        public void setPaidInFull(boolean paidInFull) { this.paidInFull = paidInFull; }
    }
}
