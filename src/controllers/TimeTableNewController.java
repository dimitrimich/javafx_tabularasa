/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.DButils;
import DB.objects.Aclass;
import DB.objects.Schedule;
import DB.objects.Teacher;
import DB.objects.WeeklyLesson;
import customControls.TimeTableCell;
import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import utils.LanguageUtils;
import utils.TimeTableHandlers;
import utils.TimeTableUtils;

/**
 *
 * @author d.michaelides
 */
public class TimeTableNewController extends StageController{
    private TimeTableService service;
    private final URL fxmlURL;
    private final ResourceBundle resources;
    
    public TimeTableNewController(URL fxml, ResourceBundle res) {
        super();
        fxmlURL = fxml;
        resources = res;
    }
    @FXML private BorderPane region;
    @FXML private ProgressIndicator progress;
    @FXML private AnchorPane root;
    @FXML private AnchorPane rightPane;
    @FXML private SplitPane splitPane;
    @FXML private VBox theV;
    @FXML private ScrollPane theScroll;
    @FXML private ChoiceBox<String> choice;
    @FXML private Label numOfClasses;
    @FXML private Label nameOfTeacher;
    @FXML private GridPane timeTable;
    @FXML private VBox theClass;
    
    @FXML private VBox mo;
    @FXML private VBox tu;
    @FXML private VBox we;
    @FXML private VBox th;
    @FXML private VBox fr;
    @FXML private VBox sa;
    @FXML private VBox su;
    @FXML private VBox times;
    @FXML private VBox notAssigned;
    @FXML private Label moDate;
    @FXML private Label tuDate;
    @FXML private Label weDate;
    @FXML private Label thDate;
    @FXML private Label frDate;
    @FXML private Label saDate;
    @FXML private Label suDate;
    
    
    
    
    @FXML private TextArea selectedClass;
    private HashMap<String, ArrayList<Node>> allTheItems;
    private Teacher theTeacher;
    private Schedule sle1;
    private Schedule sle2;
    private Schedule sle3;
    private ArrayList<Schedule> schedules = new ArrayList<>(3);
    
    private LocalDate showingMonday;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/ MMMM / yyyy");
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        
        LocalDate today = LocalDate.now();
        showingMonday = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        moDate.setText(showingMonday.format(dateFormat));
        tuDate.setText(showingMonday.plusDays(1).format(dateFormat));
        weDate.setText(showingMonday.plusDays(2).format(dateFormat));
        thDate.setText(showingMonday.plusDays(3).format(dateFormat));
        frDate.setText(showingMonday.plusDays(4).format(dateFormat));
        saDate.setText(showingMonday.plusDays(5).format(dateFormat));
        suDate.setText(showingMonday.plusDays(6).format(dateFormat));
    }
    
    
    @Override
    protected void changeLocale(){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
//        if(theTable == null)
//            return;
    }

    
    @FXML
    private void refresh(Event event) throws IOException {
        mo.getChildren().clear();
        tu.getChildren().clear();
        we.getChildren().clear();
        th.getChildren().clear();
        fr.getChildren().clear();
        sa.getChildren().clear();
        su.getChildren().clear();
        notAssigned.getChildren().clear();
        times.getChildren().clear();
        
        TimeTableService timeTableService = new TimeTableService();
        region.visibleProperty().bind(timeTableService.runningProperty());
        progress.visibleProperty().bind(timeTableService.runningProperty());
        progress.setProgress(-1);
        timeTableService.start();
        progress.requestFocus();
    }
    
    private HashMap<String, ArrayList<Node>> populateTimeTable(){
        // TODO initialize handlers from here
        
        HashMap<String, ArrayList<Node>> fullList = new HashMap<>();
        ArrayList<Node> moItems = TimeTableUtils.createEmptyDay(mo, 8, 14, selectedClass);
        ArrayList<Node> tuItems = TimeTableUtils.createEmptyDay(tu, 8, 14, selectedClass);
        ArrayList<Node> weItems = TimeTableUtils.createEmptyDay(we, 8, 14, selectedClass);
        ArrayList<Node> thItems = TimeTableUtils.createEmptyDay(th, 8, 14, selectedClass);
        ArrayList<Node> frItems = TimeTableUtils.createEmptyDay(fr, 8, 14, selectedClass);
        ArrayList<Node> saItems = TimeTableUtils.createEmptyDay(sa, 8, 14, selectedClass);
        ArrayList<Node> suItems = TimeTableUtils.createEmptyDay(su, 8, 14, selectedClass);
        ArrayList<Node> timeItems = TimeTableUtils.createTimes(times, 8, 14);
        ArrayList<Node> toAddItems = new ArrayList<>();
        
        ObservableList<Aclass> allClasses = DB.DBTableUtils.getAllClasses();
        for(Aclass aClass : allClasses){
            if(aClass.getSchedule() != null && aClass.getSchedule().getwLessons() != null){
                for(WeeklyLesson lesson : aClass.getSchedule().getwLessons()){
                    TimeTableCell aCell = new TimeTableCell(aClass, lesson);
                    TimeTableUtils.initializeTimeTableCell(aCell, selectedClass);
                    toAddItems.add(aCell);
                }
            }
        }
        fullList.put("moItems", moItems);
        fullList.put("tuItems", tuItems);
        fullList.put("weItems", weItems);
        fullList.put("thItems", thItems);
        fullList.put("frItems", frItems);
        fullList.put("saItems", saItems);
        fullList.put("suItems", suItems);
        
        
        fullList.put("timeItems", timeItems);
        fullList.put("toAddItems", toAddItems);
        return fullList;
    }
    
    
    @FXML
    private void closeWindow(Event event) throws IOException {
        stage.close();
    }
    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader); 
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
    
    private class TimeTableService extends Service<HashMap<String, ArrayList<Node>>> {
        @Override
        protected Task createTask() {
            TimeTableTask theTask = new TimeTableTask();
            theTask.setOnSucceeded(workerStateEvent -> {
//                Thread.currentThread().getName()
//                        sout    
                if(allTheItems != null){
                    mo.getChildren().setAll(allTheItems.get("moItems"));
                    tu.getChildren().setAll(allTheItems.get("tuItems"));
                    we.getChildren().setAll(allTheItems.get("weItems"));
                    th.getChildren().setAll(allTheItems.get("thItems"));
                    fr.getChildren().setAll(allTheItems.get("frItems"));
                    sa.getChildren().setAll(allTheItems.get("saItems"));
                    su.getChildren().setAll(allTheItems.get("suItems"));
                    times.getChildren().setAll(allTheItems.get("timeItems"));
                    notAssigned.getChildren().setAll(allTheItems.get("toAddItems"));
                    TimeTableHandlers.setTimesVBox(times);
                }else{
                    System.out.println("WHY is it null?!?!?!?");
                }
                });
            return theTask;
        }

        private class TimeTableTask extends Task<HashMap<String, ArrayList<Node>>> {
            @Override
            protected HashMap<String, ArrayList<Node>> call() throws Exception {
//                    Thread.sleep(2000l); // just emulates some loading time
                    allTheItems = populateTimeTable();
                return null;
            }
        }
    }
        
    @FXML
    private void nextWeek(Event e){
        showingMonday = showingMonday.plusDays(7);
        moDate.setText(showingMonday.format(dateFormat));
        tuDate.setText(showingMonday.plusDays(1).format(dateFormat));
        weDate.setText(showingMonday.plusDays(2).format(dateFormat));
        thDate.setText(showingMonday.plusDays(3).format(dateFormat));
        frDate.setText(showingMonday.plusDays(4).format(dateFormat));
        saDate.setText(showingMonday.plusDays(5).format(dateFormat));
        suDate.setText(showingMonday.plusDays(6).format(dateFormat));
    }
    
    
    @FXML
    private void previousWeek(Event e){
        showingMonday = showingMonday.minusDays(7);
        moDate.setText(showingMonday.format(dateFormat));
        tuDate.setText(showingMonday.plusDays(1).format(dateFormat));
        weDate.setText(showingMonday.plusDays(2).format(dateFormat));
        thDate.setText(showingMonday.plusDays(3).format(dateFormat));
        frDate.setText(showingMonday.plusDays(4).format(dateFormat));
        saDate.setText(showingMonday.plusDays(5).format(dateFormat));
        suDate.setText(showingMonday.plusDays(6).format(dateFormat));
    }
	@Override
	public void onLoad() {
	}
	@Override
	public void unload(){}
    
}
    