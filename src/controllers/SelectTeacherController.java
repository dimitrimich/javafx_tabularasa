/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.DBTableUtils;
import DB.objects.Teacher;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tables.TeachersTable;
import utils.dialogs.CustomDialogs;
import utils.LanguageUtils;
/**
 *
 * @author d.michaelides
 */
public class SelectTeacherController implements Initializable{
    
    private Stage stage;
//    private Stage parentStage;
    @FXML private ImageView language;
    @FXML private Button saveButton;
    private Image img;
    private ResourceBundle labels;
    @FXML private AnchorPane root;
    private Scene nextScene;
    @FXML private VBox theV;
    @FXML private ScrollPane theScroll;
    @FXML private Region region;
    @FXML private ProgressIndicator progress;
    @FXML private TableView <Teacher> theTable;
    @FXML private Button okButton;
    @FXML private Button cancelButton;
    private  ObservableList<Teacher> observableTeachers;
    
    private Teacher selectedTeacher;
    public Teacher getSelectedTeacher(){
        return selectedTeacher;
    }
    
    public void init(Stage s){
        stage = s;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        final ObservableList<TableColumn>  columnsList = TeachersTable.initializeTeachersTable(theTable);
        theTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Teacher> ov, Teacher t, Teacher t1) -> {
            okButton.setDisable((theTable.getSelectionModel().selectedItemProperty() == null));
        });
        theTable.setTableMenuButtonVisible(true);
        
    }
    /**
     Set the text to all visible elements of the stage
     * plus change flag
     */
    private void changeLocale(){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        if(theTable == null)
            return;
        
//        teacherTitle.setText(labels.getString("create.class.teacher_info"));
//        pageTitle.setText(labels.getString("create.class.title"));
    }
    
    
    @FXML
    private void changeLanguage(Event event) throws IOException {
    }
    
    @FXML
    private void okPressed(final Event event) throws IOException {
        
        if(theTable.getSelectionModel().getSelectedItem() != null){
            labels = LanguageUtils.getInstance().getCurrentLanguage();
            StringBuilder sb = new StringBuilder();
            sb.append(labels.getString("dialog.select.teacher.line1"));
            sb.append(": ");
            sb.append(theTable.getSelectionModel().getSelectedItem().getBasicInfo().getFname());
            sb.append(" ");
            sb.append(theTable.getSelectionModel().getSelectedItem().getBasicInfo().getLname());
            sb.append(" ");
            sb.append(labels.getString("dialog.select.teacher.line2")); 
            String yes = labels.getString("dialog.yes");
            Optional<ButtonType> response = CustomDialogs.yesNoDialog(
                    labels.getString("dialog.save.teacher.title"),
                    sb.toString(),
                    yes, labels.getString("dialog.no"));
            if(response.isPresent()){
                if(response.get().getText().equals(yes)){
                    System.out.println("Yes was pressed");
                    selectedTeacher = theTable.getSelectionModel().getSelectedItem();
                    System.out.println("Yes was pressed");
                    Node  source = (Node)  event.getSource();
                    Stage window  = (Stage) source.getScene().getWindow();
                    window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
                    window.close();
                }
            }
        }
    }
    
    @FXML
    private void cancelPressed(Event event) {
        selectedTeacher = null;
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
        
    }
    
    public void refreshItems() {
        theTable.getItems().clear();
        TimeTableService timeTableService = new TimeTableService();
        progress.visibleProperty().bind(timeTableService.runningProperty());
        timeTableService.start();
    }
    
    private class TimeTableService extends Service {
        @Override
        protected Task createTask() {
            TimeTableTask theTask = new TimeTableTask();
            theTask.setOnSucceeded(workerStateEvent -> {
                if(observableTeachers != null){
                    theTable.setItems(observableTeachers);
                }else{
                    System.out.println("WHY is it null?!?!?!? -TEACHERS -2-");
                }
                });
            return theTask;
        }
        private class TimeTableTask extends Task {
            @Override
            protected Object call() throws Exception {
                observableTeachers = DBTableUtils.getAllTeachers();
                return null;
            }
        }
    }

}


 
    