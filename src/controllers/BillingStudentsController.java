/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import DB.objects.Student;
import static controllers.StageController.cachingClassLoader;
import customObjects.StudentBillingYear;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Year;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import tables.BillingStudentsTable;
import utils.LanguageUtils;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class BillingStudentsController extends StageController {

    private final URL fxmlURL;
    private final ResourceBundle resources;
    public BillingStudentsController(URL fxml, ResourceBundle res) {
        super();
        fxmlURL = fxml;
        resources = res;
    }
    
    @FXML private ScrollPane mainScrollPane;
    @FXML private VBox theV;
    @FXML private TableView<StudentBillingYear> theTable;
    private ObservableList<StudentBillingYear> observableBillings;
    private ObservableList<StudentBillingYear> observableBillings2;
    private StudentBillingYear billingYear;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.minWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
        theV.maxWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
        System.out.println("---------------- start -----------------------");
        ObservableList<Student> allStus = DB.DBTableUtils.getAllStudents();
        
        
        System.out.println("---------------- got students -----------------------");
        if(allStus.size() > 3){
                
        Student s = allStus.get(0);
        Student s2 = allStus.get(1);
        System.out.println(" 1 = "+ s.getFullName());
        System.out.println(" 2 = "+ s2.getFullName());
        
        billingYear = new StudentBillingYear(s, Year.of(LocalDate.now().getYear()));
        System.out.println("---------------- billing years -----------------------");
        
        BillingStudentsTable.initializeTable(theTable);
        
        observableBillings = FXCollections.observableArrayList();
        observableBillings.add(billingYear);
        
        observableBillings2 = FXCollections.observableArrayList();
        observableBillings2.add(new StudentBillingYear(s2, Year.of(LocalDate.now().getYear())));
        
       
        System.out.println("Done..... -> "+observableBillings.get(0).getBillingStudent().getFullName());
    
        }
    }
    
    @FXML
    private void click(ActionEvent event) throws IOException {
        System.out.println("CLICKED !! ");
        theTable.setItems(observableBillings);
        System.out.println(" == null -> "+( observableBillings ==null));
        System.out.println(" == full name -> "+( observableBillings.get(0).getBillingStudent().getFullName()));
        System.out.println("theTable.setItems !! ");
        
    }
    
    @FXML
    private void click2(ActionEvent event) throws IOException {
        System.out.println("CLICKED !! ");
        theTable.setItems(observableBillings2);
        System.out.println(" == null -> "+( observableBillings2 ==null));
        System.out.println(" == full name -> "+( observableBillings2.get(0).getBillingStudent().getFullName()));
        System.out.println("theTable.setItems !! ");
        
    }
    
    
    
    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader); 
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }

    @Override
    protected void changeLocale() {
        throw new UnsupportedOperationException("Billing Students - Controller- change Locale."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
	}
    
}
