/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DBDeleteUtils;
import DB.DBTableUtils;
import DB.DBUpdateUtils;
import DB.objects.Aclass;
import DB.objects.Schedule;
import DB.objects.Teacher;
import DB.objects.WeeklyLesson;
import customControls.CustomTimePicker;
import customControls.NumberTextField;
import customObjects.MyDecimalFormater;
import images.Images;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import screens.FXMLs;
import tables.ClassesTable;
import utils.EventFilters;
import utils.LanguageUtils;
import utils.Utils;
import static utils.Utils.notNull;
import utils.WeeklyLessonsEditor;
import utils.dialogs.CustomDialogs;
import utils.menu.ScreenNavigator;

/**
 *
 * @author d.michaelides
 */
public class ViewClassesController extends StageController{
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	
	public ViewClassesController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	@FXML private AnchorPane root;
	@FXML private ScrollPane theScroll;
	@FXML private VBox theV;
	@FXML private ScrollPane theScroll2;
	@FXML private VBox theV2;
	@FXML private SplitPane splitPane;
	@FXML private BorderPane region;
	@FXML private ProgressIndicator progress;
	@FXML private ProgressIndicator progress2;
	@FXML private Label hint;
	
	@FXML private Label mainTitle;
	@FXML private TableView<Aclass> classesTable;
	@FXML private TextField searchBox;
	
	
	@FXML private Label pageTitle;
	@FXML private Label b_info;
	@FXML private Label name;
	@FXML private Label description;
	@FXML private Label classCodeLbl;
	@FXML private TextField nameText;
	@FXML private TextArea descriptionText;
	@FXML private TextField classCodeText;
	
	
	@FXML private Label teacherTitle;
	@FXML private Label teacherSelect;
	@FXML private Label selectedTeacher;
	@FXML private Label selectedTeacherName;
	@FXML private Button clearTeacher;
	@FXML private Label timeTableTitle;
	@FXML private Label timesPW;
	@FXML private Button chooseTeacherButton;
	@FXML private Button saveButton;
	@FXML private Button cancelButton;
	@FXML private Button deleteClass;
	@FXML private GridPane timeTable;
	
	@FXML private Label dayTitle;
	@FXML private Label hourTitle;
	@FXML private Label durationTitle;
	
	@FXML private ChoiceBox<String> choice;
	@FXML private Label theClassName;
	@FXML private DatePicker startDatePicker;
	@FXML private DatePicker endDatePicker;
	@FXML private TabPane mainTabPane;
	@FXML private TabPane editTabPane;
	@FXML private Tab calendarTab, connectionsTab, generalTab, editTab;
	
	@FXML private RadioButton perMonthRadio;
	@FXML private RadioButton perHourRadio;
	@FXML private GridPane billingGrid;
	private final NumberTextField perHour = new NumberTextField(new BigDecimal(Double.MAX_VALUE));
	private final NumberTextField perMonth = new NumberTextField(new BigDecimal(Double.MAX_VALUE));
	private final MyDecimalFormater myFormater = new MyDecimalFormater();
	private final ConnectionsClassController connectionsTabController = (ConnectionsClassController) ScreenNavigator.getControllersMap().get("ConnectionsClass");
	private final AgendaForClassesController agendaTabController = (AgendaForClassesController) ScreenNavigator.getControllersMap().get("AgendaForClasses");
	
	
	private ArrayList<ChoiceBox<String>> days = new ArrayList<>(11);
	private ArrayList<CustomTimePicker> timePickers = new ArrayList<>(11);
	private ArrayList<ComboBox<String>> durations = new ArrayList<>(11);
	private Aclass selectedClass;
	private Teacher theTeacherOfTheClass;
	
	
	
	private ArrayList<String> choose_times = new ArrayList<>(
			Arrays.asList("-------",
					"1", "2", "3", "4", "5", "6",
					"7", "8",  "9",  "10", "11"));
	
	private ArrayList<String> common_hours_24 = new ArrayList<>(
			Arrays.asList("",
					"13",
					"14", "15", "16", "17", "18",
					"19", "20",  "21",  "22"));
	
	private ArrayList<String> common_minutes = new ArrayList<>(
			Arrays.asList("", "00", "15", "20",
					"30", "40", "45"));
	
	
	private final ArrayList<String> getDays_strings(){
		return new ArrayList<>(
				Arrays.asList("-------",
						labels.getString("day.su"),labels.getString("day.mo"),labels.getString("day.tu"), labels.getString("day.we"),
						labels.getString("day.th"), labels.getString("day.fr"), labels.getString("day.sa")));
	}
	
	private ArrayList<String> common_durations = new ArrayList<>(
			Arrays.asList("",
					"30","45", "60",
					"75", "90", "120"));
	
	
	private  ObservableList<Aclass> observableClasses;
//    private  ObservableList<Aclass> observableClasses;
//    private  ObservableList<Schedule> observableSchedules;
//    private  ObservableList<Teacher> observableTeachers;
//    private  ObservableList<ClassScheduleTeacher> observableInfo;
//    private  ArrayList<ClassScheduleTeacher> info = new ArrayList<>();
	
	private final Image defaultImage  = new Image(Images.class.getResource("default_profile.png").toString(), 140, 140, true, true);
	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		calendarTab.setContent(agendaTabController.getScroll());
		connectionsTab.setContent(connectionsTabController.getScroll());
		
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		theV.prefWidthProperty().bind(theScroll.widthProperty());
		theV2.prefWidthProperty().bind(theScroll2.widthProperty());
		
		for(int i = 0 ; i< 11 ; i++){
			ChoiceBox <String> cb = new ChoiceBox();
			cb.setDisable(true);
			cb.setPrefWidth(150);
			timeTable.add(cb, 0, i+3, 1, 1);
			days.add(cb);
			
			ComboBox<String> dur = new ComboBox<>();
			dur.setEditable(true);
			dur.setDisable(true);
			timeTable.add(dur, 2, i+3, 1, 1);
			durations.add(dur);
			
			CustomTimePicker ctp = new CustomTimePicker(cb, dur);
			timeTable.add(ctp, 1, i+3, 1, 1);
			timePickers.add(ctp);
			GridPane.setVgrow(ctp, Priority.NEVER);
			
		}
		
		classesTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Aclass> ov, Aclass oldValue, Aclass newValue) -> {
			if(oldValue != newValue){
				if(newValue != null){
					editTab.setDisable(true);
					if(theScroll2.isDisable()){
						theScroll2.setDisable(false);
					}
					selectedClass = newValue;
					showClassInfo();
					editTab.setDisable(false);
				}
			}
		});
		EventFilters.setEditableTextField(nameText);
		EventFilters.setEditableTextField(classCodeText);
		EventFilters.setEditableTextArea(descriptionText);
		
		searchBox.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
			if(key.getCharacter().equals(" ") && searchBox.getText().contains(" ")){
				key.consume();
			}
		});
		searchBox.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable, String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		classesTable.setTableMenuButtonVisible(true);
		
		
		
		choice.getItems().clear();
		choice.getItems().addAll(choose_times);
		choice.getSelectionModel().selectFirst();
		
		
		EventHandler<KeyEvent> handler = utils.EventFilters.getOnlyNumbersFilter();
		for(int i = 0; i < 11; i++){
			
			// duration listener (minutes)
			ChangeListener<String> durationsListener = utils.EventFilters.getHoursListener(durations.get(i), 3, 240);
			durations.get(i).getEditor().textProperty().addListener(durationsListener);
			
			durations.get(i).getItems().clear();
			durations.get(i).getItems().addAll(common_durations);
			durations.get(i).getSelectionModel().selectFirst();
			days.get(i).getItems().clear();
			days.get(i).getItems().addAll(getDays_strings());
			days.get(i).getSelectionModel().selectFirst();
		}
		
		
		choice.valueProperty().addListener(
				new ChangeListener() {
					@Override
					public void changed(ObservableValue observable,
							Object oldVal, Object newVal) {
						int newValue_int;
						try{
							newValue_int = Integer.parseInt((String) newVal);
						}catch(NumberFormatException e){
							newValue_int = 0;
						}
						int oldValue_int;
						try{
							oldValue_int = Integer.parseInt((String) oldVal);
						}catch(NumberFormatException e){
							oldValue_int = 0;
						}
						
						dayTitle.setDisable(newValue_int == 0);
						hourTitle.setDisable(newValue_int == 0);
						durationTitle.setDisable(newValue_int == 0);
						
						if( newValue_int >= oldValue_int){
							// enable values
							for(int i = oldValue_int; i < newValue_int; i++){
//                        days.get(i).setDisable(false);
								durations.get(i).setDisable(false);
							}
						}else{
							if(oldValue_int == 11){
								oldValue_int = 10;
							}
							
							for(int i = oldValue_int; i >=  newValue_int; i--){
								// disable values
								days.get(i).setDisable(true);
								durations.get(i).setDisable(true);
								days.get(i).getSelectionModel().select(0);
								durations.get(i).getEditor().setText("");
								timePickers.get(i).getTimePicker().setDisable(true);
								timePickers.get(i).getTimePicker().setOpacity(0.8);
								timePickers.get(i).getTimePicker().setLocalTime(LocalTime.of(12, 00));
							}
						}
					}
				});
		initDatePickers();
		calendarTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue){
				agendaTabController.setUpCallBack();
				agendaTabController.setUpAppoinmentCallBack();
				agendaTabController.setUpPopUpCallBack();
				agendaTabController.setUpPopUpHideCallBack();
			}
		});
		final ToggleGroup toggleGroup = new ToggleGroup();
		perMonthRadio.setToggleGroup(toggleGroup);
		perHourRadio.setToggleGroup(toggleGroup);
		billingGrid.add(perHour, 2, 1);
		billingGrid.add(perMonth, 4, 1);
		perHourRadio.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue){
				perHour.setDisable(false);
				perMonth.setDisable(true);
				perMonth.setText("");
			}else{
				perHour.setDisable(true);
				perMonth.setDisable(false);
				perHour.setText("");
			}
		});
		perMonthRadio.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue){
				perMonth.setDisable(false);
				perHour.setDisable(true);
				perHour.setText("");
			}else{
				perMonth.setDisable(true);
				perHour.setDisable(false);
				perMonth.setText("");
			}
		});
		perHourRadio.setSelected(true);
	}
	
	private void showClassInfo(){
		if(connectionsTabController == null){
			System.out.println("TAB2 Controller == null");
		}else{
			connectionsTabController.initializeTables(selectedClass);
		}
		if(agendaTabController == null){
			System.out.println("Agenda Tab Controller == null");
		}else{
			agendaTabController.setSelectedClass(selectedClass);
			agendaTabController.setUpAppointments();
		}
		if(selectedClass.getStartDate() != null){
			startDatePicker.setValue(selectedClass.getStartDate());
		}
		if(selectedClass.getEndDate()!= null){
			endDatePicker.setValue(selectedClass.getEndDate());
			endDatePicker.setDisable(false);
		}
		
		theClassName.setText(selectedClass.getName());
		nameText.setText(selectedClass.getName());
		classCodeText.setText(selectedClass.getCodeName());
		descriptionText.setText(notNull(selectedClass.getDescription()));
		if(selectedClass.getTeacher() != null){
			theTeacherOfTheClass = selectedClass.getTeacher();
			selectedTeacherName.setText(selectedClass.getTeacher().getBasicInfo().getFname() + " "+ selectedClass.getTeacher().getBasicInfo().getLname());
			chooseTeacherButton.setDisable(true);
			clearTeacher.setDisable(false);
		}else{
			selectedTeacherName.setText("");
		}
		
		System.out.println("Showing info of class - "+ selectedClass.getMonthlyFee());
		if(selectedClass.isPaidPerMonth()){
			perMonthRadio.setSelected(true);
			perMonth.setNumber(selectedClass.getMonthlyFee());
			perHour.setNumber(BigDecimal.ZERO);
			perMonth.setText(myFormater.numberToString(selectedClass.getMonthlyFee()));
		}else{
			perHourRadio.setSelected(true);
			perHour.setNumber(selectedClass.getHourlyFee());
			perMonth.setNumber(BigDecimal.ZERO);
			perHour.setText(myFormater.numberToString(selectedClass.getHourlyFee()));
		}
		
		if(selectedClass.getSchedule() != null && selectedClass.getSchedule().getwLessons() != null){
			choice.getSelectionModel().select(Integer.toString(selectedClass.getSchedule().getwLessons().size()));
		}else{
			choice.getSelectionModel().select(0);
			return;
		}
		if(selectedClass.getSchedule().getwLessons().isEmpty()){
			choice.getSelectionModel().select(0);
			return;
		}
		
		int index = 0;
		for (WeeklyLesson lesson : selectedClass.getSchedule().getwLessons()) {
			try{
				days.get(index).setDisable(false);
				days.get(index).getSelectionModel().select(lesson.getDayOfTheWeek());
				timePickers.get(index).getTimePicker().setLocalTime(LocalTime.of(lesson.getStartingHour(), lesson.getStartingMinute()));
				if(lesson.getStartingHour() != 0 || lesson.getStartingMinute() != 0){
					timePickers.get(index).getTimePicker().setDisable(false);
					timePickers.get(index).getTimePicker().setOpacity(1);
				}
				durations.get(index).setDisable(false);
				durations.get(index).getEditor().setText(Integer.toString(lesson.getDuration()));
				index++;
			}catch(Exception e){
				System.out.println("Exception while creating days");
				e.printStackTrace(System.err);
			}
		}
//        theTabs.getSelectionModel().select(1);
	}
	
	@Override
	protected void changeLocale(){
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		if(mainTitle == null)
			return;
		mainTitle.setText(labels.getString("view.classes.title"));
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		teacherSelect.setText(labels.getString("create.class.teacher_title"));
		selectedTeacher.setText(labels.getString("create.class.teacher_selected"));
		teacherTitle.setText(labels.getString("create.class.teacher_info"));
		pageTitle.setText(labels.getString("create.class.title"));
		b_info.setText(labels.getString("create.class.b_info"));
		name.setText(labels.getString("create.class.name"));
		description.setText(labels.getString("create.class.description"));
		timeTableTitle.setText(labels.getString("create.class.table"));
		timesPW.setText(labels.getString("create.class.per_week"));
		dayTitle.setText(labels.getString("create.class.s_day"));
		hourTitle.setText(labels.getString("create.class.s_time"));
		durationTitle.setText(labels.getString("create.class.s_duration"));
		saveButton.setText(labels.getString("create.class.save"));
		cancelButton.setText(labels.getString("create.class.cancel"));
		deleteClass.setText(labels.getString("delete.class"));
		classCodeLbl.setText(labels.getString("create.class.codeName"));
		generalTab.setText(labels.getString("view.classes.general"));
		connectionsTab.setText(labels.getString("view.classes.students"));
		calendarTab.setText(labels.getString("view.classes.cal"));
	}
	
	
	public void searchForAll(String oldVal, String newVal) {
		if ((oldVal != null && (newVal.length() < oldVal.length())) ) {
			classesTable.setItems( observableClasses );
		}
		if(newVal.isEmpty()){ return; }
		
		String [] tokens = searchBox.getText().trim().split(" ");
		String searchToken1 = tokens[0];
		String searchToken2 = (tokens.length == 2) ? tokens[1] : null;
		
		
		ObservableList<Aclass> subentries = FXCollections.observableArrayList();
		
		classesTable.getItems().stream().forEach((Aclass aClass) -> {
			if(searchToken2 == null){
				if(aClass.containsKeyWordNotCaseSensitive(searchToken1)){
					subentries.add(aClass);
				}
			}else{
				if(aClass.containsKeyWordNotCaseSensitive(searchToken1) && aClass.containsKeyWordNotCaseSensitive(searchToken2)){
					subentries.add(aClass);
				}
			}
		});
		classesTable.setItems(subentries);
	}
	
	@FXML
	private void addTeacher(Event event) throws IOException {
		if(stage == null){
			Node source = (Node)event.getSource();
			stage = (Stage)(source.getScene().getWindow());
		}
		Stage selectTeacherStage = new Stage();
		final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("SelectTeacher.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
		final Scene selectTeacherScene = new Scene((AnchorPane)myLoader.load());
		
		stage.setOnCloseRequest((WindowEvent ev) -> {
			selectTeacherScene.getWindow().fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
		});
		
		SelectTeacherController controler = ((SelectTeacherController) myLoader.getController());
		controler.init(stage);
		selectTeacherStage.setOnShown((WindowEvent event1) -> {
			controler.refreshItems();
		});
//        controler.setStage(stage);
		
		selectTeacherStage.setScene(selectTeacherScene);
		selectTeacherStage.toFront();
		stage.setOpacity(0.8);
		root.mouseTransparentProperty().bind(selectTeacherScene.getWindow().showingProperty());
		root.mouseTransparentProperty().bind(selectTeacherScene.getWindow().showingProperty());
		selectTeacherScene.getWindow().setOnCloseRequest((WindowEvent ev) -> {
			stage.setOpacity(1);
			theTeacherOfTheClass = controler.getSelectedTeacher();
			if(theTeacherOfTheClass != null){
				clearTeacher.setDisable(false);
				selectedTeacher.setDisable(false);
				StringBuilder sb = new StringBuilder();
				sb.append(theTeacherOfTheClass.getBasicInfo().getFname()).append(" ").append(theTeacherOfTheClass.getBasicInfo().getLname());
				selectedTeacherName.setText(sb.toString());
			}
		});
		selectTeacherStage.show();
	}
	
	@FXML
	private void clearTeacher(Event event) throws IOException {
		selectedTeacher.setDisable(true);
		clearTeacher.setDisable(true);
		chooseTeacherButton.setDisable(false);
		selectedTeacherName.setText("");
		theTeacherOfTheClass = null;
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	private void refresh() throws IOException {
		classesTable.getItems().clear();
		TimeTableService timeTableService = new TimeTableService();
		progress.visibleProperty().bind(timeTableService.runningProperty());
		timeTableService.start();
		progress.requestFocus();
	}
	
	private class TimeTableService extends Service {
		@Override
		protected Task createTask() {
			TimeTableTask theTask = new TimeTableTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				if(observableClasses != null){
					classesTable.getItems().clear();
					classesTable.setItems(observableClasses);
					ClassesTable.initializeClassesTable(classesTable);
				}else{
					System.out.println("WHY is it null?!?!?!? -classes-");
				}
			});
			return theTask;
		}
		private class TimeTableTask extends Task {
			@Override
			protected Object call() throws Exception {
				observableClasses = DBTableUtils.getAllClasses();
				return null;
			}
		}
	}
	
	/* ----------------- DELETE class ------------------------------------------------ */
	@FXML
	private void deleteClass(Event event) {
		try{
			if(selectedClass != null){
				System.out.println("deleting");
				showConfirmDialog(false);
				System.out.println("done");
			}
		}catch(IOException e){
			System.err.println("ERROR WJHILE DELETING");
			e.printStackTrace(System.err);
		}
	}
	
	
	/* ----------------- Update Details ------------------------------------------------ */
	
	@FXML
	private void saveChanges(Event event) throws IOException {
		System.out.println("Save changes() -------");
		if(stage == null){
			Node source = (Node)event.getSource();
			stage = (Stage)(source.getScene().getWindow());
		}
		try {
			showConfirmDialog(true);
		} catch (IOException ex) {
			System.err.println("Error while showing confirmation dialog - View Classes-");
			ex.printStackTrace(System.err);
		}
	}
	private void showConfirmDialog(boolean toSave) throws IOException {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		Optional<ButtonType> response;
		String yes = labels.getString("dialog.yes");
		if(toSave){
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.class.title"), labels.getString("dialog.save.class"), yes, labels.getString("dialog.no"));
		}else{
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.delete.class.title"), labels.getString("dialog.delete.class")+"\n"+labels.getString("dialog.delete.line2"), yes, labels.getString("dialog.no"));
		}
		if(response.isPresent()){
			if(response.get().getText().equals(yes)){
				if(toSave){
					verifyInput();
				}else{
					saveChangesMade(false);
				}
			}
		}
	}
	
	
	private void verifyInput(){
		boolean thereIsError = false;
		if(!Utils.isValidParam(nameText.getText().trim())){
			Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
			thereIsError = true;
		}
		int howManyClasses = choice.getSelectionModel().getSelectedIndex();
		System.out.println("Classses selected are = "+ howManyClasses);
		if(howManyClasses > 0){
			for(int i = (0); i < howManyClasses; i++){
				if(!Utils.isValidParam(durations.get(i).getEditor().getText())){
					Utils.turnNodeRed(durations.get(i), null);
					thereIsError = true;
				}
			}
		}
		
		if(thereIsError){
			System.out.println("There Is error");
		}else{
			System.out.println("No error");
			if(isClassEdited()){
				System.out.println("Save edits");
				saveChangesMade(true);
			}else{
				CustomDialogs.infoDialog("", "Student was not edited, nothing to save....");
			}
		}
	}
	
	private void saveChangesMade(boolean toSave){
		SaveClassService saveClass = new SaveClassService(toSave);
		progress2.visibleProperty().bind(saveClass.runningProperty());
		saveButton.disableProperty().bind(saveClass.runningProperty());
		cancelButton.disableProperty().bind(saveClass.runningProperty());
		System.out.println(" ----> starting service");
		saveClass.start();
		progress2.requestFocus();
	}
	
	private class SaveClassService extends Service<Boolean> {
		private final boolean toSave;
		public SaveClassService(boolean isItToSave){
			super();
			toSave=isItToSave;
		}
		@Override
		protected Task<Boolean> createTask() {
			SaveClassTask theTask = new SaveClassTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				System.out.println("---------------------------------------");
				System.out.println("     RESULT = "+getValue());
				try{
					showMessage(getValue(), toSave);
				}catch(IOException e){
					System.err.println("Exception while showing confirmation message");
					e.printStackTrace(System.err);
				}
			});
			return theTask;
		}
		private class SaveClassTask extends Task {
			@Override
			protected Boolean call() throws Exception {
				try{
					
					// TODO : check Dates Here
					if(toSave){
						return DBUpdateUtils.updateClassInfo(selectedClass);
					}else{
						return DBDeleteUtils.deleteClassObject(selectedClass);
					}
				}catch(NumberFormatException e){
					System.out.println("Exception dam it -1233-");
					e.printStackTrace(System.err);
					return false;
				}
			}
		}
	}
	
	private void showMessage(boolean success, boolean toSave) throws IOException {
		if(success){
			if(toSave){
				String message = labels.getString("save.obj.class")+" "
						+ labels.getString("save.obj.success.line1");
				CustomDialogs.infoDialog("", message);
			}else{
				String message = labels.getString("save.obj.class")+" "
						+ labels.getString("delete.obj.success.line1");
				CustomDialogs.infoDialog("", message);
			}
		}else{
			if(toSave){
				String message = labels.getString("save.obj.class")+" "
						+ labels.getString("save.obj.fail.line1");
				CustomDialogs.errorDialog("", message);
			}else{
				String message = labels.getString("save.obj.class")+" "
						+ labels.getString("delete.obj.fail.line1");
				CustomDialogs.errorDialog("", message);
			}
		}
	}
	
	private void clearAll(){
		theClassName.setText("");
		classCodeText.setText("");
		nameText.setText("");
		descriptionText.setText("");
		clearTeacher.setDisable(true);
		selectedTeacher.setDisable(true);
		selectedTeacherName.setText("");
		selectedTeacherName.setDisable(true);
		choice.getSelectionModel().select(0);
		perHour.setText("");
		perMonth.setText("");
		
		for(int i = 0; i < 11; i++){
			durations.get(i).getSelectionModel().selectFirst();
			durations.get(i).getEditor().setText("");
			timePickers.get(i).setDisable(true);
			timePickers.get(i).setOpacity(0.8);
			days.get(i).getSelectionModel().selectFirst();
		}
	}
	
	/* ********************** editing class*******************************/
	
	private boolean isClassEdited(){
		boolean somethingWasEdited = false;
		
		if (!nameText.getText().trim().equals(selectedClass.getName())){
			selectedClass.setName(nameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("edit here 89");
		}
		if (!classCodeText.getText().trim().equals(selectedClass.getCodeName())){
			selectedClass.setCodeName(classCodeText.getText().trim());
			somethingWasEdited = true;
			System.out.println("edit here - code name");
		}
		if (!notNull(descriptionText.getText()).trim().equals((notNull(selectedClass.getDescription()).trim()))){
			selectedClass.setDescription(descriptionText.getText().trim());
			somethingWasEdited = true;
			System.out.println("edit here 90");
		}
		if(selectedClass.getSchedule() == null){
			Schedule classesSchedule = new Schedule();
			selectedClass.setSchedule(classesSchedule);
			classesSchedule.setTheClass(selectedClass);
			somethingWasEdited = true;
		}
		boolean startDateChanged;
		boolean endDateChanged;
//		System.out.println("S:  "+ selectedClass.getStartDate() +" Vs" + startDatePicker.getValue());
//		System.out.println("E: "+ selectedClass.getEndDate() +" Vs" + endDatePicker.getValue());
		if((startDateChanged = !selectedClass.getStartDate().equals(startDatePicker.getValue())) | (endDateChanged = !selectedClass.getEndDate().equals(endDatePicker.getValue()))){
			System.out.println(startDateChanged + " | "+endDateChanged);
			if(startDateChanged){
				WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
				editor.setNewStartDate(selectedClass, startDatePicker.getValue());
			}
			if(endDateChanged){
				System.out.println("---> End Date change - "+endDatePicker.getValue());
				WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
				editor.setNewEndDate(selectedClass, endDatePicker.getValue());
			}
			selectedClass.setStartDate(startDatePicker.getValue());
			selectedClass.setEndDate(endDatePicker.getValue());
			selectedClass.createEditBillingMonths();
			somethingWasEdited = true;
		}
		
		
		
		System.out.println("we have lessons #" + selectedClass.getSchedule().getwLessons().size());
		System.out.println("     new choice #" + choice.getSelectionModel().getSelectedIndex());
		// remove any lessons that were disabled
		int lastIndex = choice.getSelectionModel().getSelectedIndex();
		System.out.println("     from " + choice.getSelectionModel().getSelectedIndex()+" -> "+selectedClass.getSchedule().getwLessons().size());
		int initialSize = selectedClass.getSchedule().getwLessons().size();
		for(int i = lastIndex; i < initialSize; i++){
			System.out.println("remove item at "+ lastIndex);
			WeeklyLesson l = selectedClass.getSchedule().getwLessons().get(lastIndex);
			WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
			editor.removeWeeklyLesson(selectedClass, selectedClass.getSchedule().getwLessons().get(lastIndex));
			selectedClass.getSchedule().getwLessons().remove(lastIndex);
			somethingWasEdited = true;
			System.out.println("edit here 93");
		}
		
		// add lessons or Edit if existing
		for(int i = 0; i < choice.getSelectionModel().getSelectedIndex(); i++){
			if(selectedClass.getSchedule().getwLessons().size() <= i){
				// add new lessons
				somethingWasEdited = true;
				System.out.println("edit here 94");
				System.out.println("create lesson  "+i);
//
				WeeklyLesson wlesson = new WeeklyLesson();
				if(days.get(i).getSelectionModel().getSelectedIndex() > 0){
					wlesson.setDayOfTheWeek(days.get(i).getSelectionModel().getSelectedIndex());
				}
				if(!timePickers.get(i).isDisabled()){
					wlesson.setStartingHour(timePickers.get(i).getTimePicker().getLocalTime().getHour());
				}
				if(!timePickers.get(i).isDisabled()){
					wlesson.setStartingMinute(timePickers.get(i).getTimePicker().getLocalTime().getMinute());
				}
				wlesson.setDuration(Integer.parseInt(durations.get(i).getEditor().getText()));
				wlesson.setIndexNumber(i+1);
				selectedClass.getSchedule().getwLessons().add(wlesson);
				WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
				editor.addNewWeeklyLesson(selectedClass, wlesson, selectedClass.getStartDate(), selectedClass.getEndDate());
			}else{
				// edit existing lessons
				System.out.println("edit lesson  "+i);
				boolean weeklyLessonEdited = false;
				System.out.println((days.get(i).getSelectionModel().getSelectedIndex() +"!="+ selectedClass.getSchedule().getwLessons().get(i).getDayOfTheWeek()));
				
				if(days.get(i).getSelectionModel().getSelectedIndex() != selectedClass.getSchedule().getwLessons().get(i).getDayOfTheWeek()){
					somethingWasEdited = true;
					weeklyLessonEdited = true;
					System.out.println("edit here 95");
					selectedClass.getSchedule().getwLessons().get(i).setDayOfTheWeek(days.get(i).getSelectionModel().getSelectedIndex());
				}
				if(timePickers.get(i).getTimePicker().getLocalTime().getHour() != (selectedClass.getSchedule().getwLessons().get(i).getStartingHour())){
					somethingWasEdited = true;
					weeklyLessonEdited = true;
					System.out.println("edit here 96");
					selectedClass.getSchedule().getwLessons().get(i).setStartingHour(timePickers.get(i).getTimePicker().getLocalTime().getHour());
				}
				if(timePickers.get(i).getTimePicker().getLocalTime().getMinute() != (selectedClass.getSchedule().getwLessons().get(i).getStartingMinute())){
					somethingWasEdited = true;
					weeklyLessonEdited = true;
					System.out.println("edit here 97");
					selectedClass.getSchedule().getwLessons().get(i).setStartingMinute(timePickers.get(i).getTimePicker().getLocalTime().getMinute());
				}
				if(!((durations.get(i).getEditor().getText())).equals((Integer.toString(selectedClass.getSchedule().getwLessons().get(i).getDuration())))){
					somethingWasEdited = true;
					weeklyLessonEdited = true;
					System.out.println("edit here 98");
					try{
						int currentDuration = Integer.parseInt(durations.get(i).getEditor().getText());
						selectedClass.getSchedule().getwLessons().get(i).setDuration(currentDuration);
					}catch(Exception e){
						System.out.println("durations not valid integer");
					}
				}
				if(weeklyLessonEdited){
					WeeklyLessonsEditor editor = WeeklyLessonsEditor.getInstance();
					LocalDateTime toMove = LocalDateTime.now();
					if(selectedClass.getSchedule().getwLessons().get(i).isFullyScheduled()){
						System.out.println("Fully Scheduled");
						toMove = editor.getADate(selectedClass.getStartDate(),
								days.get(i).getSelectionModel().getSelectedIndex(),
								selectedClass.getSchedule().getwLessons().get(i).getStartingHour(),
								selectedClass.getSchedule().getwLessons().get(i).getStartingMinute());
					}
					System.out.println("New LocalDateTime = "+ toMove.toString());
					System.out.println("Is fully is  = "+selectedClass.getSchedule().getwLessons().get(i).isFullyScheduled());
					editor.moveWeeklyLessons(toMove,
							selectedClass.getSchedule().getAllWLessons(),
							selectedClass.getSchedule().getwLessons().get(i).getIndexNumber(),
							selectedClass.getSchedule().getwLessons().get(i).getDuration(),
							selectedClass.getSchedule().getwLessons().get(i).isFullyScheduled());
				}
			}
		}
		System.err.println(selectedClass.getMonthlyFee()+" | "+perMonth.getNumber());
		System.err.println(selectedClass.getHourlyFee()+" | "+perHour.getNumber());
		
		if(selectedClass.getHourlyFee().compareTo(perHour.getNumber()) != 0){
			selectedClass.setHourlyFee(perHour.numberProperty().getValue());
			selectedClass.setPaidPerMonth(false);
			System.out.println("Hourly fee edited");
			somethingWasEdited = true;
		}if(selectedClass.getMonthlyFee().compareTo(perMonth.getNumber()) != 0){
			selectedClass.setMonthlyFee(perMonth.numberProperty().getValue());
			somethingWasEdited = true;
			System.out.println("MOnthly fee edited");
			selectedClass.setPaidPerMonth(true);
		}
		
		// check teacher
		if(theTeacherOfTheClass != selectedClass.getTeacher()){
			selectedClass.setTeacher(theTeacherOfTheClass);
			somethingWasEdited = true;
			System.out.println("edit here 99");
		}
		return somethingWasEdited;
	}
	
	private void initDatePickers() {
		startDatePicker.valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
			if(endDatePicker.isDisable()){
				endDatePicker.setDisable(false);
			}
		});
		
		String pattern = "dd / MMMM / yyyy";
		StringConverter converter = new StringConverter<LocalDate>() {
			DateTimeFormatter dateFormatter =
					DateTimeFormatter.ofPattern(pattern);
			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateFormatter.format(date);
				} else {
					return "";
				}
			}
			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				} else {
					return null;
				}
			}
		};
		startDatePicker.setConverter(converter);
		endDatePicker.setConverter(converter);
		
		final Callback<DatePicker, DateCell> dayCellFactoryEnd =
				new Callback<DatePicker, DateCell>() {
					@Override
					public DateCell call(final DatePicker datePicker) {
						return new DateCell() {
							@Override
							public void updateItem(LocalDate item, boolean empty) {
								super.updateItem(item, empty);
								if(startDatePicker.getValue() != null){
									if (item.isBefore(startDatePicker.getValue()) || item.isAfter(startDatePicker.getValue().plusYears(1).minusDays(startDatePicker.getValue().getDayOfMonth()))) {
										setDisable(true);
										setStyle("-fx-background-color: #ffc0cb;");
									}
								}
							}
						};
					}
				};
		endDatePicker.setDayCellFactory(dayCellFactoryEnd);
	}
	@Override
	public void onLoad() {
		try {
			refresh();
			editTab.setDisable(true);
		} catch (IOException ex) {
			System.err.println("EXCEPTION - view classes -0002");
			ex.printStackTrace(System.err);
		}
	}
	@Override
	public void unload(){
		clearAll();
		classesTable.getItems().clear();
		if(observableClasses != null){
			observableClasses.clear();
		}
		SingleSelectionModel<Tab> selectionModel = mainTabPane.getSelectionModel();
		selectionModel.select(0);
		SingleSelectionModel<Tab> selectionModel1 = editTabPane.getSelectionModel();
		selectionModel1.select(0);
		searchBox.setText("");
		connectionsTabController.unload();
		
		
		
	}
}

