/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package controllers;

import DB.DBUpdateUtils;
import DB.objects.Aclass;
import DB.objects.Student;
import DB.objects.Teacher;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import services.GetAllClasses;
import services.GetAllTeachers.GetAllTeachersOfTheStudent;
import tables.ClassesTable;
import tables.StudentsTable;
import static utils.Utils.notNull;
import utils.dialogs.CustomDialogs;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class ConnectionsStudentController implements Initializable{
	
	@FXML private Button removeClassButton;
	@FXML private TableView<Aclass> allClassesTable;
	@FXML private ObservableList<Aclass> allClasses;
	@FXML private TableView<Aclass> classesTable;
	@FXML private TableView<Teacher> teachersTable;
	@FXML private VBox buttonsVBox;
	@FXML private ProgressIndicator getClassesProgress;
	@FXML private ToggleButton showTableButton;
	private Student selectedStudent;
	private boolean somethingWasEdited;
	@FXML private ScrollPane mainScrollPane;
	@FXML private VBox vBox;
	@FXML private HBox hiddenHbox;
	@FXML private GridPane searchGrid;
	@FXML private TextField filterByName;
	@FXML private TextField filterByDescription;
	
	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// bind width
		vBox.minWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
		vBox.maxWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
		
		ClassesTable.initializeClassesTable(classesTable);
		ClassesTable.initializeClassesTable(allClassesTable);
		StudentsTable.initializeStudentsTable(teachersTable);
		
		initializeSearchFields();
	}
	
	private void initializeSearchFields(){
		
		filterByName.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		filterByDescription.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		
	}
	
	public void initializeTables(Student selectedStu){
		
		this.selectedStudent = selectedStu;
		// all the classes the Teacher teaches
		if(selectedStudent.getClasses() != null){
			classesTable.setItems(FXCollections.observableArrayList(selectedStudent.getClasses()));
			if(classesTable.getItems().isEmpty()){
				removeClassButton.setDisable(true);
			}else{
				removeClassButton.setDisable(false);
			}
			// listener to disable delete button when list is empty
			classesTable.getItems().addListener((ListChangeListener.Change<? extends Aclass> c) -> {
				if(classesTable.getItems().isEmpty()){
					removeClassButton.setDisable(true);
				}else{
					if(removeClassButton.isDisable()){
						removeClassButton.setDisable(false);
					}
				}
			});
		}
		
		// all the students the Teacher teaches
		ObservableList<Student> allStudents = FXCollections.observableArrayList();
		
		GetAllTeachersOfTheStudent stuService = new GetAllTeachersOfTheStudent(selectedStudent);
		stuService.setOnSucceeded(workerStateEvent -> {
			teachersTable.setItems((ObservableList<Teacher>)stuService.getValue());
		});
		stuService.start();
	}
	
	@FXML
	private void addClass(Event event) throws IOException {
		if(allClassesTable.getSelectionModel().isEmpty()){
			CustomDialogs.infoDialog("", "Nothing is selected");
			return;
		}
		Aclass toBeMoved = allClassesTable.getSelectionModel().getSelectedItem();
		if(classesTable.getItems().contains(toBeMoved)){
			CustomDialogs.infoDialog("", "Teacher is already teaching this class");
			return;
		}
		classesTable.getItems().add(toBeMoved);
		if(selectedStudent != null){
			DBUpdateUtils dbConnection = new DBUpdateUtils();
			dbConnection.addStudentToClass(toBeMoved, selectedStudent);
		}
	}
	@FXML
	private void removeClass(Event event) throws IOException {
		if(classesTable.getSelectionModel().isEmpty()){
			CustomDialogs.infoDialog("", "Nothing is selected");
			return;
		}
		Aclass toBeMoved = classesTable.getSelectionModel().getSelectedItem();
		classesTable.getItems().remove(toBeMoved);
		if(selectedStudent != null){
			DBUpdateUtils dbConnection = new DBUpdateUtils();
			dbConnection.removeStudentFromClass(toBeMoved, selectedStudent);
		}
		classesTable.requestFocus();
	}
	
	@FXML
	private void showHiddenTable(Event event) throws IOException {
		playFadeTransitions();
		playSizeAnimation(!showTableButton.isSelected());
		GetAllClasses service  = new GetAllClasses();
		getClassesProgress.visibleProperty().bind(service.runningProperty());
		service.setOnSucceeded(workerStateEvent -> {
			buttonsVBox.setVisible(true);
			allClassesTable.setVisible(true);
			allClasses = (ObservableList<Aclass>)service.getValue();
			allClassesTable.setItems(allClasses);
		});
		service.start();
	}
	
//    @FXML
//    private void saveChanges(Event event) throws IOException {
//        showCustomDialog();
//        ResourceBundle labels = LanguageUtils.getInstance().getCurrentLanguage();
//
//        String response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.teacher.title"), labels.getString("dialog.edit.object"), labels.getString("dialog.yes"), labels.getString("dialog.no"));
//
//        if(null != response)switch (response) {
//            case CustomAction.YES:
//                saveTeacher(labels);
//            break;
//        }
//    }
	
//    private void saveTeacher(ResourceBundle labels){
//        UpdateStudentsClassesService service  = new UpdateStudentsClassesService(selectedStudent);
//        savingProgress.visibleProperty().bind(service.runningProperty());
//        service.setOnSucceeded(workerStateEvent -> {
//            if((boolean)service.getValue()){
//                CustomDialogs.infoDialog("", labels.getString("dialog.edit.success"));
//            }else{
//                CustomDialogs.errorDialog(labels.getString("error"), labels.getString("save.obj.changes"));
//            }
//        });
//        service.start();
//    }
	
	private void playFadeTransitions(){
		FadeTransition ftTable = new FadeTransition(Duration.millis(500), allClassesTable);
		FadeTransition ftVBox = new FadeTransition(Duration.millis(500), buttonsVBox);
		if(showTableButton.isSelected()){
			ftTable.setFromValue(0.0);
			ftTable.setToValue(1.0);
			ftVBox.setFromValue(0.0);
			ftVBox.setToValue(1.0);
		}else{
			ftTable.setFromValue(1.0);
			ftTable.setToValue(0.0);
			ftVBox.setFromValue(1.0);
			ftVBox.setToValue(0.0);
		}
		ftTable.play();
		ftVBox.play();
		
	}
	private void playSizeAnimation(boolean hide){
		hiddenHbox.setVisible(!hide);
		
		Timeline timeline = new Timeline();
		timeline.setCycleCount(1);
		timeline.setAutoReverse(false);
		KeyValue kv;
		if(hide){
			kv = new KeyValue(hiddenHbox.prefHeightProperty(), -196, Interpolator.EASE_BOTH);
		}
		else{
			kv = new KeyValue(hiddenHbox.prefHeightProperty(), 196, Interpolator.EASE_BOTH);
		}
		final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);
		timeline.getKeyFrames().add(kf);
		timeline.play();
	}
	
	
	private void searchForAll(String oldVal, String newVal) {
		// if text is shorter than before
		// set items to ALL so we can re-filter them
//        if ((oldVal != null && (newVal.length() < oldVal.length()))) {
		allClassesTable.setItems(allClasses);
//        }
		
		// keywords to search for
		String searchKeyName = filterByName.getText().toUpperCase();
		String searchKeyDescr = filterByDescription.getText().toUpperCase();
		
		ObservableList<Aclass> searchResults = FXCollections.observableArrayList();
		
		for ( Aclass theClass : allClassesTable.getItems() ) {
			// keywords to match
			String searchTokenName = theClass.getName().toUpperCase();
			String searchTokenDescr = notNull(theClass.getDescription()).toUpperCase();
			
			// if it matches add it to search results
			if ( searchTokenName.contains(searchKeyName) &&  searchTokenDescr.contains(searchKeyDescr)) {
				searchResults.add(theClass);
			}
		}
		allClassesTable.setItems(searchResults);
	}
	
	public void unload(){
		filterByName.setText("");
		filterByDescription.setText("");
		allClassesTable.getItems().clear();
		allClasses = null;
		showTableButton.setSelected(false);
		hiddenHbox.setPrefHeight(0);
		hiddenHbox.setVisible(false);
		allClassesTable.setVisible(false);
		buttonsVBox.setVisible(false);
		
	}
}