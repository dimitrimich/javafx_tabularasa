/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import utils.LanguageUtils;
import utils.menu.ScreenNavigator;
import utils.menu.TreeMenuBuilder;

/**
 *
 * @author d.michaelides
 */
public class HomePageController extends StageController{
	private final URL fxmlURL;
	private final ResourceBundle resources;
	@FXML private AnchorPane rightPane;
	@FXML private AnchorPane leftPane;
	@FXML private BorderPane bo;
	@FXML private ProgressBar pro;
	
	public HomePageController(URL fxml, ResourceBundle res) {
		super();
		labels = res;
		fxmlURL = fxml;
		resources = res;
	}
	@FXML private BorderPane region;
	@FXML private ProgressIndicator progress;
	@FXML private SplitPane splitPane;
	@FXML private TreeView treeMenu;
	
	
	private AnchorPane root2;
	
	@FXML
	private void handleButtonAction(ActionEvent event) throws IOException {
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		TreeMenuBuilder.buildTheMenu();
		treeMenu.setRoot(TreeMenuBuilder.getTheMenuRoot());
		setHandlers(treeMenu);
		
		
		System.out.println("go through all screens");
//
		try{
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("CreateClass").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("CreateTeacher").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("CreateStudent").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("PersonsTable_button").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("TimeTable").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("TimeTableNew").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("ViewClasses").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("ViewStudents").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("ViewTeachers").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("Settings").getScroll());
//			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("BillingStudents").getScroll());
			rightPane.getChildren().clear();
			rightPane.getChildren().add(ScreenNavigator.getControllersMap().get("Home").getScroll());
		}catch(Exception e){
			e.printStackTrace(System.err);
		}
		
		
		leftPane.hoverProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
			if(oldVal == false && newVal == true){
				splitPane.getDividers().get(0).setPosition(0.20);
			}
		});
		rightPane.hoverProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
			if(oldVal == false && newVal == true){
				splitPane.getDividers().get(0).setPosition(0.05);
			}
		});
		Locale.setDefault(LanguageUtils.getInstance().getCurrentLanguage().getLocale());
	}
	
	@Override
	protected void changeLocale(){
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		if(rightPane == null)
			return;
	}
	
	
	@FXML
	private void closeWindow(Event event) throws IOException {
		stage.close();
//        Iterator <Window> it = Window.impl_getWindows();
//        while (it.hasNext()){
//            it.next().hide();
//        }
	}
	
	public void goToScreen(String screenName) throws IOException{
		System.out.println("home -> "+ screenName);
//        ObservableList<Node> currentChildren = rightPane.getChildren();
//        for(Node n : currentChildren){
//            n.setVisible(false);
//        }
		rightPane.getChildren().clear();
		
		rightPane.getChildren().add(ScreenNavigator.getControllersMap().get(screenName).getScroll());
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	public static void setHandlers(final TreeView treeMenu){
		treeMenu.setOnMouseClicked((MouseEvent mouseEvent) -> {
			try{
				if(mouseEvent.getButton() == MouseButton.PRIMARY){
					if(mouseEvent.getClickCount() == 1) {
						TreeItem<String> item = (TreeItem<String>) treeMenu.getSelectionModel().getSelectedItem();
						if(item != null){
							if(item.getValue().equals(LanguageUtils.getString("menu.create"))){
								String parent = item.getParent().getValue();
								if(LanguageUtils.getString("menu.students").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("CreateStudent");
								}else if(LanguageUtils.getString("menu.teachers").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("CreateTeacher");
								}else if(LanguageUtils.getString("menu.classes").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("CreateClass");
								}
							}
							else if(item.getValue().equals(LanguageUtils.getString("menu.view.all"))){
								String parent = item.getParent().getValue();
								if(LanguageUtils.getString("menu.students").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("ViewStudents");
								}else if(LanguageUtils.getString("menu.teachers").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("ViewTeachers");
								}else if(LanguageUtils.getString("menu.classes").equals(parent)){
									ScreenNavigator.getInstance(null).goToScreen("ViewClasses");
								}
							}
							else if(item.getValue().equals(LanguageUtils.getString("menu.home"))){
								ScreenNavigator.getInstance(null).goToScreen("Home");
							}
							else if(item.getValue().equals(LanguageUtils.getString("menu.settings"))){
								ScreenNavigator.getInstance(null).goToScreen("Settings");
							}
							else if(item.getValue().equals("Temp")){
								ScreenNavigator.getInstance(null).goToScreen("Temp");
							}
							else if(item.getParent().getValue().equals(LanguageUtils.getString("menu.billings"))){
								String value = item.getValue();
								if(value.equals(LanguageUtils.getString("menu.billings.new"))){
									ScreenNavigator.getInstance(null).goToScreen("BillingEntry");
								}else if (value.equals(LanguageUtils.getString("menu.billings.cancel"))){
									ScreenNavigator.getInstance(null).goToScreen("BillingEditPayment");
								}
							}
//							else if(item.getParent().getValue().equals("Other")){
//								System.out.println("other pressed");
//								switch(item.getValue()){
//									case "Billing" : ScreenNavigator.getInstance(null).goToScreen("BillingInfo"); break;
//									case "Billing Students" : ScreenNavigator.getInstance(null).goToScreen("BillingStudents"); break;
//									case "Table" : ScreenNavigator.getInstance(null).goToScreen("PersonsTable_button"); break;
//									case "Time Table" : ScreenNavigator.getInstance(null).goToScreen("TimeTable"); break;
//									case "Time Table New" : ScreenNavigator.getInstance(null).goToScreen("TimeTableNew"); break;
////                                    case "Classes"  :  break;
//									default:break;
//								}
//							}
						}
					}else{
					}
				}
			}catch(IOException e){
				System.err.println("exception in menu");
				e.printStackTrace(System.err);
			}
		});
		
	}
	
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
	}
}
