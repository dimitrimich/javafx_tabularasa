/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.BillingPersister;
import DB.objects.PaymentDetails;
import static controllers.StageController.cachingClassLoader;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import utils.LanguageUtils;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import tables.PaymentsTreeTableUtils;
import tables.PaymentsTreeTableUtils.PaymentFX;
import tables.PaymentsTreeTableUtils.Root;
import tables.PaymentsTreeTableUtils.SetTreeTableItemsService;
import utils.Utils;
import utils.dialogs.CustomDialogs;

/**
 *
 * @author dimitris
 */
public class BillingEditPaymentController extends StageController{
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public BillingEditPaymentController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	@Override
	protected void changeLocale() {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		pageTitle.setText(LanguageUtils.getString("billing.edit.title"));
		searchTitle.setText(LanguageUtils.getString("billing.edit.subtitle"));
		radioPaymentBy.setText(LanguageUtils.getString("billing.edit.by"));
		radioDate.setText(LanguageUtils.getString("billing.edit.date"));
		radioReceipt.setText(LanguageUtils.getString("billing.edit.no"));
		radioPaymentFor.setText(LanguageUtils.getString("billing.edit.for"));
	}
	
	
	/****************************************************************************************************************************/
	
	private final ToggleGroup toggleGroup = new ToggleGroup();
	/****************************************************************************************************************************/
	/****************************************************************************************************************************/
	
	@FXML private ScrollPane theScroll;
	@FXML private BorderPane mainBorderPane;
	@FXML private GridPane titleGrid;
	@FXML private GridPane searchGrid;
	@FXML private RadioButton radioDate;
	@FXML private RadioButton radioReceipt;
	@FXML private RadioButton radioPaymentFor;
	@FXML private RadioButton radioPaymentBy;
	@FXML private DatePicker datePicker;
	@FXML private TextField receiptNoText;
	@FXML private Button searchBtn;
	@FXML private Button deleteBtn;
	@FXML private TreeTableView paymentsTable;
	@FXML private ProgressIndicator progressIndicator;
	@FXML private Label pageTitle, searchTitle;
	
	private RadioButton hiddenToggle = new RadioButton();
	private List<PaymentDetails> allPaymentsReturned = new ArrayList<>();
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		mainBorderPane.prefWidthProperty().bind(theScroll.widthProperty().subtract(10));
		radioDate.setToggleGroup(toggleGroup);
		radioReceipt.setToggleGroup(toggleGroup);
		hiddenToggle.setToggleGroup(toggleGroup);
		radioPaymentBy.setToggleGroup(toggleGroup);
		radioPaymentFor.setToggleGroup(toggleGroup);
		hiddenToggle.setVisible(false);
		searchBtn.setDisable(true);
		
		datePicker.setVisible(false);
		receiptNoText.setVisible(false);
		toggleGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
			if(newValue.equals(radioReceipt) || newValue.equals(radioPaymentBy) || newValue.equals(radioPaymentFor)){
				receiptNoText.setVisible(true);
				receiptNoText.requestFocus();
				receiptNoText.setText("");
				datePicker.setVisible(false);
				datePicker.setValue(null);
			}else if (newValue.equals(radioDate)){
				receiptNoText.setVisible(false);
				datePicker.requestFocus();
				datePicker.setVisible(true);
				receiptNoText.setText("");
			}
		});
		datePicker.valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
			if(newValue == null){
				searchBtn.setDisable(true);
			}else{
				searchBtn.setDisable(false);
			}
		});
		receiptNoText.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			if(!Utils.isValidParam(newValue)){
				searchBtn.setDisable(true);
			}else{
				searchBtn.setDisable(false);
			}
		});
		
		PaymentsTreeTableUtils.initPaymentsTreeTable(paymentsTable);
		
		
		TreeTableView.TreeTableViewSelectionModel selectionModel = paymentsTable.getSelectionModel();
		selectionModel.selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
			if( newValue == null ){
				deleteBtn.setDisable(true);
				return;
			}
			
			TreeItem item = (TreeItem)selectionModel.getSelectedItem();
			if(item.getValue() instanceof Root){
				deleteBtn.setDisable(true);
				return;
			}
			deleteBtn.setDisable(false);
		});
	}
	
	
	@FXML
	private void editPayment(Event event) throws IOException {
		TreeTableView.TreeTableViewSelectionModel selectionModel = paymentsTable.getSelectionModel();
		if(selectionModel.getSelectedItem() == null) {
			CustomDialogs.infoDialog(LanguageUtils.getString("billing.edit.notSelected.title"), LanguageUtils.getString("billing.edit.notSelected.text"));
			return;
		}
		TreeItem item = (TreeItem)selectionModel.getSelectedItem();
		if(item.getValue() instanceof PaymentFX){
			
			PaymentFX selectedPayment = (PaymentFX)item.getValue();
			String selectedReceipt = selectedPayment.getReceipt();
			if(selectedReceipt == null || selectedReceipt.isEmpty()){
				if(item.getParent().getValue() instanceof PaymentFX){
					System.out.println(" parent is PaymentFX");
					selectedReceipt = ((PaymentFX)(item.getParent().getValue())).getReceipt();
				}
				else{
					System.err.println("----222---");
				}
			}
			
			String yes = LanguageUtils.getString("dialog.yes");
			String no = LanguageUtils.getString("dialog.no");
			Optional<ButtonType> response = CustomDialogs.yesNoDialog(LanguageUtils.getString("billing.edit.remove.title"), LanguageUtils.getString("billing.edit.remove.text")+ selectedReceipt, yes, no);
			
			if(response.isPresent()){
				if(response.get().getText().equals(yes)){
					List<PaymentDetails> paymentsToZero = new ArrayList<>();
					for(PaymentDetails payment :allPaymentsReturned){
						if(selectedReceipt.equals(payment.getReceiptNo())){
							System.out.println("adding payment ");
							paymentsToZero.add(payment);
						}
					}
					if(!paymentsToZero.isEmpty()){
						RemovePaymentsService service = new RemovePaymentsService(paymentsToZero);
						service.setOnFailed((WorkerStateEvent event1) -> {
							System.err.println("Service to zero payments Failed");
						});
						System.out.println("starting service....");
						service.setOnSucceeded((WorkerStateEvent successEvent) -> {
							if(!service.getValue()){
								CustomDialogs.errorDialog(LanguageUtils.getString("billing.edit.cancel.title"), LanguageUtils.getString("billing.edit.cancel.text"));
							}else{
								CustomDialogs.infoDialog(LanguageUtils.getString("billing.edit.success.title"), LanguageUtils.getString("billing.edit.success.text"));
								try {
									search(null);
								} catch (IOException ex) {
									Logger.getLogger(BillingEditPaymentController.class.getName()).log(Level.SEVERE, null, ex);
								}
							}
						});
						service.restart();
					}
				}
			}
		}
	}
	
	
	
	@FXML
	private void search(Event event) throws IOException {
		SEARCH_CRITERIA currentCriteria = SEARCH_CRITERIA.date;
		if(radioDate.isSelected()){
			//search by date
			System.out.println("search by date");
			currentCriteria = SEARCH_CRITERIA.date;
		}else if (radioReceipt.isSelected()){
			// search by receipt No.
			System.out.println("search by receipt");
			currentCriteria = SEARCH_CRITERIA.receipt;
		}
		else if (radioPaymentBy.isSelected()){
			currentCriteria = SEARCH_CRITERIA.paidBy;
		}else if (radioPaymentFor.isSelected()){
			currentCriteria = SEARCH_CRITERIA.paidFor;
		}
		
		GetPaymentsService service = new GetPaymentsService(datePicker.getValue(), receiptNoText.getText(), currentCriteria);
		
		paymentsTable.setDisable(true);
		if(paymentsTable.getRoot() != null && paymentsTable.getRoot().getChildren() != null){
			paymentsTable.getRoot().getChildren().clear();
		}
		
		progressIndicator.visibleProperty().bind(service.runningProperty());
		service.setOnSucceeded((WorkerStateEvent workerStateEvent) -> {
			allPaymentsReturned = service.getValue();   //here you get the return value of your service
			
			if(allPaymentsReturned.size() > 0){
				System.out.println("payments "+ allPaymentsReturned.size());
				
				paymentsTable.setDisable(true);
				SetTreeTableItemsService treeTableservice = new SetTreeTableItemsService(allPaymentsReturned, paymentsTable);
				treeTableservice.setOnSucceeded((WorkerStateEvent serviceDoneEvent) -> {
					paymentsTable.setDisable(false);
					List<TreeItem<PaymentFX>>paymentsFX = treeTableservice.getValue();
					paymentsTable.getRoot().getChildren().addAll(paymentsFX);
				});
				treeTableservice.setOnFailed((WorkerStateEvent failEvent) -> {
					System.err.println("treeTableservice FAILED....");
				});
				treeTableservice.restart(); //here you start your service
			}else if (allPaymentsReturned.isEmpty()){
				String searchCriteria = "";
				if(radioDate.isSelected()){
					searchCriteria = LanguageUtils.getString("billing.edit.criteria.date") + datePicker.getValue().format(DateTimeFormatter.ofPattern("dd/MMM/uuuu", LanguageUtils.getCurrentLocale()));
				}else if (radioReceipt.isSelected()){
					searchCriteria = LanguageUtils.getString("billing.edit.criteria.receipt") + receiptNoText.getText();
				}
				else if (radioPaymentBy.isSelected()){
					searchCriteria = receiptNoText.getText();
				}else if (radioPaymentFor.isSelected()){
					searchCriteria = receiptNoText.getText();
				}
				CustomDialogs.infoDialog(LanguageUtils.getString("billing.edit.payments"), LanguageUtils.getString("billing.edit.payments.text")+searchCriteria);
			}
		});
		
		service.setOnFailed((WorkerStateEvent workerStateEvent) -> {
			//DO stuff on failed
			
			System.err.println("Service FAILED....");
		});
		System.out.println("Service starting....");
		service.restart(); //here you start your service
	}
	
	public enum SEARCH_CRITERIA {date, receipt, paidBy, paidFor;};
	
	public class GetPaymentsService extends Service<List<PaymentDetails>>{
		public GetPaymentsService(LocalDate date, String receipt, SEARCH_CRITERIA criterion){
			receiptNo = receipt;
			payDay = date;
			this.criteria = criterion;
		}
		private String receiptNo;
		private LocalDate payDay;
		private SEARCH_CRITERIA criteria;
		
		
		@Override
		protected Task<List<PaymentDetails>> createTask() {
			GetPaymentsTask theTask = new GetPaymentsTask();
			return theTask;
		}
		
		private class GetPaymentsTask extends Task <List<PaymentDetails>>{
			@Override
			protected List<PaymentDetails> call() throws Exception {
				return BillingPersister.getPayments(payDay, receiptNo, criteria);
				
			}
		}
	}
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
		paymentsTable.setDisable(true);
		if(paymentsTable.getRoot() != null && paymentsTable.getRoot().getChildren() != null){
			paymentsTable.getRoot().getChildren().clear();
		}
		toggleGroup.selectToggle(hiddenToggle);
		datePicker.setValue(null);
		receiptNoText.setText("");
		datePicker.setVisible(false);
		receiptNoText.setVisible(false);
		searchBtn.setDisable(true);
		allPaymentsReturned.clear();
	}
	
	
	public class RemovePaymentsService extends Service<Boolean>{
		public RemovePaymentsService(List<PaymentDetails> paymentsToZer){
			paymentsToZero = paymentsToZer;
		}
		private List<PaymentDetails> paymentsToZero;
		
		
		@Override
		protected Task<Boolean> createTask() {
			RemovePaymentsTask theTask = new RemovePaymentsTask();
			return theTask;
		}
		
		private class RemovePaymentsTask extends Task <Boolean>{
			@Override
			protected Boolean call() throws Exception {
				return BillingPersister.makePaymentsZero(paymentsToZero);
			}
		}
	}
	
	
}
