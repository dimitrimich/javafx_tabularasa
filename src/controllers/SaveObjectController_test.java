/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class SaveObjectController_test implements Initializable {
    @FXML ImageView image;
    @FXML TextArea message;
    @FXML HBox buttons;
    @FXML Button button_left;
    @FXML Button button_center;
    @FXML Button button_right;
    
    int returnValue = 0;
    public int getReturnValue(){
        return returnValue;
    }
    
    
    
    public void init(Image toShow, String text, String [] labels) throws Exception{
        button_left.setVisible(false);
        button_center.setVisible(false);
        button_right.setVisible(false);
        if(Utils.isValidParam(labels[0])){
            button_left.setVisible(true);
            button_left.setText(labels[0]);
        }
        if(Utils.isValidParam(labels[1])){
            button_center.setVisible(true);
            button_center.setText(labels[1]);
        }
        if(Utils.isValidParam(labels[2])){
            button_right.setVisible(true);
            button_right.setText(labels[2]);
        }
        if(!button_left.isVisible() && !button_center.isVisible() && !button_right.isVisible()){
               throw new Exception("Provide at least 1 label");
        }
        
        image.setImage(toShow);
        message.setText(text);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    private void leftButtonPressed(Event event) throws IOException {
        returnValue = 1;
        returnToMainScreen(event);
    }
    
    @FXML
    private void centerButtonPressed(Event event) throws IOException {
        returnValue = 2;
        returnToMainScreen(event);
    }
    
    @FXML
    private void rightButtonPressed(Event event) throws IOException {
        returnValue = 3;
        returnToMainScreen(event);
    }
    
    private void returnToMainScreen(Event event) throws IOException {
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
}
