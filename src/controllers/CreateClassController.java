/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DButils;
import DB.objects.Aclass;
import DB.objects.Schedule;
import DB.objects.Teacher;
import DB.objects.WeeklyLesson;
import customControls.CustomTimePicker;
import customControls.NumberTextField;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import screens.FXMLs;
import utils.LanguageUtils;
import utils.Utils;
import utils.dialogs.CustomDialogs;

/**
 *
 * @author d.michaelides
 */
public class CreateClassController extends StageController{
    private final URL fxmlURL;
    private final ResourceBundle resources;
    
    public CreateClassController(URL fxml, ResourceBundle res) {
        super();
        labels = res;
        fxmlURL = fxml;
        resources = res;
    }
    
    @FXML private Button saveButton;
    @FXML private Button cancelButton;
    @FXML private Button clearTeacher;
    
    @FXML private GridPane timeTable;
    @FXML private AnchorPane root;
    @FXML private VBox theV;
    @FXML private ScrollPane theScroll;
    @FXML private ProgressIndicator progress;
    
    @FXML private Label pageTitle;
    @FXML private Label b_info;
    @FXML private Label name;
    @FXML private Label description;
    @FXML private TextField nameText;
    @FXML private TextField classCodeText;
    @FXML private TextArea descriptionText;
    
    @FXML private Label teacherTitle;
    @FXML private Label teacherSelect;
    @FXML private Label timeTableTitle;
    @FXML private Label timesPW;
    @FXML private Label dayTitle;
    @FXML private Label hourTitle;
    @FXML private Label durationTitle;
    @FXML private Label selectedTeacher;
    @FXML private Label selectedTeacherName;
    @FXML private Label classCodeLbl;
    
    @FXML private ChoiceBox<String> choice;
    @FXML private Label datesTitle;
    @FXML private DatePicker startDatePicker;
    @FXML private DatePicker endDatePicker;
    @FXML private RadioButton perMonthRadio;
    @FXML private RadioButton perHourRadio;
    @FXML private GridPane billingGrid;
    private final NumberTextField perHour = new NumberTextField(BigDecimal.valueOf(12000));
    private final NumberTextField perMonth = new NumberTextField(BigDecimal.valueOf(12000));
    
    
    private ArrayList<ChoiceBox<String>> days = new ArrayList<>(11);
    private ArrayList<ComboBox<String>> durations = new ArrayList<>(11);
    private ArrayList<CustomTimePicker> timePickers = new ArrayList<>(11);
    
    private Teacher theTeacherOfTheClass;
    
    
    
    private final ArrayList<String> choose_times = new ArrayList<>(
            Arrays.asList("-------",
                    "1", "2", "3", "4", "5", "6",
                    "7", "8",  "9",  "10", "11"));
    
    private final ArrayList<String> common_hours_24 = new ArrayList<>(
            Arrays.asList("",
                    "13",
                    "14", "15", "16", "17", "18",
                    "19", "20",  "21",  "22"));
    
    private final ArrayList<String> common_minutes = new ArrayList<>(
            Arrays.asList("", "00", "15", "20",
                    "30", "40", "45"));
    
    private ArrayList<String> getDays_strings(){
        return new ArrayList<>(
                Arrays.asList("-------",
                        labels.getString("day.su"),labels.getString("day.mo"),labels.getString("day.tu"), labels.getString("day.we"),
                        labels.getString("day.th"), labels.getString("day.fr"), labels.getString("day.sa")));
    }
    
    private final ArrayList<String> common_durations = new ArrayList<>(
            Arrays.asList("",
                    "30","45", "60",
                    "75", "90", "120"));
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("init create class");
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.prefWidthProperty().bind(theScroll.widthProperty());
        
        choice.getItems().clear();
        choice.getItems().addAll(choose_times);
        choice.getSelectionModel().selectFirst();
        
        for(int i = 0 ; i< 11 ; i++){
            ChoiceBox <String> cb = new ChoiceBox();
            cb.setDisable(true);
            cb.setPrefWidth(150);
            timeTable.add(cb, 0, i+3, 1, 1);
            days.add(cb);
            
            ComboBox<String> dur = new ComboBox<>();
            dur.setEditable(true);
            dur.setDisable(true);
            timeTable.add(dur, 2, i+3, 1, 1);
            durations.add(dur);
            
            CustomTimePicker ctp = new CustomTimePicker(cb, dur);
            timeTable.add(ctp, 1, i+3, 1, 1);
            timePickers.add(ctp);
            GridPane.setVgrow(ctp, Priority.NEVER);
            
        }
        
        EventHandler<KeyEvent> handler = utils.EventFilters.getOnlyNumbersFilter();
        for(int i = 0; i < 11; i++){
            
            // duration listener (minutes)
            ChangeListener<String> durationsListener = utils.EventFilters.getHoursListener(durations.get(i), 3, 240);
            durations.get(i).getEditor().textProperty().addListener(durationsListener);
            
            durations.get(i).getItems().clear();
            durations.get(i).getItems().addAll(common_durations);
            durations.get(i).getSelectionModel().selectFirst();
            days.get(i).getItems().clear();
            days.get(i).getItems().addAll(getDays_strings());
            days.get(i).getSelectionModel().selectFirst();
        }
        
        choice.valueProperty().addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observable,
                            Object oldVal, Object newVal) {
                        int newValue_int;
                        try{
                            newValue_int = Integer.parseInt((String) newVal);
                        }catch(NumberFormatException e){
                            newValue_int = 0;
                        }
                        int oldValue_int;
                        try{
                            oldValue_int = Integer.parseInt((String) oldVal);
                        }catch(NumberFormatException e){
                            oldValue_int = 0;
                        }
                        
                        dayTitle.setDisable(newValue_int == 0);
                        hourTitle.setDisable(newValue_int == 0);
                        durationTitle.setDisable(newValue_int == 0);
                        
                        if( newValue_int >= oldValue_int){
                            for(int i = oldValue_int; i < newValue_int; i++){
//                        days.get(i).setDisable(false);
                                durations.get(i).setDisable(false);
                            }
                        }else{
                            if(oldValue_int == 11){
                                oldValue_int = 10;
                            }
                            
                            for(int i = oldValue_int; i >=  newValue_int; i--){
                                days.get(i).setDisable(true);
                                durations.get(i).setDisable(true);
                                days.get(i).getSelectionModel().select(0);
                                durations.get(i).getEditor().setText("");
                            }
                        }
                    }
                });
        initDatePickers();
        
        final ToggleGroup group = new ToggleGroup();
        perMonthRadio.setToggleGroup(group);
        perHourRadio.setToggleGroup(group);
        billingGrid.add(perHour, 2, 1);
        billingGrid.add(perMonth, 4, 1);
        perHour.numberProperty().set(BigDecimal.ZERO);
        perMonth.numberProperty().set(BigDecimal.ZERO);
        perHourRadio.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if(newValue){
                perHour.setDisable(false);
                perMonth.setDisable(true);
                perMonth.setText("");
            }else{
                perHour.setDisable(true);
                perHour.setText("");
                perMonth.setDisable(false);
            }
        });
        perMonthRadio.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if(newValue){
                perMonth.setDisable(false);
                perHour.setDisable(true);
                perHour.setText("");
            }else{
                perMonth.setDisable(true);
                perHour.setDisable(false);
                perMonth.setText("");
            }
        });
        perHourRadio.setSelected(true);
    }
    /**
     * Set the text to all visible elements of the stage
     * plus change flag
     */
    
    protected void changeLocale(){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        if(teacherSelect == null)
            return;
        datesTitle.setText(labels.getString("create.class.dates"));
        startDatePicker.setPromptText(labels.getString("create.class.dates.start"));
        endDatePicker.setPromptText(labels.getString("create.class.dates.end"));
        teacherSelect.setText(labels.getString("create.class.teacher_title"));
        selectedTeacher.setText(labels.getString("create.class.teacher_selected"));
        teacherTitle.setText(labels.getString("create.class.teacher_info"));
        pageTitle.setText(labels.getString("create.class.title"));
        b_info.setText(labels.getString("create.class.b_info"));
        name.setText(labels.getString("create.class.name"));
        description.setText(labels.getString("create.class.description"));
        timeTableTitle.setText(labels.getString("create.class.table"));
        timesPW.setText(labels.getString("create.class.per_week"));
        dayTitle.setText(labels.getString("create.class.s_day"));
        hourTitle.setText(labels.getString("create.class.s_time"));
        durationTitle.setText(labels.getString("create.class.s_duration"));
        saveButton.setText(labels.getString("create.class.save"));
        cancelButton.setText(labels.getString("create.class.cancel"));
        classCodeLbl.setText(labels.getString("create.class.codeName"));
    }
    
    
    @FXML
    private void changeLanguage(Event event) throws IOException {
    }
    
    @FXML
    private void addTeacher(Event event) throws IOException {
        if(stage == null){
            Node source = (Node)event.getSource();
            stage = (Stage)(source.getScene().getWindow());
        }
        Stage selectTeacherStage = new Stage();
        final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("SelectTeacher.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
        final Scene selectTeacherScene = new Scene((AnchorPane)myLoader.load());
        
        stage.setOnCloseRequest((WindowEvent ev) -> {
            System.out.println(" firing close event");
            selectTeacherScene.getWindow().fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        });
        
        final SelectTeacherController controler = ((SelectTeacherController) myLoader.getController());
        selectTeacherStage.setOnShown((WindowEvent event1) -> {
            controler.refreshItems();
        });
        controler.init(stage);
//        controler.setStage(stage);
        
        selectTeacherStage.setScene(selectTeacherScene);
//        stage.toBack();
        selectTeacherStage.toFront();
        stage.setOpacity(0.8);
        root.mouseTransparentProperty().bind(selectTeacherScene.getWindow().showingProperty());
        root.mouseTransparentProperty().bind(selectTeacherScene.getWindow().showingProperty());
        selectTeacherScene.getWindow().setOnCloseRequest((WindowEvent ev) -> {
            stage.setOpacity(1);
            theTeacherOfTheClass = controler.getSelectedTeacher();
            if(theTeacherOfTheClass != null){
                clearTeacher.setDisable(false);
                selectedTeacher.setDisable(false);
                StringBuilder sb = new StringBuilder();
                sb.append(theTeacherOfTheClass.getBasicInfo().getFname()).append(" ").append(theTeacherOfTheClass.getBasicInfo().getLname());
                selectedTeacherName.setText(sb.toString());
            }
        });
        selectTeacherStage.show();
    }
    @FXML
    private void clearTeacher(Event event) throws IOException {
        selectedTeacher.setDisable(true);
        clearTeacher.setDisable(true);
        selectedTeacherName.setText("");
        theTeacherOfTheClass = null;
    }
    
    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader);
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
    
    
    private void showMessage(boolean success) throws IOException {
        Optional<ButtonType> result;
        String clear = labels.getString("save.obj.clear");
        String returnButton = labels.getString("save.obj.return");
        String retry = labels.getString("save.obj.retry");
        
        if(success){
            String message = labels.getString("save.obj.class")+"\n"
                    + labels.getString("save.obj.success.line1") +"\n"
                    + labels.getString("save.obj.line2");
            result = CustomDialogs.clearCloseDialog("", message, clear,returnButton );
        }else{
            String message = labels.getString("save.obj.class")+"\n"
                    + labels.getString("save.obj.fail.line1") +"\n"
                    + labels.getString("save.obj.line2");
            result = CustomDialogs.retryClearCloseDialog("", message, retry, clear, returnButton);
            
        }
        if(result.isPresent()){
            if(result.get().getText().equals(clear)){
                clearAll();
            }else if(result.get().getText().equals(retry)){
                saveTheClass(null);
            }
        }
    }
    
    private void clearAll(){
        nameText.setText("");
        descriptionText.setText("");
        clearTeacher.setDisable(true);
        selectedTeacher.setDisable(true);
        selectedTeacherName.setText("");
        selectedTeacherName.setDisable(true);
        choice.getSelectionModel().select(0);
        endDatePicker.setValue(null);
        endDatePicker.setDisable(true);
        startDatePicker.setValue(null);
        perHour.setText("");
        perMonth.setText("");
        classCodeText.setText("");
        
        for(int i = 0; i < 11; i++){
            durations.get(i).getSelectionModel().selectFirst();
            durations.get(i).getEditor().setText("");
            timePickers.get(i).getTimePicker().setLocalTime(LocalTime.of(12, 00));
            timePickers.get(i).getTimePicker().setDisable(true);
            timePickers.get(i).getTimePicker().setOpacity(0.8);
            days.get(i).getSelectionModel().selectFirst();
        }
    }
    
    @FXML
    private void saveTheClass(Event event){
        if(stage == null){
            Node source = (Node)event.getSource();
            stage = (Stage)(source.getScene().getWindow());
        }
        try {
            showConfirmDialog();
        } catch (IOException ex) {
            System.err.println("Error while showing confirmation dialog -Class-");
            ex.printStackTrace(System.err);
        }
    }
    
    private void showConfirmDialog() throws IOException {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        String yes = labels.getString("dialog.yes");
        Optional<ButtonType> response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.class.title"), labels.getString("dialog.save.class"), yes, labels.getString("dialog.no"));
        if(response.isPresent()){
            if (response.get().getText().equals(yes)){
                verifyInput();
            }
        }
    }
    
    private void verifyInput(){
        boolean thereIsError = false;
        if(!Utils.isValidParam(nameText.getText().trim())){
            Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
            thereIsError = true;
        }
        int howManyClasses = choice.getSelectionModel().getSelectedIndex();
        System.out.println("Classses selected are = "+ howManyClasses);
        if(howManyClasses > 0){
            for(int i = (0); i < howManyClasses; i++){
                if(!Utils.isValidParam(durations.get(i).getEditor().getText())){
                    Utils.turnNodeRed(durations.get(i), null);
                    thereIsError = true;
                }
            }
        }
        
        if(thereIsError){
            System.out.println("There Is error");
        }else{
            createObjectAndSaveIt();
        }
    }
    
    
    private void createObjectAndSaveIt(){
        SaveClassService saveClass = new SaveClassService();
        progress.visibleProperty().bind(saveClass.runningProperty());
        saveButton.disableProperty().bind(saveClass.runningProperty());
        cancelButton.disableProperty().bind(saveClass.runningProperty());
        System.out.println("starting service");
        saveClass.start();
        progress.requestFocus();
    }
    
    private void initDatePickers() {
        startDatePicker.valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            if(endDatePicker.isDisable()){
                endDatePicker.setDisable(false);
            }
        });
        
        String pattern = "dd / MMMM / yyyy";
        StringConverter converter = new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter =
                    DateTimeFormatter.ofPattern(pattern);
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        };
        startDatePicker.setConverter(converter);
        
        final Callback<DatePicker, DateCell> dayCellFactoryEnd =
                new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                if(startDatePicker.getValue() != null){
                                    if (item.isBefore(startDatePicker.getValue()) || item.isAfter(startDatePicker.getValue().plusYears(1).minusDays(startDatePicker.getValue().getDayOfMonth()))) {
                                        setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }
                                }
                            }
                        };
                    }
                };
        endDatePicker.setDayCellFactory(dayCellFactoryEnd);
        endDatePicker.setConverter(converter);
    }
    
    private class SaveClassService extends Service<Boolean> {
        @Override
        protected Task<Boolean> createTask() {
            SaveClassTask theTask = new SaveClassTask();
            theTask.setOnSucceeded(workerStateEvent -> {
                System.out.println("---------------------------------------");
                System.out.println("     RESULT = "+getValue());
                try{
                    showMessage(getValue());
                }catch(IOException e){
                    e.printStackTrace(System.err);
                }
                System.out.println("---------------------------------------");
            });
            return theTask;
        }
        private class SaveClassTask extends Task {
            @Override
            protected Boolean call() throws Exception {
                try{
                    Aclass toBeSaved = new Aclass();
                    toBeSaved.setColor(DButils.getNextColor());
                    toBeSaved.setName(nameText.getText().trim());
                    toBeSaved.setCodeName(classCodeText.getText().trim());
                    if(Utils.isValidParam(descriptionText.getText().trim())){
                        toBeSaved.setDescription(descriptionText.getText().trim());
                    }
                    if(theTeacherOfTheClass != null){
                        toBeSaved.setTeacher(theTeacherOfTheClass);
                        // don't edit teacher here
//                        theTeacherOfTheClass.getClasses().add(toBeSaved);
                    }
                    System.out.println("Classses selected are = "+ choice.getSelectionModel().getSelectedIndex());
                    
                    Schedule classSchedule = new Schedule();
                    classSchedule.setTheClass(toBeSaved);
                    if(choice.getSelectionModel().getSelectedIndex() > 0){
                        for(int i = (0); i < choice.getSelectionModel().getSelectedIndex(); i++){
                            WeeklyLesson wlesson = new WeeklyLesson();
                            wlesson.setIndexNumber(i+1);
                            if(days.get(i).getSelectionModel().getSelectedIndex() > 0){
                                wlesson.setDayOfTheWeek(days.get(i).getSelectionModel().getSelectedIndex());
                            }
                            if(!timePickers.get(i).isDisabled()){
                                wlesson.setStartingHour(timePickers.get(i).getTimePicker().getLocalTime().getHour());
                                System.out.println("Hour = " +wlesson.getStartingHour() );
                            }
                            if(!timePickers.get(i).isDisabled()){
                                wlesson.setStartingMinute(timePickers.get(i).getTimePicker().getLocalTime().getMinute());
                                System.out.println("Min = " +wlesson.getStartingMinute());
                            }
                            try{
                                int currentDuration = Integer.parseInt(durations.get(i).getEditor().getText());
                                wlesson.setDuration(currentDuration);
                            }catch(Exception e){
                            }
                            classSchedule.getwLessons().add(wlesson);
                        }
                    }
                    toBeSaved.setSchedule(classSchedule);
                    if(startDatePicker.getValue() != null && endDatePicker.getValue() != null){
                        toBeSaved.setStartDate(startDatePicker.getValue());
                        toBeSaved.setEndDate(endDatePicker.getValue());
                        toBeSaved.createAllLessons();
                    }
                    if(perHourRadio.isSelected()){
                        toBeSaved.setPaidPerMonth(false);
                        toBeSaved.setHourlyFee(perHour.getNumber());
                        toBeSaved.setMonthlyFee(BigDecimal.ZERO);
                        System.out.println("perHour .getText ="+ perHour.getText());
                        System.out.println("perHour .getvalue = "+ perHour.getNumber());
                    }
                    
                    if(perMonthRadio.isSelected()){
                        toBeSaved.setPaidPerMonth(true);
                        toBeSaved.setHourlyFee(BigDecimal.ZERO);
                        toBeSaved.setMonthlyFee(perMonth.getNumber());
                        toBeSaved.createEditBillingMonths();
                        System.out.println("perMonth .getText ="+ perMonth.getText());
                        System.out.println("perMonth .getvalue = "+ perMonth.getNumber());
                    }
                    
                    DButils.saveClass(toBeSaved);
                }catch(NumberFormatException e){
                    System.out.println("Exception dam it");
                    e.printStackTrace(System.err);
                    return false;
                }
                return true;
            }
        }
    }
    
	@Override
	public void onLoad() {
		System.out.println("on Load --> Create Class");
		if(nameText != null){
			System.out.println("nameText != null");
		}else{
			System.out.println("nameText == null");
		
		}
	}
    
	@Override
	public void unload() {
	}
    
    
}

