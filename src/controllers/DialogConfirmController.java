/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.objects.Student;
import DB.objects.Teacher;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import static utils.Constants.STUDENT;
import static utils.Constants.TEACHER;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class DialogConfirmController{
    @FXML private Label message;
    @FXML private Button yesButton;
    @FXML private Button noButton;
    
    private ResourceBundle labels;
    private boolean answer;
    
    @FXML
    private void handleYesButton(ActionEvent event) throws IOException {
        System.out.println("Yes ");
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        answer = true;
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
    @FXML
    private void handleNoButton(ActionEvent event) throws IOException {
        System.out.println("NO ");
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        answer = false;
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
    public void init(String msg, int obj, boolean save, Object toSave){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        switch (obj){
            case TEACHER:
                if(save){
                    message.setText(labels.getString("dialog.save.teacher"));
                }
                else{
                    Teacher t = (Teacher)toSave;
                    System.out.println(" to Save = "+ t.getBasicInfo().getFname());
                    StringBuilder sb = new StringBuilder();
                    sb.append(labels.getString("dialog.select.teacher.line1"));
                    sb.append(": ");
                    sb.append(t.getBasicInfo().getFname());
                    sb.append(" ");
                    sb.append(t.getBasicInfo().getLname());
                    sb.append(" ");
                    sb.append(labels.getString("dialog.select.teacher.line2"));
                    message.setText(sb.toString());
                }
                break;
            case STUDENT:
                if(save){
                    message.setText(labels.getString("dialog.save.student"));    
                }
                else{
                    message.setText(msg);
                }
                break;
            default:
                message.setText(msg);
                break;
        }
        yesButton.setText(labels.getString("dialog.yes"));
        noButton.setText(labels.getString("dialog.no"));
    }
    
    public boolean getAnswer(){
        return answer;
    }
}
 
    