/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.objects.Billing;
import DB.objects.Lesson;
import customObjects.MyDecimalFormater;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import utils.Utils;
import utils.dialogs.BillingClassesDialog;
import utils.dialogs.BillingClassesDialogScene;
import utils.dialogs.BillingDialog;
import DB.objects.PaymentDetails;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CASH;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CHECK;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import utils.LanguageUtils;
import utils.dialogs.BillingDialog.AUTO_COMPLETE_FLAG;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class BillingMethodsController implements Initializable {
    @FXML private Label lessonsMinutesLbl;
    @FXML private Label amountDueLbl;
    @FXML private Label lessonsNoValue;
    @FXML private Label lessonsMinValue;
    @FXML private Label amountDueValue;
    @FXML private Button payBtn1;
    @FXML private Button payBtn2;
    @FXML private VBox mainVBox;
    private UnpaidStringTotals totals;
    private Dialog <PaymentDetails> paymentDialog = BillingDialog.getInstance(AUTO_COMPLETE_FLAG.NOT_AUTO_COMPLETE);
    private Tooltip minutesToolTip = new Tooltip();
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> paymentMethods = FXCollections.observableArrayList();
        paymentMethods.addAll(PaymentDetails.PAYMENT_METHOD.getStringValue(CASH), PaymentDetails.PAYMENT_METHOD.getStringValue(CHECK));
        totals = new UnpaidStringTotals();
        bindProperties();
        lessonsMinValue.setTooltip(minutesToolTip);
    }
    
    @FXML
    private void payButton1Clicked(Event event) throws IOException {
//        paymentDialog.setOnShown((Event showEvent) -> {
//            System.out.println("Dialog.setOnShown !!");
//            DialogScene.getController().initValues(thisBilling);
//        });
//        Optional <PaymentDetails> details = paymentDialog.showAndWait();
//        if(details.isPresent()){
//            boolean wasPaid = false;
//            if(thisBilling.isPaidInFull()){
//                wasPaid = true;
//            }
//            if(checkPaymentDetailsAndSaveBilling(details.get(), thisBilling)){
//                lessonGrid.getPaymentDetailsLbl().setText(details.get().toString());
//                if(thisBilling.isPaidInFull() && !wasPaid){
//                    paneBillingMethodsController.editTotals(aLesson, thisBilling, Constants.INCREASE);
//                }
//                if(! thisBilling.isPaidInFull() && wasPaid){
//                    paneBillingMethodsController.editTotals(aLesson, thisBilling, Constants.DECREASE);
//                }
//            }
//        }
    }
    
    @FXML
    private void payButton2Clicked(Event event) throws IOException {
        Dialog <Map<Year, Map<Month, BigDecimal>>> dialog = BillingClassesDialog.getInstance();
	List <LocalDate> dates = new ArrayList<>();
	dates.add(LocalDate.now());
        dialog.setOnShown((DialogEvent event1) -> {
            BillingClassesDialogScene.getController().init(dates, null);
        });
        
        Optional<Map<Year, Map<Month, BigDecimal>>> result  = dialog.showAndWait();
        
        
    }
    
    public void setLessonsMinutesLbl(Label lessonsMinutesLbl) { this.lessonsMinutesLbl = lessonsMinutesLbl; }
    public Label getAmountDueLbl() { return amountDueLbl; }
    public Label getLessonsNoValue() { return lessonsNoValue; }
    public Label getLessonsMinutesValue() { return lessonsMinValue; }
    public Label getAmountDueValue() { return amountDueValue; }
    
    public void editTotals(Lesson aLesson, Billing thisBilling, boolean increase){
        if(increase){
            System.out.println("before (inc)" + totals.toString());
            BigDecimal currentAmount = new BigDecimal(totals.amountDue.getValueSafe());
            currentAmount = currentAmount.add(thisBilling.getAmountFull());
            MyDecimalFormater formater = new MyDecimalFormater();
//            amountDueValue.setText(formater.numberToString(currentAmount));
            totals.amountDue.setValue(formater.numberToString(currentAmount));
            
//            Long currentNumberOfLessons = Long.valueOf(lessonsNoValue.getText());
            Long currentNumberOfLessons = Long.valueOf(totals.lessonsNum.getValueSafe());
            currentNumberOfLessons += 1;
//            lessonsNoValue.setText(Long.toString(currentNumberOfLessons));
            totals.lessonsNum.setValue(Long.toString(currentNumberOfLessons));
            
//            Long currentDuration = Long.valueOf(lessonsMinValue.getText());
            Long currentDuration = Long.valueOf(totals.lessonsMins.getValueSafe());
            currentDuration += aLesson.getDuration();
            totals.lessonsMins.setValue(Long.toString(currentDuration));
            minutesToolTip.setText(minutesAndHours(currentDuration));
            
            System.out.println("after (inc)" + totals.toString());
        }else{
            System.out.println("before (dec)" + totals.toString());
            BigDecimal currentAmount = new BigDecimal(totals.amountDue.getValueSafe());
            currentAmount = currentAmount.subtract(thisBilling.getAmountFull());
            MyDecimalFormater formater = new MyDecimalFormater();
//            amountDueValue.setText(formater.numberToString(currentAmount));
            totals.amountDue.setValue(formater.numberToString(currentAmount));
            
//            Long currentNumberOfLessons = Long.valueOf(lessonsNoValue.getText());
            Long currentNumberOfLessons = Long.valueOf(totals.lessonsNum.getValueSafe());
            currentNumberOfLessons -= 1;
//            lessonsNoValue.setText(Long.toString(currentNumberOfLessons));
            totals.lessonsNum.setValue(Long.toString(currentNumberOfLessons));
            
//            Long currentDuration = Long.valueOf(lessonsMinValue.getText());
            Long currentDuration = Long.valueOf(totals.lessonsMins.getValueSafe());
            currentDuration -= aLesson.getDuration();
//            lessonsMinValue.setText(Long.toString(currentDuration));
            totals.lessonsMins.setValue(Long.toString(currentDuration));
            System.out.println("after (dec)" + totals.toString());
            minutesToolTip.setText(minutesAndHours(currentDuration));
        }
    }
    
    public void setTotals(BillingInfoController.UnpaidTotals totals) {
        this.totals.setValues(totals);
    }
    
    private class UnpaidStringTotals{
        public StringProperty amountDue;
        public StringProperty lessonsNum;
        public StringProperty lessonsMins;
        
        public UnpaidStringTotals() {
            amountDue = new SimpleStringProperty();
            lessonsNum = new SimpleStringProperty();
            lessonsMins = new SimpleStringProperty();
        }
        
        private void setValues(BillingInfoController.UnpaidTotals totals) {
            MyDecimalFormater formater = new MyDecimalFormater();
            amountDue.setValue(formater.numberToString(totals.unpaidAmount));
            lessonsNum.setValue(Integer.toString(totals.unpaidLessons));
            lessonsMins.setValue(Long.toString(totals.unpaidDuration));
            minutesToolTip.setText(minutesAndHours(totals.unpaidDuration));
        }
        
        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder();
            sb.append(LanguageUtils.getString("billing.methods.due"))
			.append(amountDue.getValue())
			.append("| ").append(LanguageUtils.getString("billing.methods.num"))
			.append(lessonsNum.getValue())
			.append("| ").append(LanguageUtils.getString("billing.methods.mins"))
			.append(lessonsMins.getValue());
            
            return sb.toString();
        }
    }
    
    public void bindProperties(){
        amountDueValue.textProperty().bind(totals.amountDue);
        lessonsNoValue.textProperty().bind(totals.lessonsNum);
        lessonsMinValue.textProperty().bind(totals.lessonsMins);
        minutesToolTip.setText(minutesAndHours(totals.lessonsMins.getValue()));
    }
    
    
    private String minutesAndHours(long mins){
        StringBuilder sb = new StringBuilder();
        sb.append(Long.toString(mins/60)).append("Hr").append(Long.toString(mins%60)).append("mins");
        return sb.toString();
    }
    private String minutesAndHours(String mins){
        if(!Utils.isValidParam(mins)){
            mins = "0";
        }
        return minutesAndHours(Long.parseLong(mins));
    }
    
    
}
