/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DButils;
import DB.objects.Address;
import DB.objects.BasicInformation;
import DB.objects.ContactInformation;
import DB.objects.Teacher;
import images.Images;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.sql.rowset.serial.SerialBlob;
import screens.FXMLs;
import utils.dialogs.CustomDialogs;
import utils.LanguageUtils;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class CreateTeacherController extends StageController{
    
    private final URL fxmlURL;
    private final ResourceBundle resources;
    private final Image defaultImage = new Image(Images.class.getResource("default_profile.png").toString(),140,140,true,true);
    
    
    public CreateTeacherController(URL fxml, ResourceBundle res) {
        super();
        labels = res;
        fxmlURL = fxml;
        resources = res;
    }
    @FXML private AnchorPane root;
    @FXML private ImageView image;
    @FXML private ScrollPane theScroll;
    @FXML private VBox theV;
    @FXML private ProgressIndicator progress;
    @FXML private SplitPane splitPane;
    @FXML private Label mainTitle;
    @FXML private Label profileLabel;
    @FXML private Button addPictureButton;
    @FXML private Button deletePic;
    @FXML private Label b_info;
    @FXML private Label name;
    @FXML private Label surname;
    @FXML private Label mName;
    @FXML private Label email2;
    @FXML private TextField nameText;
    @FXML private TextField surnameText;
    @FXML private TextField mNameText;
    @FXML private TextField email2Text;
    @FXML private Label a_info;
    @FXML private Label address1Title;
    @FXML private Label address1StreetNum;
    @FXML private Label address1StreetName;
    @FXML private Label address1Area;
    @FXML private Label address1City;
    @FXML private Label address1Zip;
    @FXML private TextField address1StreetNumText;
    @FXML private TextField address1StreetNameText;
    @FXML private TextField address1AreaText;
    @FXML private TextField address1CityText;
    @FXML private TextField address1ZipText;
    
    @FXML private Label address2Title;
    @FXML private Label address2StreetNum;
    @FXML private Label address2StreetName;
    @FXML private Label address2Area;
    @FXML private Label address2City;
    @FXML private Label address2Zip;
    @FXML private TextField address2StreetNumText;
    @FXML private TextField address2StreetNameText;
    @FXML private TextField address2AreaText;
    @FXML private TextField address2CityText;
    @FXML private TextField address2ZipText;
    @FXML private Label c_info;
    @FXML private Label phone1;
    @FXML private Label phone2;
    @FXML private Label email;
    @FXML private TextField phone1Text;
    @FXML private TextField phone2Text;
    @FXML private TextField emailText;
    @FXML private Button confirmButton;
    @FXML private Button cancelButton;
    @FXML private Button cropButton;
    SerialBlob imgFromUser_blob;
    Image imgFromUser;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("init create Teacher");
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.prefWidthProperty().bind(theScroll.widthProperty());
//        nameText.addEventFilter(KeyEvent.KEY_TYPED, EventFilters.getNoNumbersFilter());
//        teacher = new Teacher();
    }
    
    @Override
    protected void changeLocale(){
        if(mainTitle == null){
            System.out.println(" mainTitle == null ");
            return;
        }
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        mainTitle.setText(labels.getString("new.teacher.create"));
        profileLabel.setText(labels.getString("profile_pic"));
        addPictureButton.setText(labels.getString("addpicture"));
        b_info.setText(labels.getString("basic_information"));
        name.setText(labels.getString("name"));
        surname.setText(labels.getString("surname"));
        nameText.setPromptText(labels.getString("insertname"));
        surnameText.setPromptText(labels.getString("insertsurname"));
        a_info.setText(labels.getString("address.information"));
        address1Title.setText(labels.getString("address.1"));
        address1StreetNum.setText(labels.getString("address.street.num"));
        address1StreetName.setText(labels.getString("address.street.name"));
        address1Area.setText(labels.getString("address.area"));
        address1City.setText(labels.getString("address.city"));
        address1Zip.setText(labels.getString("address.zip"));
        address1StreetNumText.setPromptText(labels.getString("address.street.numText"));
        address1StreetNameText.setPromptText(labels.getString("address.street.nameText"));
        address1AreaText.setPromptText(labels.getString("address.areaText"));
        address1CityText.setPromptText(labels.getString("address.cityText"));
        address1ZipText.setPromptText(labels.getString("address.zipText"));
        address2Title.setText(labels.getString("address.2"));
        address2StreetNum.setText(labels.getString("address.street.num"));
        address2StreetName.setText(labels.getString("address.street.name"));
        address2Area.setText(labels.getString("address.area"));
        address2City.setText(labels.getString("address.city"));
        address2Zip.setText(labels.getString("address.zip"));
        address2StreetNumText.setPromptText(labels.getString("address.street.numText"));
        address2StreetNameText.setPromptText(labels.getString("address.street.nameText"));
        address2AreaText.setPromptText(labels.getString("address.areaText"));
        address2CityText.setPromptText(labels.getString("address.cityText"));
        address2ZipText.setPromptText(labels.getString("address.zipText"));
        c_info.setText(labels.getString("contact_information"));
        phone1.setText(labels.getString("phone1"));
        phone2.setText(labels.getString("phone2"));
        email.setText(labels.getString("email"));
        phone1Text.setPromptText(labels.getString("insertphone"));
        phone2Text.setPromptText(labels.getString("insertphone"));
        emailText.setPromptText(labels.getString("insertemail"));
        confirmButton.setText(labels.getString("new.teacher.save"));
        cancelButton.setText(labels.getString("new.student.cancel"));
        email2.setText(labels.getString("email2"));
        mName.setText(labels.getString("mName"));
        mNameText.setPromptText(labels.getString("insertMName"));
        email2Text.setPromptText(labels.getString("insertEmail2"));
    }
    
    @FXML
    private void addImage(Event e){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(LanguageUtils.getInstance().getCurrentLanguage().getString("selectphoto"));
        File file = fileChooser.showOpenDialog(stage);
        ByteArrayInputStream is = null;
        if (file != null) {
            Path path = Paths.get(file.getAbsolutePath());
            try {
                byte[] data = Files.readAllBytes(path);
                imgFromUser_blob = new SerialBlob(data);
                is = new ByteArrayInputStream(data);
                imgFromUser = new Image(is);
                image.setImage(imgFromUser);
                cropButton.setDisable(false);
                deletePic.setDisable(false);
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            }finally{
                if(is != null){
                    try {
                        is.close();
                    } catch (IOException ex) {
                        Logger.getLogger(CreateStudentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    @FXML
    private void saveTeacher(Event e){
        if(stage == null){
            Node source = (Node)e.getSource();
            stage = (Stage)(source.getScene().getWindow());
        }
        try {
            showConfirmDialog();
        } catch (IOException ex) {
            System.err.println("Error while showing confirmation dialog -Teacher-");
            ex.printStackTrace(System.err);
        }
    }
    
    private void showConfirmDialog() throws IOException {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        String yes = labels.getString("dialog.yes");
        Optional<ButtonType> response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.teacher.title"), labels.getString("dialog.save.teacher"), yes, labels.getString("dialog.no"));
        if(response.isPresent()){
            if (response.get().getText().equals(yes)){
                verifyInput();
            }
        }
    }
    @FXML
    private void cropImage(Event event) throws IOException {
        Stage cropImageStage = new Stage();
        Node source = (Node)event.getSource();
        stage = (Stage)(source.getScene().getWindow());
        final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("CropImage.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
        final Scene cropImageScene = new Scene((AnchorPane)myLoader.load());
        CropImageController c = (CropImageController)myLoader.getController();
        c.init(imgFromUser);
        
        stage.setOnCloseRequest((WindowEvent ev) -> {
            cropImageScene.getWindow().fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        });
        
        cropImageStage.setScene(cropImageScene);
//        stage.toBack();
        cropImageStage.toFront();
        stage.setOpacity(0.8);
        root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
        root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
        cropImageScene.getWindow().setOnCloseRequest((WindowEvent ev) -> {
            stage.setOpacity(1);
            CropImageController c1 = (CropImageController)myLoader.getController();
            Image croppedImg = c1.getCroppedImage();
            if(croppedImg != null){
                try{
                    image.setImage(croppedImg);
                    imgFromUser_blob = new SerialBlob(utils.Utils.toByteArray(croppedImg));
                }
                catch(Exception e){
                    System.out.println("ERROR ERROR - converting image");
                    e.printStackTrace(System.err);
                }
            }
        });
        cropImageStage.show();
    }
    
    @FXML
    private void setDefaultImage(Event e){
        imgFromUser_blob = null;
        imgFromUser = null;
        deletePic.setDisable(true);
        cropButton.setDisable(true);
        image.setImage(defaultImage);
    }
    
    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader);
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
    
    private class SaveTeacherService extends Service<Boolean> {
        @Override
        protected Task<Boolean> createTask() {
            SaveTeacherTask theTask = new SaveTeacherTask();
            theTask.setOnSucceeded(workerStateEvent -> {
                System.out.println("---------------------------------------");
                System.out.println("     RESULT = "+getValue());
                try{
                    showMessage(getValue());
                }catch(IOException e){
                    e.printStackTrace(System.err);
                }
                System.out.println("---------------------------------------");
            });
            return theTask;
        }
        private class SaveTeacherTask extends Task {
            @Override
            protected Boolean call() throws Exception {
                try{
                    Teacher toBeSaved = new Teacher();
                    if(imgFromUser_blob != null){
                        toBeSaved.setPicture(imgFromUser_blob);
                    }
                    BasicInformation basicInfo = new BasicInformation();
                    basicInfo.setFname(nameText.getText().trim());
                    basicInfo.setLname(surnameText.getText().trim());
                    if(Utils.isValidParam(mNameText.getText().trim())){
                        basicInfo.setMiddleName(mNameText.getText().trim());
                    }
                    toBeSaved.setBasicInfo(basicInfo);
                    ContactInformation contactInfo = new ContactInformation();
                    if(utils.Utils.isValidParam(phone1Text.getText().trim())){
                        contactInfo.setPhone1(phone1Text.getText().trim());
                    }
                    if(utils.Utils.isValidParam(phone1Text.getText().trim())){
                        contactInfo.setPhone2(phone1Text.getText().trim());
                    }
                    if(utils.Utils.isValidParam(emailText.getText().trim())){
                        contactInfo.setEmail1(emailText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(email2Text.getText().trim())){
                        contactInfo.setEmail2(email2Text.getText().trim());
                    }
                    Address address1 = new Address();
                    if(utils.Utils.isValidParam(address1AreaText.getText().trim())){
                        address1.setArea(address1AreaText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address1CityText.getText().trim())){
                        address1.setCity(address1CityText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address1StreetNameText.getText().trim())){
                        address1.setStreetInfo1(address1StreetNameText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address1StreetNumText.getText().trim())){
                        address1.setStreetNum(address1StreetNumText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address1ZipText.getText().trim())){
                        address1.setPostCode(address1ZipText.getText().trim());
                    }
                    Address address2 = new Address();
                    if(utils.Utils.isValidParam(address2AreaText.getText().trim())){
                        address2.setArea(address2AreaText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address2CityText.getText().trim())){
                        address2.setCity(address2CityText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address2StreetNameText.getText().trim())){
                        address2.setStreetInfo1(address2StreetNameText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address2StreetNumText.getText().trim())){
                        address2.setStreetNum(address2StreetNumText.getText().trim());
                    }
                    if(utils.Utils.isValidParam(address2ZipText.getText().trim())){
                        address2.setPostCode(address2ZipText.getText().trim());
                    }
                    contactInfo.setAddress1(address1);
                    contactInfo.setAddress2(address2);
                    toBeSaved.setContactInfo(contactInfo);
                    Thread.sleep(500);
                    DButils.saveTeacher(toBeSaved);
                }catch(InterruptedException e){
                    System.out.println("Exception dam it -12-");
                    e.printStackTrace(System.err);
                    return false;
                }
                return true;
            }
        }
    }
    
    private void verifyInput(){
        boolean thereIsError = false;
        if(!Utils.isValidParam(nameText.getText().trim())){
            Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
            thereIsError = true;
        }
        if(!Utils.isValidParam(surnameText.getText().trim())){
            Utils.turnNodeRed(surnameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.surname"));
            thereIsError = true;
        }
        
        if(thereIsError){
            System.out.println("There Is error");
        }else{
            createObjectAndSaveIt();
        }
    }
    
    private void createObjectAndSaveIt(){
        SaveTeacherService saveClass = new SaveTeacherService();
        progress.visibleProperty().bind(saveClass.runningProperty());
        confirmButton.disableProperty().bind(saveClass.runningProperty());
        cancelButton.disableProperty().bind(saveClass.runningProperty());
        System.out.println("starting service");
        saveClass.start();
        progress.requestFocus();
    }
    
    private void showMessage(boolean success) throws IOException {
        Optional<ButtonType> response;
        String retry = labels.getString("save.obj.retry");
        String clear = labels.getString("save.obj.clear");
        if(success){
            String message = labels.getString("save.obj.student")+"\n"
                    + labels.getString("save.obj.success.line1") +"\n"
                    + labels.getString("save.obj.line2");
            response = CustomDialogs.clearCloseDialog("", message, clear , labels.getString("save.obj.return"));
        }else{
            String message = labels.getString("save.obj.student")+"\n"
                    + labels.getString("save.obj.fail.line1") +"\n"
                    + labels.getString("save.obj.line2");
            response = CustomDialogs.retryClearCloseDialog("", message, retry, labels.getString("save.obj.clear"), labels.getString("save.obj.return"));
        }
        if(response.isPresent()){
            if(response.get().getText().equals(clear)){
                clearAll();
            }
            else if(response.get().getText().equals(retry)){
                saveTeacher(null);
            }
        }
    }
    
    private void clearAll(){
        imgFromUser_blob = null;
        image.setImage(defaultImage);
        nameText.setText("");
        surnameText.setText("");
        mNameText.setText("");
        address1AreaText.setText("");
        address1CityText.setText("");
        address1StreetNameText.setText("");
        address1StreetNumText.setText("");
        address1ZipText.setText("");
        address2AreaText.setText("");
        address2CityText.setText("");
        address2StreetNameText.setText("");
        address2StreetNumText.setText("");
        address2ZipText.setText("");
        phone1Text.setText("");
        phone2Text.setText("");
        email2Text.setText("");
        emailText.setText("");
    }
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
	}
    
}

