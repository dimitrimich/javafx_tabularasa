/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author d.michaelides
 */
public class CropImageController implements Initializable {
    
    @FXML
    private Label label;
    private Stage stage;
    @FXML ImageView image;
    @FXML ImageView imagePreview;
    @FXML Rectangle rectangle;
    @FXML Slider sliderV;
    @FXML Slider sliderH;
    @FXML Button okButton;
    
    
    NumberBinding x;
    NumberBinding y;
        
    private double initX;
    private double initY;
    private Point2D dragAnchor;
    double newXPosition;
    double newYPosition;
    
    private Image imageFromUser;
    public Image getImageFromUser(){
        return imageFromUser;
    }
    
    public void setImageFromUser( Image iFromUser){
         imageFromUser = iFromUser;
    }
    
    private Image croppedImage;
    public Image getCroppedImage(){
        return croppedImage;
    }
    
    public void setStage(Stage stagee){
        this.stage = stagee;
    }
    
    public void init(Image fromUser) {
        setImageFromUser(fromUser);
        image.setImage(fromUser);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rectangle.heightProperty().bind(sliderV.valueProperty());
        rectangle.widthProperty().bind(sliderH.valueProperty());
        
        DoubleProperty num1 = new SimpleDoubleProperty();
        num1.bind(rectangle.translateXProperty());
        DoubleProperty num2 = new SimpleDoubleProperty();
        num2.bind(rectangle.layoutXProperty());
        x = num1.add(num2);
        DoubleProperty translateY = new SimpleDoubleProperty();
        translateY.bind(rectangle.translateYProperty());
        DoubleProperty layoutY = new SimpleDoubleProperty();
        layoutY.bind(rectangle.layoutYProperty());
        y = translateY.add(layoutY);
    }    
    
    @FXML 
    private void mouseDragged(MouseEvent me){
        double dragX = me.getSceneX() - dragAnchor.getX();
        double dragY = me.getSceneY() - dragAnchor.getY();
        //calculate new position of the rectangle
        newXPosition = initX + dragX;
        newYPosition = initY + dragY;
        //if new position do not exceeds borders of the rectangle, translate to this position
        
        if ((newXPosition > -12 ) && (newXPosition <= (530 - rectangle.getWidth()))) {
            rectangle.setTranslateX(newXPosition);
        }
        if ((newYPosition > -10) && (newYPosition <= (353 - rectangle.getHeight()))) {
            rectangle.setTranslateY(newYPosition);
        }
//        if ((newXPosition > -4 ) && (newXPosition <= image.getLayoutX() + image.getFitWidth() - rectangle.getWidth())) {
//            rectangle.setTranslateX(newXPosition);
//        }
//        if ((newYPosition >= image.getLayoutY() - rectangle.getHeight() - 100) && (newYPosition <= image.getLayoutY() + image.getFitHeight() - rectangle.getHeight() - 190)) {
//            rectangle.setTranslateY(newYPosition);
//        }
    
    }
    @FXML 
    private void mousePressed(MouseEvent e){
        initX = rectangle.getTranslateX();
        initY = rectangle.getTranslateY();
        dragAnchor = new Point2D(e.getSceneX(), e.getSceneY());
    }
    
    @FXML
    private void setView(Event e){
        SnapshotParameters sp = new SnapshotParameters();
        Rectangle2D rectangle2D = new Rectangle2D(x.doubleValue(), y.doubleValue(), rectangle.getWidth(), rectangle.getHeight());
        sp.setViewport(rectangle2D);
        
        WritableImage img = new WritableImage((int) rectangle.getWidth(), (int) rectangle.getHeight());
        image.snapshot(sp, img);
        imagePreview.setImage(img);
        croppedImage = img;
        okButton.setDisable(false);
    }
    
    @FXML
    private void okButtonPressed(Event event) throws IOException {
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
}
