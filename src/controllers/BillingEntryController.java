/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import static controllers.StageController.cachingClassLoader;
import fxObjects.MonthFX;
import fxObjects.MonthFXUtil;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import utils.LanguageUtils;
import fxObjects.StudentFX;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import jfxtras.scene.control.ListSpinner;
import services.GetAllFXMonths;
import services.GetAllFXStudents;
import utils.dialogs.BillingClassesDialog;
import utils.dialogs.BillingClassesDialogScene;
import utils.dialogs.CustomDialogs;

/**
 * Controller for the entry screen to make a payment
 * @author dimitris
 */
public class BillingEntryController extends StageController{
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public BillingEntryController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	@Override
	protected void changeLocale() {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		pageTitle.setText(labels.getString("billing.entry.title"));
		filterStudents.setText(labels.getString("filter.results"));
		fromLabel.setText(labels.getString("billing.entry.from"));
		toLabel.setText(labels.getString("billing.entry.to"));
		studentsTitle.setText(labels.getString("students"));
		monthsTitle.setText(labels.getString("months"));
	}
	
	
	/****************************************************************************************************************************/
	/****************************************************************************************************************************/
	/****************************************************************************************************************************/
	
	@FXML private ScrollPane theScroll;
	@FXML private BorderPane mainBorderPane;
	@FXML private GridPane theGrid, yearsGrid;
	
	@FXML private VBox studentsSearchVbox;
	@FXML private TableView<StudentFX> studentsTable;
	@FXML private ProgressIndicator progressIndicatorStu;
	@FXML private BorderPane progressPaneStu;
	@FXML private TextField studentNameTxt;
	
	@FXML private TableView<MonthFX> monthsTable;
	@FXML private ProgressIndicator progressIndicatorMonths;
	@FXML private BorderPane progressPaneMonths;
	@FXML private VBox monthsSearchVbox;
	@FXML private Label pageTitle, filterStudents, fromLabel, toLabel, studentsTitle, monthsTitle;
	
	// list of students
	private ObservableList<StudentFX> theStudentsFX;
	// list of months
	private ObservableList<MonthFX> theMonthsFX;
	private GetAllFXStudents fxStudentsService = new GetAllFXStudents();
	private GetAllFXMonths fxMonthsService = new GetAllFXMonths();
	private ListSpinner<Integer> fromYear;
	private ListSpinner<Integer> toYear;
	
	@FXML private Button payNowButton;
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		theGrid.prefWidthProperty().bind(theScroll.widthProperty().subtract(10));
		mainBorderPane.prefWidthProperty().bind(theScroll.widthProperty().subtract(10));
		
		int currentYear = LocalDate.now().getYear();
		
		fromYear = new ListSpinner<>(currentYear-10, currentYear, 1);
		fromYear.setValue(currentYear-2);
		yearsGrid.add(fromYear, 1, 0);
		toYear = new ListSpinner<>(currentYear-10, currentYear, 1);
		yearsGrid.add(toYear, 3, 0);
		toYear.setValue(currentYear);
		
		fromYear.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
			if(newValue > fromYear.valueProperty().get()){
				fromYear.setValue(oldValue);
				return;
			}
			filterMonths(oldValue, newValue, true);
			
		});
		toYear.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
			if(newValue < fromYear.valueProperty().get()){
				toYear.setValue(oldValue);
				return;
			}
			filterMonths(oldValue, newValue, false);
		});
		
		monthsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		TableColumn<StudentFX, String> nameColumn = new TableColumn<>(LanguageUtils.getString("billing.entry.name"));
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		nameColumn.prefWidthProperty().bind(studentsTable.widthProperty());
		studentsTable.getColumns().add(nameColumn);
		
		studentsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StudentFX>() {
			@Override
			public void changed(ObservableValue<? extends StudentFX> observable, StudentFX oldValue, StudentFX newValue) {
				if(oldValue != null && oldValue.getStudent() != null && newValue != null){
					if(newValue.getStudent().getId().compareTo(oldValue.getStudent().getId()) != 0){
						System.out.println("Student Changed");
						progressPaneMonths.setVisible(true);
						loadMonthsData(newValue);
						filterMonths(5, 10, true);
					}
				}else{
					if(newValue != null){
						System.out.println("Student Changed");
						progressPaneMonths.setVisible(true);
						loadMonthsData(newValue);
						filterMonths(5, 10, true);
					}
				}
			}
		});
		monthsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MonthFX>() {
			@Override
			public void changed(ObservableValue<? extends MonthFX> observable, MonthFX oldValue, MonthFX newValue) {
				if(oldValue == null && newValue == null){
					payNowButton.setDisable(true);
					return;
				}
				if(newValue != null){
					payNowButton.setDisable(false);
				}
				if(newValue == null){
					payNowButton.setDisable(true);
				}
			}
		});
		
//	TableColumn<MonthFX, String> monthNameColumn = new TableColumn<>("Name");
//	monthNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
//	monthNameColumn.prefWidthProperty().bind(monthsTable.widthProperty());
//	monthsTable.getColumns().add(monthNameColumn);
//
		MonthFXUtil.initMonthFXTable(monthsTable);
		initListeners();
		
	}
	
	
	@FXML
	private void refresh(Event event) throws IOException {
		loadData();
	}
	
	/**
	 * load data on studentsFX table
	 */
	private void loadData() {
		
		studentsTable.setVisible(false);
		progressPaneStu.setVisible(true);
		studentsSearchVbox.setDisable(true);
		
		//Here you tell your progress indicator is visible only when the service is runing
		fxStudentsService.setOnSucceeded((WorkerStateEvent workerStateEvent) -> {
			System.out.println("succeed");
			
			theStudentsFX = fxStudentsService.getValue();   //here you get the return value of your service
			System.out.println("theRows "+ theStudentsFX.size());
			studentsTable.setItems(theStudentsFX);
			studentsTable.setVisible(true);
			studentsTable.setDisable(false);
			studentsSearchVbox.setDisable(false);
			progressPaneStu.setVisible(false);
		});
		
		fxStudentsService.setOnFailed((WorkerStateEvent workerStateEvent) -> {
			
			CustomDialogs.errorDialog(LanguageUtils.getString("billing.entry.error"), LanguageUtils.getString("billing.entry.error.text"));
			System.err.println("Get FX students FAILED");
			System.err.println("Get FX students FAILED");
			System.err.println("Get FX students FAILED");
			//DO stuff on failed
		});
		fxStudentsService.restart(); //here you start your service
	}
	
	/**
	 * load MonthFX data
	 */
	private void loadMonthsData(StudentFX studentFX) {
		fxMonthsService.setStudent(studentFX);
		payNowButton.setDisable(true);
		//Here you tell your progress indicator is visible only when the service is runing
		progressPaneMonths.setVisible(true);
		monthsTable.setVisible(false);
		monthsSearchVbox.setDisable(true);
		fxMonthsService.setOnSucceeded((WorkerStateEvent workerStateEvent) -> {
			System.out.println("succeed - months");
			
			theMonthsFX = fxMonthsService.getValue();   //here you get the return value of your service
			System.out.println("theRows (month)"+ theMonthsFX.size());
			monthsTable.setItems(theMonthsFX);
			monthsTable.setVisible(true);
			monthsTable.setDisable(false);
			monthsSearchVbox.setDisable(false);
			progressPaneMonths.setVisible(false);
		});
		
		fxMonthsService.setOnFailed((WorkerStateEvent workerStateEvent) -> {
			CustomDialogs.errorDialog(LanguageUtils.getString("billing.entry.error"), LanguageUtils.getString("billing.entry.error.text2"));
			System.err.println("Get FX months FAILED");
			System.err.println("Get FX months FAILED");
			System.err.println("Get FX months FAILED");
			//DO stuff on failed
		});
		fxMonthsService.restart(); //here you start your service
	}
	
	
	/*
	filter students TableView
	*/
	public void filterStudents(String oldVal, String newVal) {
		if ((oldVal != null && (newVal.length() < oldVal.length()))) {
			studentsTable.setItems( theStudentsFX );
		}
		
		
		if(newVal.isEmpty()){ return; }
		newVal = newVal.toUpperCase();
		String [] tokens = newVal.trim().split(" ");
		String searchToken1 = tokens[0];
		String searchToken2 = (tokens.length == 2) ? tokens[1] : null;
		
		
		ObservableList<StudentFX> subentries = FXCollections.observableArrayList();
		
		for ( StudentFX student : studentsTable.getItems() ) {
			
			String searchTokenFN = student.getName().toUpperCase();
			
			if(searchToken2 == null){
				if(searchTokenFN.contains(searchToken1)){
					subentries.add(student);
				}
			}else{
				if(searchTokenFN.contains(searchToken1) && searchTokenFN.contains(searchToken2)){
					subentries.add(student);
				}
			}
		}
		studentsTable.setItems(subentries);
	}
	
	private void initListeners() {
		
		studentNameTxt.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
			if(key.getCharacter().equals(" ") && studentNameTxt.getText().contains(" ")){
				key.consume();
			}
		});
		studentNameTxt.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						filterStudents(oldVal, newVal);
					}
				});
	}
	
	@FXML
	/**
	 * show the payment dialog for current selected month
	 */
	private void payNow(Event event) throws IOException {
		List<LocalDate> monthsToPay = new ArrayList<>();
		for(MonthFX aMonth : monthsTable.getSelectionModel().getSelectedItems()){
			monthsToPay.add(LocalDate.of(aMonth.getYear().getValue(), aMonth.getMonth(), 1));
			System.out.println("Months to pay ->" + aMonth.getMonth());
		}
		
		
		if(monthsToPay == null || monthsToPay.isEmpty()){
			System.err.println("MONTHFX list is null OR empty");
			return;
		}
		
		StudentFX selectedStudentFX = studentsTable.getSelectionModel().getSelectedItem();
		if(selectedStudentFX == null){
			System.err.println("student FX  is null");
			return;
		}
		
		Dialog <Map<Year, Map<Month, BigDecimal>>> dialog = BillingClassesDialog.getInstance();
		dialog.setOnShown((DialogEvent event1) -> {
			BillingClassesDialogScene.getController().init(monthsToPay, selectedStudentFX.getStudent());
		});
		
		Optional<Map<Year, Map<Month, BigDecimal>>> result  = dialog.showAndWait();
		if(result.isPresent() ){
			Map<Year, Map<Month, BigDecimal>> resultMap = result.get();
			for(Entry<Year, Map<Month, BigDecimal>> paymentsOfTheYear : resultMap.entrySet()){
				Year currentYear = paymentsOfTheYear.getKey();
				for(Entry<Month, BigDecimal> monthlyPayments : paymentsOfTheYear.getValue().entrySet()){
					Month currentMonth = monthlyPayments.getKey();
					BigDecimal amount = monthlyPayments.getValue();
					for(MonthFX fxMonth : monthsTable.getItems()){
						if((fxMonth.getYear().compareTo(currentYear) == 0 ) && (fxMonth.getMonth().compareTo(currentMonth) == 0) ){
							fxMonth.decreaseRemainingAmountToPayProperty(amount);
						}
					}
				}
			}
		}
	}
	@Override
	public void onLoad() {
		loadData();
	}
	@Override
	public void unload() {
		monthsTable.getItems().clear();
		studentsTable.getItems().clear();
		monthsTable.setDisable(true);
		studentsTable.setDisable(true);
		studentsSearchVbox.setDisable(true);
		studentNameTxt.setText("");
	}
	
	public void filterMonths(Integer oldVal, Integer newVal, boolean isFromLower) {
		if(isFromLower){
			if(newVal < oldVal){
				monthsTable.setItems(theMonthsFX);
			}
		}else{
			if(oldVal < newVal){
				monthsTable.setItems(theMonthsFX);
			}
		}
		
		ObservableList<MonthFX> subentries = FXCollections.observableArrayList();
		
		monthsTable.getItems().stream().forEach((MonthFX month) -> {
			int monthYear = month.getYear().getValue();
			if(fromYear.getValue() <= monthYear && toYear.getValue() >= monthYear){
				subentries.add(month);
			}
		});
		monthsTable.setItems(subentries);
	}
}


