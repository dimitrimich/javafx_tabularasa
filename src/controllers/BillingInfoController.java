/**
 *
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import DB.BillingPersister;
import DB.DButils;
import DB.objects.Aclass;
import DB.objects.Attendance;
import DB.objects.Billing;
import DB.objects.BillingMonth;
import DB.objects.Lesson;
import DB.objects.Student;
import static controllers.StageController.cachingClassLoader;
import customControls.NumberTextField;
import customControls.LessonBillingInfo;
import customObjects.MyDecimalFormater;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import utils.Constants;
import utils.LanguageUtils;
import utils.Utils;
import utils.dialogs.BillingDialog;
import utils.dialogs.BillingDialogScene;
import DB.objects.PaymentDetails;
import java.time.Year;
import utils.dialogs.BillingDialog.AUTO_COMPLETE_FLAG;
import utils.menu.ScreenNavigator;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class BillingInfoController extends StageController {
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public BillingInfoController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	@FXML private ScrollPane mainScrollPane;
	@FXML private ScrollPane lessonsScrollPane;
	@FXML private VBox lessonsParentPane;
	@FXML private BorderPane lessonsBorderPane;
	
	@FXML private VBox theV;
	@FXML private Label studentNameLbl;
	@FXML private Label classNameLbl;
	@FXML private Label monthLbl;
	
	@FXML private Label dateLbl;
	@FXML private Label dateValueLbl;
	@FXML private CheckBox wasCanceledCbox;
	@FXML private Label wasCanceledLbl;
	@FXML private Label amountDueLlb;
	@FXML private Label amountPaidLbl;
	@FXML private Label discountLbl;
	@FXML private TextField amountPaidValue;
	@FXML private TextField discountValue;
	@FXML private Label amountDueValue;
	
	@FXML private GridPane totalsGrid;
	@FXML private Label numOfLessonsLbl;
	@FXML private Label numOfLessonsValue;
	@FXML private Label totalDiscountLbl;
	private NumberTextField totalDiscountValue;
	@FXML private Label totalIncomeLbl;
	private NumberTextField totalIncomeValue;
	@FXML private Label totalsTitle;
	@FXML private Label canceledLessonsLbl;
	@FXML private Label canceledLessonsValue;
	@FXML private Label totalExtraLessonsLbl;
	@FXML private Label totalExtraLessonsValue;
	@FXML private Label expectedIncomeLbl;
	@FXML private Label expectedIncomeValue;
	@FXML private Label studentName, className, monthName;
	@FXML private AnchorPane paneBillingMethods;
	@FXML private BillingMethodsController paneBillingMethodsController;
	private static final boolean INCREASE = true;
	private static final boolean DECREASE = false;
	
	
	private NumberFormat numFormat = NumberFormat.getInstance();
	private MyDecimalFormater myFormater = new MyDecimalFormater();
	private final DateTimeFormatter dateFormater = DateTimeFormatter.ofPattern("dd / MMMM / yyyy");
	private UnpaidTotals totals = new UnpaidTotals();
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		theV.minWidthProperty().bind(mainScrollPane.widthProperty().subtract(20));
		theV.maxWidthProperty().bind(mainScrollPane.widthProperty().subtract(20));
		theV.minHeightProperty().bind(mainScrollPane.heightProperty());
		theV.maxHeightProperty().bind(mainScrollPane.heightProperty());
		lessonsParentPane.minWidthProperty().bind(lessonsScrollPane.widthProperty().subtract(20));
		lessonsParentPane.maxWidthProperty().bind(lessonsScrollPane.widthProperty().subtract(20));
		lessonsBorderPane.minHeightProperty().bind(theV.heightProperty().subtract(63));
		lessonsBorderPane.maxHeightProperty().bind(theV.heightProperty().subtract(63));
		
		totalIncomeValue = new NumberTextField(BigDecimal.valueOf(Double.MAX_VALUE));
		totalDiscountValue = new NumberTextField(BigDecimal.valueOf(Double.MAX_VALUE));
		totalsGrid.add(totalIncomeValue, 1, 6);
		totalsGrid.add(totalDiscountValue, 1, 5);
	}
	
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	@Override
	protected void changeLocale() {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		studentName.setText(labels.getString("billing.info.stu"));
		className.setText(labels.getString("billing.info.class"));
		monthName.setText(labels.getString("billing.info.month"));
		totalsTitle.setText(labels.getString("billing.info.totals"));
		numOfLessonsLbl.setText(labels.getString("billing.info.alll"));
		canceledLessonsLbl.setText(labels.getString("billing.info.canceledl"));
		totalExtraLessonsLbl.setText(labels.getString("billing.info.extral"));
		expectedIncomeLbl.setText(labels.getString("billing.info.expectedl"));
		totalDiscountLbl.setText(labels.getString("billing.info.totaldisc"));
		totalIncomeLbl.setText(labels.getString("billing.info.actual"));
	}
	
	public void setStudent(Student selected){
		selectedStudent = selected;
		
		Dialog paymentDialog = BillingDialog.getInstance(AUTO_COMPLETE_FLAG.AUTO_COMPLETE);
		
		System.out.println("------------------ dialog clicked ");
		System.out.println("Student was Set - Will Show:");
		try {
			ScreenNavigator.getInstance(null).goToScreen("BillingInfo");
			monthLbl.setText(LocalDate.now().getMonth().toString());
			if(! selectedStudent.getClasses().isEmpty()){
				classNameLbl.setText(selectedStudent.getClasses().get(0).getName());
			}
			studentNameLbl.setText(selectedStudent.getFullName());
			SortedList<Aclass> sortedClassesList = createSortedClasses();
//                ArrayList<Billing> monthsBillings = new ArrayList<>();
			int totalNumOfLessons = 0;
			int canceledLessons = 0;
			int totalNumOfExtraLessons = 0;
			BigDecimal totalEarnings = BigDecimal.ZERO;
			BigDecimal totalDiscount = BigDecimal.ZERO;
			BigDecimal expectedIncome = BigDecimal.ZERO;
			totals.resetTotals();
			paneBillingMethodsController.setTotals(totals);
			for(Aclass theClass : sortedClassesList){
//                BillingTables classesTables = new BillingTables(mainScrollPane);
				
				boolean isEven = true;
				lessonsParentPane.getChildren().clear();
				
				if(! theClass.isPaidPerMonth()){
					totalIncomeValue.setEditable(false);
					totalDiscountValue.setEditable(false);
					totalIncomeValue.setMouseTransparent(true);
					totalDiscountValue.setMouseTransparent(true);
					totalIncomeValue.setMaxValue(BigDecimal.valueOf(Double.MAX_VALUE));
					totalDiscountValue.setMaxValue(BigDecimal.valueOf(Double.MAX_VALUE));
					totalIncomeValue.setText("0.00");
					totalDiscountValue.setText("0.00");
					totalIncomeValue.setNumber(BigDecimal.ZERO);
					totalDiscountValue.setNumber(BigDecimal.ZERO);
				}else{
					totalIncomeValue.setEditable(true);
					totalDiscountValue.setEditable(true);
					totalIncomeValue.setMouseTransparent(false);
					totalDiscountValue.setMouseTransparent(false);
					BillingMonth billingMonth = theClass.getBillingMonth(LocalDate.now().getMonth(), Year.of(LocalDate.now().getYear()));
					System.out.println("Billing Month full Amount = "+ billingMonth.getAmountFull());
					BigDecimal totalPaid = billingMonth.getTotalAmountPaid();
					BigDecimal totalMonthDiscount = billingMonth.getTotalDiscount();
					System.out.println("Billing Month paid Amount = "+ totalPaid);
					System.out.println("Billing Month Discount Amount = "+ totalMonthDiscount);
					
					totalDiscountValue.setText(myFormater.numberToString(totalMonthDiscount));
					totalDiscountValue.setNumber(billingMonth.getTotalDiscount());
					totalIncomeValue.setText(myFormater.numberToString(totalPaid));
					totalIncomeValue.setNumber(totalPaid);
					totalDiscountValue.setMaxValue(billingMonth.getAmountFull());
					totalIncomeValue.setMaxValue(billingMonth.getAmountFull());
					
					expectedIncomeValue.setText(myFormater.numberToString(billingMonth.getAmountFull()));
//                    totalIncomeValue.numberProperty().addListener(new ChangeListener<BigDecimal>() {
//                        @Override
//                        public void changed(ObservableValue<? extends BigDecimal> observable, BigDecimal oldValue, BigDecimal newValue) {
//                            if(oldValue.compareTo(newValue) == 0){
//                                System.out.println("Amount paid edited - "+ newValue+" |" +oldValue);
//                                billingMonthAmountPaidEdited(billingMonth, newValue);
//                            }
//                        }
//                    });
//                    totalDiscountValue.numberProperty().addListener(new ChangeListener<BigDecimal>() {
//                        @Override
//                        public void changed(ObservableValue<? extends BigDecimal> observable, BigDecimal oldValue, BigDecimal newValue) {
//                            if(oldValue.compareTo(newValue) != 0){
//                                System.out.println("Discount edited - "+ newValue+" |" +oldValue);
//                                billingMonthDiscountEdited(billingMonth, newValue);
//                            }
//                        }
//                    });
				}
				SortedList<Lesson> monthsLessons = createSortedLessons(theClass);
				
				for(Lesson aLesson : monthsLessons){
					if(aLesson.isExtraLesson()){
						totalNumOfExtraLessons++;
					}
					totalNumOfLessons++;
					Billing lessonsBilling = aLesson.getBilling().get(selectedStudent.getId());
					PaymentDetails remainingPayment = lessonsBilling.getRemainingPayment();
					
					LessonBillingInfo lessonGrid = new LessonBillingInfo();
					if(isEven){
						lessonGrid.setStyle("-fx-background-color: #C2C2C2;");
					}else{
						lessonGrid.setStyle("-fx-background-color: #E4EDED;");
					}
					totalDiscount = totalDiscount.add(lessonsBilling.getTotalDiscount());
					totalEarnings = totalEarnings.add(lessonsBilling.getTotalPaidAmount());
					expectedIncome = expectedIncome.add(remainingPayment.getTotalAmount());
					
					if(aLesson.getTheDate().compareTo(Constants.UNKOWN_DATE) == 0 ){
						lessonGrid.getDateLbl().setText(LanguageUtils.getString("billing.info.dateNot"));
					}else{
						lessonGrid.getDateLbl().setText(LanguageUtils.getString("billing.info.dateColon")+aLesson.getTheDate().format(dateFormater));
					}
					
					
					canceledLessons = setCanceledLabel(aLesson, canceledLessons, lessonGrid);
					if(! theClass.isPaidPerMonth()){
						lessonGrid.getAmountFullValue().setText(myFormater.numberToString(remainingPayment.getTotalAmount()));
//                                lessonGrid.getAmountPaidValue().setText(myFormater.numberToString(thisBilling.getAmountPaid()));
//                                lessonGrid.getAmountPaidValue().setNumber(thisBilling.getAmountPaid());
//                                lessonGrid.getAmountPaidValue().setMaxValue(thisBilling.getAmountFull());
					}else{
						lessonGrid.getBillingInfoGrid().setVisible(false);
					}
					
					setAttendanceLabel(aLesson, selectedStudent.getId(), lessonGrid);
					
//                            lessonGrid.getAmountPaidValue().numberProperty().addListener(new ChangeListener<BigDecimal>() {
//                                @Override
//                                public void changed(ObservableValue<? extends BigDecimal> observable, BigDecimal oldValue, BigDecimal newValue) {
//                                    if(newValue.doubleValue() != (oldValue.doubleValue())){
////                                        amountPaidEdited(thisBilling, lessonGrid, ((BigDecimal)(oldValue)), ((BigDecimal)(newValue)));
//                                    }
//                                }
//                            });
					lessonGrid.getPayBtn().setOnAction((ActionEvent event) -> {
						paymentDialog.setOnShown((Event showEvent) -> {
							System.out.println("Billing (inti):");
							System.out.println("  paid: "+lessonsBilling.getTotalPaidAmount());
							System.out.println("  discount: "+lessonsBilling.getTotalDiscount());
							System.out.println("  full: "+lessonsBilling.getAmountFull());
							BillingDialogScene.getController().initValues(remainingPayment);
						});
						Optional <PaymentDetails> details = paymentDialog.showAndWait();
						if(details.isPresent()){
							
							if(checkPaymentDetailsAndSaveBilling(details.get(), lessonsBilling)){
								boolean isPaidInFull = lessonsBilling.isPaidInFull();
								if(isPaidInFull){
									lessonGrid.getPayBtn().getStyleClass().add("custom_button_paid");
									lessonGrid.getPayBtn().setText(LanguageUtils.getString("billing.info.paid"));
								}else{
									lessonGrid.getPayBtn().getStyleClass().remove("custom_button_paid");
									lessonGrid.getPayBtn().setText(LanguageUtils.getString("billing.info.pay"));
								}
								lessonGrid.getPaymentDetailsLbl().setText(details.get().toString());
								
								System.out.println("thisBilling.isPaidInFull()= "+ isPaidInFull);
								paneBillingMethodsController.editTotals(aLesson, lessonsBilling, Constants.INCREASE);
							}
						}
					});
					
					PaymentDetails temp = new PaymentDetails();
					temp = temp.withDiscount(lessonsBilling.getTotalDiscount())
							.withAmountPaid(lessonsBilling.getTotalPaidAmount())
							.withTotalAmount(lessonsBilling.getAmountFull());
					lessonGrid.getPaymentDetailsLbl().setText(temp.toString());
					
					
					lessonGrid.getClassCodeLbl().setText(LanguageUtils.getString("billing.info.code")+theClass.getCodeName());
					lessonGrid.getDurationLbl().setText(Integer.toString(aLesson.getDuration()) +LanguageUtils.getString("billing.info.mins"));
					if(lessonsBilling.isPaidInFull()){
//                                 lessonGrid.getPayBtn().getStyleClass().clear();
						lessonGrid.getPayBtn().getStyleClass().add("custom_button_paid");
						lessonGrid.getPayBtn().setText(LanguageUtils.getString("billing.info.paid"));
					}
					else{
						totals.unpaidLessons++;
						totals.unpaidDuration += (long)aLesson.getDuration();
						totals.unpaidAmount = totals.unpaidAmount.add(lessonsBilling.getAmountFull());
					}
					
					
					VBox.setMargin(lessonGrid, new Insets(1, 0, 0, 0));
					lessonsParentPane.getChildren().add(lessonGrid);
					
//                            monthsBillings.add(thisBilling);
//                            System.out.println("     "+aLesson.getTheDate() +":"+ thisBilling.getAmountPaid1() +" / "+ thisBilling.getAmountFull());
//                            System.out.println("     Attendance : "+ aLesson.getAttendance().get(theID).getStatus()+ " (1 == YES)");
					isEven = !isEven;
					
					
				}
			}
			
			numOfLessonsValue.setText(Integer.toString(totalNumOfLessons));
			canceledLessonsValue.setText(Integer.toString(canceledLessons));
			totalExtraLessonsValue.setText(Integer.toString(totalNumOfExtraLessons));
			totalDiscountValue.setNumber(totalDiscount);
			totalDiscountValue.setText(myFormater.numberToString(totalDiscount));
			totalIncomeValue.setNumber(totalEarnings);
			totalIncomeValue.setText(myFormater.numberToString(totalEarnings));
			expectedIncomeValue.setText(myFormater.numberToString(expectedIncome));
			System.out.println("setting Totals "+totals.unpaidAmount);
			paneBillingMethodsController.setTotals(totals);
			
			
		} catch (IOException ex) {
			System.err.println("Exception while going to screen - BillingInfo");
			ex.printStackTrace(System.err);
		}
	}
	
	private SortedList<Aclass> createSortedClasses() {
		SortedList<Aclass> sortedClassesList = new SortedList<>( FXCollections.observableArrayList(selectedStudent.getClasses()),
				(Aclass classA, Aclass classB) -> {
					return classA.getName().compareTo(classB.getName());
				});
		return sortedClassesList;
	}
	
	private SortedList<Lesson> createSortedLessons(Aclass theClass) {
		ArrayList<Lesson> monthsToSort = theClass.getLessonsForAMonth(LocalDate.now().getMonth(), Year.of(LocalDate.now().getYear()));
		SortedList<Lesson> monthsLessons = new SortedList<>( FXCollections.observableArrayList(monthsToSort),
				(Lesson lessonA, Lesson lessonB) -> {
					if( lessonA.getTheDate().isBefore(lessonB.getTheDate() )) {
						return -1;
					} else if(lessonA.getTheDate().isAfter(lessonB.getTheDate() ) ) {
						return 1;
					} else {
						return 0;
					}
				});
		return monthsLessons;
	}
	
	private int setCanceledLabel(Lesson aLesson, int canceledLessons, LessonBillingInfo lessonGrid) {
		if(aLesson.isCanceled()){
			canceledLessons++;
			StringBuilder sb = new StringBuilder();
			
			
			
			
			
			
			
			
			sb.append(LanguageUtils.getString("billing.info.canceled")).append(" ");
			if(aLesson.getCanceledBy() == Lesson.BY_STUDENT){
				if(utils.Utils.isValidID(aLesson.getStudentWhoCanceledID())){
					Student s = (Student)DButils.getObjectBasedOnID(Student.class, aLesson.getStudentWhoCanceledID());
					sb.append(" | ").append(LanguageUtils.getString("billing.info.by"));
					sb.append(s.getFullName());
				}
			}else{
				sb.append(" | ").append(LanguageUtils.getString("billing.info.byteacher"));
			}
			if(utils.Utils.isValidParam(aLesson.getExcuseForCancel())){
				sb.append(" | ");
				sb.append(aLesson.getExcuseForCancel());
				sb.append(" ");
			}
			lessonGrid.getCanceledLbl().setText(sb.toString());
		}else{
			lessonGrid.getCanceledLbl().setText(LanguageUtils.getString("billing.info.notcanceled"));
		}
		return canceledLessons;
	}
	
	private void setAttendanceLabel(Lesson aLesson, Long theID, LessonBillingInfo lessonGrid) {
		switch(aLesson.getAttendance().get(theID).getStatus()){
			case Attendance.NO:
				if(Utils.isValidParam(aLesson.getAttendance().get(theID).getReason())){
					lessonGrid.getAttendanceLbl().setText(
							LanguageUtils.getString("billing.info.stunattend")
									+" | "+ aLesson.getAttendance().get(theID).getReason());
				}else{
					lessonGrid.getAttendanceLbl().setText(LanguageUtils.getString("billing.info.stunattend"));
				}
				break;
			case Attendance.UNKNOWN:
				lessonGrid.getAttendanceLbl().setText(LanguageUtils.getString("billing.info.notknown"));
				break;
			case Attendance.YES:
				lessonGrid.getAttendanceLbl().setText(LanguageUtils.getString("billing.info.stuattend"));
				break;
		}
	}
	private Student selectedStudent;
	
	private boolean isValidAmountPaid(Billing thisBilling, BigDecimal newAmountPaid) {
		return (newAmountPaid.add(thisBilling.getTotalDiscount()).compareTo(thisBilling.getAmountFull()) == 0);
	}
//    private boolean isValidDiscount(Billing thisBilling, BigDecimal newDiscount) {
//        if(thisBilling.getAmount1Paid().compareTo(BigDecimal.ZERO) == 0){
//            return true;
//        }
//        if(newDiscount.add(thisBilling.getAmountPaid()).compareTo(thisBilling.getAmountFull()) == 0){
//            return true;
//        }
//        return false;
//    }
	
	/**
	 * Discount value edited - make necessary changes
	 */
//    private void discountEdited(Billing thisBilling, LessonBillingInfo lessonGrid, BigDecimal oldValue, BigDecimal newValue){
//        System.out.println("DIscount Edited - "+newValue+ " | "+oldValue);
//
//        if(lessonGrid.getDiscountValue().getText().trim().isEmpty()){
//            lessonGrid.getDiscountValue().setText("0.0");
//            lessonGrid.getDiscountValue().numberProperty().set(BigDecimal.ZERO);
//            thisBilling.setAmountDiscount(BigDecimal.ZERO);
//
//            BigDecimal newTotalDiscount = new BigDecimal(totalDiscountValue.getText()).subtract(oldValue);
//            totalDiscountValue.setText(myFormater.numberToString(newTotalDiscount));
//            totalDiscountValue.setNumber(newTotalDiscount);
//            updateBorderColor(thisBilling, lessonGrid);
//            BillingPersister.saveBillingEdits(thisBilling);
//            return;
//        }
//        if(thisBilling.getAmountDiscount().compareTo(lessonGrid.getDiscountValue().getNumber()) != 0){
//
//            thisBilling.setAmountDiscount(newValue);
//            thisBilling.setAmountPaid(thisBilling.getAmountFull().subtract(thisBilling.getAmountDiscount()));
//            BigDecimal amountPaidDiff = thisBilling.getAmountPaid().subtract(lessonGrid.getAmountPaidValue().numberProperty().get());
//
//            lessonGrid.getAmountPaidValue().setNumber(thisBilling.getAmountPaid());
//            lessonGrid.getAmountPaidValue().setText(myFormater.numberToString(thisBilling.getAmountPaid()));
//            updateBorderColor(thisBilling, lessonGrid);
//            BillingPersister.saveBillingEdits(thisBilling);
//
//            BigDecimal newTotalDiscount = new BigDecimal(totalDiscountValue.getText()).add(newValue.subtract(oldValue));
//            totalDiscountValue.setText(myFormater.numberToString(newTotalDiscount));
//            totalDiscountValue.setNumber(newTotalDiscount);
//
//            if(amountPaidDiff.compareTo(BigDecimal.ZERO) <= 0){
//                BigDecimal newTotalIncome = new BigDecimal(totalIncomeValue.getText()).subtract(amountPaidDiff.abs());
//                totalIncomeValue.setText(myFormater.numberToString(newTotalIncome));
//                totalIncomeValue.setNumber(newTotalIncome);
//            }
//            if(amountPaidDiff.compareTo(BigDecimal.ZERO) > 0){
//                BigDecimal newTotalIncome = new BigDecimal(totalIncomeValue.getText()).add(amountPaidDiff.abs());
//                totalIncomeValue.setText(myFormater.numberToString(newTotalIncome));
//                totalIncomeValue.setNumber(newTotalIncome);
//            }
//        }
//    }
//
//    /**
//     * Amount paid was edited - make necessary changes
//     */
//    private void amountPaidEdited(Billing thisBilling, LessonBillingInfo lessonGrid, BigDecimal oldValue, BigDecimal newValue){
//
//        if(lessonGrid.getAmountPaidValue().getText().trim().isEmpty()){
//            lessonGrid.getAmountPaidValue().setText("0.00");
//            lessonGrid.getAmountPaidValue().setNumber(BigDecimal.ZERO);
//            thisBilling.setAmountPaid(BigDecimal.ZERO);
//            BigDecimal newTotalIncome = new BigDecimal(totalIncomeValue.getText()).subtract(oldValue);
//            totalIncomeValue.setText(myFormater.numberToString(newTotalIncome));
//            totalIncomeValue.setNumber(newTotalIncome);
//            updateBorderColor(thisBilling, lessonGrid);
//            BillingPersister.saveBillingEdits(thisBilling);
//            return;
//        }
//        if(thisBilling.getAmountPaid().compareTo(lessonGrid.getAmountPaidValue().getNumber()) != 0){
//            System.out.println("new Value = "+newValue);
//            System.out.println("old Value = "+oldValue);
//            System.out.println("thisBilling.getAmountFull() = "+thisBilling.getAmountFull());
//            System.out.println("thisBilling.getAmountDiscount1 = "+thisBilling.getAmountDiscount());
//
//            thisBilling.setAmountPaid(newValue);
//            thisBilling.setAmountDiscount(thisBilling.getAmountFull().subtract(thisBilling.getAmountPaid()));
//            BigDecimal amountDiscountDiff = thisBilling.getAmountDiscount().subtract(lessonGrid.getDiscountValue().getNumber());
//
//            lessonGrid.getDiscountValue().numberProperty().set(thisBilling.getAmountDiscount());
//            lessonGrid.getDiscountValue().setText(myFormater.numberToString(thisBilling.getAmountDiscount()));
//            updateBorderColor(thisBilling, lessonGrid);
//            BillingPersister.saveBillingEdits(thisBilling);
//
//            BigDecimal newTotalIncome = new BigDecimal(totalIncomeValue.getText()).add(newValue.subtract(oldValue));
//            totalIncomeValue.setText(myFormater.numberToString(newTotalIncome));
//            totalIncomeValue.setNumber(newTotalIncome);
//
//            if(amountDiscountDiff.compareTo(BigDecimal.ZERO) < 0){
//                BigDecimal newDiscount = new BigDecimal(totalDiscountValue.getText()).subtract(amountDiscountDiff.abs());
//                totalDiscountValue.setText(myFormater.numberToString(newDiscount));
//                totalDiscountValue.setNumber(newDiscount);
//            }
//            if(amountDiscountDiff.compareTo(BigDecimal.ZERO) > 0){
//                BigDecimal newDiscount = new BigDecimal(totalDiscountValue.getText()).add(amountDiscountDiff.abs());
//                totalDiscountValue.setText(myFormater.numberToString(newDiscount));
//                totalDiscountValue.setNumber(newDiscount);
//            }
//        }
//    }
//
	/**
	 * fix the color of the grid - red = not paid | green = paid
	 * also updates status of billing NOTE this
	 */
//    private void updateBorderColor(Billing thisBilling, LessonBillingInfo lessonGrid) {
//
//        if(thisBilling.getAmountFull().compareTo(thisBilling.getAmountPaid().add(thisBilling.getAmountDiscount())) == 0){
//            thisBilling.setStatus(Billing.STATUS_PAID);
//            if(lessonGrid.getBillingInfoGrid().getStyleClass().contains("red-border-lesson")){
//                lessonGrid.getBillingInfoGrid().getStyleClass().remove("red-border-lesson");
//            }
//            lessonGrid.getBillingInfoGrid().getStyleClass().add("green-border-lesson");
//        }else{
//            if(lessonGrid.getBillingInfoGrid().getStyleClass().contains("green-border-lesson")){
//                lessonGrid.getBillingInfoGrid().getStyleClass().remove("green-border-lesson");
//            }
//            lessonGrid.getBillingInfoGrid().getStyleClass().add("red-border-lesson");
//        }
//    }
//    private void updateBorderColor(BillingMonth billingMonth) {
//        if(billingMonth.getAmountFull().compareTo(billingMonth.getAmountPaid1().add(billingMonth.getAmountDiscount1())) == 0){
//            if(totalsGrid.getStyleClass().contains("red-border-lesson")){
//                totalsGrid.getStyleClass().remove("red-border-lesson");
//            }
//            totalsGrid.getStyleClass().add("green-border-lesson");
//        }else{
//            if(totalsGrid.getStyleClass().contains("green-border-lesson")){
//                totalsGrid.getStyleClass().remove("green-border-lesson");
//            }
//            totalsGrid.getStyleClass().add("red-border-lesson");
//        }
//    }
//    /**
//     * Discount value edited - make necessary changes
//     */
//    private void billingMonthDiscountEdited(BillingMonth billingMonth, BigDecimal newValue){
//
//        if(totalDiscountValue.getText().trim().isEmpty()){
//            totalDiscountValue.setText("0.0");
//            totalDiscountValue.setNumber(BigDecimal.ZERO);
//            BillingPersister.saveBillingMonthEdits(billingMonth);
//            return;
//        }
//        if(billingMonth.getTotalDiscount().compareTo(totalDiscountValue.numberProperty().get()) != 0){
//
//            billingMonth.setAmountDiscount1(newValue);
//            billingMonth.setAmountPaid1(billingMonth.getAmountFull().subtract(billingMonth.getAmountDiscount1()));
//
//            totalDiscountValue.setNumber(billingMonth.getTotalDiscount());
//            totalDiscountValue.setText(myFormater.numberToString(billingMonth.getAmountDiscount1()));
////            updateBorderColor(billingMonth);
//
//            System.out.println("Save Billing Month Change");
//            BillingPersister.saveBillingMonthEdits(billingMonth);
//            totalIncomeValue.setText(myFormater.numberToString(billingMonth.getAmountPaid1()));
//            totalIncomeValue.setNumber(billingMonth.getAmountPaid1());
//        }
//    }
//
//    /**
//     * Discount value edited - make necessary changes
//     */
//    private void billingMonthAmountPaidEdited(BillingMonth billingMonth, BigDecimal newValue){
//
//        if(totalIncomeValue.getText().trim().isEmpty()){
//            System.out.println("totalIncomeValue.text.isEmpty");
//            totalIncomeValue.setText("0.0");
//            totalIncomeValue.setNumber(BigDecimal.ZERO);
//            BillingPersister.saveBillingMonthEdits(billingMonth);
//            return;
//        }
//
//        billingMonth.setAmountPaid1(newValue);
//        billingMonth.setAmountDiscount1(billingMonth.getAmountFull().subtract(billingMonth.getAmountPaid1()));
//
//        totalIncomeValue.setNumber(billingMonth.getAmountPaid1());
//        totalIncomeValue.setText(myFormater.numberToString(billingMonth.getAmountPaid1()));
////        updateBorderColor(billingMonth);
//
//        System.out.println("Save Billing Month Change");
//        BillingPersister.saveBillingMonthEdits(billingMonth);
//        totalDiscountValue.setText(myFormater.numberToString(billingMonth.getAmountDiscount1()));
//        totalDiscountValue.setNumber(billingMonth.getAmountDiscount1());
//    }
	
	private boolean checkPaymentDetailsAndSaveBilling(PaymentDetails paymentDetails, Billing billing) {
//		if(!paymentDetails.isValidPayment() && billing.getPayments().get(0) != null && billing.getPayments().get(0).isValidPayment()){
//			BillingPersister.deleteAllPaymentsAndSaveBilling(billing);
//			return true;
//		}
		if ( !paymentDetails.isValidPayment() && billing.getPayments().isEmpty()) {
			return false;
		}
		else if(paymentDetails.isValidPayment()){
			paymentDetails.setReceiptNo(DButils.getNextReceiptId());
			billing.addPayment(paymentDetails);
			BillingPersister.savePaymentAndBilling(paymentDetails, billing);
			return true;
		}
		return false;
	}
	
	public class UnpaidTotals{
		public UnpaidTotals(){
			unpaidLessons = 0;
			unpaidDuration = Long.valueOf(0);
			unpaidAmount = BigDecimal.ZERO;
		}
		public void resetTotals(){
			unpaidLessons = 0;
			unpaidDuration = Long.valueOf(0);
			unpaidAmount = BigDecimal.ZERO;
		}
		public int unpaidLessons;
		public Long unpaidDuration;
		public BigDecimal unpaidAmount;
	}
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
	}
}
