/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.objects.Schedule;
import DB.objects.Teacher;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import services.TransitionService;
import utils.TimeTableUtils;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class TimeTableController extends StageController{
    private TransitionService service;
    private final URL fxmlURL;
    private final ResourceBundle resources;
    
    public TimeTableController(URL fxml, ResourceBundle res) {
        super();
        labels = res;
        fxmlURL = fxml;
        resources = res;
    }
    @FXML private BorderPane region;
    @FXML private ProgressIndicator progress;
    @FXML private AnchorPane root;
    @FXML private AnchorPane rightPane;
    @FXML private SplitPane splitPane;
    @FXML private VBox theV;
    @FXML private ScrollPane theScroll;
    @FXML private ChoiceBox<String> choice;
    @FXML private Label numOfClasses;
    @FXML private Label nameOfTeacher;
    @FXML private GridPane timeTable;
    
    private Teacher theTeacher;
    private Schedule sle1;
    private Schedule sle2;
    private Schedule sle3;
    private ArrayList<Schedule> schedules = new ArrayList<>(3);
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.prefWidthProperty().bind(theScroll.widthProperty());
        choice.getItems().clear();
        choice.getItems().add("Teacher");
        choice.valueProperty().addListener(
            new ChangeListener<String>() {
                public void changed(ObservableValue observable, 
                                    String oldVal, String newVal) {
                System.out.println(" New Value = "+ newVal);
                
                theTeacher = utils.Utils.getTeachersList(1).get(0);
                nameOfTeacher.setText(theTeacher.getBasicInfo().getFname()+ " "+ theTeacher.getBasicInfo().getLname());
                sle2 = utils.Utils.createSchedule2x2(0, 3); // 2 timeSlots
                sle1 = utils.Utils.createSchedule1x2(-1); // 1 timeSlot
                sle3 = utils.Utils.createSchedule3x1(-1, 3, 5); // 3 timeSlots
                schedules.add(sle1); schedules.add(sle2); schedules.add(sle3);
                numOfClasses.setText("3");
                }
            });
        TimeTableUtils.initializeCells(timeTable);
//        Iterator <Node> it = (Iterator)timeTable.getChildren().iterator();
//        System.out.println("Labels: ");
//        while(it.hasNext()){
//            Node n = it.next();
//            Label l = (Label)n;
//            System.out.println("  "+ l.getText());
//        }
//        System.out.println("________");
    }
    
    
    @Override
    protected void changeLocale(){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
//        if(theTable == null)
//            return;
    }
    
    @FXML
    private void closeWindow(Event event) throws IOException {
        stage.close();
    }
    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader); 
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
	@Override
	public void onLoad() {
	}
	@Override
	public void unload(){}
}
    