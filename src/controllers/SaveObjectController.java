/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import images.icons.Icons;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author d.michaelides
 */
public class SaveObjectController implements Initializable {
    @FXML ImageView image;
    @FXML TextArea message;
    @FXML HBox buttons;
    @FXML Button button_retry;
    @FXML Button button_clear;
    @FXML Button button_ok;
    
    int returnValue = 0;
    public int getReturnValue(){
        return returnValue;
    }
    
    
    
    public void init(boolean success, String text) {
        if(success){
            button_retry.setVisible(false);
            image.setImage(new Image(Icons.class.getResource("yes_big.png").toString(),100,100,true,true));
        }else{
            image.setImage(new Image(Icons.class.getResource("no_big.png").toString(),100,100,true,true));
        }
        message.setText(text);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    private void retryPressed(Event event) throws IOException {
        returnValue = 1;
        returnToMainScreen(event);
    }
    
    @FXML
    private void okPressed(Event event) throws IOException {
        returnValue = 2;
        returnToMainScreen(event);
    }
    
    @FXML
    private void clearPressed(Event event) throws IOException {
        returnValue = 3;
        returnToMainScreen(event);
    }
    
    private void returnToMainScreen(Event event) throws IOException {
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
}
