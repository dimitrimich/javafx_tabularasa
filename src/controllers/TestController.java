/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Controller that i used to test including external FXML to another FXML
 * @author dimitris
 */
public class TestController implements Initializable{
    @FXML private Label copyFrom;
    @FXML private Label copyTo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Initializing Test Controller");
    }
    
    public void copyLabels(){
        copyTo.setText(copyFrom.getText());
        copyFrom.setText("");
        System.out.println("Copy Done");
    }
    
    
    public void copyLabelsBack(){
        copyFrom.setText(copyTo.getText());
        copyTo.setText("");
        System.out.println("Copy them back");
    }
    @FXML 
    private void copyBack(Event e){
        copyLabelsBack();
    }
    
}
