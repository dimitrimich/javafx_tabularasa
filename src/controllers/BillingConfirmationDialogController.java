/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import customObjects.MyDecimalFormater;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import screens.FXMLs;
import utils.LanguageUtils;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class BillingConfirmationDialogController extends Stage implements Initializable {
	
	@FXML private Label amountReceivedText;
	@FXML private Label amountDiscountText;
	@FXML private CheckBox genReceiptCheckBox;
	@FXML private Label titleLabel, receivedLabel, withDiscountLabel;
	
	
	public BillingConfirmationDialogController(String defaultName, String defaultAddress, BigDecimal total)
	{
		setTitle(LanguageUtils.getString("billing.confirm.title"));
		FXMLLoader fxmlLoader = new FXMLLoader(FXMLs.class.getResource("BillingConfirmationDialog.fxml"));
		fxmlLoader.setController(this);
		fxmlLoader.setResources(LanguageUtils.getInstance().getCurrentLanguage());
		// Nice to have this in a load() method instead of constructor, but this seems to be de-facto standard.
		try
		{
			setScene(new Scene((Parent) fxmlLoader.load()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}
	
	
	public boolean isGenerateReceipt(){
		return genReceiptCheckBox.isSelected();
	}
	
	public void setAmounts(BigDecimal received, BigDecimal discount){
		MyDecimalFormater format = new MyDecimalFormater();
		amountReceivedText.setText(format.numberToString(received));
		amountDiscountText.setText(format.numberToString(discount));
	}
}
