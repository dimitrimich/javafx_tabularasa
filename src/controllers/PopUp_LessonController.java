/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package controllers;

import DB.LessonsPersister;
import DB.objects.Attendance;
import DB.objects.Lesson;
import DB.objects.Student;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import jfxtras.agenda.Agenda.Appointment;
import org.controlsfx.control.PopOver;
import screens.FXMLs;
import utils.Utils;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class PopUp_LessonController extends Stage implements Initializable {
	private PopOver thePopUp;
	@FXML private Label className;
	@FXML private Label teacherName;
	@FXML private Label date;
	@FXML private Label time;
	@FXML private Accordion theAccordion;
	
	@FXML private CheckBox isCanceled;
	@FXML private GridPane cancelGrid;
	@FXML private ChoiceBox <String> teacherOrStudent;
	@FXML private ChoiceBox <Student> whichStudent;
	@FXML private Label whichStudentLabel;
	@FXML private Label reasonForCancel;
	@FXML private TextField reasonForCancelText;
	@FXML private TableView<Attendance> attendanceTable;
	private Appointment currentAppointment;
	
	public void initFirstPanel(Appointment app){
		currentAppointment = app;
		Lesson lesson = app.getLesson();
		theAccordion.setExpandedPane(theAccordion.getPanes().get(0));
		className.setText(lesson.getTheClass().getName());
		if(lesson.getTheClass().getTeacher() != null){
			teacherName.setText(Utils.notNull(lesson.getTheClass().getTeacher().getFullName()));
		}else{
			teacherName.setText("---");
		}
		date.setText(Utils.notNull(lesson.getTheDate().toString()));
		time.setText(Utils.notNull(lesson.getStartingTime()));
		whichStudent.setItems(FXCollections.observableArrayList(lesson.getTheClass().getStudents()));
		
		if(lesson.isCanceled()){
			isCanceled.setSelected(true);
			switch(lesson.getCanceledBy()){
				case Lesson.BY_TEACHER: teacherOrStudent.getSelectionModel().select(0);break;
				case Lesson.BY_STUDENT: teacherOrStudent.getSelectionModel().select(1);break;
				default: teacherOrStudent.getSelectionModel().clearSelection(); break;
			}
			reasonForCancelText.setText(lesson.getExcuseForCancel());
			Long idThatCanceled = lesson.getStudentWhoCanceledID();
			if(idThatCanceled != null){
				for(Student aStudent : lesson.getTheClass().getStudents()){
					if(Long.compare(idThatCanceled, aStudent.getId()) == 0){
						whichStudent.getSelectionModel().select(aStudent);
					}
				}
			}
		}else{
			isCanceled.setSelected(false);
			teacherOrStudent.getSelectionModel().clearSelection();
			reasonForCancelText.setText("");
			whichStudent.getSelectionModel().clearSelection();
		}
	}
	
	public PopUp_LessonController(String defaultName, String defaultAddress, Parent parent)
	{
		setTitle("Lesson Details");
		FXMLLoader fxmlLoader = new FXMLLoader(FXMLs.class.getResource("PopUp_Lesson.fxml"));
		fxmlLoader.setController(this);
		try
		{
			setScene(new Scene((Parent) fxmlLoader.load()));
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
	}
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		Callback<TableColumn<Attendance, Integer>, TableCell<Attendance, Integer>> checkBoxFactory
				= (TableColumn<Attendance, Integer> p) -> new CheckBoxCell();
		TableColumn checkBoxCol = new TableColumn("Attending");
		checkBoxCol.setEditable(true);
		checkBoxCol.setCellFactory(checkBoxFactory);
		checkBoxCol.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Attendance, Integer>, SimpleIntegerProperty>() {
					@Override
					public SimpleIntegerProperty call(TableColumn.CellDataFeatures<Attendance, Integer> data) {
						return data.getValue().getStatusProp();
					}
				});
		
		TableColumn studentInfoCol = new TableColumn("Student");
		TableColumn reasonCol = new TableColumn("Reason");
		
		Callback<TableColumn<Attendance, String>, TableCell<Attendance, String>> cellFactory
				= (TableColumn<Attendance, String> p) -> new EditingCell();
		
		reasonCol.setCellFactory(cellFactory);
//        reasonCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Attendance, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<Attendance, String> t) {
//                 ((Attendance) t.getTableView().getItems().get(
//                    t.getTablePosition().getRow())
//                    ).setReason(t.getNewValue());
//            }
//        });
		reasonCol.setEditable(true);
		reasonCol.setMinWidth(45);
		reasonCol.setCellValueFactory(
				new PropertyValueFactory<>("reasonProp")
		);
		studentInfoCol.setCellValueFactory(
				new PropertyValueFactory<>("studentDetailsProp")
		);
		studentInfoCol.setEditable(false);
		
		attendanceTable.setEditable(true);
		
		attendanceTable.getColumns().addAll(checkBoxCol, reasonCol, studentInfoCol);
		
		isCanceled.selectedProperty().addListener(
				(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
					cancelGrid.setDisable(!newValue);
					currentAppointment.getLesson().setCanceled(newValue);
					currentAppointment.setCanceledProperty(newValue);
				}
		);
		ObservableList<String> bywhom =FXCollections.observableArrayList();
		bywhom.add("Teacher"); bywhom.add("Student");
		teacherOrStudent.setItems(bywhom);
		teacherOrStudent.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			switch(newValue.intValue()){
				case 0: whichStudentLabel.setDisable(true); whichStudent.setDisable(true); break;
				case 1: whichStudentLabel.setDisable(false); whichStudent.setDisable(false); break;
			}
		});
		whichStudent.getItems().clear();
		
	}
	
	public void setThePopUp(PopOver thePopUp) {
		this.thePopUp = thePopUp;
		// when PopUP is hidden save all the changes made to Lesson
		this.thePopUp.setOnHiding((WindowEvent event) -> {
		});
	}
	
	public void saveChanges(){
		if(currentAppointment == null || currentAppointment.getLesson() == null){return;}
		Lesson theLesson = currentAppointment.getLesson();
		switch(teacherOrStudent.getSelectionModel().getSelectedIndex()){
			case 0: theLesson.setCanceledBy(Lesson.BY_TEACHER); break;
			case 1: theLesson.setCanceledBy(Lesson.BY_STUDENT); break;
		}
		if(theLesson.getCanceledBy() == Lesson.BY_STUDENT){
			if(!whichStudent.getSelectionModel().isEmpty()){
				theLesson.setStudentWhoCanceledID(whichStudent.getSelectionModel().getSelectedItem().getId());
			}
		}
		theLesson.setExcuseForCancel(Utils.notNull(reasonForCancelText.getText().trim()));
		
		LessonsPersister persister = new LessonsPersister();
		persister.saveLessonDetails(theLesson);
	}
	
	public void setTableItems(ArrayList<Attendance> items){
		System.out.println("Set items - "+items.size());
		for(Attendance att : items){
			if(att == null){
				System.out.println("Att is null1?!");
			}else{
				if(att.getReason() == null){
					System.out.println("att.getReason() is null1?!");
				}
				if(att.getStudentDetailsProp() == null){
					System.out.println("att.getStudentDetailsProp() is null1?!");
				}else{
					
					if(att.getStudentDetailsProp() == null){
						System.out.println("att.getStudentDetailsProp.getValueSafe() is null1?!");
					}else{
						
					}
				}
				
			}
			System.out.println("|"+att.getReason()+ " | "+ att.getStudentDetailsProp()+"|");
		}
		attendanceTable.getItems().clear();
		attendanceTable.setItems(FXCollections.observableArrayList(items));
	}
	
	private class EditingCell extends TableCell<Attendance, String> {
		
		private TextField textField;
		
		public EditingCell() {
		}
		
		@Override
		public void startEdit() {
			if (!isEmpty()) {
				super.startEdit();
				createTextField();
				setText(null);
				setGraphic(textField);
				textField.selectAll();
				textField.requestFocus();
			}
		}
		
//        @Override
//        public void cancelEdit() {
//            super.cancelEdit();
//
//            setText((String) getItem());
//            setGraphic(null);
//        }
		
		@Override
		public void commitEdit(String newValue) {
			System.out.println("Commit Edit");
			super.commitEdit(newValue); //To change body of generated methods, choose Tools | Templates.
			setText(newValue);
			setGraphic(null);
			if(getTableView().getItems().size() >= getTableRow().getIndex()){
				((Attendance) getTableView().getItems().get(getTableRow().getIndex())).setReason(newValue);
			}
		}
		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			
			if (empty) {
				setText(null);
				setGraphic(null);
			} else {
				if (isEditing()) {
					if (textField != null) {
						textField.setText(getString());
					}
					setText(null);
					setGraphic(textField);
				} else {
					setText(getString());
					setGraphic(null);
				}
			}
		}
		
		private void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()* 2);
			textField.focusedProperty().addListener(
					(ObservableValue<? extends Boolean> arg0,
							Boolean arg1, Boolean arg2) -> {
						if (!arg2) {
							commitEdit(textField.getText());
						}
					});
			textField.setOnKeyReleased((KeyEvent event) -> {
				if(event.getCode() == KeyCode.ENTER){
					commitEdit(textField.getText());
				}
			});
		}
		
		private String getString() {
			return getItem() == null ? "" : getItem().toString();
		}
	}
	private class CheckBoxCell extends TableCell<Attendance, Integer> {
		
		private CheckBox check;
		{
			check = new CheckBox();
			check.setAllowIndeterminate(true);
			
			check.indeterminateProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					System.out.println("isIndeterminate = "+ check.isIndeterminate());
					if(check.isIndeterminate()){
						if(getTableRow() != null){
							((Attendance) getTableView().getItems().get(getTableRow().getIndex())).setStatusProp(3);
						}
						setItem((3));
					}
				}
			});
			check.selectedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					System.out.println("selected  = "+ check.isSelected());
					if(check.isSelected()){
						if(getTableRow() != null){
							((Attendance) getTableView().getItems().get(getTableRow().getIndex())).setStatusProp(1);
						}
						setItem((1));
					}else{
						if(getTableRow() != null){
							((Attendance) getTableView().getItems().get(getTableRow().getIndex())).setStatusProp(2);
						}
						setItem((2));
					}
				}
			});
		}
		
		public CheckBoxCell() {
		}
		
		@Override
		public void updateItem(Integer item, boolean empty) {
			super.updateItem(item, empty);
			if (empty) {
				setText(null);
				setGraphic(null);
			} else {
				if(item != null){
					switch(item){
						case 1: check.setSelected(true); setGraphic(check);break;
						case 2: check.setSelected(false); setGraphic(check);break;
						case 3: check.setIndeterminate(true); setGraphic(check);break;
						default: setGraphic(null); break;
					}
					setText(null);
				}else{
					setText(null);setGraphic(null);
				}
			}
		}
	}
}
