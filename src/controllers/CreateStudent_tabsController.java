/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.objects.Student;
import images.Images;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javax.sql.rowset.serial.SerialBlob;
import utils.LanguageUtils;
import utils.Utils;

/**
 *
 * @author d.michaelides
 */
public class CreateStudent_tabsController extends StageController{
    
    private final URL fxmlURL;
    private final ResourceBundle resources;
    
    public CreateStudent_tabsController(URL fxml, ResourceBundle res) {
        super();
        fxmlURL = fxml;
        resources = res;
    }
    @FXML private TextField phone1Text;
    @FXML private TextField phone2Text;
    @FXML private TextArea address1Text;
    @FXML private TextArea address2Text;
    @FXML private TextField nameText;
    @FXML private TextField surnameText;
    @FXML private TextField emailText;
    @FXML private Tab basicinfo;
    @FXML private Tab contactinfo;
    @FXML private Label name;
    @FXML private Label surname;
    @FXML private Label address1;
    @FXML private Label address2;
    @FXML private Label phone1;
    @FXML private Label phone2;
    @FXML private Label email;
    @FXML private ImageView language;
    @FXML private ImageView image;
    private Image img;
    
    private Student student;
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        nameText.addEventFilter(KeyEvent.KEY_TYPED, EventFilters.getNoNumbersFilter());
        student = new Student();
    }
    
    @FXML
    private void changeLanguage(Event event) throws IOException {
        labels = LanguageUtils.getInstance().switchLanguage();
        if(LanguageUtils.getInstance().getCurrentLanguage().getLocale() == Locale.ENGLISH){
            img = new Image(Images.class.getResource("UnitedKingdom.png").toString());
        }else{
            img = new Image(Images.class.getResource("Greece.png").toString());
        }
        language.setImage(img);
        changeLocale();
    }
    
    @Override
    protected void changeLocale(){
        
        basicinfo.setText(labels.getString("basic_information"));
        contactinfo.setText(labels.getString("contact_information"));
        name.setText(labels.getString("name"));
        surname.setText(labels.getString("surname"));
        address1.setText(labels.getString("address1"));
        address2.setText(labels.getString("address2"));
        phone1.setText(labels.getString("phone1"));
        phone2.setText(labels.getString("phone2"));
        email.setText(labels.getString("email"));
        nameText.setPromptText(labels.getString("insertname"));
        surnameText.setPromptText(labels.getString("insertsurname"));
        phone1Text.setPromptText(labels.getString("insertphone"));
        phone2Text.setPromptText(labels.getString("insertphone"));
        address1Text.setPromptText(labels.getString("addressinfo"));
        address2Text.setPromptText(labels.getString("addressinfo"));
        emailText.setPromptText(labels.getString("insertemail")); 
    }
    
    @FXML 
    private void addImage(Event e){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(LanguageUtils.getInstance().getCurrentLanguage().getString("selectphoto"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
             Path path = Paths.get(file.getAbsolutePath());
            ByteArrayInputStream is = null;
            try {
                System.out.println(" read file");
                byte[] data = Files.readAllBytes(path);
                System.out.println(" read file 2 ");
                
                SerialBlob thePic = new SerialBlob(data);
                student.setPicture(thePic);
                System.out.println(" Serial Blob 2 ");
                
                is = new ByteArrayInputStream(data);
                Image img = new Image(is);
                image.setImage(img);
   //            openFile(file);
            } catch (Exception ex) {
                System.err.println("----  EX  ---");
                ex.printStackTrace(System.err);
            }
            finally{
                try{
                    is.close();
                }catch(Exception ex){  System.err.println("EXCEPTION 345356");}
            }
        }
    }
    @FXML
    private void saveStudent(Event e){
        boolean thereIsError = false;
        if(!Utils.isValidParam(nameText.getText())){
            Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
            thereIsError = true;
        }
        if(!Utils.isValidParam(surnameText.getText())){
            Utils.turnNodeRed(surnameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.surname"));
            thereIsError = true;
        }
        
//        
//        student.setFirstname(nametext.getText());
//        student.setLastname(surnametext.getText());
//        student.setEmail(emailtext.getText());
//        
//        DButils.saveObject(student);
    }
    
//    @FXML
//    private void noNumbers(KeyEvent k){
//        System.out.println("--- key---");
//        System.out.println("  toString() = " + k.toString());
//        System.out.println("  getCharacter() = " + k.getCharacter());
//        System.out.println("  getCode() = " + k.getCode());
//        System.out.println("  toString() = " + k.getText());
//    }
    
    
    
    @FXML
    private void goToHomePage(Event event) throws IOException {
//        service = new TransitionService("HomePage", null, myFXMLLoader, stage);
//        service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
//            @Override
//            public void handle(WorkerStateEvent t) {
//                 stage.setScene(new Scene (service.getValue().getRoot(), service.getValue().getX(), service.getValue().getY()));
//            }});
//        
////        region.visibleProperty().bind(service.runningProperty());
////        progress.visibleProperty().bind(service.runningProperty());
////        progress.progressProperty().bind(service.progressProperty());
//        service.start();
    }

    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader); 
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
	}
}
 
    