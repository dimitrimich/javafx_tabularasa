/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import jfxtras.scene.control.LocalDateTimePicker;
import jfxtras.scene.control.agenda.Agenda;
import jfxtras.scene.control.agenda.Agenda.Appointment;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class TempController extends StageController {

    private final URL fxmlURL;
    private final ResourceBundle resources;
    private Agenda agenda;

    public TempController(URL fxml, ResourceBundle res) {
        super();
        super.setLanguageUsed(res);
        fxmlURL = fxml;
        resources = res;
    }

    @FXML
    private AnchorPane root;
    @FXML
    private ImageView image;
    @FXML
    private VBox theV;
    @FXML
    private DatePicker datePicker;

    @FXML
    private ScrollPane theScroll;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("TEMP init ");
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.prefWidthProperty().bind(theScroll.widthProperty());
        datePicker.setValue(LocalDate.now());
        datePicker.valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            LocalDateTime l = LocalDateTime.of(newValue.getYear(), newValue.getMonth(), newValue.getDayOfMonth(),0, 0);
            agenda.setDisplayedLocalDateTime(l);
        });
        setUp();
        theV.getChildren().add(agenda);
    }

    @Override
    protected void changeLocale() {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
    }

    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader);
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent) loader.load());
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }

    @Override
    public void onLoad() {
    }

    @Override
    public void unload() {
    }

    static private Calendar getFirstDayOfWeekCalendar(Locale locale, Calendar c) {
        // result
        int lFirstDayOfWeek = Calendar.getInstance(locale).getFirstDayOfWeek();
        int lCurrentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        int lDelta = 0;
        if (lFirstDayOfWeek <= lCurrentDayOfWeek) {
            lDelta = -lCurrentDayOfWeek + lFirstDayOfWeek;
        } else {
            lDelta = -lCurrentDayOfWeek - (7 - lFirstDayOfWeek);
        }
        c = ((Calendar) c.clone());
        c.add(Calendar.DATE, lDelta);
        return c;
    }

    private void setUp() {
        agenda = new Agenda();
        agenda.setAllowResize(false);
        agenda.displayedLocalDateTime();    

        // setup appointment groups
        final Map<String, Agenda.AppointmentGroup> lAppointmentGroupMap = new TreeMap<String, Agenda.AppointmentGroup>();
        for (Agenda.AppointmentGroup lAppointmentGroup : agenda.appointmentGroups()) {
            lAppointmentGroupMap.put(lAppointmentGroup.getDescription(), lAppointmentGroup);
        }
        // accept new appointments
        agenda.newAppointmentCallbackProperty().set(new Callback<Agenda.LocalDateTimeRange, Agenda.Appointment>() {
            @Override
            public Agenda.Appointment call(Agenda.LocalDateTimeRange dateTimeRange) {

                System.out.println(dateTimeRange.getStartLocalDateTime().getDayOfWeek()+" --> "+dateTimeRange.getEndLocalDateTime().getDayOfWeek());
                if (dateTimeRange.getStartLocalDateTime().getDayOfMonth() != dateTimeRange.getEndLocalDateTime().getDayOfMonth()) {
                    System.out.println("Different day. Return");
                    return null;
                }
                System.out.println("New Appointment --> from " + dateTimeRange.getStartLocalDateTime() + " to " + dateTimeRange.getEndLocalDateTime());
                return new Agenda.AppointmentImplLocal()
                        .withStartLocalDateTime(dateTimeRange.getStartLocalDateTime())
                        .withEndLocalDateTime(dateTimeRange.getEndLocalDateTime())
                        .withSummary("new")
                        .withDescription("new")
                        .withAppointmentGroup(lAppointmentGroupMap.get("group01"));
            }
        });

        agenda.setActionCallback((appointment) -> {
            System.out.println("agenda, ");
            return null;
        });
    }
}
