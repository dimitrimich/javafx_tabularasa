/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package controllers;

import DB.LessonsPersister;
import DB.objects.Aclass;
import DB.objects.Attendance;
import DB.objects.Lesson;
import DB.objects.ListOfELessons;
import DB.objects.ListOfWLessons;
import DB.objects.Student;
import static controllers.StageController.cachingClassLoader;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Dialog;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import jfxtras.agenda.Agenda;
import jfxtras.agenda.Agenda.CalendarRange;
import jfxtras.agenda.AgendaBuilder;
import jfxtras.agenda.AgendaWeekSkin;
import jfxtras.agenda.AgendaWeekSkin.PopUpContent;
import services.CreateAgenda;
import static utils.ScheduleUtils.getDayOfTheWeekFromLocalDate;
import utils.dialogs.AppointmentDialog;
import utils.dialogs.AppointmentDialog.AppointmentDetails;
import utils.dialogs.PopupLessonDialog;
import utils.dialogs.PopupLessonDialogScene;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class AgendaForClassesController extends StageController {
	@FXML private BorderPane agendaRoot;
	@FXML private ImageView nextWeek;
	@FXML private ImageView prevWeek;
	@FXML private VBox controlBox;
	@FXML private HBox controls;
	@FXML private ScrollPane mainScroll;
	
	private PopUp_LessonController popUpController;
	
	private AgendaBuilder builder;
	private Aclass selectedClass;
	public void setSelectedClass(Aclass sC){
		selectedClass = sC;
	}
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public AgendaForClassesController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	private LessonsPersister dbConnection;
	
	private Dialog <Object> dialog = PopupLessonDialog.getInstance();
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		agendaRoot.minWidthProperty().bind(mainScroll.widthProperty());
		agendaRoot.maxWidthProperty().bind(mainScroll.widthProperty());
		agendaRoot.minHeightProperty().bind(mainScroll.heightProperty());
		agendaRoot.maxHeightProperty().bind(mainScroll.heightProperty());
		
		PopupLessonDialogScene.getInstance();
		popUpController = PopupLessonDialogScene.getController();
		
		CreateAgenda service = new CreateAgenda();
		service.start();
		service.setOnSucceeded(workerStateEvent -> {
			try {
				builder = service.getValue();
				agendaRoot.setCenter(builder.getAgenda());
				controls.getChildren().set(0, builder.getMyCalendarNode());
				// needed to start with correct day(don't know why)
				prevWeek(null); nextWeek(null);
				builder.getAgenda().getDisplayedCalendarProperty().addListener(
						(ObservableValue<? extends Calendar> observable, Calendar oldValue, Calendar newValue) -> {
							System.out.println("in listener ");
							setUpAppointments();
						});
				
			} catch (Exception ex) {
				System.out.println("Error while creating agenda for Classes");
				ex.printStackTrace(System.err);
			}
		});
	}
	public void setUpCallBack(){
		AgendaWeekSkin theSkin = (AgendaWeekSkin)builder.getAgenda().getSkin();
		if(theSkin == null){
			System.out.println("The Skin == null");
		}else{
			System.out.println("set up callback");
			theSkin.setUpCallback(this::createExtraLessonCallback);
		}
	}
	public void setUpAppoinmentCallBack(){
		AgendaWeekSkin theSkin = (AgendaWeekSkin)builder.getAgenda().getSkin();
		if(theSkin == null){
			System.out.println("The Skin == null");
		}else{
			System.out.println("set up callback");
			theSkin.movedAppoinmentCallBack(this::moveAppointment);
		}
	}
	public void setUpPopUpCallBack(){
		System.out.println("set up POP up callback");
		AgendaWeekSkin theSkin = (AgendaWeekSkin)builder.getAgenda().getSkin();
		if(theSkin != null){
			theSkin.setPopUpCallBack(this::showPopUp);
		}
	}
	public void setUpPopUpHideCallBack(){
		System.out.println("set up POP up callback");
		AgendaWeekSkin theSkin = (AgendaWeekSkin)builder.getAgenda().getSkin();
		if(theSkin != null){
			theSkin.setPopUpHideCallBack(this::hidePopUp);
		}
	}
	private Void hidePopUp(Void range){
		if(dialog.isShowing()){
			Stage sb = (Stage)dialog.getDialogPane().getScene().getWindow();//use any one object
			popUpController.saveChanges();
			sb.close();
		}
		return null;
	}
	private Void showPopUp(PopUpContent range){
		if(!dialog.isShowing()){
			ArrayList <Attendance> tableItems = new ArrayList<>();
			popUpController.initFirstPanel(range.getTheAppointment());
			for(Student aStudent : selectedClass.getStudents()){
				tableItems.add(range.getTheAppointment().getLesson().getAttendance().get(aStudent.getId()));
			}
			popUpController.setTableItems(tableItems);
			dialog.show();
		}
		return null;
	}
	
	private Boolean moveAppointment(Agenda.CalendarRange range){
		if(dbConnection == null){dbConnection = new LessonsPersister();}
		
		Lesson lesson = range.getAppointment().getLesson();
		if(lesson.isExtraLesson()){
			return moveExtraLesson(range);
		}else{
			return moveWeeklyLesson(range);
		}
	}
	
	private boolean moveExtraLesson(CalendarRange range){
		if(dbConnection == null){dbConnection = new LessonsPersister();}
//        System.out.println("Start:");
		Calendar start = range.getStartCalendar();
//        System.out.println("Move For all weeks");
//        ScheduleUtils.moveAllLessonsToNewDate(start, selectedClass, range.getAppointment().getLesson().getIndexNumber());
//        System.out.println("Done");
		int startHour = start.get(Calendar.HOUR_OF_DAY);
		int startMin = start.get(Calendar.MINUTE);
		System.out.println("s.YEAR = "+ start.get(Calendar.YEAR));
		System.out.println("s.HOUR_OF_DAY = "+ startHour);
		System.out.println("s.MINUTE = "+ startMin);
		Map <LocalDate, ListOfELessons>theMap = selectedClass.getSchedule().getAllELessons();
		Lesson lesson = range.getAppointment().getLesson();
		
		System.out.println("lesson.getTheDate() = "+ lesson.getTheDate().toString());
		LocalDate oldDate = lesson.getTheDate();
		
		ListOfELessons oldList = theMap.get(oldDate);
		
		LocalDate newDate = LocalDate.of(start.get(Calendar.YEAR), (start.get(Calendar.MONTH)+1),  start.get(Calendar.DATE));
		
		ListOfELessons newList;
		if(theMap.containsKey(newDate)){
			System.out.println("We have a list - "+ newDate.toString());
			newList = theMap.get(newDate);
		}else{
			System.out.println("Create new List - "+ newDate.toString());
			newList = new ListOfELessons();
			newList.setTheDate(newDate);
		}
		
		if(!oldDate.equals(newDate)){
			System.out.println("Dragged in another Day");
			// only change Hour
			if(oldList == null){System.out.println("OLD list is null"); return false;}
			if(oldList.getAllLessons() == null){System.out.println("oldList.getAllLessons() list is null");}
			System.out.println("removed lesson from list "+ oldDate.toString());
			oldList.getAllLessons().remove(lesson);
			newList.getAllLessons().add(lesson);
			System.out.println("Added lesson to list "+ newDate.toString());
			lesson.setTheDate(newDate);
			lesson.setDayOfTheWeek(getDayOfTheWeekFromLocalDate(LocalDate.of(start.get(Calendar.YEAR), (start.get(Calendar.MONTH)+1),  start.get(Calendar.DATE))));
		}else{
			System.out.println("Dragged in the same Day");
		}
		lesson.setStartingHour(startHour);
		lesson.setStartingMinute(startMin);
		if(!lesson.isFullyScheduled() ){
			System.out.println("Lesson NOT fully Scheduled");
		}
		if(start.get(Calendar.YEAR) == 3000){
			System.out.println("Lesson was from the year 3000");
			// undefined set to be NOT fully Scheduled
			lesson.setStartingHour(0);
			lesson.setStartingMinute(0);
			lesson.setDayOfTheWeek(0);
		}
		
		System.out.println("Lessons new details: "+lesson.getTheDate()+" " +lesson.getDayOfTheWeek()+" "+lesson.getStartingTime());
		
		if(!theMap.containsKey(newDate)){
			theMap.put(newDate, newList);
		}
		if(utils.Utils.isValidID(newList.getId())){
			dbConnection.moveExtraLesson(lesson, oldList, newList, selectedClass, true);
		}else{
			dbConnection.moveExtraLesson(lesson, oldList, newList, selectedClass, false);
		}
		return true;
	}
	private boolean moveWeeklyLesson(CalendarRange range){
//        System.out.println("Start:");
		Calendar start = range.getStartCalendar();
		// too complecated
		// also if needed to do this, do it from class overview screen
//        ScheduleUtils.moveAllLessonsToNewDate(start, selectedClass, range.getAppointment().getLesson().getIndexNumber());
//        System.out.println("Done");
		
		
//
		int startHour = start.get(Calendar.HOUR_OF_DAY);
		int startMin = start.get(Calendar.MINUTE);
		System.out.println("s.YEAR = "+ start.get(Calendar.YEAR));
		System.out.println("s.HOUR_OF_DAY = "+ startHour);
		System.out.println("s.MINUTE = "+ startMin);
		Map <LocalDate, ListOfWLessons>theMap = selectedClass.getSchedule().getAllWLessons();
		Lesson lesson = range.getAppointment().getLesson();
		
		System.out.println("lesson.getTheDate() = "+ lesson.getTheDate().toString());
		LocalDate oldDate = lesson.getTheDate();
		
		ListOfWLessons oldList = theMap.get(oldDate);
		
		LocalDate newDate = LocalDate.of(start.get(Calendar.YEAR), (start.get(Calendar.MONTH)+1),  start.get(Calendar.DATE));
		
		ListOfWLessons newList;
		if(theMap.containsKey(newDate)){
			System.out.println("We have a list - "+ newDate.toString());
			newList = theMap.get(newDate);
		}else{
			System.out.println("Create new List - "+ newDate.toString());
			newList = new ListOfWLessons();
			newList.setTheDate(newDate);
		}
		
		if(!oldDate.equals(newDate)){
			System.out.println("Dragged in another Day");
			// only change Hour
			if(oldList == null){System.out.println("OLD list is null"); return false;}
			if(oldList.getAllLessons() == null){System.out.println("oldList.getAllLessons() list is null");}
			System.out.println("removed lesson from list "+ oldDate.toString());
			oldList.getAllLessons().remove(lesson);
			newList.getAllLessons().add(lesson);
			System.out.println("Added lesson to list "+ newDate.toString());
			lesson.setTheDate(newDate);
			lesson.setDayOfTheWeek(getDayOfTheWeekFromLocalDate(LocalDate.of(start.get(Calendar.YEAR), (start.get(Calendar.MONTH)+1),  start.get(Calendar.DATE))));
		}else{
			System.out.println("Dragged in the same Day");
		}
		lesson.setStartingHour(startHour);
		lesson.setStartingMinute(startMin);
		if(!lesson.isFullyScheduled() ){
			System.out.println("Lesson NOT fully Scheduled");
		}
		if(start.get(Calendar.YEAR) == 3000){
			System.out.println("Lesson going to the year 3000");
			// undefined set to be NOT fully Scheduled
			lesson.setStartingHour(0);
			lesson.setStartingMinute(0);
			lesson.setDayOfTheWeek(0);
		}
		
		System.out.println("Lessons new details: "+lesson.getTheDate()+" " +lesson.getDayOfTheWeek()+" "+lesson.getStartingTime());
		
		if(dbConnection == null){dbConnection = new LessonsPersister();}
		if(!theMap.containsKey(newDate)){
			theMap.put(newDate, newList);
		}
		if(utils.Utils.isValidID(newList.getId())){
			dbConnection.moveWeeklyLesson(lesson, oldList, newList, selectedClass, false);
		}else{
			dbConnection.moveWeeklyLesson(lesson, oldList, newList, selectedClass, true);
		}
		return true;
	}
	
	
	private Void createExtraLessonCallback(Void v){
		if(dialog.isShowing()){
			Stage sb = (Stage)dialog.getDialogPane().getScene().getWindow();//use any one object
			popUpController.saveChanges();
			sb.close();
		}
		
		ArrayList <Aclass> singleClassList = new ArrayList<>();
		singleClassList.add(selectedClass);
		AppointmentDialog tD = new AppointmentDialog(null, "", singleClassList);
		Optional<AppointmentDialog.AppointmentDetails> result = tD.showDialog();
		AppointmentDetails lessonDetails;
		if(result.isPresent()){
			if((lessonDetails = result.get()) != null)
				addExtraLesson(lessonDetails);
		}else{
			System.out.println("Didn't Create Appointment");
		}
		return null;
	}
	@FXML
	private void prevWeek(Event me){
		
		builder.getMyCalendarTextField().setValue(builder.getMyCalendarTextField().getValue().minusDays(7));
	}
	@FXML
	private void nextWeek(Event me){
		System.err.println(" > "+builder.getMyCalendarTextField().getValue());
		System.err.println(" > "+builder.getMyCalendarTextField().getValue().plusDays(7));
		builder.getMyCalendarTextField().setValue(builder.getMyCalendarTextField().getValue().plusDays(7));
	}
	private void initCalendarOnClassesStartingDate(){
		if(selectedClass == null && selectedClass.getStartDate() != null) return;
		builder.getMyCalendarTextField().setValue(selectedClass.getStartDate());
	}
	
	public void setUpAppointments(){
		if(builder == null){return;}
		if(selectedClass == null){return;}
		
		Calendar c = builder.getAgenda().getDisplayedCalendar();
		
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		int sunday = c.get(Calendar.DATE);
		int month = builder.getAgenda().getDisplayedCalendar().get(Calendar.MONTH);
		month ++;
		int year = builder.getAgenda().getDisplayedCalendar().get(Calendar.YEAR);
		LocalDate sundayDate = LocalDate.of(year, month, sunday);
		System.err.println("sundayDate = "+ sundayDate);
		sundayDate = sundayDate.minusDays(7);
		System.err.println("sundayDate(-7) = "+ sundayDate);
		sunday -= 7;
		
		if(selectedClass.getSchedule() != null && selectedClass.getSchedule().getAllWLessons() != null){
			
			/* ************************* Weekly lessons*******************************************************/
			Map <LocalDate, ListOfWLessons> allLessons = selectedClass.getSchedule().getAllWLessons();
			ArrayList<Agenda.AppointmentImpl> newWAppoinments = new ArrayList<>();
			if(allLessons == null){return;}
			// iterate through the 7 days of the Week
			for(LocalDate currentDay = sundayDate; currentDay.isBefore(sundayDate.plusDays(7)); currentDay = currentDay.plusDays(1)){
				System.out.println("thisDay = "+ currentDay.toString());
				
				if(allLessons.get(currentDay) != null){
					// get all lessons in this day
					for(Lesson aLesson : allLessons.get(currentDay).getAllLessons()){
						System.out.println("We have a lesson today");
						int durationH = aLesson.getDuration() / 60;
						int durationM = aLesson.getDuration() % 60;
						// has day of the week + time set
						if(aLesson.isFullyScheduled()){
//							System.out.print(" -> "+year+" / "+month+ " / "+  (sunday+aLesson.getDayOfTheWeek()-1));
//							System.out.println(" | "+(sunday+" + "+aLesson.getDayOfTheWeek()+" -1"));
							GregorianCalendar st = new GregorianCalendar(year, (month - 1 ), (sunday+aLesson.getDayOfTheWeek()-1),
									aLesson.getStartingHour(), aLesson.getStartingMinute());
							
//							System.out.println(st.get(Calendar.MONTH));
							
							GregorianCalendar en = new GregorianCalendar(year, (month - 1 ), (sunday+aLesson.getDayOfTheWeek()-1),
									aLesson.getStartingHour()+durationH, aLesson.getStartingMinute()+durationM);
//							System.out.println(en.get(Calendar.MONTH));
							newWAppoinments.add(
									new Agenda.AppointmentImpl()
											.withStartTime(st)
											.withEndTime(en)
											.withSummary(selectedClass.getName())
											.withDescription("")
											.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()))
											.withLesson(aLesson)
											.withCanceled(aLesson.isCanceled()));
						}
					}
				}
			}
			
			// NOT scheduled
			LocalDate futureSunday = LocalDate.of(3000, sundayDate.getMonthValue(), sundayDate.getDayOfMonth());
			System.out.println("future Sunday = "+futureSunday);
			futureSunday = futureSunday.plusDays(7);
			System.out.println("future Sunday = "+futureSunday);
			if(allLessons.get(futureSunday) != null){
				ListOfWLessons todaysLessons = allLessons.get(futureSunday);
				int notAssignedHour = 6;
				for(Lesson thisLesson : todaysLessons.getAllLessons()){
					if(!thisLesson.isFullyScheduled()){
						System.out.println("NOT Scheduled on "+ futureSunday.toString());
						int durationH = thisLesson.getDuration() / 60;
						int durationM = thisLesson.getDuration() % 60;
						GregorianCalendar startCal = new GregorianCalendar(futureSunday.getYear(), (month -1 ), sundayDate.getDayOfMonth()+7, notAssignedHour, 00); 
						System.out.println("startCal "+ startCal.get(Calendar.DATE) +" / "+ startCal.get(Calendar.MONTH)+ " / "+ startCal.get(Calendar.YEAR) +" | time" +notAssignedHour +":00");
						newWAppoinments.add(
								new Agenda.AppointmentImpl()
										.withStartTime(startCal)
										.withEndTime(new GregorianCalendar(futureSunday.getYear(), (month - 1), sundayDate.getDayOfMonth()+7, notAssignedHour+durationH, durationM))
										.withSummary(selectedClass.getName())
										.withDescription("")
										.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()))
										.withLesson(thisLesson)
										.withCanceled(thisLesson.isCanceled()));
						notAssignedHour++;
					}
				}
			}
			
			/* ************************* Extra lessons*******************************************************/
			
			Map <LocalDate, ListOfELessons> allELessons = selectedClass.getSchedule().getAllELessons();
			ArrayList<Agenda.AppointmentImpl> newEAppoinments = new ArrayList<>();
			if(allELessons == null){return;}
			// iterate through the 7 days of the Week
			for(LocalDate currentDay = sundayDate; currentDay.isBefore(sundayDate.plusDays(7)); currentDay = currentDay.plusDays(1)){
				System.out.println("thisDay = "+ currentDay.toString());
				
				if(allELessons.get(currentDay) != null){
					// get all lessons in this day
					for(Lesson aLesson : allELessons.get(currentDay).getAllLessons()){
						System.out.println("We have a lesson today - extra lesson");
						int durationH = aLesson.getDuration() / 60;
						int durationM = aLesson.getDuration() % 60;
						// has day of the week + time set
						if(aLesson.isFullyScheduled()){
//							System.out.println("Fully scheduled -> "+year+" / "+month+ " / "+  (sunday+aLesson.getDayOfTheWeek()-1));
							GregorianCalendar st = new GregorianCalendar(year, (month - 1), (sunday+aLesson.getDayOfTheWeek()-1),
									aLesson.getStartingHour(), aLesson.getStartingMinute());
							
							
							GregorianCalendar en = new GregorianCalendar(year, (month - 1), (sunday+aLesson.getDayOfTheWeek()-1),
									aLesson.getStartingHour()+durationH, aLesson.getStartingMinute()+durationM);
							newEAppoinments.add(
									new Agenda.AppointmentImpl()
											.withStartTime(st)
											.withEndTime(en)
											.withSummary(selectedClass.getName())
											.withDescription("")
											.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()+"-extraLesson"))
											.withLesson(aLesson)
											.withCanceled(aLesson.isCanceled()));
						}
					}
				}
			}
			
			// NOT scheduled
			LocalDate futureSundayExtraL = LocalDate.of(3000, sundayDate.getMonthValue(), sundayDate.getDayOfMonth());
			if(allELessons.get(futureSundayExtraL) != null){
				ListOfELessons todaysLessons = allELessons.get(futureSundayExtraL);
				int notAssignedHour = 6;
				for(Lesson thisLesson : todaysLessons.getAllLessons()){
					if(!thisLesson.isFullyScheduled()){
//						System.out.println("NOT Scheduled on "+ futureSundayExtraL.toString() +" - extra lesson");
						int durationH = thisLesson.getDuration() / 60;
						int durationM = thisLesson.getDuration() % 60;
						newEAppoinments.add(
								new Agenda.AppointmentImpl()
										.withStartTime(new GregorianCalendar(futureSundayExtraL.getYear(), (month - 1), sundayDate.getDayOfMonth(), notAssignedHour, 00))
										.withEndTime(new GregorianCalendar(futureSundayExtraL.getYear(), (month - 1), sundayDate.getDayOfMonth(), notAssignedHour+durationH, durationM))
										.withSummary(selectedClass.getName())
										.withDescription("")
										.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()+"-extraLesson"))
										.withLesson(thisLesson)
										.withCanceled(thisLesson.isCanceled()));
						notAssignedHour++;
					}
				}
			}
			builder.getAgenda().getAppointments().setAll(newEAppoinments);
			System.out.println("Cound Weekly appoints : "+ newWAppoinments.size());
			builder.getAgenda().getAppointments().addAll(newWAppoinments);
		}
	}
	
	private void addExtraLesson(AppointmentDetails details){
		if(dbConnection == null){dbConnection = new LessonsPersister();}
		//@TODO  : Save
		Calendar c = builder.getAgenda().getDisplayedCalendar();
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		int sunday = c.get(Calendar.DATE);
		int month = builder.getAgenda().getDisplayedCalendar().get(Calendar.MONTH);
		int year = builder.getAgenda().getDisplayedCalendar().get(Calendar.YEAR);
		int dayInt = details.getDay();
		int hourInt = details.getHour();
		int minuteInt = details.getMinute();
		int durationInt = details.getDuration();
		
		Lesson extraLesson = new Lesson();
		extraLesson.setDayOfTheWeek(dayInt+1);
		extraLesson.setDescription("extra lesson");
		extraLesson.setDuration(durationInt);
		extraLesson.setStartingHour(hourInt);
		extraLesson.setStartingMinute(minuteInt);
		extraLesson.setTheClass(selectedClass);
		extraLesson.setCanceled(false);
		extraLesson.createAttendance();
		extraLesson.createBilling();
		
		LocalDate newDate = LocalDate.of(year, (month+1), sunday);
		newDate = newDate.plusDays(dayInt);
		extraLesson.setTheDate(newDate);
		extraLesson.setExtraLesson(true);
		System.out.println("Persist lesson");
		
		if(selectedClass != null && selectedClass.getSchedule() != null){
			Map<LocalDate,ListOfELessons> theMap =  selectedClass.getSchedule().getAllELessons();
			if(theMap != null){
				ListOfELessons theList = theMap.get(newDate);
				System.out.println(" date for extra lesson = "+ newDate.toString());
				if(theList == null){
					System.out.println("The list is null - create new ");
					theList = new ListOfELessons();
					theList.setTheDate(newDate);
				}
				theList.getAllLessons().add(extraLesson);
				theMap.put(newDate, theList);
				if(utils.Utils.isValidID(theList.getId())){
					dbConnection.addExtraLesson(extraLesson, theList, selectedClass, false);
				}else{
					dbConnection.addExtraLesson(extraLesson, theList, selectedClass, true);
				}
			}
		}else{
			System.err.println("Selected Class == null ");
			return;
		}
		
		int durationH = durationInt / 60;
		int durationM = durationInt % 60;
		builder.getAgenda().getAppointments().add(
				new Agenda.AppointmentImpl()
						.withStartTime(new GregorianCalendar(year, month, (sunday+dayInt),
								hourInt, minuteInt))
						.withEndTime(new GregorianCalendar(year, month, (sunday+dayInt),
								hourInt+durationH, minuteInt+durationM))
						.withSummary(selectedClass.getName())
						.withDescription("")
						.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()+"-extraLesson"))
						.withLesson(extraLesson));
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	@Override
	protected void changeLocale() {
		System.out.println("ChangeLocale not supported yet");
	}
	
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
		builder.getAgenda().getAppointments().clear();
		selectedClass = null;
	}
	
}
