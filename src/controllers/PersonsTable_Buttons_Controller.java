/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import customObjects.Person;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import tables.TableUtils_buttons;
import utils.dialogs.CustomDialogs;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class PersonsTable_Buttons_Controller extends StageController{
    
    private final URL fxmlURL;
    private final ResourceBundle resources;
    
    public PersonsTable_Buttons_Controller(URL fxml, ResourceBundle res) {
        super();
        labels = res;
        fxmlURL = fxml;
        resources = res;
    }
    @FXML private BorderPane region;
    @FXML private ProgressIndicator progress;
    @FXML private SplitPane splitPane;
    @FXML private AnchorPane root;
    @FXML private ImageView image;
    @FXML private ScrollPane theScroll;
    @FXML private VBox theV;
    @FXML private TableView fromTable;
    @FXML private TableView toTable;
    @FXML private Button button1;
    @FXML private Button button2;
    @FXML private ImageView imageV;
    @FXML private TextField search;
    @FXML private TextField searchForIndex;
    @FXML private ChoiceBox choice;
    @FXML private Button nextP;
    @FXML private Button previousP;
    
    private ObservableList<Person> toItems;
    private ObservableList<Person> toItems_now;
    private ObservableList<Person> fromItems;
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        theV.prefWidthProperty().bind(theScroll.widthProperty());
        
        ArrayList<Person> persons = utils.Utils.getPersonsList(200);
        fromItems = FXCollections.observableArrayList(persons);
        fromTable.setItems(fromItems);
        fromTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
       
        for(Person p : fromItems){
//            System.out.println(" > "+p.getFName());
        }
        TableUtils_buttons.initializePersonsTable(fromTable);
//        DragNDropUtils dndUtils = new DragNDropUtils(fromTable, toTable);
//        utils.TableUtils.initializePersonsTable(fromTable);
        
        
        ArrayList<Person> personsTo = new ArrayList<>();
        toItems = FXCollections.observableArrayList(personsTo);
        toTable.setItems(toItems);
        toTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        TableUtils_buttons.initializePersonsTable(toTable);
//        utils.TableUtils.initializePersonsTable(toTable);
//        dndUtils.initializeTables();
        search.textProperty().addListener(
            new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, 
                                    Object oldVal, Object newVal) {
                handleSearchByKey((String)oldVal, (String)newVal);
                }
        });
        
        searchForIndex.addEventFilter(KeyEvent.KEY_TYPED, utils.EventFilters.getOnlyNumbersFilter_Red_Border());
        searchForIndex.textProperty().addListener(
            new ChangeListener() {
                public void changed(ObservableValue observable, 
                                    Object oldVal, Object newVal) {
                    
                if(!utils.Utils.isValidParam((String)newVal)){
                    fromTable.setItems(fromItems);
                }
                handleSearchByIndex(toInt(oldVal), toInt(newVal));
                }
        });
        choice.getItems().clear();
        choice.getItems().add("ALL");
        choice.getItems().add("10");
        choice.getItems().add("20");
        choice.getItems().add("50");
        choice.getSelectionModel().selectFirst();
        
        choice.valueProperty().addListener(
            (ObservableValue observable, Object oldVal, Object newVal) -> {
                if("ALL".equals(newVal)){
                    fromTable.setItems(fromItems);
                }
                else{
                    showFirstX(newVal);
                }
        });
    }
    private int toInt(Object param){
        if(!utils.Utils.isValidParam((String)param)){
            return -1;
        }
        return Integer.parseInt((String)param);
    }
    
    private void showFirstX(Object newVal){
        int amount = Integer.parseInt((String) newVal);
        if(amount > fromItems.size()){
            amount =  fromItems.size();
        }
//        startIndex = 0;
//        if((amount) > fromItems.size()){
//            endIndex = fromItems.size() - 1;
//        }else{
//            endIndex = amount;
//        } 
        ObservableList<Person> itemsAtTheStart = FXCollections.observableArrayList();
            System.out.println(0+ " -> "+ amount);
        for(int i = 0; i < amount; i++){
            itemsAtTheStart.add(fromItems.get(i));
        }
//        fromItems_now = itemsAtTheStart;
//        fromItems_now.nextX(amount);
//        fromTable.setItems(fromItems_now.showFirstX(amount));
        fromTable.setItems(itemsAtTheStart);
    }
    
    @Override
    protected void changeLocale(){
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        if(fromTable == null)
            return;
        
    }
    
    private void previousX(int value){
        System.out.println("previousX - 1");
        
//        if(value >= startIndex){
//            startIndex = 0;
//        }else{
//            startIndex = (startIndex-value);
//        }
//        if(value >= endIndex){
//            endIndex = value;
//        }else{
//            endIndex = (endIndex-value);
//        }
//        System.out.println(startIndex +" -> "+endIndex);
//        List <Person> subList = fromItems.subList(startIndex, endIndex);
//        
//        fromItems_now.clear();
//        fromItems_now = FXCollections.observableList(subList);
//        fromTable.setItems(fromItems_now);
//        
        
//        fromTable.setItems(fromItems_now.previousX(value));
//        
        int firstIndex = ((Person)(fromTable.getItems().get(0))).getIndex();
        System.out.println("fIndex = " + firstIndex);

        ObservableList<Person> previousItems = FXCollections.observableArrayList();
        if(firstIndex == 1){
            return;
        }
        if(firstIndex < value){
            System.out.println(0+ " -> "+ value);
            for(int i = 0; i < value ; i++){
                previousItems.add(fromItems.get(i));
            }
        }else{
            System.out.println((firstIndex-value - 1)+ " -> "+ (firstIndex-1));
            for(int i = (firstIndex-value - 1); i < (firstIndex-1); i++){
                previousItems.add(fromItems.get(i));
            }
        }
        
        fromTable.setItems(previousItems);

    }
    
    private void nextX(int value){
        System.out.println("nextX - 1");
        
//        if((startIndex + value) > fromItems.size()){
//            startIndex = fromItems.size() - 1;
//        }else{
//        System.out.println(" startIndex  ++");
//            startIndex += value;
//        }
//        
//        if((endIndex + value) > fromItems.size()){
//            endIndex = fromItems.size() - 1;
//        }else{
//        System.out.println(" endIndex  ++");
//            endIndex += value;
//        }
//        
//        System.out.println(startIndex +" -> "+endIndex);
//        List <Person> subList = fromItems.subList(startIndex, endIndex);
//        fromItems_now.clear();
//        fromItems_now = FXCollections.observableList(subList);
//        fromTable.setItems(fromItems_now);
//        
//        fromTable.setItems(fromItems_now.nextX(value));
         
        
        
        
        int lastIndex = ((Person)(fromTable.getItems().get(fromTable.getItems().size() - 1))).getIndex();
//        fromTable.getItems().sget(lastIndex);
        
        
        System.out.println("nextX() - lastIndex = "+ lastIndex);
        ObservableList<Person> nextPageItems = FXCollections.observableArrayList();
        if(lastIndex == fromItems.size()){
        System.out.println("nextX() - return");
            return;
        }
        if(lastIndex > (fromItems.size() - value)){
            
            System.out.println((fromItems.size() - value)+ " -> "+ (fromItems.size()));
            for(int i = (fromItems.size() - value); i < (fromItems.size()); i++){
                nextPageItems.add(fromItems.get(i));
            }
        }else{
            System.out.println(lastIndex+ " -> "+ (lastIndex+value));
            for(int i = lastIndex; i < (lastIndex+value); i++){
                nextPageItems.add(fromItems.get(i));
            }
        }
        fromTable.setItems(nextPageItems);
    }
    
    @FXML
    private void nextPage(Event event) {
        if("ALL".equals(choice.getValue())){
            fromTable.setItems(fromItems);
            return;
        }
        switch(Integer.parseInt((String)choice.getValue())){
            case 10: nextX(10); break;
            case 20: nextX(20); break;
            case 50: nextX(50); break;
            case 100: nextX(100); break;
        }
    }
    
    @FXML
    private void previousPage(Event event) {
        if("ALL".equals(choice.getValue())){
            fromTable.setItems(fromItems);
            return;
        }
        switch(Integer.parseInt((String)choice.getValue())){
            case 10: previousX(10); break;
            case 20: previousX(20); break;
            case 50: previousX(50); break;
            case 100: previousX(100); break;
        }
    }
    
    
    
    @FXML
    private void button1Pressed(Event event) throws IOException {
        
        Person p = new Person(9999, "name", "surname", 0, 1.0, true);
        fromTable.getItems().add(p);
                
    }
    @FXML
    private void button2Pressed(Event event) throws IOException {
        Person p = new Person(9999, "name", "surname", 0, 1.0, true);
        toTable.getItems().add((p));
    
    }
    @FXML
    private void removeLastEntrySource(Event event) {
        fromTable.getItems().remove(fromTable.getItems().size()-1);
    }
    @FXML
    private void removeLastEntryTarget(Event event) {
        toTable.getItems().remove(toTable.getItems().size()-1);
    }
    @FXML
    private void print1(Event event) {
        Iterator<Person> i  = fromTable.getItems().iterator();
        System.out.println("From Table items:");
        while(i.hasNext()){
            Person p = i.next();
            System.out.println("   "+p.getFn()+" "+p.getlName());
        
        }
    }
    
    @FXML
    private void print2(Event event) {
        Iterator<Person> i  = toTable.getItems().iterator();
        System.out.println("From Table items:");
        while(i.hasNext()){
            Person p = i.next();
            System.out.println("   "+p.getFn()+" "+p.getlName());
        
        }
        
    }
    @FXML
    private void moveAllRight(Event event) {
        if(!fromTable.getSelectionModel().getSelectedIndices().isEmpty()){
            
            Iterator <Integer> adder = fromTable.getSelectionModel().getSelectedIndices().listIterator();
           
            int ii = 0;
            while(adder.hasNext()){
                int i = adder.next();
                Person p  = (Person)fromTable.getItems().get(i);
                if( ! toTable.getItems().contains(p)){
                    toTable.getItems().add(p);
                }
//                fromItems_now.remove(p);
//                fromTable.getItems().remove(p);
                ii++;
            }
//            fromTable.getItems().removeAll(toBeMoved);
//            fromItems_now.removeAll(toBeMoved);
//            fromTable.getItems().removeAll(toBeMoved);
            
        }
    }
    
    @FXML
    private void moveAllLeft(Event event) {
        if(!toTable.getSelectionModel().getSelectedIndices().isEmpty()){
            Iterator <Integer> adder = toTable.getSelectionModel().getSelectedIndices().listIterator();
            ArrayList<Person> toBeMoved = new ArrayList<>();
            toBeMoved.ensureCapacity(toTable.getItems().size());
           
            while(adder.hasNext()){
                int i = adder.next();
                Person p  = (Person)toTable.getItems().get(i);
//                fromItems.add(p);
//                fromItems_now.add(p);
//                fromTable.getItems().add(p);
//                toTable.getItems().remove(p);
                toBeMoved.add(p);
            }
            
            toTable.getItems().removeAll(toBeMoved);
        }
    }
    /**
     * From http://www.drdobbs.com/jvm/simple-searching-in-java/232700121
     * @author EJB
     */
    public void handleSearchByKey(String oldVal, String newVal) {
        // If the number of characters in the text box is less than last time
        // it must be because the user pressed delete
        
        if ((oldVal != null && (newVal.length() < oldVal.length()))  || fromTable.getItems().size() == 0) {
            // Restore the lists original set of entries 
            // and start from the beginning
            fromTable.setItems( fromItems );
        }
         
        // Break out all of the parts of the search text 
        // by splitting on white space
//        String[] parts = newVal.toUpperCase().split(" ");
        String searchKey = newVal.toUpperCase();
 
        // Filter out the entries that don't contain the entered text
        ObservableList<Person> subentries = FXCollections.observableArrayList();
        for ( Object entry: fromTable.getItems() ) {
            Person p = (Person) entry;
            boolean match = true;
            String name = p.getFn();
            
            if ( name.toUpperCase().contains(searchKey) ) {
                subentries.add(p);
            }
        }
        fromTable.setItems(subentries);
    }
    
    
    public void handleSearchByIndex(int oldVal, int newVal) {
        // If the number of characters in the text box is less than last time
        // it must be because the user pressed delete
        if ( newVal == -1 || !utils.Utils.isValidParam(searchForIndex.getText()) || fromTable.getItems().size() == 0) {
            // Restore the lists original set of entries 
            // and start from the beginning
            fromTable.setItems( fromItems );
        }
         
        // Break out all of the parts of the search text 
        // by splitting on white space
//        String[] parts = newVal.toUpperCase().split(" ");
//        String searchKey = newVal.toUpperCase();
 
        // Filter out the entries that don't contain the entered text
        ObservableList<Person> subentries = FXCollections.observableArrayList();
        for ( Object entry: fromTable.getItems() ) {
            Person p = (Person) entry;
            boolean match = true;
            int index = p.getIndex();
            
            if ( newVal == index ) {
                subentries.add(p);
            }
        }
        fromTable.setItems(subentries);
    }
    
    private void showConfirmDialog() throws IOException {
        labels = LanguageUtils.getInstance().getCurrentLanguage();
        String yes =labels.getString("dialog.yes");
        Optional<ButtonType> response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.teacher.title"), labels.getString("dialog.save.teacher"), yes, labels.getString("dialog.no"));
        if(response.isPresent()){
            System.out.println(response.get().getButtonData() +" was pressed ");
        }
    }

    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setClassLoader(cachingClassLoader); 
        this.setStage(stage);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        this.setLanguageUsed(resources);
        loader.setController(this);
        try {
            setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            System.out.println("loader.getResources()=" + loader.getResources());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }
    //CheckBoxTableCell for creating a CheckBox in a table cell
    private class MyCheckBoxTableCell<S, T> extends TableCell<S, T> {
        private final CheckBox checkBox;
        private ObservableValue<T> ov;
 
        public MyCheckBoxTableCell() {
            this.checkBox = new CheckBox();
            this.checkBox.setAlignment(Pos.CENTER);
 
            setAlignment(Pos.CENTER);
            setGraphic(checkBox);
        } 
         
        @Override public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setGraphic(checkBox);
                if (ov instanceof BooleanProperty) {
                    checkBox.selectedProperty().unbindBidirectional((BooleanProperty) ov);
                }
                ov = getTableColumn().getCellObservableValue(getIndex());
                if (ov instanceof BooleanProperty) {
                    checkBox.selectedProperty().bindBidirectional((BooleanProperty) ov);
                }
            }
        }
    }
	@Override
	public void onLoad() {
	}
	@Override
	public void unload(){}
}
 
    