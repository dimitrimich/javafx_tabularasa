/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DBDeleteUtils;
import DB.DBTableUtils;
import DB.DBUpdateUtils;
import DB.objects.Address;
import DB.objects.ContactInformation;
import DB.objects.Student;
import images.Images;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import screens.FXMLs;
import services.TransitionService;
import tables.StudentsTable;
import utils.Constants;
import utils.dialogs.CustomDialogs;
import utils.EventFilters;
import utils.LanguageUtils;
import utils.Utils;
import static utils.Utils.isValidParam;
import static utils.Utils.notEqual;
import utils.menu.ScreenNavigator;

/**
 *
 * @author d.michaelides
 */
public class ViewStudentsController extends StageController{
	private TransitionService service;
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	
	public ViewStudentsController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	
	@FXML TabPane mainTabPane;
	@FXML TabPane tabPane;
	@FXML Tab searchTab;
	@FXML Tab editTab;
	@FXML private AnchorPane root;
	@FXML private ScrollPane theScroll;
	@FXML private VBox theV;
	@FXML private ScrollPane theScroll2;
	@FXML private VBox theV2;
//    @FXML private SplitPane splitPane;
//    @FXML private SplitPane splitPaneStudents;
	@FXML private ProgressIndicator progress;
	@FXML private ProgressIndicator loadingProgress;
	@FXML private Label hint;
	
	@FXML private Label mainTitle;
	@FXML private TableView<Student> studentsTable;
	
	@FXML private Label profileLabel;
	@FXML private ImageView profileImage;
	@FXML private Button addPictureButton;
	@FXML private Button cropButton;
	@FXML private Button deletePic;
	@FXML private Label surname;
	@FXML private Label name;
	@FXML private Label b_info;
	@FXML private TextField nameText;
	@FXML private TextField surnameText;
	@FXML private Label mName;
	@FXML private Label email2;
	@FXML private TextField mNameText;
	@FXML private TextField email2Text;
	
	@FXML private Label a_info;
	@FXML private Label subTitle;
	@FXML private Label address1Title;
	@FXML private Label address1StreetNum;
	@FXML private Label address1StreetName;
	@FXML private Label address1Area;
	@FXML private Label address1City;
	@FXML private Label address1Zip;
	@FXML private TextField address1StreetNumText;
	@FXML private TextField address1StreetNameText;
	@FXML private TextField address1AreaText;
	@FXML private TextField address1CityText;
	@FXML private TextField address1ZipText;
	
	@FXML private Label address2Title;
	@FXML private Label address2StreetNum;
	@FXML private Label address2StreetName;
	@FXML private Label address2Area;
	@FXML private Label address2City;
	@FXML private Label address2Zip;
	@FXML private TextField address2StreetNumText;
	@FXML private TextField address2StreetNameText;
	@FXML private TextField address2AreaText;
	@FXML private TextField address2CityText;
	@FXML private TextField address2ZipText;
	@FXML private Label c_info;
	@FXML private Label phone1;
	@FXML private Label phone2;
	@FXML private Label email;
	@FXML private TextField phone1Text;
	@FXML private TextField phone2Text;
	@FXML private TextField emailText;
	@FXML private Button confirmButton;
	@FXML private Button cancelButton;
	@FXML private Button deleteStudent;
	@FXML private Label fullNameLabel;
	@FXML private ConnectionsStudentController connectionsTabController;
	@FXML private Tab calendarTab, generalTab, connectionsTabHead;
	
	
	@FXML private TextField searchBox;
	private  ObservableList<Student> observableStudents;
	protected  Student selectedStudent;
	protected SerialBlob imgFromUser_blob;
	protected Image imgFromUser;
	private final AgendaForStudentsController agendaTabController = (AgendaForStudentsController) ScreenNavigator.getControllersMap().get("AgendaForStudents");
	
	
	private final Image defaultImage  = new Image(Images.class.getResource("default_profile.png").toString(), 140, 140, true, true);
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		if(agendaTabController == null){System.out.println(" 1 is null");}
		if(calendarTab == null){System.out.println(" 2 is null");}
		calendarTab.setContent(agendaTabController.getScroll());
		calendarTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue){
				agendaTabController.setUpCallBack();
				agendaTabController.setUpAppoinmentCallBack();
				agendaTabController.setUpPopUpCallBack();
				agendaTabController.setUpPopUpHideCallBack();
				agendaTabController.disableEdits();
			}
		});
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		theV.prefWidthProperty().bind(theScroll.widthProperty());
		theV2.prefWidthProperty().bind(theScroll2.widthProperty());
		
//        theScroll.getContent().hoverProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
//            if(oldVal == false && newVal == true){
//                splitPaneStudents.getDividers().get(0).setPosition(0.98);
//            }
//        });
//        theScroll2.getContent().hoverProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
//            if(oldVal == false && newVal == true){
//                splitPaneStudents.getDividers().get(0).setPosition(0.02);
//            }
//        });
		
		// Animate divider
//        final Timeline increaseSize = new Timeline();
//        increaseSize.setCycleCount(1);
//        increaseSize.setAutoReverse(false);
//        final KeyValue kv = new KeyValue(splitPaneStudents.getDividers().get(0).positionProperty(), -0.5);
//        final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);
//        increaseSize.getKeyFrames().add(kf);
		studentsTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Student> ov, Student oldValue, Student newValue) -> {
			if(oldValue != newValue){
				if(newValue != null){
					if(theScroll2.isDisable()){
						theScroll2.setDisable(false);
					}
					selectedStudent = newValue;
					showStudentsInfo();
					editTab.setDisable(false);
				}
			}
		});
		EventFilters.setEditableTextField(nameText);
		EventFilters.setEditableTextField(surnameText);
		EventFilters.setEditableTextField(address2StreetNumText);
		EventFilters.setEditableTextField(address2StreetNameText);
		EventFilters.setEditableTextField(address2AreaText);
		EventFilters.setEditableTextField(address2CityText);
		EventFilters.setEditableTextField(address2ZipText);
		EventFilters.setEditableTextField(address1StreetNumText);
		EventFilters.setEditableTextField(address1StreetNameText);
		EventFilters.setEditableTextField(address1AreaText);
		EventFilters.setEditableTextField(address1CityText);
		EventFilters.setEditableTextField(address1ZipText);
		EventFilters.setEditableTextField(phone1Text);
		EventFilters.setEditableTextField(phone2Text);
		EventFilters.setEditableTextField(emailText);
		EventFilters.setEditableTextField(email2Text);
		EventFilters.setEditableTextField(mNameText);
		
		
		searchBox.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
			if(key.getCharacter().equals(" ") && searchBox.getText().contains(" ")){
				key.consume();
			}
		});
		searchBox.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		studentsTable.setTableMenuButtonVisible(true);
	}
	
	private void showStudentsInfo(){
		if(connectionsTabController == null){
			System.out.println("TAB2 Controller == null");
		}else{
			connectionsTabController.initializeTables(selectedStudent);
		}
		
		if(agendaTabController == null){
			System.out.println("Agenda Tab Controller == null");
		}else{
			agendaTabController.setSelectedStudent(selectedStudent);
			agendaTabController.setUpAppointments();
		}
		nameText.setText(selectedStudent.getBasicInfo().getFname());
		surnameText.setText(selectedStudent.getBasicInfo().getLname());
		if(utils.Utils.isValidParam(selectedStudent.getBasicInfo().getMiddleName())){
			mNameText.setText(selectedStudent.getBasicInfo().getMiddleName());
		}else{
			mNameText.setText("");
		}
		fullNameLabel.setText(selectedStudent.getFullName());
		
		if(selectedStudent.getPicture() != null){
			Image img;
			InputStream stream = null;
			try {
				imgFromUser_blob = selectedStudent.getPicture();
				stream = selectedStudent.getPicture().getBinaryStream();
				img = new Image(stream, 140, 140, true, true);
				profileImage.setImage(img);
			} catch (SerialException ex) {
				System.err.println("exception while reading picture");
				ex.printStackTrace(System.err);
			}
			finally{
				if(stream != null){
					try {
						stream.close();
					} catch (IOException ex) {
						Logger.getLogger(ViewStudentsController.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
			cropButton.setDisable(false);
			deletePic.setDisable(false);
		}else{
			cropButton.setDisable(true);
			deletePic.setDisable(true);
			profileImage.setImage(defaultImage);
		}
		if(selectedStudent.getContactInfo().getAddress1() != null){
			if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getAddress1().getStreetInfo1())){
				address1StreetNumText.setText(selectedStudent.getContactInfo().getAddress1().getStreetNum());
				address1StreetNameText.setText(selectedStudent.getContactInfo().getAddress1().getStreetInfo1());
				address1AreaText.setText(selectedStudent.getContactInfo().getAddress1().getArea());
				address1CityText.setText(selectedStudent.getContactInfo().getAddress1().getCity());
				address1ZipText.setText(selectedStudent.getContactInfo().getAddress1().getPostCode());
			}else{
				address1StreetNumText.setText("");
				address1StreetNameText.setText("");
				address1AreaText.setText("");
				address1CityText.setText("");
				address1ZipText.setText("");
			}
		}
		if(selectedStudent.getContactInfo().getAddress2() != null){
			if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getAddress2().getStreetInfo1())){
				address2StreetNumText.setText(selectedStudent.getContactInfo().getAddress2().getStreetNum());
				address2StreetNameText.setText(selectedStudent.getContactInfo().getAddress2().getStreetInfo1());
				address2AreaText.setText(selectedStudent.getContactInfo().getAddress2().getArea());
				address2CityText.setText(selectedStudent.getContactInfo().getAddress2().getCity());
				address2ZipText.setText(selectedStudent.getContactInfo().getAddress2().getPostCode());
			}else{
				address2StreetNumText.setText("");
				address2StreetNameText.setText("");
				address2AreaText.setText("");
				address2CityText.setText("");
				address2ZipText.setText("");
			}
		}
		if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getPhone1())){
			phone1Text.setText(selectedStudent.getContactInfo().getPhone1());
		}else{
			phone1Text.setText("");
		}
		if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getPhone2())){
			phone2Text.setText(selectedStudent.getContactInfo().getPhone2());
		}else{
			phone2Text.setText("");
		}
		if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getEmail1())){
			emailText.setText(selectedStudent.getContactInfo().getEmail1());
		}else{
			emailText.setText("");
		}
		if(utils.Utils.isValidParam(selectedStudent.getContactInfo().getEmail2())){
			email2Text.setText(selectedStudent.getContactInfo().getEmail2());
		}else{
			email2Text.setText("");
		}
	}
	
	@Override
	protected void changeLocale(){
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		if(mainTitle == null)
			return;
		
		mainTitle.setText(labels.getString("view.students.title"));
		subTitle.setText(labels.getString("view.students.edit.title"));
		profileLabel.setText(labels.getString("profile_pic"));
		addPictureButton.setText(labels.getString("addpicture"));
		b_info.setText(labels.getString("basic_information"));
		name.setText(labels.getString("name"));
		surname.setText(labels.getString("surname"));
		nameText.setPromptText(labels.getString("insertname"));
		surnameText.setPromptText(labels.getString("insertsurname"));
		a_info.setText(labels.getString("address.information"));
		address1Title.setText(labels.getString("address.1"));
		address1StreetNum.setText(labels.getString("address.street.num"));
		address1StreetName.setText(labels.getString("address.street.name"));
		address1Area.setText(labels.getString("address.area"));
		address1City.setText(labels.getString("address.city"));
		address1Zip.setText(labels.getString("address.zip"));
		address1StreetNumText.setPromptText(labels.getString("address.street.numText"));
		address1StreetNameText.setPromptText(labels.getString("address.street.nameText"));
		address1AreaText.setPromptText(labels.getString("address.areaText"));
		address1CityText.setPromptText(labels.getString("address.cityText"));
		address1ZipText.setPromptText(labels.getString("address.zipText"));
		address2Title.setText(labels.getString("address.2"));
		address2StreetNum.setText(labels.getString("address.street.num"));
		address2StreetName.setText(labels.getString("address.street.name"));
		address2Area.setText(labels.getString("address.area"));
		address2City.setText(labels.getString("address.city"));
		address2Zip.setText(labels.getString("address.zip"));
		address2StreetNumText.setPromptText(labels.getString("address.street.numText"));
		address2StreetNameText.setPromptText(labels.getString("address.street.nameText"));
		address2AreaText.setPromptText(labels.getString("address.areaText"));
		address2CityText.setPromptText(labels.getString("address.cityText"));
		address2ZipText.setPromptText(labels.getString("address.zipText"));
		c_info.setText(labels.getString("contact_information"));
		phone1.setText(labels.getString("phone1"));
		phone2.setText(labels.getString("phone2"));
		email.setText(labels.getString("email"));
		phone1Text.setPromptText(labels.getString("insertphone"));
		phone2Text.setPromptText(labels.getString("insertphone"));
		emailText.setPromptText(labels.getString("insertemail"));
		confirmButton.setText(labels.getString("new.student.save"));
		cancelButton.setText(labels.getString("new.student.cancel"));
		hint.setText(labels.getString("view.students.edit.hint"));
		email2.setText(labels.getString("email2"));
		mName.setText(labels.getString("mName"));
		mNameText.setPromptText(labels.getString("insertMName"));
		email2Text.setPromptText(labels.getString("insertEmail2"));
		deleteStudent.setText(labels.getString("delete.student"));
		calendarTab.setText(labels.getString("view.students.cal"));
		generalTab.setText(labels.getString("view.students.general"));
		connectionsTabHead.setText(labels.getString("view.students.conns"));
	}
	
	/**
	 * From http://www.drdobbs.com/jvm/simple-searching-in-java/232700121
	 * @author EJB
	 * @author edited d.michaelides
	 * @param oldVal
	 * @param newVal
	 * @param fieldName
	 */
	public void handleSearchByKey(String oldVal, String newVal, int fieldName) {
		if ((oldVal != null && (newVal.length() < oldVal.length()))) {
			System.out.println("ALL STUDENTS");
			studentsTable.setItems( observableStudents );
		}
		
		String searchKey = newVal.toUpperCase();
		
		ObservableList<Student> subentries = FXCollections.observableArrayList();
		
		for ( Student student : studentsTable.getItems() ) {
			String searchToken = "";
			switch(fieldName){
				case Constants.FIELD_FNAME:
					searchToken = student.getBasicInfo().getFname();
					break;
				case Constants.FIELD_LNAME:
					searchToken = student.getBasicInfo().getLname();
					break;
				case Constants.FIELD_ADDRESS_1:
					searchToken = student.getContactInfo().getAddress1().getStreetInfo1();
					break;
				case Constants.FIELD_STREET_NO_1:
					break;
				case Constants.FIELD_STREET_NAME_1:
					break;
				case Constants.FIELD_STREET_AREA_1:
					break;
				case Constants.FIELD_STREET_CITY_1:
					break;
				case Constants.FIELD_STREET_ZIP_1:
					break;
				case Constants.FIELD_ADDRESS_2:
					searchToken = student.getContactInfo().getAddress2().getStreetInfo1();
					break;
				case Constants.FIELD_STREET_NO_2:
					break;
				case Constants.FIELD_STREET_NAME_2:
					break;
				case Constants.FIELD_STREET_AREA_2:
					break;
				case Constants.FIELD_STREET_CITY_2:
					break;
				case Constants.FIELD_PHONE_1:
					searchToken = student.getContactInfo().getPhone1();
					break;
				case Constants.FIELD_PHONE_2:
					searchToken = student.getContactInfo().getPhone2();
					break;
				case Constants.FIELD_EMAIL:
					searchToken = student.getContactInfo().getEmail1();
					break;
			}
			if(utils.Utils.isValidParam(searchToken)){
				
				//
			}
		}
		studentsTable.setItems(subentries);
	}
	
	public void searchForAll(String oldVal, String newVal) {
		if ((oldVal != null && (newVal.length() < oldVal.length())) ) {
			studentsTable.setItems( observableStudents );
		}
		if(newVal.isEmpty()){ return; }
		
		String [] tokens = searchBox.getText().trim().split(" ");
		String searchToken1 = tokens[0];
		String searchToken2 = (tokens.length == 2) ? tokens[1] : null;
		
		
		ObservableList<Student> subentries = FXCollections.observableArrayList();
		
		studentsTable.getItems().stream().forEach((Student student) -> {
			if(searchToken2 == null){
				if(student.containsKeyWordNotCaseSensitive(searchToken1)){
					subentries.add(student);
				}
			}else{
				if(student.containsKeyWordNotCaseSensitive(searchToken1) && student.containsKeyWordNotCaseSensitive(searchToken2)){
					subentries.add(student);
				}
			}
		});
		studentsTable.setItems(subentries);
	}
	
	@FXML
	private void addImage(Event e){
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(LanguageUtils.getInstance().getCurrentLanguage().getString("selectphoto"));
		File file = fileChooser.showOpenDialog(stage);
		if (file != null) {
			InputStream is = null;
			Path path = Paths.get(file.getAbsolutePath());
			try {
				byte[] data = Files.readAllBytes(path);
				imgFromUser_blob = new SerialBlob(data);
				is = new ByteArrayInputStream(data);
				imgFromUser = new Image(is);
				profileImage.setImage(imgFromUser);
				cropButton.setDisable(false);
				deletePic.setDisable(false);
			} catch (IOException | SQLException ex) {
				ex.printStackTrace(System.err);
			}
			finally{
				if(is != null){
					try {
						is.close();
					} catch (IOException ex) {
						Logger.getLogger(ViewStudentsController.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}
	
	@FXML
	private void cropImage(Event event) throws IOException {
		Stage cropImageStage = new Stage();
		Node source = (Node)event.getSource();
		stage = (Stage)(source.getScene().getWindow());
		final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("CropImage.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
		final Scene cropImageScene = new Scene((AnchorPane)myLoader.load());
		CropImageController c = (CropImageController)myLoader.getController();
		c.init(profileImage.getImage());
		
		stage.setOnCloseRequest((WindowEvent ev) -> {
			cropImageScene.getWindow().fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
		});
		
		cropImageStage.setScene(cropImageScene);
//        stage.toBack();
		cropImageStage.toFront();
		stage.setOpacity(0.8);
		root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
		root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
		cropImageScene.getWindow().setOnCloseRequest((WindowEvent ev) -> {
			stage.setOpacity(1);
			CropImageController c1 = (CropImageController)myLoader.getController();
			Image croppedImg = c1.getCroppedImage();
			if(croppedImg != null){
				try{
					profileImage.setImage(croppedImg);
					imgFromUser_blob = new SerialBlob(utils.Utils.toByteArray(croppedImg));
				}
				catch(Exception e){
					System.out.println("ERROR ERROR - converting image");
					e.printStackTrace(System.err);
				}
			}
		});
		cropImageStage.show();
	}
	
	@FXML
	private void setDefaultImage(Event e){
		imgFromUser_blob = null;
		imgFromUser = null;
		deletePic.setDisable(true);
		cropButton.setDisable(true);
		profileImage.setImage(defaultImage);
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	private void refresh() throws IOException {
		TimeTableService timeTableService = new TimeTableService();
		loadingProgress.visibleProperty().bind(timeTableService.runningProperty());
		timeTableService.start();
		loadingProgress.requestFocus();
	}
	
	private class TimeTableService extends Service {
		@Override
		protected Task createTask() {
			TimeTableTask theTask = new TimeTableTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				if(observableStudents != null){
					studentsTable.setItems(observableStudents);
					StudentsTable.initializeStudentsTable(studentsTable);
				}else{
					System.out.println("WHY is it null?!?!?!? -students-");
				}
			});
			return theTask;
		}
		private class TimeTableTask extends Task {
			@Override
			protected Object call() throws Exception {
				observableStudents = DBTableUtils.getAllStudents();
				return null;
			}
		}
	}
	
	
	
	/* ----------------- DELETE student ------------------------------------------------ */
	@FXML
	private void deleteStudent(Event event) throws Exception {
		try{
			if(selectedStudent != null){
				showConfirmDialog(false);
			}
		}catch(IOException e){
			e.printStackTrace(System.err);
		}
	}
	@FXML
	private void billings(Event ev) throws IOException{
		if(selectedStudent != null){
			((BillingInfoController)ScreenNavigator.getControllersMap().get("BillingInfo")).setStudent(selectedStudent);
		}
	}
	
	/* *****************************************************************/
	@FXML
	private void saveChanges(Event ev) throws IOException{
		if(stage == null){
			Node source = (Node)ev.getSource();
			stage = (Stage)(source.getScene().getWindow());
		}
		try {
			showConfirmDialog(true);
		} catch (Exception ex) {
			System.err.println("Error while showing confirmation dialog -student-");
			ex.printStackTrace(System.err);
		}
	}
	
	private void showConfirmDialog(boolean toSave) throws Exception {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		String yes = labels.getString("dialog.yes");
		Optional<ButtonType> response;
		if(toSave){
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.student.title"), labels.getString("dialog.save.student"), yes, labels.getString("dialog.no"));
		}else{
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.delete.student.title"), labels.getString("dialog.delete.student"), yes, labels.getString("dialog.no"));
		}
		if(response.isPresent()){
			System.out.println("response.ispresent");
			System.out.println(response.get());
			System.out.println(response.get().getClass());
			if(response.get().getText().equals(yes)){
				System.out.println("== YES");
				if(toSave){
					System.out.println("VERIFY INput");
					verifyInput();
				}else{
					System.out.println("save changes");
					saveChangesMade(false);
				}
			}
		}else{
			
			System.out.println("response. NOT ispresent");
		}
	}
	
	private void verifyInput() throws IOException{
		boolean thereIsError = false;
		if(!Utils.isValidParam(nameText.getText().trim())){
			Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
			thereIsError = true;
		}
		if(!Utils.isValidParam(surnameText.getText().trim())){
			Utils.turnNodeRed(surnameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.surname"));
			thereIsError = true;
		}
		if(!Utils.isValidParam(phone1Text.getText().trim())){
			Utils.turnNodeRed(phone1Text, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.phone"));
			thereIsError = true;
		}
		
		if(thereIsError){
			System.out.println("There Is error");
		}else{
			if(isStudentEdited()){
				saveChangesMade(true);
			}else{
				CustomDialogs.infoDialog("", "Student was not edited, nothing to save....");
			}
		}
	}
	
	private void saveChangesMade(boolean toSave){
		SaveStudentService saveClass = new SaveStudentService(toSave);
		progress.visibleProperty().bind(saveClass.runningProperty());
		confirmButton.disableProperty().bind(saveClass.runningProperty());
		cancelButton.disableProperty().bind(saveClass.runningProperty());
		saveClass.start();
		progress.requestFocus();
	}
	
	private class SaveStudentService extends Service<Boolean> {
		private final boolean toSave;
		public SaveStudentService(boolean isItToSave){
			super();
			toSave = isItToSave;
		}
		
		@Override
		protected Task<Boolean> createTask() {
			SaveStudentTask theTask = new SaveStudentTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				System.out.println("     RESULT = "+getValue());
				if(getValue()){
					if(toSave){
						String message = labels.getString("save.obj.student")+" "
								+ labels.getString("save.obj.success.line1");
						CustomDialogs.infoDialog("", message);
					}else{
						String message = labels.getString("save.obj.student")+" "
								+ labels.getString("delete.obj.success.line1");
						CustomDialogs.infoDialog("", message);
						clearAll();
					}
				}else{
					if(toSave){
						String message = labels.getString("save.obj.student")+" "
								+ labels.getString("delete.obj.fail.line1");
						CustomDialogs.errorDialog("", message);
					}else{
						String message = labels.getString("save.obj.student")+" "
								+ labels.getString("delete.obj.fail.line1");
						CustomDialogs.errorDialog("", message);
					}
				}
			});
			return theTask;
		}
		private class SaveStudentTask extends Task {
			@Override
			protected Boolean call() throws Exception {
				try{
					Thread.sleep(200);
					System.out.println("Updating student...");
					if(toSave){
						DBUpdateUtils.updateStudentInfo(selectedStudent);
					}else{
						DBDeleteUtils.deleteStudentObject(selectedStudent);
					}
				}catch(InterruptedException e){
					System.out.println("Exception dam it");
					e.printStackTrace(System.err);
					return false;
				}
				return true;
			}
		}
	}
	
	
	/**
	 * checks if any fields of selected student were edited
	 * if there were edits @return true
	 */
	private boolean isStudentEdited(){
		boolean somethingWasEdited = false;
		if (notEqual(nameText, selectedStudent.getBasicInfo().getFname())){
			selectedStudent.getBasicInfo().setFname(nameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 55 -");
		}
		if(imgFromUser_blob != selectedStudent.getPicture()){
			selectedStudent.setPicture(imgFromUser_blob);
			somethingWasEdited = true;
			System.out.println("Edited - 56 -");
		}
		
		if (notEqual(surnameText, selectedStudent.getBasicInfo().getLname())){
			selectedStudent.getBasicInfo().setLname(surnameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 57 -");
		}
		if (notEqual(mNameText, selectedStudent.getBasicInfo().getMiddleName())){
			selectedStudent.getBasicInfo().setMiddleName(mNameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 58 -");
		}
		if (isValidParam(address1StreetNumText.getText()) ||
				isValidParam(address1StreetNameText.getText()) ||
				isValidParam(address1AreaText.getText()) ||
				isValidParam(address1CityText.getText()) ||
				isValidParam(address1ZipText.getText())){
			if(selectedStudent.getContactInfo() == null){
				selectedStudent.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
				System.out.println("Edited - 59 -");
			}
			if(selectedStudent.getContactInfo().getAddress1() == null){
				selectedStudent.getContactInfo().setAddress1(new Address());
				somethingWasEdited = true;
				System.out.println("Edited - 60 -");
			}
		}
		
		if (isValidParam(address2StreetNumText.getText()) ||
				isValidParam(address2StreetNameText.getText()) ||
				isValidParam(address2AreaText.getText()) ||
				isValidParam(address2CityText.getText()) ||
				isValidParam(address2ZipText.getText())){
			if(selectedStudent.getContactInfo() == null){
				selectedStudent.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
				System.out.println("Edited - 61 -");
			}
			if(selectedStudent.getContactInfo().getAddress2() == null){
				selectedStudent.getContactInfo().setAddress2(new Address());
				somethingWasEdited = true;
				System.out.println("Edited - 62 -");
			}
		}
		
		// address 1 fields
		if (notEqual(address1StreetNumText, selectedStudent.getContactInfo().getAddress1().getStreetNum())){
			selectedStudent.getContactInfo().getAddress1().setStreetNum(address1StreetNumText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 63 -");
		}
		if (notEqual(address1StreetNameText, selectedStudent.getContactInfo().getAddress1().getStreetInfo1())){
			selectedStudent.getContactInfo().getAddress1().setStreetInfo1(address1StreetNameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 64 -");
		}
		if (notEqual(address1AreaText, selectedStudent.getContactInfo().getAddress1().getArea())){
			selectedStudent.getContactInfo().getAddress1().setArea(address1AreaText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 65 -");
		}
		if (notEqual(address1CityText, selectedStudent.getContactInfo().getAddress1().getCity())){
			selectedStudent.getContactInfo().getAddress1().setCity(address1CityText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 66 -");
		}
		if (notEqual(address1ZipText, selectedStudent.getContactInfo().getAddress1().getPostCode())){
			selectedStudent.getContactInfo().getAddress1().setPostCode(address1ZipText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 67 -");
		}
		
		// address 2 fields
		if (notEqual(address2StreetNumText, selectedStudent.getContactInfo().getAddress2().getStreetNum())){
			selectedStudent.getContactInfo().getAddress2().setStreetNum(address2StreetNumText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 68 -");
		}
		if (notEqual(address2StreetNameText, selectedStudent.getContactInfo().getAddress2().getStreetInfo1())){
			selectedStudent.getContactInfo().getAddress2().setStreetInfo1(address2StreetNameText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 69 -");
		}
		if (notEqual(address2AreaText, selectedStudent.getContactInfo().getAddress2().getArea())){
			selectedStudent.getContactInfo().getAddress2().setArea(address2AreaText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 70 -");
		}
		if (notEqual(address2CityText, selectedStudent.getContactInfo().getAddress2().getCity())){
			selectedStudent.getContactInfo().getAddress2().setCity(address2CityText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 71 -");
		}
		if (notEqual(address2ZipText, selectedStudent.getContactInfo().getAddress2().getPostCode())){
			selectedStudent.getContactInfo().getAddress2().setPostCode(address2ZipText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 72 -");
		}
		
		// contact Info fields
		if(isValidParam(phone1Text.getText()) ||
				isValidParam(phone2Text.getText()) ||
				isValidParam(emailText.getText()) ||
				isValidParam(email2Text.getText())){
			if(selectedStudent.getContactInfo() == null){
				selectedStudent.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
				System.out.println("Edited - 73-");
			}
		}
		if (notEqual(phone1Text, selectedStudent.getContactInfo().getPhone1())){
			selectedStudent.getContactInfo().setPhone1(phone1Text.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 74 -");
		}
		if (notEqual(phone2Text, selectedStudent.getContactInfo().getPhone2())){
			selectedStudent.getContactInfo().setPhone2(phone2Text.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 75 -");
		}
		if (notEqual(emailText, selectedStudent.getContactInfo().getEmail1())){
			selectedStudent.getContactInfo().setEmail1(emailText.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 76 -");
		}
		if (notEqual(email2Text, selectedStudent.getContactInfo().getEmail2())){
			selectedStudent.getContactInfo().setEmail2(email2Text.getText().trim());
			somethingWasEdited = true;
			System.out.println("Edited - 77 -");
		}
		return somethingWasEdited;
	}
	
	private void clearAll(){
		cropButton.setDisable(true);
		deletePic.setDisable(true);
		selectedStudent = null;
		imgFromUser_blob = null;
		profileImage.setImage(defaultImage);
		nameText.setText("");
		surnameText.setText("");
		mNameText.setText("");
		address1AreaText.setText("");
		address1CityText.setText("");
		address1StreetNameText.setText("");
		address1StreetNumText.setText("");
		address1ZipText.setText("");
		address2AreaText.setText("");
		address2CityText.setText("");
		address2StreetNameText.setText("");
		address2StreetNumText.setText("");
		address2ZipText.setText("");
		phone1Text.setText("");
		phone2Text.setText("");
		email2Text.setText("");
		emailText.setText("");
	}
	@Override
	public void onLoad() {
		try {
			refresh();
		} catch (IOException ex) {
			System.err.println("Couldn;t load students ");
			ex.printStackTrace(System.err);
		}
	}
	
	@Override
	public void unload(){
		clearAll();
		SingleSelectionModel<Tab> selectionModel = mainTabPane.getSelectionModel();
		selectionModel.select(0);
		studentsTable.getItems().clear();
		if(observableStudents != null){
			observableStudents.clear();
		}
		editTab.setDisable(true);
		searchBox.setText("");
		SingleSelectionModel<Tab> editSelectionModel = tabPane.getSelectionModel();
		editSelectionModel.select(0);
		
		connectionsTabController.unload();
		
	}
}

