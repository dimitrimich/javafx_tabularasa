/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author d.michaelides
 */
public class ShowInfoController{
    @FXML private TextArea message;
    @FXML private Button okButton;
    @FXML private ImageView imgView;
    
    @FXML
    private void handleYesButton(ActionEvent event) throws IOException {
        Node  source = (Node)  event.getSource(); 
        Stage window  = (Stage) source.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
        window.close();
    }
    
    public void init(String msg, Image toShow){
        if(toShow != null){
            imgView.setImage(toShow);
        }
        message.setText(msg);
    }
    
}
 
    