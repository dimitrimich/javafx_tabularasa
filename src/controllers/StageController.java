/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import utils.LanguageUtils;
import utils.menu.MyClassLoader;

/**
 *
 * @author d.michaelides
 */
public abstract class StageController implements Initializable {
    protected Stage stage;
    public static ClassLoader cachingClassLoader = new MyClassLoader(FXMLLoader.getDefaultClassLoader()); 
    protected ResourceBundle labels;
    private Scene scene;
    private Parent root;
    private ScrollPane scroll;
    
    
    public Parent getRoot() {
        if (root == null) {
            makeRoot();
        }
        return root;
    }
    public ScrollPane getScroll() {
        if(labels != null){
            if(labels  != LanguageUtils.getInstance().getCurrentLanguage()){
                labels = LanguageUtils.getInstance().getCurrentLanguage();
                changeLocale();
            }
        }
        if (scroll == null) {
            makeScroll();
        }
        return scroll;
    }
    public void makeScroll(){
        if (root == null) {
            makeRoot();
        }
        scroll = (ScrollPane)root.getChildrenUnmodifiable().get(0);
    }
    
    public Scene getScene() {
        if (scene == null) {
            scene = new Scene(getRoot());
        }
        return scene;
    }
    /**
     * to be implemented by sub-classes
     */
    protected abstract void makeRoot();
    /**
     * to be implemented by sub-classes
     */
    protected abstract void changeLocale();
    
    
    /**
     * to be implemented by sub-classes
     */
    protected void controllerDidCreateScene() {
    }
    
    protected final void setLanguageUsed(ResourceBundle l) {
        labels = l;
    }
    
    protected  final void setRoot(Parent root) {
        assert root != null;
        this.root = root;
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
	public abstract void onLoad();
	public abstract void unload();
    
}
