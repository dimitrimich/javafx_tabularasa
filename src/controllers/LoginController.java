/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DBTableUtils;
import DB.objects.Aclass;
import DB.objects.Attendance;
import DB.objects.Lesson;
import DB.objects.ListOfWLessons;
import DB.objects.PaymentDetails;
import DB.objects.Student;
import images.Images;
import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import screens.FXMLs;
import utils.LanguageUtils;
import utils.dialogs.BillingClassesDialog;
import utils.dialogs.BillingClassesDialogScene;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.PasswordField;
import jfxtras.agenda.Agenda;
import jfxtras.agenda.Agenda.AppointmentImpl;
import utils.dialogs.CustomDialogs;
import utils.dialogs.PopupLessonDialog;
import utils.dialogs.PopupLessonDialogScene;
import utils.menu.ScreenNavigator;

/**
 *
 * @author d.michaelides
 */
public class LoginController{
	
	@FXML private Button cancelButton;
	@FXML private Label welcomeLabel;
	@FXML private Label usernameLabel;
	@FXML private Label passwordLabel;
	@FXML private PasswordField passwordTxt;
	
	@FXML private ImageView language;
	private Image img;
	private ResourceBundle labels;
	
	@FXML
	private void handleButtonAction(ActionEvent event) throws IOException {
	}
	
	/**
	 * Set the text to all visible elements of the stage
	 * plus change flag
	 */
	private void changeLocale(ResourceBundle labels){
		usernameLabel.setText(labels.getString("username"));
		cancelButton.setText(labels.getString("cancel"));
		welcomeLabel.setText(labels.getString("welcome"));
		passwordLabel.setText(labels.getString("password"));
		if(labels.getLocale() == Locale.ENGLISH){
			img = new Image(Images.class.getResource("UnitedKingdom.png").toString());
		}else{
			img = new Image(Images.class.getResource("Greece.png").toString());
		}
		language.setImage(img);
	}
	
	@FXML
	private void changeLanguage(Event event) throws IOException {
		labels = LanguageUtils.getInstance().switchLanguage();
		changeLocale(labels);
	}
	
	@FXML
	private void goToHomePage(Event event) throws IOException {
		if(!"123qwe".equals(passwordTxt.getText())){
			CustomDialogs.errorDialog(LanguageUtils.getString("dialog.login.fail.title"), LanguageUtils.getString("dialog.login.fail.text"));
			return;
		}
		Node  source = (Node)  event.getSource();
		ScreenNavigator.getInstance((Stage) source.getScene().getWindow()).goToScreen("HomePage");
	}
	
	public void initializeAll(Stage stage){
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		if(labels == null){
			System.out.println("LABELS == null 1");
		}
		passwordTxt.requestFocus();
		ScreenNavigator.getInstance(stage);
		
		ScreenNavigator.getControllersMap().put("HomePage",      new HomePageController(     FXMLs.class.getResource("HomePage.fxml"),      labels));
		Platform.runLater(() -> {
			ScreenNavigator.getControllersMap().put("CreateClass",          new CreateClassController(  FXMLs.class.getResource("CreateClass.fxml"),   labels));
			ScreenNavigator.getControllersMap().put("CreateStudent",        new CreateStudentController(FXMLs.class.getResource("CreateStudent.fxml"), labels));
			ScreenNavigator.getControllersMap().put("CreateTeacher",        new CreateTeacherController(FXMLs.class.getResource("CreateTeacher.fxml"), labels));
			ScreenNavigator.getControllersMap().put("PersonsTable_button",  new PersonsTable_Buttons_Controller(FXMLs.class.getResource("PersonsTable_button.fxml"), labels));
			ScreenNavigator.getControllersMap().put("TimeTable",            new TimeTableController(FXMLs.class.getResource("TimeTable.fxml"), labels));
			ScreenNavigator.getControllersMap().put("TimeTableNew",         new TimeTableNewController(FXMLs.class.getResource("TimeTableNew.fxml"), labels));
			ScreenNavigator.getControllersMap().put("Home",                 new HomeController(FXMLs.class.getResource("Home.fxml"), labels));
			ScreenNavigator.getControllersMap().put("Settings",             new SettingsController(FXMLs.class.getResource("Settings.fxml"), labels));
			ScreenNavigator.getControllersMap().put("AgendaForClasses",     new AgendaForClassesController(FXMLs.class.getResource("AgendaForClasses.fxml"), labels));
			ScreenNavigator.getControllersMap().put("AgendaForStudents",    new AgendaForStudentsController(FXMLs.class.getResource("AgendaForStudents.fxml"), labels));
			ScreenNavigator.getControllersMap().put("AgendaForTeachers",    new AgendaForTeachersController(FXMLs.class.getResource("AgendaForTeachers.fxml"), labels));
			ScreenNavigator.getControllersMap().put("ConnectionsClass",     new ConnectionsClassController(FXMLs.class.getResource("ConnectionsClass.fxml"), labels));
			ScreenNavigator.getControllersMap().put("ViewClasses",          new ViewClassesController(FXMLs.class.getResource("ViewClasses.fxml"), labels));
			ScreenNavigator.getControllersMap().put("ViewStudents",         new ViewStudentsController(FXMLs.class.getResource("ViewStudents.fxml"), labels));
			ScreenNavigator.getControllersMap().put("ViewTeachers",         new ViewTeachersController(FXMLs.class.getResource("ViewTeachers.fxml"), labels));
			ScreenNavigator.getControllersMap().put("BillingInfo",          new BillingInfoController(FXMLs.class.getResource("BillingInfo.fxml"), labels));
			ScreenNavigator.getControllersMap().put("BillingStudents",      new BillingStudentsController(FXMLs.class.getResource("BillingStudents.fxml"), labels));
			ScreenNavigator.getControllersMap().put("BillingEntry",			new BillingEntryController(FXMLs.class.getResource("BillingEntry.fxml"), labels));
			ScreenNavigator.getControllersMap().put("BillingEditPayment",   new BillingEditPaymentController(FXMLs.class.getResource("BillingEditPayment.fxml"), labels));
                    ScreenNavigator.getControllersMap().put("Temp",   new TempController(FXMLs.class.getResource("Temp.fxml"), labels));
		});
		
//		InitControllersService service = new InitControllersService();
//		service.restart();
	}
	
	@FXML
	private void showDialog(ActionEvent v){
		Dialog <Map<Year, Map<Month, BigDecimal>>> dialog = BillingClassesDialog.getInstance();
		List <LocalDate> dates = new ArrayList<>();
		dates.add(LocalDate.now());
		dialog.setOnShown((DialogEvent event1) -> {
			BillingClassesDialogScene.getController().init(dates, null);
		});
		
		Optional<Map<Year, Map<Month, BigDecimal>>> result  = dialog.showAndWait();
	}
	@FXML
	private void printPayments(ActionEvent v){
		List <PaymentDetails> payments = DBTableUtils.getAllPayments();
		System.out.println("Payments: "+payments.size());
		for(PaymentDetails payment : payments){
			System.out.println("  "+payment.getReceiptNo()+" | "+payment.getPaymentDate() +" | "+payment.getPaidAmount());
		}
		System.out.println("================================================================================");
	}
	
	@FXML
	private void genReceipt(ActionEvent v){
		
		Dialog <Object> dialog = PopupLessonDialog.getInstance();
		PopupLessonDialogScene.getInstance();
		PopUp_LessonController popUpController = PopupLessonDialogScene.getController();
		
		if(!dialog.isShowing()){
			ArrayList <Attendance> tableItems = new ArrayList<>();
			
			
			ObservableList<Aclass> classes = DBTableUtils.getAllClasses();
			Aclass selectedClass = classes.get(0);
			AppointmentImpl appoint = new Agenda.AppointmentImpl()
					.withStartTime(new GregorianCalendar(2015, 3, (4), 14, 0))
					.withEndTime(new GregorianCalendar(2015, 3, (4), 15, 0))
					.withSummary(selectedClass.getName())
					.withDescription("")
					.withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass(selectedClass.getColor()+"-extraLesson"));
			
			Lesson len = null;
			Collection<ListOfWLessons> lessons =  selectedClass.getSchedule().getAllWLessons().values();
			for(ListOfWLessons c : lessons){
				if(!c.getAllLessons().isEmpty()){
					len = c.getAllLessons().get(0);
				}
			}
			appoint = appoint.withLesson(len);
			
			popUpController.initFirstPanel(appoint);
			for(Student aStudent : selectedClass.getStudents()){
				tableItems.add(appoint.getLesson().getAttendance().get(aStudent.getId()));
			}
			popUpController.setTableItems(tableItems);
			dialog.show();
		}
		
	}
	private class InitControllersService extends Service {
		@Override
		protected Task createTask() {
			TimeTableTask theTask = new TimeTableTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				System.err.println("Loading Completed Successfully");
				
			});
			theTask.setOnFailed(event -> {
				System.err.println("Loading Completed with ERROR");
				System.err.println("Loading Completed with ERROR");
				System.err.println("Loading Completed with ERROR"+ event.getEventType());
				System.err.println("Loading Completed with ERROR"+ event.getClass());
				this.getException().printStackTrace(System.err);
			});
			return theTask;
		}
		private class TimeTableTask extends Task {
			@Override
			protected Object call() throws Exception {
				ScreenNavigator.getControllersMap().put("CreateClass",          new CreateClassController(  FXMLs.class.getResource("CreateClass.fxml"),   labels));
				ScreenNavigator.getControllersMap().put("CreateStudent",        new CreateStudentController(FXMLs.class.getResource("CreateStudent.fxml"), labels));
				ScreenNavigator.getControllersMap().put("CreateTeacher",        new CreateTeacherController(FXMLs.class.getResource("CreateTeacher.fxml"), labels));
				ScreenNavigator.getControllersMap().put("PersonsTable_button",  new PersonsTable_Buttons_Controller(FXMLs.class.getResource("PersonsTable_button.fxml"), labels));
				ScreenNavigator.getControllersMap().put("TimeTable",            new TimeTableController(FXMLs.class.getResource("TimeTable.fxml"), labels));
				ScreenNavigator.getControllersMap().put("TimeTableNew",         new TimeTableNewController(FXMLs.class.getResource("TimeTableNew.fxml"), labels));
				ScreenNavigator.getControllersMap().put("Home",                 new HomeController(FXMLs.class.getResource("Home.fxml"), labels));
				ScreenNavigator.getControllersMap().put("Settings",             new SettingsController(FXMLs.class.getResource("Settings.fxml"), labels));
				ScreenNavigator.getControllersMap().put("AgendaForClasses",     new AgendaForClassesController(FXMLs.class.getResource("AgendaForClasses.fxml"), labels));
				ScreenNavigator.getControllersMap().put("AgendaForStudents",    new AgendaForStudentsController(FXMLs.class.getResource("AgendaForStudents.fxml"), labels));
				ScreenNavigator.getControllersMap().put("AgendaForTeachers",    new AgendaForTeachersController(FXMLs.class.getResource("AgendaForTeachers.fxml"), labels));
				ScreenNavigator.getControllersMap().put("ConnectionsClass",     new ConnectionsClassController(FXMLs.class.getResource("ConnectionsClass.fxml"), labels));
				ScreenNavigator.getControllersMap().put("ViewClasses",          new ViewClassesController(FXMLs.class.getResource("ViewClasses.fxml"), labels));
				ScreenNavigator.getControllersMap().put("ViewStudents",         new ViewStudentsController(FXMLs.class.getResource("ViewStudents.fxml"), labels));
				ScreenNavigator.getControllersMap().put("ViewTeachers",         new ViewTeachersController(FXMLs.class.getResource("ViewTeachers.fxml"), labels));
				ScreenNavigator.getControllersMap().put("BillingInfo",          new BillingInfoController(FXMLs.class.getResource("BillingInfo.fxml"), labels));
				ScreenNavigator.getControllersMap().put("BillingStudents",      new BillingStudentsController(FXMLs.class.getResource("BillingStudents.fxml"), labels));
				ScreenNavigator.getControllersMap().put("BillingEntry",			new BillingEntryController(FXMLs.class.getResource("BillingEntry.fxml"), labels));
				ScreenNavigator.getControllersMap().put("BillingEditPayment",   new BillingEditPaymentController(FXMLs.class.getResource("BillingEditPayment.fxml"), labels));
				return null;
			}
		}
	}
}

