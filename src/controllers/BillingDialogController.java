/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import customControls.NumberTextField;
import customObjects.MyDecimalFormater;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import screens.FXMLs;
import utils.Constants;
import utils.dialogs.CustomDialogs;
import DB.objects.PaymentDetails;
import DB.objects.PaymentDetails.PAYMENT_METHOD;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CASH;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.CHECK;
import static DB.objects.PaymentDetails.PAYMENT_METHOD.DEFAULT;
import utils.LanguageUtils;
import utils.Utils;
import utils.dialogs.BillingDialog;
import utils.dialogs.BillingDialog.AUTO_COMPLETE_FLAG;

/**
 *
 * @author dimitris
 */
public class BillingDialogController extends Stage implements Initializable{
	@FXML private GridPane rootGid;
	@FXML private Label payMethodLbl;
	@FXML private ChoiceBox<String> payMethodChoice;
	@FXML private TextField checkNo;
	@FXML private TextField paymentBy;
	@FXML private Label amountPaidLbl;
	@FXML private Label amountDiscountLbl;
	@FXML private Label dateLbl;
	@FXML private Label paymentByLbl;
	@FXML private DatePicker date;
	@FXML private Button clearAllBtn;
	private NumberTextField amountDiscountValue;
	private NumberTextField amountPaidValue;
	private BigDecimal amountTotal;
	private AUTO_COMPLETE_FLAG autoCompleteOptions;
	
	private ChangeListener<BigDecimal> discountListener = (ObservableValue<? extends BigDecimal> observable, BigDecimal oldValue, BigDecimal newValue) -> {
		if(oldValue.compareTo(newValue) != 0){
			//                        if(newValue.compareTo(BigDecimal.ZERO) != 0){
			if(autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.AUTO_COMPLETE)){
				amountPaidValue.setNumber(amountTotal.subtract(newValue));
			}
			else if(autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.NOT_AUTO_COMPLETE)){
				if(newValue.add(amountPaidValue.getNumber()).compareTo(amountTotal) == 1){
					System.out.println("is bigger");
					amountDiscountValue.setNumber(amountTotal.subtract(amountPaidValue.getNumber()));
				}
			}
		}
	};
	private ChangeListener<BigDecimal> paidListener = (ObservableValue<? extends BigDecimal> observable, BigDecimal oldValue, BigDecimal newValue) -> {
		if(oldValue.compareTo(newValue) != 0){
//                        if(newValue.compareTo(BigDecimal.ZERO) != 0){
			
			if(autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.AUTO_COMPLETE)){
				amountDiscountValue.setNumber(amountTotal.subtract(newValue));
			}
			else if(autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.NOT_AUTO_COMPLETE)){
				if(newValue.add(amountDiscountValue.getNumber()).compareTo(amountTotal) == 1){
					System.out.println("is bigger");
					amountPaidValue.setNumber(amountTotal.subtract(amountDiscountValue.getNumber()));
				}
			}
//                        }
		}
	};
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<String> choices = FXCollections.observableArrayList();
		choices.addAll(PAYMENT_METHOD.getStringValue(DEFAULT), PAYMENT_METHOD.getStringValue(CASH), PAYMENT_METHOD.getStringValue(CHECK));
		payMethodChoice.setItems(choices);
		
		date.setValue(LocalDate.now());
		
		checkNo.setVisible(false);
		amountDiscountValue = new NumberTextField();
		amountPaidValue = new NumberTextField();
		amountDiscountValue.setMaxHeight(27);
		amountPaidValue.setMaxHeight(27);
		GridPane.setColumnIndex(amountDiscountValue, 2);
		GridPane.setColumnIndex(amountPaidValue, 2);
		GridPane.setRowIndex(amountDiscountValue, 2);
		GridPane.setRowIndex(amountPaidValue, 1);
		
		rootGid.getChildren().addAll(amountPaidValue, amountDiscountValue);
		payMethodChoice.valueProperty().addListener(
				(ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
					if(newValue != null){
						if(! newValue.equals(oldValue)){
							checkNo.setVisible(PAYMENT_METHOD.getStringValue(CHECK).equals(newValue));
						}
					}
				});
		amountPaidValue.numberProperty().addListener(paidListener);
		amountDiscountValue.numberProperty().addListener(discountListener);
	}
	
	public void setInitialValues(PaymentDetails details){
		this.amountTotal = details.getTotalAmount();
		this.payMethodChoice.getSelectionModel().select(PAYMENT_METHOD.getStringValue(details.getPaymentMethod()));
		MyDecimalFormater format = new MyDecimalFormater();
		
		if(details.getDiscount() != null){
			this.amountDiscountValue.setText(format.numberToString(details.getDiscount()));
		}
		if(details.getPaidAmount() != null){
			this.amountPaidValue.setText(format.numberToString(details.getPaidAmount()));
		}
		if(details.getCheckNo() != null){
			this.checkNo.setText(details.getCheckNo());
		}
		if(details.getPaymentDate() != null){
			this.date.setValue(details.getPaymentDate());
		}
		if(Utils.isValidParam(details.getPaymentBy())){
			this.paymentBy.setText(details.getPaymentBy());
		}
	}
	
	public BillingDialogController(String defaultName, String defaultAddress, Parent parent, BigDecimal total)
	{
		setTitle(LanguageUtils.getString("billing.dialog.title"));
		FXMLLoader fxmlLoader = new FXMLLoader(FXMLs.class.getResource("BillingDialog.fxml"));
		fxmlLoader.setController(this);
		fxmlLoader.setResources(LanguageUtils.getInstance().getCurrentLanguage());
		// Nice to have this in a load() method instead of constructor, but this seems to be de-facto standard.
		try
		{
			setScene(new Scene((Parent) fxmlLoader.load()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public BillingDialogController(String defaultName, String defaultAddress, Parent parent, BigDecimal total, AUTO_COMPLETE_FLAG options)
	{
		this.autoCompleteOptions = options;
		setTitle("Payment Details");
		FXMLLoader fxmlLoader = new FXMLLoader(FXMLs.class.getResource("BillingDialog.fxml"));
		fxmlLoader.setController(this);
		// Nice to have this in a load() method instead of constructor, but this seems to be de-facto standard.
		try
		{
			setScene(new Scene((Parent) fxmlLoader.load()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void setTotalAmount(BigDecimal total){
		this.amountTotal = total;
		
		if(this.amountTotal == null){
			System.err.println("this.amountTotal == null");
		}
		if(this.amountDiscountValue == null){
			System.err.println("this.amountDiscountValue == null");
		}
//	amountDiscountValue.setMaxValue(this.amountTotal);
//	amountPaidValue.setMaxValue(this.amountTotal);
	}
	
	
	@FXML
	private void clearAll(Event e){
		Optional<ButtonType> response = CustomDialogs.yesNoDialog(LanguageUtils.getString("billing.dialog.clearQ"),
				LanguageUtils.getString("billing.dialog.question"),
				LanguageUtils.getString("dialog.yes"),
				LanguageUtils.getString("dialog.no"));
		if(response.isPresent() && !LanguageUtils.getString("dialog.yes").equals(response.get().getText())){
			return;
		}
		
		payMethodChoice.getSelectionModel().selectFirst();
		amountDiscountValue.numberProperty().removeListener(discountListener);
		amountPaidValue.numberProperty().removeListener(paidListener);
		amountDiscountValue.setNumber(BigDecimal.ZERO);
		amountPaidValue.setNumber(BigDecimal.ZERO);
		amountPaidValue.numberProperty().addListener(paidListener);
		amountDiscountValue.numberProperty().addListener(discountListener);
		checkNo.setVisible(false);
		checkNo.setText("");
		date.setValue(LocalDate.now());
		paymentBy.setText("");
	}
	
	public ChoiceBox<String> getPayMethodChoice() { return payMethodChoice; }
	public void setPayMethodChoice(ChoiceBox<String> payMethodChoice) { this.payMethodChoice = payMethodChoice; }
	public TextField getCheckNo() { return checkNo; }
	public void setCheckNo(TextField checkNo) { this.checkNo = checkNo; }
	public NumberTextField getAmountDiscountValue() { return amountDiscountValue; }
	public void setAmountDiscountValue(NumberTextField amountDiscountValue) { this.amountDiscountValue = amountDiscountValue; }
	public NumberTextField getAmountPaidValue() { return amountPaidValue; }
	public void setAmountPaidValue(NumberTextField amountPaidValue) { this.amountPaidValue = amountPaidValue; }
	public BigDecimal getAmountTotal() { return amountTotal; }
	public void setAmountTotal(BigDecimal amountTotal) { this.amountTotal = amountTotal; }
	public DatePicker getDate() { return date; }
	public void setDate(DatePicker date) { this.date = date; }
	public TextField getPaymentBy() { return paymentBy; }
	public void setPaymentBy(TextField paymentBy) { this.paymentBy = paymentBy; }
	
	
	public void initValues(PaymentDetails payment){
		amountDiscountValue.numberProperty().removeListener(discountListener);
		amountPaidValue.numberProperty().removeListener(paidListener);
		setTotalAmount(payment.getTotalAmount());
		getAmountDiscountValue().setNumber(payment.getDiscount());
		getAmountPaidValue().setNumber(payment.getPaidAmount());
		amountPaidValue.numberProperty().addListener(paidListener);
		amountDiscountValue.numberProperty().addListener(discountListener);
		if(payment.getPaymentDate().equals(Constants.UNKOWN_DATE)){
			getDate().setValue(LocalDate.now());
		}
		else{
			getDate().setValue(payment.getPaymentDate());
		}
		this.payMethodChoice.getSelectionModel().select(PAYMENT_METHOD.getStringValue(payment.getPaymentMethod()));
		this.paymentBy.setText(payment.getPaymentBy());
	}
	
	public void setAutoComplete(BillingDialog.AUTO_COMPLETE_FLAG options) {
		this.autoCompleteOptions = options;
//	if(autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.NOT_AUTO_COMPLETE)){
//	    amountDiscountValue.numberProperty().removeListener(discountListener);
//	    amountPaidValue.numberProperty().removeListener(paidListener);
//	}
//	if (autoCompleteOptions.equals(AUTO_COMPLETE_FLAG.AUTO_COMPLETE)){
//	    amountPaidValue.numberProperty().addListener(paidListener);
//	    amountDiscountValue.numberProperty().addListener(discountListener);
//	}
	}
}
