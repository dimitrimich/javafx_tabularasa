/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.BillingPersister;
import DB.DButils;
import DB.objects.Aclass;
import DB.objects.Billing;
import DB.objects.BillingMonth;
import DB.objects.Lesson;
import DB.objects.Student;
import customObjects.MyDecimalFormater;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import screens.FXMLs;
import utils.dialogs.BillingClassesRow;
import fxObjects.BillingClassesRowFX;
import utils.dialogs.BillingClassesTable;
import utils.dialogs.BillingDialog;
import utils.dialogs.BillingDialogScene;
import DB.objects.PaymentDetails;
import com.lowagie.text.DocumentException;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.TextStyle;
import java.util.Map;
import java.util.Map.Entry;
import utils.LanguageUtils;
import utils.dialogs.BillingClassesUtil;
import utils.dialogs.BillingConfirmationDialog;
import utils.dialogs.BillingConfirmationDialogScene;
import utils.dialogs.BillingDialog.AUTO_COMPLETE_FLAG;
import utils.dialogs.CustomDialogs;
import utils.pdf.ReceiptGenerator;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class BillingClassesDialogController extends Stage implements Initializable {
	private @FXML BorderPane mainPane, progressPane;
	private @FXML StackPane stackPane;
	private @FXML ProgressIndicator progressIndicator;
	private @FXML TableView<BillingClassesRowFX> classesTable;
	private @FXML ScrollPane toBePaidScroll;
	private @FXML Label noOfClassesValue, totalAmountValue;
	private @FXML Button payBtn;
	private @FXML Label totalsLabel, classesLabel, dueAmountLabel;
	
	private MyVBox totalsContainer;
	
	private MyDecimalFormater formater = new MyDecimalFormater();
	private int noOfClasses = 0;
	private BigDecimal totalAmount = BigDecimal.ZERO;
	private PaymentDetails totalPayment = new PaymentDetails();
	private List<LocalDate> months;
	private Student currentStudent;
	
	private Map<Year, Map<Month, BigDecimal>> paymentsToReturn;
	
	public BillingClassesDialogController(String defaultName, String defaultAddress, Parent parent)
	{
		setTitle(LanguageUtils.getString("billing.classes.title"));
		FXMLLoader fxmlLoader = new FXMLLoader(FXMLs.class.getResource("BillingClassesDialog.fxml"));
		fxmlLoader.setController(this);
		fxmlLoader.setResources(LanguageUtils.getInstance().getCurrentLanguage());
		try
		{
			setScene(new Scene((Parent) fxmlLoader.load()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public Void payForClass(BillingClassesRowFX toPay, Boolean isSelected){
		if(toPay.getRemainingAmountToPay().compareTo(BigDecimal.ZERO) == 0)
		{
			if(totalsContainer.contents.containsKey(toPay)){
				System.out.println("Removing fully Paid Class");
				totalsContainer.removeClass(toPay);
				return null;
			}
		}
		if(isSelected){
			totalsContainer.addNewClass(toPay);
		}else{
			totalsContainer.removeClass(toPay);
		}
		return null;
	}
	
	
	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		toBePaidScroll.setContent(totalsContainer = new MyVBox());
		mainPane.setVisible(false);
		progressPane.setVisible(true);
		BillingClassesTable.initTable(classesTable, this::payForClass);
	}
	
	
	public void init(List<LocalDate> monthsToPay, Student stu){
		this.months = monthsToPay;
		this.currentStudent = stu;
		this.noOfClasses = 0;
		this.totalPayment = new PaymentDetails();
		this.totalAmount = BigDecimal.ZERO;
		this.toBePaidScroll.setContent(totalsContainer = new MyVBox());
		this.mainPane.setVisible(false);
		this.progressPane.setVisible(true);
		this.classesTable.getItems().clear();
		this.noOfClassesValue.setText("");
		this.totalAmountValue.setText("");
		this.payBtn.setDisable(true);
		paymentsToReturn = new HashMap<>();
		loadData();
	}
	
	/**
	 * start Service that loads Data
	 */
	private void loadData() {
		final BillingRowFXLoader serviceExample = new BillingRowFXLoader();
		
		//Here you tell your progress indicator is visible only when the service is runing
		progressIndicator.visibleProperty().bind(serviceExample.runningProperty());
		serviceExample.setOnSucceeded((WorkerStateEvent workerStateEvent) -> {
			ObservableList<BillingClassesRowFX> theRows = serviceExample.getValue();   //here you get the return value of your service
			System.out.println("classesTable.setItems "+ theRows.size());
			classesTable.setItems(theRows);
			mainPane.setVisible(true);
			progressPane.setVisible(false);
		});
		
		serviceExample.setOnFailed((WorkerStateEvent workerStateEvent) -> {
			//DO stuff on failed
		});
		System.out.println("Service starting....");
		serviceExample.restart(); //here you start your service
		
	}
	
	/**
	 * add the payment to return it to main screen in order to update the tableView
	 */
	private void addPaymentToReturnPayments(PaymentDetails paymentToAdd, Month currentMonth, Year currentYear) {
		
		Map<Month, BigDecimal> paymentsOfTheYear = paymentsToReturn.get(currentYear);
		// this year is not added yet
		if(paymentsOfTheYear == null){
			paymentsOfTheYear = new HashMap<>();
			paymentsOfTheYear.put(currentMonth, paymentToAdd.getDiscount().add(paymentToAdd.getPaidAmount()));
			paymentsToReturn.put(currentYear, paymentsOfTheYear);
		}else{
			// this year was added before
			BigDecimal paymentOfTheMonth = paymentsOfTheYear.get(currentMonth);
			// this month is not added yet
			if(paymentOfTheMonth == null){
				paymentsOfTheYear.put(currentMonth, paymentToAdd.getDiscount().add(paymentToAdd.getPaidAmount()));
			}else{
				// increase existing payment
				paymentOfTheMonth.add(paymentToAdd.getDiscount().add(paymentToAdd.getPaidAmount()));
			}
		}
	}
	/**
	 * Create the BillingRowFX rows
	 */
	public class BillingRowFXLoader extends Service<ObservableList<BillingClassesRowFX>> {
		@Override
		protected Task<ObservableList<BillingClassesRowFX>> createTask() {
			return new Task<ObservableList<BillingClassesRowFX>>() {
				@Override
				protected ObservableList<BillingClassesRowFX> call() throws Exception {
					System.out.println("start loading....");
					//DO YOU HARD STUFF HERE
					ObservableList<BillingClassesRowFX> theRows = FXCollections.observableArrayList();
					try{
						
						if(currentStudent == null){
							currentStudent = DButils.getFirstStudent();
						}
						System.out.println("Found stu !");
						List <Aclass> classes = currentStudent.getClasses();
						
						List<BillingClassesRow> rows = new ArrayList<>();
						for(LocalDate monthDetails : months){
							System.out.println("for month: "+monthDetails.getMonth());
							Month currentMonth = monthDetails.getMonth();
							Year currentYear = Year.of(monthDetails.getYear());
							
							for(Aclass aClass : classes){
								System.out.println("for class: "+aClass.getName());
								BillingClassesRow totals;
								if(aClass.isPaidPerMonth()){
									totals = calculateMontlyClassBillingClassesRow(aClass, currentStudent.getId(), currentMonth, currentYear);
								}else{
									totals = calculateHourlyClassBillingClassesRow(aClass, currentStudent.getId(), currentMonth, currentYear);
								}
								totals.setTheClass(aClass);
								totals.setName(new StringBuilder(aClass.getName()).append(" (").append(aClass.getCodeName()).append(")").toString());
								totals.setYear(currentYear);
								totals.setMonth(currentMonth);
								rows.add(totals);
							}
							System.out.println("rows.size() = "+ rows.size());
						}
						for(BillingClassesRow temp : rows){
							BillingClassesRowFX perm = new BillingClassesRowFX(temp);
							theRows.add(perm);
						}
						System.out.println("theRows.size() = "+ theRows.size());
						System.out.println("done loading.!");
					} catch(Exception e){
						System.err.println("Exception while loading");
						e.printStackTrace(System.err);
					}
					return theRows;
				}
				
				/**
				 * class paid per month
				 */
				private BillingClassesRow calculateMontlyClassBillingClassesRow(Aclass aClass, Long studentID, Month currentMonth, Year currentYear) {
					BillingClassesRow totals = new BillingClassesRow();
					List<Lesson> lessons =  aClass.getLessonsForAMonth(currentMonth, currentYear);
					for(Lesson aLesson : lessons){
						totals.increaseNumberOfLessonsBy1();
						totals.increaseDurationBy(aLesson.getDuration());
						if(aLesson.isCanceled()){
							totals.increaseCanceledLessonsBy1();
						}
						if(aLesson.isExtraLesson()){
							totals.increaseExtraLessonsBy1();
						}
					}
					totals.setFullAmountOfLessons(aClass.getMonthlyFee());
					BillingMonth theBillingMonth = aClass.getBillingMonth(currentMonth, currentYear);
					List<BillingMonth> billings = aClass.getBillingMonths();
					int i = 0;
					for(BillingMonth monthh : billings){
						System.out.println(i+" > "+monthh.getBillingYear()+" , "+monthh.getBillingMonth());
						i++;
					}
					System.out.println("month = "+currentMonth+" | year = " + currentYear +"| billing = "+theBillingMonth);
					if(theBillingMonth != null){
						totals.setDiscountOfLessons(theBillingMonth.getTotalDiscountForStudent(studentID));
						totals.setPaidAmountOfLessons(theBillingMonth.getTotalPaidForStudent(studentID));
					}
					return totals;
					
				}
				/**
				 * class paid per hour
				 */
				private BillingClassesRow calculateHourlyClassBillingClassesRow(Aclass aClass, Long studentId, Month currentMonth, Year currentYear) {
					BillingClassesRow totals = new BillingClassesRow();
					List<Lesson> lessons =  aClass.getLessonsForAMonth(currentMonth, currentYear);
					System.out.println("Lessons: ");
					for(Lesson aLesson : lessons){
						totals.increaseNumberOfLessonsBy1();
						totals.increaseDurationBy(aLesson.getDuration());
						if(aLesson.isCanceled()){
							totals.increaseCanceledLessonsBy1();
						}
						if(aLesson.isExtraLesson()){
							totals.increaseExtraLessonsBy1();
						}
						Billing aBilling = aLesson.getBilling().get(studentId);
						System.out.println("   "+aBilling.getAmountFull() +" | "+ aBilling.getTotalDiscount()+ " | "+ aBilling.getTotalPaidAmount());
						totals.increaseFullAmountOfLessons(aBilling.getAmountFull());
						totals.increaseDiscountOfLessons(aBilling.getTotalDiscount());
						totals.increasePaidAmountOfLessons(aBilling.getTotalPaidAmount());
					}
					return totals;
				}
			};
		}
	}
	@FXML
	private void showPaymentDialog(ActionEvent e) throws Exception{
		totalPayment = new PaymentDetails();
		Dialog paymentDialog = BillingDialog.getInstance(AUTO_COMPLETE_FLAG.NOT_AUTO_COMPLETE);
		for(BillingClassesRowFX billing : totalsContainer.contents.keySet()){
			totalPayment.increaseTotalAmount(billing.getRemainingAmountToPay());
//			totalPayment.increasePaidAmount(billing.getPaidAmountOfLessons());
//			totalPayment.increaseDiscount(billing.getDiscountOfLessons());
		}
		
		paymentDialog.setOnShown((Event showEvent) -> {
			System.out.println("totals.paid ="+totalPayment.getPaidAmount());
			System.out.println("totals.discount ="+totalPayment.getDiscount());
			System.out.println("totals.full ="+totalPayment.getTotalAmount());
			
			BillingDialogScene.getController().initValues(totalPayment);
		});
		
		
		Optional <PaymentDetails> details = paymentDialog.showAndWait();
		if(details.isPresent()){
			PaymentDetails paymentFromDialog = details.get();
			if(paymentFromDialog.isValidPayment()){
				String receiptId = DButils.getNextReceiptId();
				paymentFromDialog.setReceiptNo(receiptId);
				System.out.println("Receipt : "+ receiptId);
				System.out.println("Receipt : "+ receiptId);
				System.out.println("Receipt : "+ receiptId);
				System.out.println("Receipt : "+ receiptId);
				
				HashMap<BillingClassesRowFX, PaymentDetails> payments = new HashMap<>();
				System.out.println("contents = "+ totalsContainer.contents.size());
				for(BillingClassesRowFX billing : totalsContainer.contents.keySet()){
					payments.put(billing, new PaymentDetails());
				}
				BillingClassesUtil.dividePayments(payments, paymentFromDialog);
				System.out.println("Calculation done \n ==============================================");
				BillingClassesUtil.printMap(payments);
				System.out.println("================================================================");
				
				
				// for the receipt
				List <String> classAndMonths = new ArrayList<>();
				for(Entry<BillingClassesRowFX, PaymentDetails> entryToAdd : payments.entrySet()){
					BillingClassesRowFX row = entryToAdd.getKey();
					Month currentMonth = row.getMonth();
					Year currentYear = row.getYear();
					PaymentDetails paymentToAdd = entryToAdd.getValue();
					paymentFromDialog.copyToExceptAmounts(paymentToAdd);
					
					Aclass classToEdit = row.getTheClass();
					
					// paid per month
					if(classToEdit.isPaidPerMonth()){
						System.out.println("Class is Paid Per Month "+ classToEdit.getName());
//					// save the new payment and the edits to billing Month
						BillingMonth monthToEdit = classToEdit.getBillingMonth(currentMonth, currentYear);
						paymentToAdd.setBillingMonth(currentMonth);
						paymentToAdd.setClassName(row.getName()+" | "+row.getTheClass().getCodeName());
						monthToEdit.getAllPayments().get(currentStudent.getId()).getPayments().add(paymentToAdd);
						addPaymentToReturnPayments(paymentToAdd, currentMonth, currentYear);
						paymentToAdd.setPaymentFor(currentStudent.getFullName());
						paymentToAdd.setPaymentForId(currentStudent.getId());
						if(monthToEdit.getRemainingAmountForStudent(currentStudent).compareTo(BigDecimal.ZERO) == 0 ){
							System.err.println(" <><> CLASS PAID in full <><>");
							paymentToAdd.setPaidInFullForReceipt(true);
						}else{
							System.err.println(" <><> CLASS NOT paid in full <><>");
							paymentToAdd.setPaidInFullForReceipt(false);
						}
						// Save Payment
						BillingPersister.saveNewPaymentAndBillingMonthEdits(paymentToAdd, monthToEdit, currentStudent);
						
						// for the receipt
						String classAndMonth = "Pliromi " + currentMonth.getDisplayName(TextStyle.FULL, LanguageUtils.getCurrentLocale())+" gia taksi "+classToEdit.getName()+" ("+classToEdit.getCodeName()+")";
						if(!classAndMonths.contains(classAndMonth)){
							classAndMonths.add(classAndMonth);
						}
					}
					else{ // paid per lesson
						System.out.println("class is hourly "+ classToEdit.getName());
						ArrayList<Lesson> lessons = classToEdit.getLessonsForAMonth(currentMonth, currentYear);
						Map<Lesson, PaymentDetails> allLessonsPayments = new HashMap<>();
						int i = 0;
						// get all lessons that are not paid
						for (Lesson aLesson : lessons){
							Billing billingForStudent = aLesson.getBillingForStudent(currentStudent);
							// billing has remaining
							if(!billingForStudent.isPaidInFull()){
								PaymentDetails lessonPayment = new PaymentDetails();
								paymentFromDialog.copyToExceptAmounts(lessonPayment);
								lessonPayment.setPaymentFor(currentStudent.getFullName());
								lessonPayment.setPaymentForId(currentStudent.getId());
								allLessonsPayments.put(aLesson, lessonPayment);
								i++;
							}
						}
						// calculate payment per lesson
						BillingClassesUtil.divideLessonPayments(allLessonsPayments, paymentToAdd, currentStudent);
						addPaymentToReturnPayments(paymentToAdd, currentMonth, currentYear);
						
						// add the payment to lesson's billing and save them
						for(Entry<Lesson, PaymentDetails> entry : allLessonsPayments.entrySet()){
							PaymentDetails newPayment = entry.getValue();
							newPayment.setBillingMonth(currentMonth);
							newPayment.setClassName(row.getName()+" | "+row.getTheClass().getCodeName());
							System.out.println(" 2. saving --->"+row.getName()+" | "+row.getTheClass().getCodeName());
							paymentToAdd.setBillingMonth(currentMonth);
							paymentToAdd.setClassName(row.getName()+" | "+row.getTheClass().getCodeName());
							Billing billingToEdit = entry.getKey().getBillingForStudent(currentStudent);
							billingToEdit.addPayment(newPayment);
//							if(billingToEdit.isPaidInFull()){
//								System.err.println(" <><> LESSON PAID in full <><>");
//								paymentToAdd.setPaidInFullForReceipt(true);
//							}else{
//								System.err.println(" <><> LESSON NOT paid in full <><>");
							paymentToAdd.setPaidInFullForReceipt(false);
//							}
							// Save Payment
							BillingPersister.savePaymentAndBilling(newPayment, billingToEdit);
							// for the receipt
							String classAndMonth = "Pliromi " + currentMonth.getDisplayName(TextStyle.FULL, LanguageUtils.getCurrentLocale())+" gia taksi "+classToEdit.getName()+" ("+classToEdit.getCodeName()+")";
							if(!classAndMonths.contains(classAndMonth)){
								classAndMonths.add(classAndMonth);
							}
						}
					}
					// should update the table with the payments
					int rowIndex = classesTable.getItems().indexOf(row);
					System.out.println("row "+rowIndex+" edited");
					System.out.println(" old values: ");
					System.out.println("   getName: "+ row.getName());
					System.out.println("   getPaidAmountOfLessons: "+ row.getPaidAmountOfLessons());
					System.out.println("   getRemainingAmountToPay: "+ row.getRemainingAmountToPay());
					
					System.out.println("Increase Amount by: "+ paymentToAdd.getPaidAmount() +" (paid)|"+paymentToAdd.getDiscount()+" (discount)");
					row.addPayment(paymentToAdd);
					totalsContainer.editDueAmount(row);
					System.out.println("increasesd ??!?!");
				}// Confirmation Dialog / Generate Receipt
				Dialog confirmationDialog = BillingConfirmationDialog.getInstance();
				confirmationDialog.setOnShown((Event showEvent) -> {
					BillingConfirmationDialogScene.getController().setAmounts(paymentFromDialog.getPaidAmount(), paymentFromDialog.getDiscount());
				});
				Optional <Boolean> genRecepipt = confirmationDialog.showAndWait();
				if(genRecepipt.isPresent()){
					if(genRecepipt.get()){
						ReceiptGenerator receipt = new ReceiptGenerator();
						receipt.setPayment(paymentFromDialog);
						receipt.setClassesAndMonths(classAndMonths);
						try {
							receipt.createPdfReceipt();
						} catch (IOException | DocumentException ex) {
							ex.printStackTrace(System.err);
						}
						System.out.println("GENERATE RECEIPT ");
					}
				}
			}else{
				CustomDialogs.infoDialog("Billing", "Didn't specify amounts...");
				System.out.println("Payment does not include any amount");
			}
		}
	}
	
	public Map<Year, Map<Month, BigDecimal>> getPaymentsToReturn() {
		return paymentsToReturn;
	}
	
	
	
	class MyVBox extends VBox{
		public MyVBox(){
			this.setMaxWidth(283);
			this.setMinWidth(283);
		}
		private HashMap<BillingClassesRowFX, GridPane> contents = new HashMap<>();
		
		public void addNewClass(BillingClassesRowFX toPay) {
			GridPane classGrid = new GridPane();
			contents.put(toPay, classGrid);
			ColumnConstraints internalGridColConstraints1 = new ColumnConstraints();
			internalGridColConstraints1.setHgrow(Priority.SOMETIMES);
			internalGridColConstraints1.setMinWidth(10);
			internalGridColConstraints1.setPrefWidth(100);
			ColumnConstraints internalGridColConstraints2 = new ColumnConstraints();
			internalGridColConstraints2.setHgrow(Priority.SOMETIMES);
			internalGridColConstraints2.setMinWidth(10);
			internalGridColConstraints2.setPrefWidth(100);
			classGrid.getColumnConstraints().addAll(internalGridColConstraints1, internalGridColConstraints2);
			
			RowConstraints internalGridRowConstraints1 = new RowConstraints();
			internalGridRowConstraints1.setMinHeight(10);
			internalGridRowConstraints1.setPrefHeight(30);
			internalGridRowConstraints1.setVgrow(Priority.SOMETIMES);
			
			classGrid.getRowConstraints().addAll(internalGridRowConstraints1);
			
			Label name = new Label();
			name.setText(new StringBuilder(toPay.getName()).append(" (").append(toPay.getTheClass().getCodeName()).append("):  ").toString());
			Label total = new Label();
			total.setText(formater.numberToString(toPay.getRemainingAmountToPay()));
			GridPane.setColumnIndex(name, 0);
			GridPane.setRowIndex(name, 0);
			GridPane.setColumnIndex(total, 1);
			GridPane.setRowIndex(total, 0);
			classGrid.getChildren().addAll(name, total);
			
			this.getChildren().add(classGrid);
			noOfClasses++;
			totalAmount = totalAmount.add(toPay.getRemainingAmountToPay());
			noOfClassesValue.setText(Integer.toString(noOfClasses));
			totalAmountValue.setText(formater.numberToString(totalAmount));
			if(payBtn.isDisable()){
				payBtn.setDisable(false);
			}
		}
		
		public void removeClass(BillingClassesRowFX toPay) {
			this.getChildren().remove(contents.get(toPay));
			contents.remove(toPay);
			if(noOfClasses != 0){
				noOfClasses--;
				refreshTotalAmount(); // refresh amount because when removing it can be fully paid(remaining == 0)
				noOfClassesValue.setText(Integer.toString(noOfClasses));
			}
			if(noOfClasses == 0){
				payBtn.setDisable(true);
			}
		}
		public void editDueAmount(BillingClassesRowFX toPay){
			if(this.getChildren().indexOf(contents.get(toPay)) != -1){
				GridPane toEdit = (GridPane)this.getChildren().get(this.getChildren().indexOf(contents.get(toPay)));
				((Label)toEdit.getChildren().get(1)).setText(formater.numberToString(toPay.getRemainingAmountToPay()));
				refreshTotalAmount();
			}
		}
		
		private void refreshTotalAmount(){
			totalAmount = BigDecimal.ZERO;
			for(BillingClassesRowFX row : contents.keySet()){
				totalAmount = totalAmount.add(row.getRemainingAmountToPay());
			}
			totalAmountValue.setText(formater.numberToString(totalAmount));
		}
	}
	
	public void changeLocale(){
		ResourceBundle labels = LanguageUtils.getInstance().getCurrentLanguage();
		payBtn.setText(labels.getString("billing.classes.pay"));
		totalsLabel.setText(labels.getString("billing.classes.totals"));
		classesLabel.setText(labels.getString("billing.classes.no"));
		dueAmountLabel.setText(labels.getString("billing.classes.due"));
		
		
	}
}
