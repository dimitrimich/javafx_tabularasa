
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import utils.Constants;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class SettingsController extends StageController{
	@Override
	public void onLoad() {
	}
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public SettingsController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	
	@FXML private Label settingsLabel;
	@FXML private Label lSettingsLabel;
	@FXML private ChoiceBox<String> choice;
	
	final String[] languages = new String []{"English", "Ελληνικά"};
//    final String[] languagesEl = new String []{"Γλώσσα", "English", "Ελληνικά"};
	private ObservableList <String> languagesList = FXCollections.observableArrayList(languages);
	
	@FXML
	private void handleButtonAction(ActionEvent event) throws IOException {
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb){
		choice.getItems().setAll(languagesList);
		if(LanguageUtils.getInstance().getCurrentLanguageCode() == Constants.ENGLISH){
			choice.getSelectionModel().select(0);
		}else{
			choice.getSelectionModel().select(1);
		}
		choice.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if(newValue == oldValue){ return; }
			switch(newValue.intValue()){
				case 0:
					changeLanguage(Constants.ENGLISH);
					break;
				case 1:
					changeLanguage(Constants.GREEK);
					break;
			}
		});
	}
	
	/**  Set the text to all visible elements of the stage */
	@Override
	protected void changeLocale(){
		settingsLabel.setText(labels.getString("settings.title"));
		lSettingsLabel.setText(labels.getString("settings.language.title"));
	}
	
	private void changeLanguage(int language){
		labels = LanguageUtils.getInstance().switchLanguage(language);
		changeLocale();
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	@Override
	public void unload(){}
	
}

