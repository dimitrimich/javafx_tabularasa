/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package controllers;

import DB.DBDeleteUtils;
import DB.DBTableUtils;
import DB.DBUpdateUtils;
import DB.objects.Address;
import DB.objects.ContactInformation;
import DB.objects.Teacher;
import images.Images;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import screens.FXMLs;
import tables.TeachersTable;
import utils.Constants;
import utils.dialogs.CustomDialogs;
import utils.EventFilters;
import utils.LanguageUtils;
import utils.Utils;
import static utils.Utils.isValidParam;
import static utils.Utils.notEqual;
import utils.menu.ScreenNavigator;

/**
 *
 * @author d.michaelides
 */
public class ViewTeachersController extends StageController{
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	
	public ViewTeachersController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	@FXML private AnchorPane root;
	@FXML private ScrollPane theScroll;
	@FXML private VBox theV;
	@FXML private ScrollPane theScroll2;
	@FXML private VBox theV2;
	@FXML private SplitPane splitPane;
	@FXML private BorderPane region;
	@FXML private ProgressIndicator progress;
	@FXML private ProgressIndicator refreshProgress;
	@FXML private Label hint;
	
	@FXML private Label mainTitle;
	@FXML private TableView<Teacher> teachersTable;
	@FXML private TextField searchBox;
	
	@FXML private Label profileLabel;
	@FXML private ImageView profileImage;
	@FXML private Button addPictureButton;
	@FXML private Button cropButton;
	@FXML private Button deletePic;
	@FXML private Label surname;
	@FXML private Label name;
	@FXML private Label b_info;
	@FXML private TextField nameText;
	@FXML private TextField surnameText;
	@FXML private Label mName;
	@FXML private Label email2;
	@FXML private TextField mNameText;
	@FXML private TextField email2Text;
	
	@FXML private Label a_info;
	@FXML private Label subTitle;
	@FXML private Label address1Title;
	@FXML private Label address1StreetNum;
	@FXML private Label address1StreetName;
	@FXML private Label address1Area;
	@FXML private Label address1City;
	@FXML private Label address1Zip;
	@FXML private TextField address1StreetNumText;
	@FXML private TextField address1StreetNameText;
	@FXML private TextField address1AreaText;
	@FXML private TextField address1CityText;
	@FXML private TextField address1ZipText;
	
	@FXML private Label address2Title;
	@FXML private Label address2StreetNum;
	@FXML private Label address2StreetName;
	@FXML private Label address2Area;
	@FXML private Label address2City;
	@FXML private Label address2Zip;
	@FXML private TextField address2StreetNumText;
	@FXML private TextField address2StreetNameText;
	@FXML private TextField address2AreaText;
	@FXML private TextField address2CityText;
	@FXML private TextField address2ZipText;
	@FXML private Label c_info;
	@FXML private Label phone1;
	@FXML private Label phone2;
	@FXML private Label email;
	@FXML private TextField phone1Text;
	@FXML private TextField phone2Text;
	@FXML private TextField emailText;
	@FXML private Button confirmButton;
	@FXML private Button cancelButton;
	@FXML private Button deleteTeacher;
	@FXML private Label theTeachersName;
	
	@FXML private TabPane mainTabPane;
	@FXML private TabPane editTabPane;
	@FXML private Tab editTab;
	@FXML private Tab calendarTab, connectionsTabHead, genTab;
	
	@FXML private ConnectionsTeacherController connectionsTabController;
	private final AgendaForTeachersController agendaTabController = (AgendaForTeachersController) ScreenNavigator.getControllersMap().get("AgendaForTeachers");
	
	
	private  ObservableList<Teacher> observableTeachers;
	private SerialBlob imgFromUser_blob;
	private Image imgFromUser;
	private Teacher selectedTeacher;
	
	private final Image defaultImage  = new Image(Images.class.getResource("default_profile.png").toString(), 140, 140, true, true);
	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		calendarTab.setContent(agendaTabController.getScroll());
		calendarTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue){
				agendaTabController.setUpCallBack();
				agendaTabController.setUpAppoinmentCallBack();
				agendaTabController.setUpPopUpCallBack();
				agendaTabController.setUpPopUpHideCallBack();
			}
		});
		
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		theV.prefWidthProperty().bind(theScroll.widthProperty());
		theV2.prefWidthProperty().bind(theScroll2.widthProperty());
		
		
		teachersTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Teacher> ov, Teacher oldValue, Teacher newValue) -> {
			if(oldValue != newValue){
				if(newValue != null){
					editTab.setDisable(true);
					if(theScroll2.isDisable()){
						theScroll2.setDisable(false);
					}
					selectedTeacher = newValue;
					showTeachersInfo();
					editTab.setDisable(false);
				}
			}
		});
		EventFilters.setEditableTextField(nameText);
		EventFilters.setEditableTextField(surnameText);
		EventFilters.setEditableTextField(address2StreetNumText);
		EventFilters.setEditableTextField(address2StreetNameText);
		EventFilters.setEditableTextField(address2AreaText);
		EventFilters.setEditableTextField(address2CityText);
		EventFilters.setEditableTextField(address2ZipText);
		EventFilters.setEditableTextField(address1StreetNumText);
		EventFilters.setEditableTextField(address1StreetNameText);
		EventFilters.setEditableTextField(address1AreaText);
		EventFilters.setEditableTextField(address1CityText);
		EventFilters.setEditableTextField(address1ZipText);
		EventFilters.setEditableTextField(phone1Text);
		EventFilters.setEditableTextField(phone2Text);
		EventFilters.setEditableTextField(emailText);
		EventFilters.setEditableTextField(email2Text);
		EventFilters.setEditableTextField(mNameText);
		
		
		searchBox.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent key) -> {
			if(key.getCharacter().equals(" ") && searchBox.getText().contains(" ")){
				key.consume();
			}
		});
		searchBox.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		teachersTable.setTableMenuButtonVisible(true);
	}
	
	private void showTeachersInfo(){
		connectionsTabController.initializeTables(selectedTeacher);
		
		if(agendaTabController == null){
			System.out.println("Agenda Tab Controller == null");
		}else{
			agendaTabController.setSelectedTeacher(selectedTeacher);
			agendaTabController.setUpAppointments();
		}
		theTeachersName.setText(selectedTeacher.getFullName());
		nameText.setText(selectedTeacher.getBasicInfo().getFname());
		surnameText.setText(selectedTeacher.getBasicInfo().getLname());
		if(utils.Utils.isValidParam(selectedTeacher.getBasicInfo().getMiddleName())){
			mNameText.setText(selectedTeacher.getBasicInfo().getMiddleName());
		}else{
			mNameText.setText("");
		}
		
		if(selectedTeacher.getPicture() != null){
			Image img;
			try {
				imgFromUser_blob = selectedTeacher.getPicture();
				img = new Image(selectedTeacher.getPicture().getBinaryStream(), 140, 140, true, true);
				profileImage.setImage(img);
			} catch (SerialException ex) {
				System.err.println("exception while reading picture");
				ex.printStackTrace(System.err);
			}
			cropButton.setDisable(false);
			deletePic.setDisable(false);
		}else{
			cropButton.setDisable(true);
			deletePic.setDisable(true);
			profileImage.setImage(defaultImage);
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getAddress1().getStreetInfo1())){
			address1StreetNumText.setText(selectedTeacher.getContactInfo().getAddress1().getStreetNum());
			address1StreetNameText.setText(selectedTeacher.getContactInfo().getAddress1().getStreetInfo1());
			address1AreaText.setText(selectedTeacher.getContactInfo().getAddress1().getArea());
			address1CityText.setText(selectedTeacher.getContactInfo().getAddress1().getCity());
			address1ZipText.setText(selectedTeacher.getContactInfo().getAddress1().getPostCode());
		}else{
			address1StreetNumText.setText("");
			address1StreetNameText.setText("");
			address1AreaText.setText("");
			address1CityText.setText("");
			address1ZipText.setText("");
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getAddress2().getStreetInfo1())){
			address2StreetNumText.setText(selectedTeacher.getContactInfo().getAddress2().getStreetNum());
			address2StreetNameText.setText(selectedTeacher.getContactInfo().getAddress2().getStreetInfo1());
			address2AreaText.setText(selectedTeacher.getContactInfo().getAddress2().getArea());
			address2CityText.setText(selectedTeacher.getContactInfo().getAddress2().getCity());
			address2ZipText.setText(selectedTeacher.getContactInfo().getAddress2().getPostCode());
		}else{
			address2StreetNumText.setText("");
			address2StreetNameText.setText("");
			address2AreaText.setText("");
			address2CityText.setText("");
			address2ZipText.setText("");
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getPhone1())){
			phone1Text.setText(selectedTeacher.getContactInfo().getPhone1());
		}else{
			phone1Text.setText("");
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getPhone2())){
			phone2Text.setText(selectedTeacher.getContactInfo().getPhone2());
		}else{
			phone2Text.setText("");
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getEmail1())){
			emailText.setText(selectedTeacher.getContactInfo().getEmail1());
		}else{
			emailText.setText("");
		}
		if(utils.Utils.isValidParam(selectedTeacher.getContactInfo().getEmail2())){
			email2Text.setText(selectedTeacher.getContactInfo().getEmail2());
		}else{
			email2Text.setText("");
		}
	}
	
	@Override
	protected void changeLocale(){
		if(mainTitle == null)
			return;
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		mainTitle.setText(labels.getString("view.teachers.title"));
		subTitle.setText(labels.getString("view.teachers.edit.title"));
		profileLabel.setText(labels.getString("profile_pic"));
		addPictureButton.setText(labels.getString("addpicture"));
		b_info.setText(labels.getString("basic_information"));
		name.setText(labels.getString("name"));
		surname.setText(labels.getString("surname"));
		nameText.setPromptText(labels.getString("insertname"));
		surnameText.setPromptText(labels.getString("insertsurname"));
		a_info.setText(labels.getString("address.information"));
		address1Title.setText(labels.getString("address.1"));
		address1StreetNum.setText(labels.getString("address.street.num"));
		address1StreetName.setText(labels.getString("address.street.name"));
		address1Area.setText(labels.getString("address.area"));
		address1City.setText(labels.getString("address.city"));
		address1Zip.setText(labels.getString("address.zip"));
		address1StreetNumText.setPromptText(labels.getString("address.street.numText"));
		address1StreetNameText.setPromptText(labels.getString("address.street.nameText"));
		address1AreaText.setPromptText(labels.getString("address.areaText"));
		address1CityText.setPromptText(labels.getString("address.cityText"));
		address1ZipText.setPromptText(labels.getString("address.zipText"));
		address2Title.setText(labels.getString("address.2"));
		address2StreetNum.setText(labels.getString("address.street.num"));
		address2StreetName.setText(labels.getString("address.street.name"));
		address2Area.setText(labels.getString("address.area"));
		address2City.setText(labels.getString("address.city"));
		address2Zip.setText(labels.getString("address.zip"));
		address2StreetNumText.setPromptText(labels.getString("address.street.numText"));
		address2StreetNameText.setPromptText(labels.getString("address.street.nameText"));
		address2AreaText.setPromptText(labels.getString("address.areaText"));
		address2CityText.setPromptText(labels.getString("address.cityText"));
		address2ZipText.setPromptText(labels.getString("address.zipText"));
		c_info.setText(labels.getString("contact_information"));
		phone1.setText(labels.getString("phone1"));
		phone2.setText(labels.getString("phone2"));
		email.setText(labels.getString("email"));
		phone1Text.setPromptText(labels.getString("insertphone"));
		phone2Text.setPromptText(labels.getString("insertphone"));
		emailText.setPromptText(labels.getString("insertemail"));
		confirmButton.setText(labels.getString("new.teacher.save"));
		cancelButton.setText(labels.getString("new.teacher.cancel"));
		hint.setText(labels.getString("view.teachers.edit.hint"));
		email2.setText(labels.getString("email2"));
		mName.setText(labels.getString("mName"));
		mNameText.setPromptText(labels.getString("insertMName"));
		email2Text.setPromptText(labels.getString("insertEmail2"));
		deleteTeacher.setText(labels.getString("delete.teacher"));
		calendarTab.setText(labels.getString("view.students.cal"));
		connectionsTabHead.setText(labels.getString("view.students.conns"));
		genTab.setText(labels.getString("view.teachers.general"));
	}
	
	/**
	 * From http://www.drdobbs.com/jvm/simple-searching-in-java/232700121
	 * @author EJB
	 * @author edited d.michaelides
	 */
	public void handleSearchByKey(String oldVal, String newVal, int fieldName) {
		if ((oldVal != null && (newVal.length() < oldVal.length()))) {
			teachersTable.setItems( observableTeachers );
		}
		
		String searchKey = newVal.toUpperCase();
		
		ObservableList<Teacher> subentries = FXCollections.observableArrayList();
		
		for ( Teacher teacher : teachersTable.getItems() ) {
			String searchToken = "";
			switch(fieldName){
				case Constants.FIELD_FNAME:
					searchToken = teacher.getBasicInfo().getFname();
					break;
				case Constants.FIELD_LNAME:
					searchToken = teacher.getBasicInfo().getLname();
					break;
				case Constants.FIELD_ADDRESS_1:
					searchToken = teacher.getContactInfo().getAddress1().getStreetInfo1();
					break;
				case Constants.FIELD_STREET_NO_1:
					break;
				case Constants.FIELD_STREET_NAME_1:
					break;
				case Constants.FIELD_STREET_AREA_1:
					break;
				case Constants.FIELD_STREET_CITY_1:
					break;
				case Constants.FIELD_STREET_ZIP_1:
					break;
				case Constants.FIELD_ADDRESS_2:
					searchToken = teacher.getContactInfo().getAddress2().getStreetInfo1();
					break;
				case Constants.FIELD_STREET_NO_2:
					break;
				case Constants.FIELD_STREET_NAME_2:
					break;
				case Constants.FIELD_STREET_AREA_2:
					break;
				case Constants.FIELD_STREET_CITY_2:
					break;
				case Constants.FIELD_PHONE_1:
					searchToken = teacher.getContactInfo().getPhone1();
					break;
				case Constants.FIELD_PHONE_2:
					searchToken = teacher.getContactInfo().getPhone2();
					break;
				case Constants.FIELD_EMAIL:
					searchToken = teacher.getContactInfo().getEmail1();
					break;
			}
			if(utils.Utils.isValidParam(searchToken)){
				if ( searchToken.toUpperCase().contains(searchKey) ) {
					subentries.add(teacher);
				}
			}
		}
		teachersTable.setItems(subentries);
	}
	
	public void searchForAll(String oldVal, String newVal) {
		if ((oldVal != null && (newVal.length() < oldVal.length())) ) {
			teachersTable.setItems( observableTeachers );
		}
		if(newVal.isEmpty()){ return; }
		
		String [] tokens = searchBox.getText().trim().split(" ");
		String searchToken1 = tokens[0];
		String searchToken2 = (tokens.length == 2) ? tokens[1] : null;
		
		
		ObservableList<Teacher> subentries = FXCollections.observableArrayList();
		
		teachersTable.getItems().stream().forEach((Teacher teacher) -> {
			if(searchToken2 == null){
				if(teacher.containsKeyWordNotCaseSensitive(searchToken1)){
					subentries.add(teacher);
				}
			}else{
				if(teacher.containsKeyWordNotCaseSensitive(searchToken1) && teacher.containsKeyWordNotCaseSensitive(searchToken2)){
					subentries.add(teacher);
				}
			}
		});
		teachersTable.setItems(subentries);
	}
	
	@FXML
	private void addImage(Event e){
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(LanguageUtils.getInstance().getCurrentLanguage().getString("selectphoto"));
		File file = fileChooser.showOpenDialog(stage);
		if (file != null) {
			Path path = Paths.get(file.getAbsolutePath());
			try {
				byte[] data = Files.readAllBytes(path);
				imgFromUser_blob = new SerialBlob(data);
				InputStream is = new ByteArrayInputStream(data);
				imgFromUser = new Image(is);
				profileImage.setImage(imgFromUser);
				cropButton.setDisable(false);
				deletePic.setDisable(false);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		}
	}
	
	@FXML
	private void cropImage(Event event) throws IOException {
		Stage cropImageStage = new Stage();
		Node source = (Node)event.getSource();
		stage = (Stage)(source.getScene().getWindow());
		final FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("CropImage.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
		final Scene cropImageScene = new Scene((AnchorPane)myLoader.load());
		CropImageController c = (CropImageController)myLoader.getController();
		c.init(profileImage.getImage());
		
		stage.setOnCloseRequest((WindowEvent ev) -> {
			cropImageScene.getWindow().fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
		});
		
		cropImageStage.setScene(cropImageScene);
//        stage.toBack();
		cropImageStage.toFront();
		stage.setOpacity(0.8);
		root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
		root.mouseTransparentProperty().bind(cropImageScene.getWindow().showingProperty());
		cropImageScene.getWindow().setOnCloseRequest((WindowEvent ev) -> {
			stage.setOpacity(1);
			CropImageController c1 = (CropImageController)myLoader.getController();
			Image croppedImg = c1.getCroppedImage();
			if(croppedImg != null){
				try{
					profileImage.setImage(croppedImg);
					imgFromUser_blob = new SerialBlob(utils.Utils.toByteArray(croppedImg));
				}
				catch(Exception e){
					System.out.println("ERROR ERROR - converting image");
					e.printStackTrace(System.err);
				}
			}
		});
		cropImageStage.show();
	}
	
	@FXML
	private void setDefaultImage(Event e){
		imgFromUser_blob = null;
		imgFromUser = null;
		deletePic.setDisable(false);
		cropButton.setDisable(true);
		addPictureButton.setDisable(false);
		profileImage.setImage(defaultImage);
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
//            controllerDidLoadFxml();
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	private void refresh() throws IOException {
		teachersTable.getItems().clear();
		TimeTableService timeTableService = new TimeTableService();
		refreshProgress.visibleProperty().bind(timeTableService.runningProperty());
		timeTableService.start();
		refreshProgress.requestFocus();
	}
	
	private class TimeTableService extends Service {
		@Override
		protected Task createTask() {
			TimeTableTask theTask = new TimeTableTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				if(observableTeachers != null){
					teachersTable.setItems(observableTeachers);
					TeachersTable.initializeTeachersTable(teachersTable);
				}else{
					System.out.println("WHY is it null?!?!?!? -TEACHERS-");
				}
			});
			return theTask;
		}
		private class TimeTableTask extends Task {
			@Override
			protected Object call() throws Exception {
				observableTeachers = DBTableUtils.getAllTeachers();
				return null;
			}
		}
	}
	
	/* ----------------- DELETE Teacher ------------------------------------------------ */
	@FXML
	private void deleteTeacher(Event event) throws Exception {
		try{
			if(selectedTeacher != null){
				showConfirmDialog(false);
			}
		}catch(IOException e){
			e.printStackTrace(System.err);
		}
	}
	
	
	@FXML
	public void saveChanges(Event e) throws IOException{
		if(stage == null){
			Node source = (Node)e.getSource();
			stage = (Stage)(source.getScene().getWindow());
		}
		System.out.println("clicked Save Teacher");
		
		try {
			showConfirmDialog(true);
		} catch (IOException ex) {
			System.err.println("Error while showing confirmation dialog -Teacher-");
			ex.printStackTrace(System.err);
		}
	}
	
	private void verifyInput() throws IOException{
		boolean thereIsError = false;
		if(!Utils.isValidParam(nameText.getText().trim())){
			Utils.turnNodeRed(nameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.name"));
			thereIsError = true;
		}
		if(!Utils.isValidParam(surnameText.getText().trim())){
			Utils.turnNodeRed(surnameText, LanguageUtils.getInstance().getCurrentLanguage().getString("tooltip.surname"));
			thereIsError = true;
		}
		
		if(thereIsError){
			System.out.println("There Is error");
		}else{
			if(isTeacherEdited()){
				System.out.println("Calling Service");
				saveChangesMade(true);
			}else{
				CustomDialogs.infoDialog("", "Teacher was not edited, nothing to save....");
			}
		}
	}
	
	private void saveChangesMade(boolean toSave){
		SaveTeacherService saveClass = new SaveTeacherService(toSave);
		progress.visibleProperty().bind(saveClass.runningProperty());
		confirmButton.disableProperty().bind(saveClass.runningProperty());
		cancelButton.disableProperty().bind(saveClass.runningProperty());
		saveClass.start();
		progress.requestFocus();
	}
	
	private void showConfirmDialog(boolean toSave) throws IOException {
		labels = LanguageUtils.getInstance().getCurrentLanguage();
		Optional<ButtonType> response;
		String yes = labels.getString("dialog.yes");
		if(toSave){
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.teacher.title"), labels.getString("dialog.save.teacher"), yes, labels.getString("dialog.no"));
		}else{
			response = CustomDialogs.yesNoDialog(labels.getString("dialog.delete.teacher.title"), labels.getString("dialog.delete.teacher"), yes, labels.getString("dialog.no"));
		}
		
		if(response.isPresent()){
			if(response.get().getText().equals(yes)){
				if(toSave){
					verifyInput();
				}else{
					saveChangesMade(false);
				}
			}
		}
	}
	
	/**
	 * checks if any fields of selected student were edited
	 * if there were edits @return true
	 */
	private boolean isTeacherEdited(){
		boolean somethingWasEdited = false;
		if (notEqual(nameText, selectedTeacher.getBasicInfo().getFname())){
			selectedTeacher.getBasicInfo().setFname(nameText.getText().trim());
			somethingWasEdited = true;
		}
		if(imgFromUser_blob != selectedTeacher.getPicture()){
			selectedTeacher.setPicture(imgFromUser_blob);
			somethingWasEdited = true;
		}
		
		if (notEqual(surnameText, selectedTeacher.getBasicInfo().getLname())){
			selectedTeacher.getBasicInfo().setLname(surnameText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(mNameText, selectedTeacher.getBasicInfo().getMiddleName())){
			selectedTeacher.getBasicInfo().setMiddleName(mNameText.getText().trim());
			somethingWasEdited = true;
		}
		if (isValidParam(address1StreetNumText.getText()) ||
				isValidParam(address1StreetNameText.getText()) ||
				isValidParam(address1AreaText.getText()) ||
				isValidParam(address1CityText.getText()) ||
				isValidParam(address1ZipText.getText())){
			if(selectedTeacher.getContactInfo() == null){
				selectedTeacher.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
			}
			if(selectedTeacher.getContactInfo().getAddress1() == null){
				selectedTeacher.getContactInfo().setAddress1(new Address());
				somethingWasEdited = true;
			}
		}
		
		if (isValidParam(address2StreetNumText.getText()) ||
				isValidParam(address2StreetNameText.getText()) ||
				isValidParam(address2AreaText.getText()) ||
				isValidParam(address2CityText.getText()) ||
				isValidParam(address2ZipText.getText())){
			if(selectedTeacher.getContactInfo() == null){
				selectedTeacher.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
			}
			if(selectedTeacher.getContactInfo().getAddress2() == null){
				selectedTeacher.getContactInfo().setAddress2(new Address());
				somethingWasEdited = true;
			}
		}
		
		// address 1 fields
		if (notEqual(address1StreetNumText, selectedTeacher.getContactInfo().getAddress1().getStreetNum())){
			selectedTeacher.getContactInfo().getAddress1().setStreetNum(address1StreetNumText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address1StreetNameText, selectedTeacher.getContactInfo().getAddress1().getStreetInfo1())){
			selectedTeacher.getContactInfo().getAddress1().setStreetInfo1(address1StreetNameText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address1AreaText, selectedTeacher.getContactInfo().getAddress1().getArea())){
			selectedTeacher.getContactInfo().getAddress1().setArea(address1AreaText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address1CityText, selectedTeacher.getContactInfo().getAddress1().getCity())){
			selectedTeacher.getContactInfo().getAddress1().setCity(address1CityText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address1ZipText, selectedTeacher.getContactInfo().getAddress1().getPostCode())){
			selectedTeacher.getContactInfo().getAddress1().setPostCode(address1ZipText.getText().trim());
			somethingWasEdited = true;
		}
		
		// address 2 fields
		if (notEqual(address2StreetNumText, selectedTeacher.getContactInfo().getAddress2().getStreetNum())){
			selectedTeacher.getContactInfo().getAddress2().setStreetNum(address2StreetNumText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address2StreetNameText, selectedTeacher.getContactInfo().getAddress2().getStreetInfo1())){
			selectedTeacher.getContactInfo().getAddress2().setStreetInfo1(address2StreetNameText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address2AreaText, selectedTeacher.getContactInfo().getAddress2().getArea())){
			selectedTeacher.getContactInfo().getAddress2().setArea(address2AreaText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address2CityText, selectedTeacher.getContactInfo().getAddress2().getCity())){
			selectedTeacher.getContactInfo().getAddress2().setCity(address2CityText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(address2ZipText, selectedTeacher.getContactInfo().getAddress2().getPostCode())){
			selectedTeacher.getContactInfo().getAddress2().setPostCode(address2ZipText.getText().trim());
			somethingWasEdited = true;
		}
		
		// contact Info fields
		if(isValidParam(phone1Text.getText()) ||
				isValidParam(phone2Text.getText()) ||
				isValidParam(emailText.getText()) ||
				isValidParam(email2Text.getText())){
			if(selectedTeacher.getContactInfo() == null){
				selectedTeacher.setContactInfo(new ContactInformation());
				somethingWasEdited = true;
			}
		}
		if (notEqual(phone1Text, selectedTeacher.getContactInfo().getPhone1())){
			selectedTeacher.getContactInfo().setPhone1(phone1Text.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(phone2Text, selectedTeacher.getContactInfo().getPhone2())){
			selectedTeacher.getContactInfo().setPhone2(phone2Text.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(emailText, selectedTeacher.getContactInfo().getEmail1())){
			selectedTeacher.getContactInfo().setEmail1(emailText.getText().trim());
			somethingWasEdited = true;
		}
		if (notEqual(email2Text, selectedTeacher.getContactInfo().getEmail2())){
			selectedTeacher.getContactInfo().setEmail2(email2Text.getText().trim());
			somethingWasEdited = true;
		}
		return somethingWasEdited;
	}
	
	
	private class SaveTeacherService extends Service<Boolean> {
		private final boolean toSave;
		public SaveTeacherService(boolean isItToSave){
			super();
			toSave=isItToSave;
		}
		@Override
		protected Task<Boolean> createTask() {
			SaveTeacherTask theTask = new SaveTeacherTask();
			theTask.setOnSucceeded(workerStateEvent -> {
				System.out.println("     RESULT = "+getValue());
				try{
					if(getValue()){
						if(toSave){
							String message = labels.getString("save.obj.teacher")+" "
									+ labels.getString("save.obj.success.line1");
							CustomDialogs.infoDialog("", message);
						}else{
							String message = labels.getString("save.obj.teacher")+" "
									+ labels.getString("delete.obj.success.line1");
							CustomDialogs.infoDialog("", message);
							clearAll();
						}
					}else{
						if(toSave){
							String message = labels.getString("save.obj.teacher")+" "
									+ labels.getString("delete.obj.fail.line1");
							CustomDialogs.errorDialog("", message);
						}else{
							String message = labels.getString("save.obj.teacher")+" "
									+ labels.getString("delete.obj.fail.line1");
							CustomDialogs.errorDialog("", message);
						}
					}
				}catch(Exception e){
					e.printStackTrace(System.err);
				}
			});
			return theTask;
		}
		private class SaveTeacherTask extends Task {
			@Override
			protected Boolean call() throws Exception {
				try{
					Thread.sleep(200);
					if(toSave){
						DBUpdateUtils.updateTeacherInfo(selectedTeacher);
					}else{
						DBDeleteUtils.deleteTeacherObject(selectedTeacher);
					}
				}catch(InterruptedException e){
					System.out.println("Exception dam it");
					e.printStackTrace(System.err);
					return false;
				}
				System.out.println("updated!");
				return true;
			}
		}
	}
	
	private void clearAll(){
		theTeachersName.setText("");
		cropButton.setDisable(true);
		deletePic.setDisable(true);
		selectedTeacher = null;
		imgFromUser_blob = null;
		profileImage.setImage(defaultImage);
		nameText.setText("");
		surnameText.setText("");
		mNameText.setText("");
		address1AreaText.setText("");
		address1CityText.setText("");
		address1StreetNameText.setText("");
		address1StreetNumText.setText("");
		address1ZipText.setText("");
		address2AreaText.setText("");
		address2CityText.setText("");
		address2StreetNameText.setText("");
		address2StreetNumText.setText("");
		address2ZipText.setText("");
		phone1Text.setText("");
		phone2Text.setText("");
		email2Text.setText("");
		emailText.setText("");
	}
	@Override
	public void onLoad() {
		try {
			refresh();
			editTab.setDisable(true);
		} catch (IOException ex) {
			System.err.println("EXCEPTION - view classes -0002");
			ex.printStackTrace(System.err);
		}
	}
	@Override
	public void unload() {
		clearAll();
		teachersTable.getItems().clear();
		if(observableTeachers != null){
			observableTeachers.clear();
		}
		SingleSelectionModel<Tab> selectionModel = mainTabPane.getSelectionModel();
		selectionModel.select(0);
		SingleSelectionModel<Tab> selectionModel1 = editTabPane.getSelectionModel();
		selectionModel1.select(0);
		searchBox.setText("");
		connectionsTabController.unload();
	}
}

