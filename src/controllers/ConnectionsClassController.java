/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package controllers;

import DB.DBUpdateUtils;
import DB.objects.Aclass;
import DB.objects.Student;
import static controllers.StageController.cachingClassLoader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import services.GetAllStudents;
import tables.StudentsTable;
import utils.dialogs.CustomDialogs;

/**
 * FXML Controller class
 *
 * @author dimitris
 */
public class ConnectionsClassController extends StageController{
	
	private final URL fxmlURL;
	private final ResourceBundle resources;
	public ConnectionsClassController(URL fxml, ResourceBundle res) {
		super();
		fxmlURL = fxml;
		resources = res;
	}
	
	@FXML private Button removeStudentButton;
	@FXML private TableView<Student> allStudentsTable;
	@FXML private ObservableList<Student> allStudents;
	@FXML private TableView<Student> studentsTable;
	@FXML private VBox buttonsVBox;
	@FXML private ProgressIndicator getClassesProgress;
	@FXML private ProgressIndicator savingProgress;
	@FXML private ToggleButton showTableButton;
	private Aclass selectedClass;
	private boolean somethingWasEdited;
	@FXML private ScrollPane mainScrollPane;
	@FXML private VBox vBox;
	@FXML private HBox hiddenHbox;
	@FXML private GridPane searchGrid;
	@FXML private TextField filterByName;
	@FXML private TextField filterByLName;
	
	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// bind width
		vBox.minWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
		vBox.maxWidthProperty().bind(mainScrollPane.widthProperty().subtract(10));
		
		StudentsTable.initializeStudentsTable(allStudentsTable);
		StudentsTable.initializeStudentsTable(studentsTable);
		
		initializeSearchFields();
	}
	
	private void initializeSearchFields(){
		
		filterByName.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		filterByLName.textProperty().addListener(
				new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue observable,
							String oldVal, String newVal) {
						searchForAll(oldVal, newVal);
					}
				});
		
	}
	
	public void initializeTables(Aclass selectedCl){
		
		this.selectedClass = selectedCl;
		// all the Students that attend class
		if(selectedCl.getStudents() != null){
			studentsTable.setItems(FXCollections.observableArrayList(selectedCl.getStudents()));
			if(studentsTable.getItems().isEmpty()){
				removeStudentButton.setDisable(true);
			}else{
				removeStudentButton.setDisable(false);
			}
			// listener to disable delete button when list is empty
			studentsTable.getItems().addListener((ListChangeListener.Change<? extends Student> c) -> {
				if(studentsTable.getItems().isEmpty()){
					removeStudentButton.setDisable(true);
				}else{
					if(removeStudentButton.isDisable()){
						removeStudentButton.setDisable(false);
					}
				}
			});
		}
	}
	
	@FXML
	private void addStudent(Event event) throws IOException {
		if(allStudentsTable.getSelectionModel().isEmpty()){
			CustomDialogs.infoDialog("", "Nothing is selected");
			return;
		}
		Student toBeMoved = allStudentsTable.getSelectionModel().getSelectedItem();
		if(studentsTable.getItems().contains(toBeMoved)){
			CustomDialogs.infoDialog("", "Student is already attending this class");
			return;
		}
		studentsTable.getItems().add(toBeMoved);
		if(selectedClass != null){
			DBUpdateUtils dbConnection = new DBUpdateUtils();
			dbConnection.addStudentToClass(selectedClass, toBeMoved);
		}
	}
	
	@FXML
	private void removeStudent(Event event) throws IOException {
		System.out.println("Remove Student -1-");
		if(studentsTable.getSelectionModel().isEmpty()){
			CustomDialogs.infoDialog("", "Nothing is selected");
			return;
		}
		System.out.println("Remove Student -2-");
		Student toBeMoved = studentsTable.getSelectionModel().getSelectedItem();
		studentsTable.getItems().remove(toBeMoved);
		System.out.println("Remove Student -3-");
		if(selectedClass != null){
			System.out.println("Remove Student -4-");
			DBUpdateUtils dbConnection = new DBUpdateUtils();
			dbConnection.removeStudentFromClass(selectedClass, toBeMoved);
			System.out.println("Remove Student -5-");
		}
		studentsTable.requestFocus();
	}
	
	@FXML
	private void showHiddenTable(Event event) throws IOException {
		playFadeTransitions();
		playSizeAnimation(!showTableButton.isSelected());
		GetAllStudents service  = new GetAllStudents();
		getClassesProgress.visibleProperty().bind(service.runningProperty());
		service.setOnSucceeded(workerStateEvent -> {
			buttonsVBox.setVisible(true);
			allStudentsTable.setVisible(true);
			allStudents = (ObservableList<Student>)service.getValue();
			System.out.println("Size = "+ allStudents.size());
			allStudentsTable.setItems(allStudents);
		});
		service.start();
	}
	
//    @FXML
//    private void saveChanges(Event event) throws IOException {
////        showCustomDialog();
//        ResourceBundle labels = LanguageUtils.getInstance().getCurrentLanguage();
//
//        String response = CustomDialogs.yesNoDialog(labels.getString("dialog.save.teacher.title"), labels.getString("dialog.edit.object"), labels.getString("dialog.yes"), labels.getString("dialog.no"));
//
//        if(null != response)switch (response) {
//            case CustomAction.YES:
//                saveClass(labels);
//            break;
//        }
//    }
	
//    private void saveClass(ResourceBundle labels){
//        UpdateClassesStudentsService service  = new UpdateClassesStudentsService(selectedClass);
//        savingProgress.visibleProperty().bind(service.runningProperty());
//        service.setOnSucceeded(workerStateEvent -> {
//            if((boolean)service.getValue()){
//                CustomDialogs.infoDialog("", labels.getString("dialog.edit.success"));
//            }else{
//                CustomDialogs.errorDialog(labels.getString("error"), labels.getString("save.obj.changes"));
//            }
//        });
//        service.start();
//    }
	
	private void playFadeTransitions(){
		FadeTransition ftTable = new FadeTransition(Duration.millis(500), allStudentsTable);
		FadeTransition ftVBox = new FadeTransition(Duration.millis(500), buttonsVBox);
		if(showTableButton.isSelected()){
			ftTable.setFromValue(0.0);
			ftTable.setToValue(1.0);
			ftVBox.setFromValue(0.0);
			ftVBox.setToValue(1.0);
		}else{
			ftTable.setFromValue(1.0);
			ftTable.setToValue(0.0);
			ftVBox.setFromValue(1.0);
			ftVBox.setToValue(0.0);
		}
		ftTable.play();
		ftVBox.play();
		
	}
	private void playSizeAnimation(boolean hide){
		hiddenHbox.setVisible(!hide);
		
		Timeline timeline = new Timeline();
		timeline.setCycleCount(1);
		timeline.setAutoReverse(false);
		KeyValue kv;
		if(hide){
			kv = new KeyValue(hiddenHbox.prefHeightProperty(), -196, Interpolator.EASE_BOTH);
		}
		else{
			kv = new KeyValue(hiddenHbox.prefHeightProperty(), 196, Interpolator.EASE_BOTH);
		}
		final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);
		timeline.getKeyFrames().add(kf);
		timeline.play();
	}
	
	
	private void searchForAll(String oldVal, String newVal) {
		// if text is shorter than before
		// set items to ALL so we can re-filter them
//        if ((oldVal != null && (newVal.length() < oldVal.length()))) {
		allStudentsTable.setItems(allStudents);
//        }
		
		// keywords to search for
		String searchKeyName = filterByName.getText().toUpperCase();
		String searchKeyLName = filterByLName.getText().toUpperCase();
		
		ObservableList<Student> searchResults = FXCollections.observableArrayList();
		
		for ( Student theStudent : allStudentsTable.getItems() ) {
			// keywords to match
			String searchTokenName = theStudent.getBasicInfo().getFname().toUpperCase();
			String searchTokenLName = theStudent.getBasicInfo().getLname().toUpperCase();
			
			// if it matches add it to search results
			if ( searchTokenName.contains(searchKeyName) &&  searchTokenLName.contains(searchKeyLName)) {
				searchResults.add(theStudent);
			}
		}
		allStudentsTable.setItems(searchResults);
	}
	
	@Override
	protected void makeRoot() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setClassLoader(cachingClassLoader);
		this.setStage(stage);
		loader.setLocation(fxmlURL);
		loader.setResources(resources);
		this.setLanguageUsed(resources);
		loader.setController(this);
		try {
			setRoot((Parent)loader.load());
		} catch (RuntimeException | IOException x) {
			System.out.println("loader.getController()=" + loader.getController());
			System.out.println("loader.getLocation()=" + loader.getLocation());
			System.out.println("loader.getResources()=" + loader.getResources());
			throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
		}
	}
	
	@Override
	protected void changeLocale() {
		System.out.println("-----------> NOT SUPPORTED FOR CONNECTIONS CLASS <----------------------");
	}
	@Override
	public void onLoad() {
	}
	@Override
	public void unload() {
		filterByName.setText("");
		filterByLName.setText("");
		allStudentsTable.getItems().clear();
		allStudents = null;
		showTableButton.setSelected(false);
		hiddenHbox.setPrefHeight(0);
		hiddenHbox.setVisible(false);
		allStudentsTable.setVisible(false);
		buttonsVBox.setVisible(false);
	}
}