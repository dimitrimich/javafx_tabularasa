/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controllers.LoginController;
import controllers.StageController;
import images.icons.Icons;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import screens.FXMLs;
import utils.LanguageUtils;

/**
 *
 * @author d.michaelides
 */
public class TabulaRasaFX extends Application {
    
    
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Tabula Rasa");
        // 2. Create FXML loader
        FXMLLoader myLoader = new FXMLLoader(FXMLs.class.getResource("Login.fxml"), LanguageUtils.getInstance().getCurrentLanguage());
        myLoader.setClassLoader(StageController.cachingClassLoader); 
        // 3. Load the screen
        AnchorPane login = (AnchorPane)myLoader.load();
        // 4. get Controller to set the stage in it

        LoginController controler = ((LoginController) myLoader.getController());
        controler.initializeAll(stage);
        Scene myScene = new Scene(login);
		stage.getIcons().add(new Image(Icons.class.getResource("temp_icon_16px.png").toString()));
		stage.getIcons().add(new Image(Icons.class.getResource("temp_icon_64px.png").toString()));

        stage.setScene(myScene);
        stage.show();
        stage.toFront();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}